var dayClickEventClass;
var commandType; // 1 = Add; 2 = Edit;
var removedEventsSfIds = [];
var metaDataVarClassVar;
var uiTaskId = 0;
var clickedTaskId;
var clickedName;
var holidayList = [];
var copyTaskErrorObj = [];
var monthJSONClass;
var monthDetails;
var userDetailsClass;
var userGFSClass;
var accountChangedClass;
var cancelReasonMessage = null;
//Account Names
var UNPLANNED_ACCOUNT = 'UNPLANNED ACCOUNT';
var MULTIPLE_ACCOUNT = 'MULTIPLE ACCOUNTS';
var CUSTGROUP_OTHERS = 'others';
var EDIT_APPROVED_TASKS_ON = 'Thu';
var d = new Date();
var END_OF_MONTH = new Date(d.getFullYear(), d.getMonth() + 1, 0);
var NEXT_MONDAY = new Date(d.setDate(d.getDate() + (1 + 7 - d.getDay()) % 7));
var CONFIG_EDIT = false;


$(document).ready(function() {

    documentReadyFunction();

});



function cancelEvnentWithReason() {
    cancelReasonMessage = $('#cancelReasonMessage').val();
    removeCalEvents();
    sendEventDataToController();
    commandType = 2;
    //$("#doneId").trigger('click');
    $('#cancelReasonModal').modal('toggle');
    $('#cancelReasonMessage').val('');
}

function hardDeleteTask() {
    Visualforce.remoting.Manager.invokeAction(
        'CalendarController.deleteTaskData',
        clickedTaskId,
        function(result, event) {
            if (event.status) {
                fetchUserRecords();
                $("#blurDivId").hide();
                alert('task deleted successfully...');
            }
        }, {
            escape: false
        }
    );
}


function documentReadyFunction() {

    monthDetails = JSON.parse(localStorage.getItem("monthDetails"));
    console.log("month----details---", localStorage.getItem("monthDetails"));

    calendarEvents();
    //Cancel Reason validation
    $('#cancelReasonMessage').on('keyup', function() {
        if (this.value.length >= 1) {
            $('#reasonSubmitButton').removeAttr("disabled");
        } else {
            $('#reasonSubmitButton').attr("disabled", "disabled");
        }
    });
    $("#deleteEventId").hide();
    $("#deleteEventId").click(function() {

        if (confirm("Are You Sure to delete this task ???")) {
            $("#blurDivId").show();
            hardDeleteTask();
        } else {
            $("#blurDivId").hide();
            return;
        }

        //$("#blurDivId").hide();
    });


    //------------------------------------------Create evet data on click of day--------------------------------------------------
    $("#doneId").click(function() {

        var errorPresent = true;
        console.log("--28--", commandType);

        if (1 == commandType) {
            errorPresent = createEventData(null)
        } else {
            if (!confirm("Do you want to save these changes ??")) {
                return;
            }
            errorPresent = editEventData(true, null);
        }

        if (!errorPresent) {
            sendEventDataToController();
        }
        colorCompute();
    });

    // ----------------------------------------Remove any event from page---------------------------
    $("#removeEventId").click(function() {
        $("#cancelReasonMessage").val("");
        //if(confirm("Are You Sure to remove this event ???")) {
        $('#cancelReasonModal').modal('toggle');
        //removeCalEvents();
        //sendEventDataToController();


        //} else {
        return;
        //}
    });


    // To send data to controller to save data and delete data on click of submit.
    $("#submitId").click(function() {
        sendEventDataToController();
    });

    // To send data to controller to save data and delete data on click of submit.
    $("#sendApprovalId").click(function() {
        sendForApprovalController();
    });

    $("#goToId").click(function() {
        if (dayClickEventClass && dayClickEventClass.taskmap[clickedTaskId].sfId) {
            window.open("/" + dayClickEventClass.taskmap[clickedTaskId].sfId);
        }
    });

    $("#goToAccounts").click(function() {
        if (dayClickEventClass &&
            dayClickEventClass.taskmap &&
            dayClickEventClass.taskmap[clickedTaskId] &&
            dayClickEventClass.taskmap[clickedTaskId].numOfAccounts
        ) {
            openConfigAccounts();
        }
    });

    $("#goToEform").click(function() {
        if (  dayClickEventClass
           && dayClickEventClass.taskmap
           && dayClickEventClass.taskmap[clickedTaskId]
           && dayClickEventClass.taskmap[clickedTaskId].eformUrl
           ) {
            window.open("/" + dayClickEventClass.taskmap[clickedTaskId].eformUrl, '_blank');
        }
    });

    $("#goToConfigEform").click(function() {
        if (  dayClickEventClass
           && dayClickEventClass.configTaskMap
           && dayClickEventClass.configTaskMap[clickedName]
           && dayClickEventClass.configTaskMap[clickedName][clickedTaskId]
           && dayClickEventClass.configTaskMap[clickedName][clickedTaskId].eformUrl
           ) {
            window.open("/" + dayClickEventClass.configTaskMap[clickedName][clickedTaskId].eformUrl, '_blank');
        }
    });

    $("#changeUserSubmitId").click(function() {
        $("#submitId").click();
    });

    $("#changeUserId").click(function() {
        fetchUserData();
    });

    $("#homeRedId").click(function() {
        window.open("/home/home.jsp", "_self");
    });

    $("#monthRedId").click(function() {
        if (monthJSONClass &&
            monthJSONClass.length > 0 &&
            monthJSONClass[0].Id
        ) {
            window.open("/" + monthJSONClass[0].Id, "_blank");
        }
    });


    $("#proceedRedirectId").click(function() {
        var monthRec = getSelectedMonth();
        localStorage.setItem(
            "monthDetails",
            JSON.stringify({
                ownerId: $("#userpickListId").val(),
                month: monthRec.month,
                year: monthRec.year
            })
        );
        window.open('/apex/RecurringTasks', '_blank');
    });

    $("#Work").change(function() {
        $('#leaveSelectId').val('Full Day');
        $("#leaveReasonSelectId").val($("#leaveReasonSelectId option:first").val());
    });

    $("#leaveTypeId").change(function() {
        $('#halfLeaveSelectId').val('Morning');
    });

    $("#Work, #leaveSelectId").change(function() {

        $("#selTaskTypeSpanId").html("");
        $("#selTaskSpanId").html("");
        typeOfActivityCompute();
        $('#buPlistId').find('option').remove();
        $('#buPlistId').append('<option>--none--</option>');
        //editEventBUCompute(dayClickEventClass.userId);

        setTitle();

    });

    $("#buTypePlistId").change(function() {

        $('#buPlistId').val("--none--");
        $("#selTaskSpanId").html("");

        var buPickListVal = $('#buTypePlistId').val();

        if ("--none--" == buPickListVal) {
            $("#selTaskTypeSpanId").html("")
        } else {
            $("#selTaskTypeSpanId").html(buPickListVal);
        }

        editEventBUCompute();

        setTitle();
        showTargetRevenue();

    });

    $("#buPlistId").change(function() {

        var buPickListVal = $('#buPlistId').val();

        if ("--none--" == buPickListVal) {
            $("#selTaskSpanId").html("")
        } else {
            $("#selTaskSpanId").html(buPickListVal);
        }

        setTitle();
        showTargetRevenue();

    });

    $(".fc-next-button, .fc-prev-button, .fc-today-button").click(function() {

        fetchUserData();

        //showHideCreateActivity();

    });


    $("#copyTasksId").click(function() {

        $("#copyTaskModal").modal({
            "backdrop": "static"
        });

        var copyDefDate = new Date(getSelectedMonth().year +
            '-' +
            getSelectedMonth().month +
            '-01'
        );
        copyDefDate.setMonth(copyDefDate.getMonth() - 1);

        $('#selectCopyMonthId').val(copyDefDate.toString().split(' ')[1]);
        $('#selectCopyYearId').val(copyDefDate.toString().split(' ')[3]);

        openCopyTaskModal();
    });

    $("#copyEventId").click(function() {
        $("#copyDateId").val("");
        clearModalErrorMessages();
        $('#addEventModalId').modal('toggle');

        $("#copyEventModal").modal({
            "backdrop": "static"
        });
    });

    $("#doneEventCopyId").click(function() {

        if (!$("#copyDateId").val()) {
            alert("Add Destination Date !!");
            return;
        }

        var errorPresent = true;
        errorPresent = editEventData(false, $("#copyDateId").val());

        if (!errorPresent) {
            $('#finalCopyEventDoneId').click();
            sendEventDataToController();
        }
        colorCompute();
    });

    $("#createTaskId").click(function() {
        sendToCopyPreviousMonthTasks();
    });

    $("#userpickListId").change(function() {
        fetchUserData();
    });

    $("#createActivityId").click(function() {
        var monthRec = getSelectedMonth();
        localStorage.setItem(
            "monthDetails",
            JSON.stringify({
                ownerId: $("#userpickListId").val(),
                month: monthRec.month,
                year: monthRec.year
            })
        );
        window.open('/apex/RecurringTasks', '_blank');
        //$("#slctRedirect").modal();
    });

    $("#doneCopyId").click(function() {
        var copySuccess = copyTaskTime();

        if (copySuccess) {
            sendEventDataToController();
            colorCompute();
        }
    });

    $("#selectCopyMonthId, #selectCopyYearId").change(function() {
        openCopyTaskModal();
    });


    $("#editAsmMessage").click(function() {

        openASMEditModal();

    });


    $("#asmMessageButton").click(function() {

        asmMessageButtonClick();
    });


    // Auto complete searches.
    fetchCityRecords();
    fetchAccountRecords();
    fetchCustomerGroupRecords();
    fetchProvinceRecords();

    $("#provinceId").keyup(function() {

        if(!$("#customerGroupId").val().trim()) {
            //$("#numOfAccId").val("");
            $("#accountId").val("");
            $("#accountId").removeAttr("data");
            $("#accountId").removeAttr("data2");
            $("#accountId").removeAttr("name");
            $("#accountId").prop('disabled', true);
        }

        $("#cityId").val("");
        $("#cityId").removeAttr("data");
        $("#cityId").removeAttr("name");

        if ($("#provinceId").val().trim()) {

            $("#cityId").removeAttr('disabled');

        } else {
            $("#provinceId").removeAttr("data");
            $("#provinceId").removeAttr("name");

            $("#cityId").prop('disabled', true);
        }

        computeNumOfAccKeyUp();

        //validationOnBlur(this);

    });

    $("#cityId").keyup(function() {

        if(!$("#customerGroupId").val().trim() && !$("#numOfAccId").val().trim()) {
            //$("#numOfAccId").val("");
            $("#accountId").val("");
            $("#accountId").removeAttr("data");
            $("#accountId").removeAttr("data2");
            $("#accountId").removeAttr("name");
        }

        if ($("#cityId").val().trim() && !$("#numOfAccId").val().trim()) {
            $("#accountId").removeAttr('disabled');
        } else {
            $("#cityId").removeAttr("data");
            $("#cityId").removeAttr("name");
            $("#accountId").prop('disabled', true);
        }

        //this.data2 = 'cityIdPressed';

        validationOnBlur(this, true);

    });

    $("#customerGroupId").keyup(function() {

        if ($("#customerGroupId").val().trim()) {
            $("#accountId").removeAttr("disabled");
        } else {
            $("#accountId").prop('disabled', true);
            $("#customerGroupId").removeAttr("data");
            $("#customerGroupId").removeAttr("data2");
            $("#customerGroupId").removeAttr("name");
        }

        //validationOnBlur(this);

    });

    $('#numOfAccId').on('keyup change', function() {

        numOfAccIdKeyUp();

    });

    $('#targetRevenueId').on('keyup change', function() {

        if(  $('#targetRevenueId').val().trim()
          && $('#targetRevenueId').val().trim() < 1
          ) {
            alert('Target Revenue cannot be less than 1');
            $('#targetRevenueId').val('');
            return;
        }

    });


    $("#accountId").keyup(function() {

        var activityStr = $("#buPlistId").val() == "--none--" ?
            "" :
            $("#buPlistId").val();

        var accName = $("#accountId").val().trim();
        var custGrpName = $("#customerGroupId").val().trim();

        if (activityStr && accName) {
            if(custGrpName.toLowerCase() != CUSTGROUP_OTHERS) {
                $("#titleId").val(accName + " - " + activityStr);
            } else {
                $("#titleId").val(activityStr);
            }
        } else if (accName) {
            $("#titleId").val(accName);
        } else if (activityStr) {
            $("#titleId").val(activityStr);
        } else {
            $("#titleId").val('');
        }

        $("#accountId").removeAttr("data");
        $("#accountId").removeAttr("data2");
        $("#accountId").removeAttr("name");

        validationOnBlur(this, null);
    });

    fetchUserRecords();

    populateCopyTaskVals();

    $('button[class^="fc-agendaDay-button"], button[class^="fc-agendaWeek-button"], button[class^="fc-month-button"]').click(function() {

        fetchUserData();

    });

    // To fetch the event data on change of user pick list.

    $( "#Work,"
     + "#accountId,"
     + "#buTypePlistId,"
     + "#titleId,"
     + "#buPlistId,"
     + "#startDateId,"
     + "#startId,"
     + "#endId,"
     + "#customerGroupId,"
     + "#provinceId,"
     + "#cityId,"
     + "#numOfAccId"
     ).on('change blur', function() {
        validationOnBlur(this, null);
    });

    var tempAlertDisable = window.alert;
    $('#closeEditModal').mousedown(function(e) {
        window.alert = function() {};
    });

    $('#closeEditModal').mouseup(function(e) {
        window.alert = tempAlertDisable;
    });


    $("#startDateId").datepicker({
        dateFormat: 'mm-dd-yy', //check change
        changeMonth: true,
        changeYear: true,
        minDate: new Date()
    });

    $("#copyDateId").datepicker({
        dateFormat: 'mm-dd-yy', //check change
        changeMonth: true,
        changeYear: true,
        minDate: new Date()
    });

    $('#startId').timepicker({
        timeFormat: 'hh:mm:ss p',
        interval: 5,
        startTime: '00:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true,
        change: function(time) {
            $("#endId").val("");
            setEndTime(time);
            validationOnBlur(this, null);
        }
    });

    $('#endId').timepicker({
        timeFormat: 'hh:mm:ss p',
        interval: 5,
        startTime: '00:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true,
        change: function(time) {
            validationOnBlur(this, null);
        }
    });

    $("#addEventModalId, #asmEditModalModal").scroll(function() {
        $('.ui-timepicker-container, #ui-datepicker-div').hide();

    });


    $('#asmStartDate').datepicker({
        dateFormat: 'mm-dd-yy', //check change
        changeMonth: true,
        changeYear: true,
        minDate: new Date(),
        onSelect: function(datePar, instPar) {
            $("#asmEndDate").val("");
            setEndDate(datePar);
        }
    });

    $('#asmEndDate').datepicker({
        dateFormat: 'mm-dd-yy', //check change
        changeMonth: true,
        changeYear: true,
        minDate: new Date()
    });

    localStorage.clear();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//---------------------------------End of Ready method-----------------------------------------------------------------------------------------------
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function numOfAccIdKeyUp() {
    $("#accountId").removeAttr("data");
    $("#accountId").removeAttr("data2");
    $("#accountId").removeAttr("name");

    if(!$('#numOfAccId').val().trim()) {
        $("#provinceId").val("");
        $("#provinceId").removeAttr("data");
        $("#provinceId").removeAttr("name");

        $("#cityId").val("");
        $("#cityId").removeAttr("data");
        $("#cityId").removeAttr("name");
        $("#cityId").prop('disabled', true);
    }

    $("#buTypePlistId").val("--none--");
    $("#buPlistId").val("--none--");

    computeNumOfAccKeyUp();
}


function computeBanner() {

    try {

        var startDate = userDetailsClass.asmRecordManager.Start_Date__c ?
            moment(userDetailsClass.asmRecordManager.Start_Date__c).format('MM-DD-YYYY') :
            '';

        var endDate = userDetailsClass.asmRecordManager.Start_Date__c ?
            moment(userDetailsClass.asmRecordManager.End_Date__c).format('MM-DD-YYYY') :
            '';

        if (startDate && ((moment(new Date()) - moment(new Date(userDetailsClass.asmRecordManager.Start_Date__c))) < 0)) {
            $('#bannerAnchorMsgId').text('');
            computeHeader();
            return;
        }

        if (endDate && ((moment(new Date()) - moment(new Date(userDetailsClass.asmRecordManager.End_Date__c)).add(1, 'days')) >= 0)) {
            $('#bannerAnchorMsgId').text('');
            computeHeader();
            return;
        }

        $('#bannerAnchorMsgId').text(userDetailsClass.asmRecordManager.Message__c);
        computeHeader();

    } catch (err) {
        $('#bannerAnchorMsgId').text('');
        computeHeader();
    }

}

function computeHeader() {
    if (userDetailsClass.currentUserRecord.Business_Unit__c == 'Feeds') {
        $('#feedsBanner').show();
    } else if (userDetailsClass.currentUserRecord.Business_Unit__c == 'Poultry') {
        $('#pAndMBanner').show();
    } else {
        $('#defaultBanner').show();
    }

    if($('#bannerAnchorMsgId').text()) {
        $("#weeklyMessageDivId").show();
    } else {
        $("#weeklyMessageDivId").hide();
    }
}

function computeNumOfAccKeyUp() {

    if ($('#numOfAccId').val().trim()) {
        $("#accountId").val('Multiple Accounts ' + $('#numOfAccId').val());
        $("#titleId").val('Multiple Accounts ' + $('#numOfAccId').val());
        $("#provinceReqId, #cityReqId").hide();
        $("#accountId").prop('disabled', true);
        $("#bu-AccSectionId>#activityDivId").hide();
    } else {
        $("#accountId").val('');
        $("#titleId").val('');
        $("#provinceReqId, #cityReqId").show();
        if(  $("#cityId").val().trim()
          || (!$("#provinceId").val().trim() && $("#customerGroupId").val().trim())
          ) {
            $("#accountId").removeAttr('disabled');
        } else {
            $("#accountId").prop('disabled', true);
        }

        if($("#customerGroupId").val().trim() && $("#provinceId").val().trim()) {
            $("#provinceReqId, #cityReqId").show();
        } else {
            $("#provinceReqId, #cityReqId").hide();
        }

        $("#bu-AccSectionId>#activityDivId").show();
    }

}


function getURLParameter(name) {
    var urlParam = decodeURIComponent((new RegExp('[?|&]month=' + '([^&;]+?)(&|#|;|$)').exec(location.href) || [null, ''])[1].replace(/\+/g, '%20')) || null;
    var retVal;

    if (urlParam) {
        retVal = decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.href) || [null, ''])[1].replace(/\+/g, '%20')) || null;
        console.log("--url present--");
    } else {
        try {
            retVal = monthDetails[name];
        } catch (err) {

        }
    }
    return retVal;
}


function calendarEvents() {

    var defDate = new Date();
    console.log("---253---", defDate);
    defDate.setMonth(defDate.getMonth() + 1);
    console.log("---254---" + defDate);

    var curDateArr = ((defDate).toString()).split(" ");
    var curDate = curDateArr[3] +
        "-" +
        getnumMonth(curDateArr[1]) +
        "-" +
        curDateArr[2];

    var monthStr = getURLParameter("month");
    var yearStr = getURLParameter("year");
    var userIdStr = getURLParameter("ownerId");

    var defDate = (monthStr && yearStr && userIdStr) ?
        yearStr + "-" + getnumMonth(monthStr.substring(0, 3)) + "-" + "01" :
        moment().format("MM-DD-YYYY");

    var varGoToDate = (monthStr && yearStr && userIdStr) ?
        getnumMonth(monthStr.substring(0, 3)) == '01' ?
        yearStr + "-01-" + "02" :
        yearStr + "-" + getnumMonth(monthStr.substring(0, 3)) + "-" + "01" :
        moment().day() == 1 ?
        moment().add(1, 'days').format("YYYY-MM-DD") :
        moment().format("YYYY-MM-DD");

    $('#calendar').fullCalendar({
        themeSystem: 'bootstrap4',
        timeFormat: 'H(:mm)',
        displayEventTime: false,
        showNonCurrentDates: false,
        allDaySlot: false,
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },

        views: {
            agendaTwoDay: {
                type: 'agenda',
                duration: {
                    days: 2
                },

                // views that are more than a day will NOT do this behavior by default
                // so, we need to explicitly enable it
                groupByResource: true

                //// uncomment this line to group by day FIRST with resources underneath
                //groupByDateAndResource: true
            },
            agendaDay: {
                slotLabelFormat: ['h:mm a']
            },
            agendaWeek: {
                slotLabelFormat: ['h:mm a']
            }
        },

        defaultDate: defDate,
        eventDurationEditable: false,
        eventStartEditable: false,
        //defaultDate: "2018-03-10",
        navLinks: true, // can click day/week names to navigate views
        editable: true,
        eventLimit: true, // allow "more" link when too many events

        //------------------------------------------------------------------------------------------------------------
        // Update logic to update the events on click.
        eventClick: function(eventObj) {

            eventClickExtend(eventObj);


            return;

        },
        //-----------------------------------------------------------------------------------------------------------

        //-----------------------------------------------------------------------------------------------------------
        // Click functionality on click of day to add event.
        dayClick: function(dayClickEvent) {

            CONFIG_EDIT = false;

            $("#copyEventId").css("display", "none");
            if ($('#numOfAccId').val().trim()) {
                $("#provinceReqId, #cityReqId").hide();
            } else {
                $("#provinceReqId, #cityReqId").show();
            }

            $('#buPlistId').find('option').remove();
            $('#buPlistId').append('<option>--none--</option>');

            dayClickEventClass = dayClickEvent;
            var selectedValue = _.find($("select[id$=userpickListId").children(), 'selected');
            dayClickEventClass.userId = selectedValue.value;

            editEventBUCompute();

            console.log("--337--", dayClickEventClass);

            var dateWhole = ((dayClickEventClass._d).toString()).split(" ");
            var switchDateValue;
            var userName = $("#userpickListId").text().trim();
            var userSFId = $("#userpickListId").val();

            if (dayClickEventClass._i[2]) {
                switchDateValue = 2 == (dayClickEventClass._i[2]).toString().length ?
                    (dayClickEventClass._i[2]).toString() :
                    "0" + (dayClickEventClass._i[2]).toString()
            } else {
                switchDateValue = dateWhole[2];
            }

            var date = dateWhole[3] + "-" + getnumMonth(dateWhole[1]) + "-" + switchDateValue;

            var currDateWhole = ((new Date()).toString()).split(" ");
            var currDateCompVar = currDateWhole[3] + "-" + getnumMonth(currDateWhole[1]) + "-" + currDateWhole[2];
            var todaysDay = currDateWhole[0];

            console.log(":::377:::", currDateCompVar, "::::", date);

            var timeDiff = (new Date(date + " 00:00:00") - new Date(currDateCompVar + " 00:00:00")) / 1000 / 60 / 60;


            var lastDayMonthWhole = ((new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0)).toString()).split(" ");
            var lastDateCompVar = lastDayMonthWhole[3] + "-" + getnumMonth(lastDayMonthWhole[1]) + "-" + lastDayMonthWhole[2];

            var lastDatetimeDiff = (new Date(date + " 00:00:00") - new Date(lastDateCompVar + " 00:00:00")) / 1000 / 60 / 60;

            if (moment(dayClickEventClass._d).format("YYYY-MM-DD") >= moment(NEXT_MONDAY).format("YYYY-MM-DD") &&
                ($("#monStatId").text() == "Approved" || $("#monStatId").text() == "Edited After Approval")) { //&& moment(dayClickEventClass._d).format("YYYY-MM-DD") <= moment(END_OF_MONTH).format("YYYY-MM-DD")

            } else if ($("#monStatId").text() == "Approved" || $("#monStatId").text() == "Edited After Approval") {
                //alert('Cannot Edit');
                return;
            }

            if (todaysDay != EDIT_APPROVED_TASKS_ON && ($("#monStatId").text() == "Approved" || $("#monStatId").text() == "Edited After Approval") && userDetailsClass.currentUserRecord.Business_Unit__c == 'GFS') { //
                alert("Tasks for Approved Month can be added only on Thursday");
                return;
            }
            if (($("#monStatId").text() == "Approved" || $("#monStatId").text() == "Submitted") && userDetailsClass.currentUserRecord.Business_Unit__c != 'GFS') {
                return;
            }
            if (userDetailsClass.currentUserRecord.Business_Unit__c == 'GFS' && $("#monStatId").text() == "Submitted") {
                return;
            }
            if (timeDiff < 0
                //|| _.includes(holidayList, date)
            ) {
                console.log("--533--");
                return;
            }

            if ( //getSelectedMonth().month == dateWhole[1] ||
                (monthJSONClass && monthJSONClass[0] &&
                    ((monthJSONClass[0].Status__c == "Edited After Approval" || monthJSONClass[0].Status__c == "Approved") && todaysDay != EDIT_APPROVED_TASKS_ON)
                    //&& (timeDiff <= 0 || lastDatetimeDiff > 0)
                    //&& lastDatetimeDiff <= 0
                )
            ) {
                console.log("--538--");
                return;
            }

            if ($('#userpickListId').val() != userDetailsClass.currUser) {
                return;
            }

            var startTimeArg = "";

            if (dayClickEventClass._i &&
                dayClickEventClass._i[3]
            ) {

                var minVar = "00";
                if (dayClickEventClass._i[4]) {
                    minVar = dayClickEventClass._i[4];
                }

                var hoursStr = 2 == dayClickEventClass._i[3].toString().length ?
                    dayClickEventClass._i[3] :
                    "0" + dayClickEventClass._i[3];

                startTimeArg = hoursStr +
                    ":" +
                    minVar +
                    ":" +
                    "00";
            }

            console.log("383:::", timeDiff);



            $("#doneId").attr("style", "display: block;");
            enableModalFields();

            setModalParameters(userName, userSFId, "", 'Half Day', $("#leaveReasonSelectId option:first").val(), 'Morning', "", date, startTimeArg, "", "", "", "", "", "Field work", "", "", "", "", "", "", "", "", "", "");

            setEndTime("");

            commandType = 1;
            clearModalErrorMessages();
            $("#addEventModalId").modal();
            $("#removeEventId").attr("style", "display: none;");
            $("#deleteEventId").hide();

            typeOfActivityCompute();
            editEventBUCompute();

            if (dayClickEventClass.sfId) {
                $("#goToId").attr("style", "display: block;");
            } else {
                $("#goToId").attr("style", "display: none;");
            }

            if (dayClickEventClass &&
                dayClickEventClass.taskmap &&
                dayClickEventClass.taskmap[clickedTaskId] &&
                dayClickEventClass.taskmap[clickedTaskId].eformUrl &&
                dayClickEventClass.taskmap[clickedTaskId].status &&
                dayClickEventClass.taskmap[clickedTaskId].status.toUpperCase() == 'APPROVED'
            ) {
                $("#goToEform").show();
            } else {
                $("#goToEform").hide();
            }

            if (  dayClickEventClass
               && dayClickEventClass.configTaskMap
               && dayClickEventClass.configTaskMap[clickedName]
               && dayClickEventClass.configTaskMap[clickedName][clickedTaskId]
               && dayClickEventClass.configTaskMap[clickedName][clickedTaskId].eformUrl
               ) {
                $("#goToConfigEform").show();
            } else {
                $("#goToConfigEform").hide();
            }

            if ($("#numOfAccId").val()) {
                $("#bu-AccSectionId>#activityDivId").hide();
            } else {
                $("#bu-AccSectionId>#activityDivId").show();
            }

            $("#cityId").prop('disabled', true);
            $("#accountId").prop('disabled', true);

            accountChangedClass = true;
            return;
        },


        eventMouseover: function(calEvent, jsEvent) {
            var tooltip = '<div class="tooltipevent"' +
                ' style="position: relative;display: inline-block;border-bottom: 1px dotted black;width: 120px;background-color:' +
                ' #d1ecf1;color: black;text-align: center;padding: 5px 0;border-radius: 6px;position: absolute;z-index: 1;">' +
                calEvent.title +
                '</div>';
            $("body").append(tooltip);
            $(this).mouseover(function(e) {
                $(this).css('z-index', 10000);
                $('.tooltipevent').fadeIn('500');
                $('.tooltipevent').fadeTo('10', 1.9);
            }).mousemove(function(e) {
                $('.tooltipevent').css('top', e.pageY + 10);
                $('.tooltipevent').css('left', e.pageX + 20);
            });
        },

        eventMouseout: function(calEvent, jsEvent) {
            $(this).css('z-index', 8);
            $('.tooltipevent').remove();
        },

        eventRender: function(event, element) {
            if (event && (event.accDescription || event.accDescription == 0 && event.accDescription != '')) {
                element.find('.fc-title').append('<div style="color: lightgrey">Rem: ' + event.accDescription + '</div>');
            }
        }
        //----------------------------------------------------------------------------------------------------------------------
    });

    $('#calendar').fullCalendar('gotoDate', varGoToDate);
    //$('#calendar').fullCalendar('gotoDate', '2020-01-02');
}

function eventClickExtend(eventObj) {

    $("#cityId").prop('disabled', true);
    $("#accountId").prop('disabled', true);

    $("#copyEventId").css("display", "");

    var startDateVar;
    var startTimeVar;
    var endTimeVar;
    var endDateVar;

    console.log('--163--', eventObj);
    dayClickEventClass = eventObj;

    // Check if event on day clicked is before curr date
    var dateWhole = ((dayClickEventClass.start._d).toString()).split(" ");

    var date = dateWhole[3] + "-" + getnumMonth(dateWhole[1]) + "-" + dateWhole[2];

    var currDateWhole = ((new Date()).toString()).split(" ");
    var currDateCompVar = currDateWhole[3] + "-" + getnumMonth(currDateWhole[1]) + "-" + currDateWhole[2];

    console.log(":::377:::", currDateCompVar, "::::", date);

    var timeDiff = (new Date(date + " 00:00:00") - new Date(currDateCompVar + " 00:00:00")) / 1000 / 60 / 60;

    console.log("383:::", timeDiff);

    if (timeDiff < 0) {
        $("#doneId").attr("style", "display: none;");
        $("#removeEventId").attr("style", "display: none;");
        disableModalFields();
    } else {
        $("#doneId").attr("style", "display: block;");
        $("#removeEventId").attr("style", "display: block;");
        enableModalFields();
    }


    // check end.

    commandType = 2;
    CONFIG_EDIT = false;

    if ("acc" == dayClickEventClass.eventType) {
        $("#accountModal").modal({
            "backdrop": "static"
        });

        $("#taskListId").empty();

        $("#clickAccNameId").text(" " + dayClickEventClass.title);

        var dateSortArr = [];

        $.each(Object.keys(dayClickEventClass.taskmap), function(index, value) {
            dateSortArr.push(dayClickEventClass.taskmap[value]);
        })


        dateSortArr.sort(function(a, b) {
            // Turn your strings into dates, and then subtract them
            // to get a value that is either negative, positive, or zero.
            var earlierDate = new Date(a.start.split("T")[0] + " " + a.start.split("T")[1]);
            var currDate = new Date(b.start.split("T")[0] + " " + b.start.split("T")[1]);
            return earlierDate - currDate;
        });

        $.each(dateSortArr, function(index, value) {

            $("#taskListId").append("<div><a href='#' id=" + value.sfId + ">" + value.taskTitle + "</a></div>");

            $("#" + value.sfId).click(function() {

                $('#goToAccounts').hide();

                $("#accountModal").modal("toggle");
                clearModalErrorMessages();
                $("#addEventModalId").modal();

                clickedTaskId = value.sfId;

                eventClickCompute(value, timeDiff, date);
                //Cancel functionality
                if ($('#userpickListId').val() == userDetailsClass.currUser) { //check for current user
                    if ($("#monStatId").text() == 'Approved' || $("#monStatId").text() == 'Submitted') {
                        $("#removeEventId").show();
                        if (userDetailsClass.currentUserRecord.Business_Unit__c != 'GFS' && ($("#monStatId").text() == "Edited After Approval" || $("#monStatId").text() == "Approved") ||
                            timeDiff < 0
                        ) {
                            $("#removeEventId").hide();
                        }else if(userDetailsClass.currentUserRecord.Business_Unit__c == 'GFS' && ($("#monStatId").text() == "Edited After Approval" || $("#monStatId").text() == "Approved")) {
                            $("#removeEventId").hide();
                            if(EDIT_APPROVED_TASKS_ON == new Date().toString().split(" ")[0]){
                                $("#removeEventId").show();
                            }
                        }
                        $("#deleteEventId").hide();
                    } else {
                        $("#removeEventId").hide();
                        $("#deleteEventId").show();
                        if (timeDiff < 0) {
                            $("#deleteEventId").hide();
                        }
                    }
                    if (value.taskCompletionDate) {
                        $("#removeEventId").hide();
                        $("#deleteEventId").hide();
                    }
                } else {
                    $("#removeEventId").hide();
                    $("#deleteEventId").hide();
                }
                if(  userDetailsClass.currentUserRecord.Business_Unit__c != 'GFS'
                  || $("#monStatId").text() == "Submitted"
                  ||(  userDetailsClass.currentUserRecord.Business_Unit__c == 'GFS'
                    && ($("#monStatId").text() == "Approved" || $("#monStatId").text() == "Edited After Approval")
                    && (moment(dayClickEventClass.start).format("YYYY-MM-DD") < moment(NEXT_MONDAY).format("YYYY-MM-DD"))
                    && new Date().toString().split(" ")[0] == EDIT_APPROVED_TASKS_ON
                    )
                  ) {
                    $('#removeEventId').hide();
                }
            });
        });

        console.log('--1118--', $("#cityReqId").is(":visible"));


    } else {
        clearModalErrorMessages();
        $("#addEventModalId").modal({
            "backdrop": "static"
        });

        if(  !dayClickEventClass.configTaskMap
          || $.isEmptyObject(dayClickEventClass.configTaskMap)
          || dayClickEventClass.configTaskMap.length <= 0
          ) {
            $('#goToAccounts').hide();
        } else {
            $('#goToAccounts').show();
        }

        if (Object.keys(dayClickEventClass.taskmap)) {
            clickedTaskId = Object.keys(dayClickEventClass.taskmap)[0];
            eventClickCompute(dayClickEventClass.taskmap[Object.keys(dayClickEventClass.taskmap)[0]], timeDiff, date)
        }
    }

    ////////////////////////////////////////////////////        Compute BU      /////////////////////////////////////////////

    typeOfActivityCompute();

    if ($("#selTaskTypeSpanId").text()) {
        $("#buTypePlistId").val($("#selTaskTypeSpanId").text());
    } else {
        $("#buTypePlistId").val("--none--");
    }

    editEventBUCompute();

    if ($("#selTaskSpanId").text()) {
        $("#buPlistId").val($("#selTaskSpanId").text());
    } else {
        $("#buPlistId").val("--none--");
    }

    if (dayClickEventClass &&
        dayClickEventClass.taskmap &&
        dayClickEventClass.taskmap[clickedTaskId] &&
        dayClickEventClass.taskmap[clickedTaskId].eformUrl &&
        dayClickEventClass.taskmap[clickedTaskId].status &&
        dayClickEventClass.taskmap[clickedTaskId].status.toUpperCase() == 'APPROVED'
    ) {
        $("#goToEform").show();
    } else {
        $("#goToEform").hide();
    }

    if (  dayClickEventClass
       && dayClickEventClass.configTaskMap
       && dayClickEventClass.configTaskMap[clickedName]
       && dayClickEventClass.configTaskMap[clickedName][clickedTaskId]
       && dayClickEventClass.configTaskMap[clickedName][clickedTaskId].eformUrl
       ) {
        $("#goToConfigEform").show();
    } else {
        $("#goToConfigEform").hide();
    }

    accountChangedClass = false;

    if ($('#userpickListId').val() == userDetailsClass.currUser) { //check for current user
        if (($("#monStatId").text() == 'Approved' && EDIT_APPROVED_TASKS_ON != new Date().toString().split(" ")[0])) { //and not equals Thus
            $("#removeEventId").show();
            $("#deleteEventId").hide();
        } else {
            $("#removeEventId").hide();
            $("#deleteEventId").show();
            if (timeDiff < 0) {
                $("#deleteEventId").hide();
            }
        }

        if (clickedTaskId != null && dayClickEventClass.taskmap[clickedTaskId] != undefined && dayClickEventClass.taskmap[clickedTaskId].taskCompletionDate) {
            $("#removeEventId").hide();
            $("#deleteEventId").hide();
        }
    } else {
        $("#removeEventId").hide();
        $("#deleteEventId").hide();
    }

    if (
        ((new Date().toString().split(" ")[0] != EDIT_APPROVED_TASKS_ON) &&
            ($("#monStatId").text() == "Approved" || $("#monStatId").text() == "Edited After Approval") &&
            userDetailsClass.currentUserRecord.Business_Unit__c == 'GFS') ||
        (timeDiff < 0) ||
        $("#monStatId").text() == "Submitted" ||
        (userDetailsClass.currentUserRecord.Business_Unit__c != 'GFS' && $("#monStatId").text() == "Approved")
    ) { // not thursday

        disableModalFields();

        $("#copyEventId").show();
        $("#deleteEventId").hide();
        $("#doneId").hide();
        $("#removeEventId").show();
        if (userDetailsClass.currentUserRecord.Business_Unit__c != 'GFS') {
            $("#removeEventId").hide();
        }
        $("#numOfAccId").prop('disabled', true);
    } else if (timeDiff > 0 && $('#userpickListId').val() == userDetailsClass.currUser) {
        enableModalFields();
        $("#copyEventId").show();
        $("#deleteEventId").show();
        if (timeDiff < 0) {
            $("#deleteEventId").hide();
        }
        $("#doneId").show();
        $("#removeEventId").hide();
        $("#numOfAccId").prop('disabled', false);
        //$("#sendApprovalId").prop('disabled', false);
    }


    if (new Date().toString().split(" ")[0] == EDIT_APPROVED_TASKS_ON &&
        ($("#monStatId").text() == "Approved" || $("#monStatId").text() == "Edited After Approval" || $("#monStatId").text() == "Submitted") &&
        (timeDiff < 0)) {
        //$("#sendApprovalId").prop('disabled', false);
        $("#doneId").hide();
        //alert('here');
    }


    if ((userDetailsClass.currentUserRecord.Business_Unit__c == 'GFS' &&
            ($("#monStatId").text() == "Edited After Approval" || $("#monStatId").text() == "Approved") &&
            (moment(dayClickEventClass.start).format("YYYY-MM-DD") >= moment(NEXT_MONDAY).format("YYYY-MM-DD")) &&
            timeDiff >= 0 && $('#userpickListId').val() == userDetailsClass.currUser) ||
        (($("#monStatId").text() == "Not Submitted" || $("#monStatId").text() == "Rejected") && timeDiff >= 0 && $('#userpickListId').val() == userDetailsClass.currUser)
    ) {
        enableModalFields();
        $("#copyEventId").show();
        $("#deleteEventId").show();
        if (timeDiff < 0) {
            $("#deleteEventId").hide();
        }
        $("#doneId").show();
        $("#removeEventId").hide();
        $("#numOfAccId").prop('disabled', false);

        if ($('#numOfAccId').val().trim()) {
            $("#Work").prop('disabled', true);
        }
        //$("#sendApprovalId").prop('disabled', false);
    } else {
        disableModalFields();

        $("#copyEventId").show();
        $("#deleteEventId").hide();
        $("#doneId").hide();
        $("#removeEventId").show();
        if (userDetailsClass.currentUserRecord.Business_Unit__c != 'GFS' && ($("#monStatId").text() == "Edited After Approval" || $("#monStatId").text() == "Approved") ||
            timeDiff < 0
        ) {
            $("#removeEventId").hide();
        }
        $("#numOfAccId").prop('disabled', true);
    }

    if ((userDetailsClass.currentUserRecord.Business_Unit__c == 'Poultry' || userDetailsClass.currentUserRecord.Business_Unit__c == 'Feeds') &&
        ("Approved" == $('#monStatId').text())
    ) {
        $('#removeEventId').hide();
        $('#createActivityId').prop('disabled', true).css('opacity',0.5);

    }

    if( userDetailsClass.currentUserRecord.Business_Unit__c == 'GFS' //GFS
    &&  ($("#monStatId").text() == "Edited After Approval" || $("#monStatId").text() == "Approved") //Approved
    &&  new Date().toString().split(" ")[0] == EDIT_APPROVED_TASKS_ON //Thursday
    //&&  timeDiff > 0 //Further date
    &&  (moment(dayClickEventClass.start).format("YYYY-MM-DD") >= moment(NEXT_MONDAY).format("YYYY-MM-DD")) //Next Monday onwards
    && $('#userpickListId').val() == userDetailsClass.currUser
    ){
        enableModalFields();
        $('#removeEventId').show();
        $("#deleteEventId").hide();
        $("#doneId").show();

    }else if( userDetailsClass.currentUserRecord.Business_Unit__c == 'GFS' //GFS
    &&  ($("#monStatId").text() == "Edited After Approval" || $("#monStatId").text() == "Approved") //Approved
    &&  new Date().toString().split(" ")[0] != EDIT_APPROVED_TASKS_ON //Thursday
    ){
        disableModalFields();
        $('#removeEventId').hide();
        $('#createActivityId').prop('disabled', true).css('opacity',0.5);
        $("#deleteEventId").hide();
        $("#doneId").hide();
    }

    if(userDetailsClass.currentUserRecord.Business_Unit__c != 'GFS' //GFS
    &&  $("#monStatId").text() == "Approved"){//Approved
        $('#removeEventId').hide();
        $('#createActivityId').prop('disabled', true).css('opacity',0.5);
    }


    if(  userDetailsClass.currentUserRecord.Business_Unit__c != 'GFS'
      || $("#monStatId").text() == "Submitted"
      ||(  userDetailsClass.currentUserRecord.Business_Unit__c == 'GFS'
            && ($("#monStatId").text() == "Approved" || $("#monStatId").text() == "Edited After Approval")
            && (moment(dayClickEventClass.start).format("YYYY-MM-DD") < moment(NEXT_MONDAY).format("YYYY-MM-DD"))
            && new Date().toString().split(" ")[0] == EDIT_APPROVED_TASKS_ON
        )
      ) {
        $('#removeEventId').hide();
    }

    if ($('#userpickListId').val() != userDetailsClass.currUser) {
        $("#copyEventId").hide();
        $("#removeEventId").hide();
        $("#deleteEventId").hide();
    }
}

function colorCompute() {

    var valArr = [];
    $.each($("#calendar").fullCalendar('clientEvents'), function(index2, value2) {

        var colorCode = "";

        $.each(value2.taskmap, function(index, value) {

            if (value.Activity_Status__c &&
                "Incomplete" == value.Activity_Status__c
            ) {
                colorCode = "#ff0000"; // Red.
                return false;
            }

            if (value.Activity_Status__c &&
                ("Delayed" == value.Activity_Status__c) &&
                colorCode != "#ff0000"
            ) {
                colorCode = "#0070d2"; // Blue.
                return;
            }

            if (colorCode != "#0070d2" &&
                colorCode != "#ff0000"
            ) {
                colorCode = "#378400"; // Green.
                return;
            }


        });

        value2.color = colorCode;
        value2.textColor = 'white';
        valArr.push(value2);
    });

    $('#calendar').fullCalendar('removeEvents');

    $.each(valArr, function(index, value) {
        $('#calendar').fullCalendar('renderEvent', value, stick = true);
    });
}

function setTitle() {

    var accountValue;

    var accountName = $("#accountId").val();
    var accountSetName = $("#accountId").attr("data");
    var accountId = $("#accountId").attr("name");

    if ($("#accountDivId").attr("style") != "display: none" &&
        accountName.trim() &&
        accountId &&
        accountSetName &&
        accountName.trim() == accountSetName
    ) {

        accountValue = accountSetName;
    } else {
        accountValue = "";
    }

    var activityStr = $("#buPlistId").val() == "--none--" ?
        "" :
        $("#buPlistId").val();

    var accName = $("#accountId").val().trim();
    var custGrpName = $("#customerGroupId").val().trim();

    if (activityStr && accName && $('#Work').val() == 'Field work') {
        if(custGrpName.toLowerCase() != CUSTGROUP_OTHERS) {
            $("#titleId").val(accName + " - " + activityStr);
        } else {
            $("#titleId").val(activityStr);
        }
    } else if (accName && $('#Work').val()== 'Field work') {
        $("#titleId").val(accName);
    } else if (activityStr) {
        $("#titleId").val(activityStr);
    } else {
        $("#titleId").val('');
    }

}

function showTargetRevenue() {
    if ($('#buTypePlistId').val() == 'In Field, In Store and On Floor' &&
        $('#buPlistId').val() == 'Account Sales Call / Order Booking'
    ) {
        $('#targetRevenueDivId').show();
    } else {

        $('#targetRevenueDivId').hide();
    }
}

function setEndTime(time) {
    //$('#endId').val("");
    $('#endId').timepicker("destroy");

    if (moment(time, "hh:mm:ss A", true).isValid()) {
        console.log("265---", time);
        time = moment(time, 'hh:mm:ss A').add(5, 'minutes').format('hh:mm:ss A');
        console.log("266---", time);
        $('#endId').timepicker({
            timeFormat: 'hh:mm:ss p',
            interval: 5,
            minTime: time,
            startTime: time,
            dynamic: false,
            dropdown: true,
            scrollbar: true,
            change: function() {
                validationOnBlur(this, null);
            }
        });
    } else {
        $('#endId').timepicker({
            timeFormat: 'hh:mm:ss p',
            interval: 5,
            startTime: '00:00',
            dynamic: false,
            dropdown: true,
            scrollbar: true,
            change: function() {
                validationOnBlur(this, null);
            }
        });
    }
}

function setEndDate(datePar) {

    $('#asmEndDate').datepicker("destroy");

    if (moment(datePar, "MM-DD-YYYY", true).isValid()) {
        console.log("265---", datePar);
        console.log("266---", datePar);
        $('#asmEndDate').datepicker({
            dateFormat: 'mm-dd-yy', //check change
            changeMonth: true,
            changeYear: true,
            minDate: datePar
        });
    } else {
        $('#asmEndDate').datepicker({
            dateFormat: 'mm-dd-yy', //check change
            changeMonth: true,
            changeYear: true,
            minDate: new Date()
        });
    }
}


function validationOnBlur(parThis, cityIdPressed) {

    if(  $('#numOfAccId').val().trim()
      && $('#numOfAccId').val().trim() < 2
      ) {
        alert('Multiple accounts cannot be less than 2');
        $('#numOfAccId').val('');
        numOfAccIdKeyUp();
        return;
    }

    var varThis;
    try {
        varThis = parThis[0];
        if (varThis && varThis.id) {
            parThis = varThis;
        }
    } catch (err) {}

    console.log('--see--', eventName);

    clearModalErrorMessages();

    if (parThis && parThis.id == 'Work') {
        return false;
    }

    var eventName = $("#titleId").val();
    var startDateVar = moment($("#startDateId").val()).format('YYYY-MM-DD');
    var startTimeVar = $("#startId").val();
    var endTimeVar = $("#endId").val();

    var accountName = $("#accountId").val();
    var accountSetName = $("#accountId").attr("data");
    var accountId = "";

    var custGroupName = $("#customerGroupId").val();
    var custGroupSetName = $("#customerGroupId").attr("data");
    var custGroupId = "";

    var provinceName = $("#provinceId").val();
    var provinceSetName = $("#provinceId").attr("data");
    var provinceId = "";

    var cityName = $("#cityId").val();
    var citySetName = $("#cityId").attr("data");
    var cityId = "";

    var potCustVar = $("#potCustId").val();
    var descriptionVar = $("#commentId").val();
    var leaveTypeVar = $('#leaveSelectId').val();
    var leaveReasonVar = $('#leaveReasonSelectId').val();
    var halfleaveTypeVar = $('#halfLeaveSelectId').val();

    if (accountName) {
        accountId = $("#accountId").get(0).name;
    } else {
        $("#accountId").attr("name", "");
    }

    if (custGroupName) {
        custGroupId = $("#customerGroupId").get(0).name;
    } else {
        $("#customerGroupId").attr("name", "");
    }

    if (provinceName) {
        provinceId = $("#provinceId").get(0).name;
    } else {
        $("#provinceId").attr("name", "");
    }

    if (cityName) {
        cityId = $("#cityId").get(0).name;
    } else {
        $("#cityId").attr("name", "");
    }

    var workOptionVar = $("#Work").val();
    var taskTypeOptionVar = $('#buPlistId').val();
    var actTypeOptionVar = $('#buTypePlistId').val();
    var userId = _.find($("select[id$=userpickListId]").children(), 'selected').value;

    console.log("929--", actTypeOptionVar);

    var startDate;
    var endDate;
    var eventType = "";

    if ("Leave" == workOptionVar) {
        if (leaveTypeVar) {
            if (leaveTypeVar == 'Full Day') {
                startDate = startDateVar + 'T' + '09:00:00';
                endDate = startDateVar + 'T' + '17:00:00';
            } else if (leaveTypeVar == 'Half Day') {

                if (halfleaveTypeVar) {

                    if (halfleaveTypeVar == 'Morning') {
                        startDate = startDateVar + 'T' + '09:00:00';
                        endDate = startDateVar + 'T' + '14:00:00';
                    } else {
                        startDate = startDateVar + 'T' + '14:00:00';
                        endDate = startDateVar + 'T' + '18:00:00';
                    }

                }

            }
        }
    } else {


        if ("Field work" == workOptionVar) {

            if (accountChangedClass) {

                if (accountId &&
                    accountName &&
                    accountName == accountSetName &&
                    (UNPLANNED_ACCOUNT.toLowerCase() == accountName.toLowerCase() || MULTIPLE_ACCOUNT.toLowerCase() == accountName.toLowerCase())
                ) {
                    $('#provinceIdError').css('display', 'none');
                    $('#cityIdError').css('display', 'none');
                    $('#custGroupIdError').css('display', 'none');
                    if (parThis && parThis.id == 'provinceId') {
                        return false;
                    }
                    if (parThis && parThis.id == 'cityId') {
                        return false;
                    }
                    if (parThis && parThis.id == 'customerGroupId') {
                        return false;
                    }
                } else {

                    if (((userDetailsClass.currentUserRecord.Business_Unit__c.toLowerCase() == 'Feeds'.toLowerCase() ||
                            userDetailsClass.currentUserRecord.Business_Unit__c.toLowerCase() == 'Poultry'.toLowerCase()
                        ))
                        //|| userDetailsClass.currentUserRecord.Meats__c
                    ) { // Agro

                        if (!$('#numOfAccId').val().trim()) {

                            if (!provinceId ||
                                !provinceName ||
                                provinceName != provinceSetName
                            ) {
                                $('#provinceIdError').css('display', '');
                                if (parThis && (parThis.id == 'provinceId' || cityIdPressed)) {
                                    return true;
                                }
                            } else {
                                $('#provinceIdError').css('display', 'none');
                                if (parThis && parThis.id == 'provinceId') {
                                    return false;
                                }
                            }

                            if (!cityId ||
                                !cityName ||
                                cityName != citySetName
                            ) {
                                $('#cityIdError').css('display', '');
                                if (parThis && parThis.id == 'cityId') {
                                    return true;
                                }
                            } else {
                                $('#cityIdError').css('display', 'none');
                                if (parThis && parThis.id == 'cityId') {
                                    return false;
                                }
                            }

                        } else {
                            $('#provinceIdError').hide();
                            if (parThis && (parThis.id == 'provinceId' || cityIdPressed)) {
                                return true;
                            }
                            $('#cityIdError').hide();
                            if (parThis && parThis.id == 'cityId') {
                                return false;
                            }
                        }

                    } else if (userDetailsClass.currentUserRecord.Business_Unit__c.toLowerCase() == 'GFS'.toLowerCase()) { // GFS

                        if (!custGroupId ||
                            !custGroupName ||
                            custGroupName != custGroupSetName
                        ) {
                            $('#custGroupIdError').css('display', '');
                            if (parThis && parThis.id == 'customerGroupId') {
                                return true;
                            }
                        } else {
                            $('#custGroupIdError').css('display', 'none');
                            if (parThis && parThis.id == 'customerGroupId') {
                                return false;
                            }
                        }

                        if (parThis && parThis.id == 'provinceId') {
                            return false;
                        }

                        if (parThis && parThis.id == 'cityId') {
                            return false;
                        }

                    }
                }
            }

            if (custGroupSetName &&
                custGroupSetName.toLowerCase() != CUSTGROUP_OTHERS.toLowerCase() &&
                accountName &&
                (!accountId ||
                    accountName != accountSetName
                )
            ) {
                $('#accountIdError').css('display', '');
                if (parThis && parThis.id == 'accountId') {
                    return true;
                }
            } else {
                $('#accountIdError').css('display', 'none');
                if (parThis && parThis.id == 'accountId') {
                    return false;
                }
            }
        }

        // Field work and account is compulsory
        if ("Field work" == workOptionVar &&
            custGroupSetName.toLowerCase() != CUSTGROUP_OTHERS.toLowerCase() &&
            accountName &&
            (!accountId ||
                accountName != accountSetName
            )
        ) {

            $('#accountIdError').css('display', '');
            if (parThis && parThis.id == 'accountId') {
                return true;
            }
        } else {
            $('#accountIdError').css('display', 'none');
            if (parThis && parThis.id == 'accountId') {
                return false;
            }

        }

        // Field work and account is compulsory
        if ("Field work" == workOptionVar &&
            $('#numOfAccId').val()
        ) {

            $('#accountIdError').hide();
            if (parThis && parThis.id == 'accountId') {
                return true;
            }
        }


        // Type of activity
        if (!actTypeOptionVar ||
            "--none--" == actTypeOptionVar
        ) {
            $('#buTypePlistIdError').css('display', '');
            if (parThis && parThis.id == 'buTypePlistId') {
                return true;
            }
        } else {
            $('#buTypePlistIdError').css('display', 'none');
            if (parThis && parThis.id == 'buTypePlistId') {
                return false;
            }

        }

        // Activity.
        if (!taskTypeOptionVar ||
            "--none--" == taskTypeOptionVar
        ) {
            $('#buPlistIdError').css('display', '');
            if (parThis && parThis.id == 'buPlistId') {
                return true;
            }
        } else {
            $('#buPlistIdError').css('display', 'none');
            if (parThis && parThis.id == 'buPlistId') {
                return false;
            }

        }


        // Title.
        if (!eventName || !(eventName.trim()) || eventName.length > 250) {

            $('#titleIdError').css('display', '');
            if (parThis && parThis.id == 'titleId') {
                return true;
            }
        } else {
            $('#titleIdError').css('display', 'none');
            if (parThis && parThis.id == 'titleId') {
                return false;
            }
        }

        // Date.
        if (!(moment(startDateVar, "YYYY-MM-DD", true).isValid())) {
            $('#startDateIdError').css('display', '');
            if (parThis && parThis.id == 'startDateId') {
                return true;
            }
        } else {
            $('#startDateIdError').css('display', 'none');
            if (parThis && parThis.id == 'startDateId') {
                return false;
            }

        }

        // StartTime.
        if (!startTimeVar || !(startTimeVar.trim())) {
            $('#startIdError').css('display', '');
            if (parThis && parThis.id == 'startId') {
                return true;
            }
        } else {
            $('#startIdError').css('display', 'none');
            if (parThis && parThis.id == 'startId') {
                return false;
            }
        }

        // End Time.
        if (!endTimeVar || !(endTimeVar.trim())) {
            $('#endIdError').css('display', '');
            if (parThis && parThis.id == 'endId') {
                return true;
            }
        } else {
            $('#endIdError').css('display', 'none');
            if (parThis && parThis.id == 'endId') {
                return false;
            }
        }


        // Start Time Conversion 12 to 24.
        try {
            startTimeVar = convert12to24Hrs(startTimeVar);
            if (parThis && parThis.id == 'startId') {
                return false;
            }
        } catch (err) {
            //alert("Enter Valid Start Date / Time");
            $('#startIdError').css('display', '');
            if (parThis && parThis.id == 'startId') {
                return true;
            }
        }

        // End Time Conversion 12 to 24.
        try {
            endTimeVar = convert12to24Hrs(endTimeVar);
            if (parThis && parThis.id == 'endId') {
                return false;
            }
        } catch (err) {
            //alert("Enter Valid End Date / Time");
            $('#endIdError').css('display', '');
            if (parThis && parThis.id == 'endId') {
                return true;
            }
        }

        // Start Time 24 hrs format
        if (!moment(startTimeVar, "HH:mm:ss", true).isValid()) {
            $('#startIdError').css('display', '');
            if (parThis && parThis.id == 'startId') {
                return true;
            }
        } else {
            $('#startIdError').css('display', 'none');
            if (parThis && parThis.id == 'startId') {
                return false;
            }
        }

        // End Time 24 hrs format
        if (!moment(endTimeVar, "HH:mm:ss", true).isValid()) {
            $('#endIdError').css('display', '');
            if (parThis && parThis.id == 'endId') {
                return true;
            }
        } else {
            $('#endIdError').css('display', 'none');
            if (parThis && parThis.id == 'endId') {
                return false;
            }
        }


        startDate = startDateVar + 'T' + startTimeVar;
        endDate = startDateVar + 'T' + endTimeVar;
    }

    console.log('--' + startDate + '--' + endDate);
    // StartDateTime as whole.
    if (!(moment(startDate, "YYYY-MM-DDTHH:mm:ss", true).isValid())) {
        //alert("Enter Valid Start Date / Time");
        $('#startDateIdError').css('display', '');
        return true;
    } else {
        $('#startDateIdError').css('display', 'none');
        return false;

    }

    // EndDateTime as whole.
    if (!(moment(endDate, "YYYY-MM-DDTHH:mm:ss", true).isValid())) {
        //alert("Enter Valid End Date / Time");
        $('#startDateIdError').css('display', '');
        return true;
    } else {
        $('#startDateIdError').css('display', 'none');
        return false;

    }

    // Start Date after End Date.
    if (!moment(endDate).isAfter(startDate)) {
        //alert("Enter Start Time less than End Time");
        $('#startDateIdError').css('display', '');
        return true;
    } else {
        $('#startDateIdError').css('display', 'none');
        return false;

    }

    return false;

}

function asmMessageButtonClick() {

    var startDate = '';
    var endDate = '';
    var msgVar = $('#asmNewMessage').val().trim();

    if (msgVar &&
        $('#asmStartDate').val().trim() &&
        moment($('#asmStartDate').val(), "MM-DD-YYYY", true).isValid()
    ) {
        startDate = moment($("#asmStartDate").val()).format('YYYY-MM-DD');
    } else if (msgVar) {
        alert('Enter Valid Start Date');
        return;
    }

    if (msgVar &&
        $('#asmEndDate').val().trim() &&
        moment($('#asmEndDate').val(), "MM-DD-YYYY", true).isValid()
    ) {
        endDate = moment($("#asmEndDate").val()).format('YYYY-MM-DD');
    } else if (msgVar) {
        alert('Enter Valid End Date');
        return;
    }

    if(userDetailsClass.asmRecordSelf && userDetailsClass.asmRecordSelf.attributes) {
        delete userDetailsClass.asmRecordSelf.attributes;
    }
    $("#blurDivId").show();

    var asmRecordStr = JSON.stringify(userDetailsClass.asmRecordSelf)
                     ? JSON.stringify(userDetailsClass.asmRecordSelf)
                     : '';

    Visualforce.remoting.Manager.invokeAction(
        'CalendarController.insertASMMessage',
        userDetailsClass.loggedUserId,
        asmRecordStr,
        msgVar,
        startDate,
        endDate,
        function(result, event) {
            if (event.status) {
                console.log('success in remoting--');
                try {
                    var resJSON = JSON.parse(result);
                    if (resJSON.Message__c && resJSON.Start_Date__c && resJSON.End_Date__c) {
                        userDetailsClass.asmRecordSelf = resJSON;
                        userDetailsClass.asmRecordManager = resJSON;
                        computeBanner();
                    }
                } catch (err) {
                    $('#bannerAnchorMsgId').text('');
                    $('#bannerAnchorDateId').text('');
                    userDetailsClass.asmRecordSelf = '';
                }
                $('#asmEditModalModal').modal('hide');
                $("#blurDivId").hide();
            } else if (event.type === 'exception') {
                // code.
                $('#asmEditModalModal').modal('hide');
                $("#blurDivId").hide();
            } else {
                // code.
                $('#asmEditModalModal').modal('hide');
                $("#blurDivId").hide();
            }
        }, {
            escape: false
        }
    );

}

function clearModalErrorMessages() {

    $('#accountIdError').css('display', 'none');
    $('#buTypePlistIdError').css('display', 'none');
    $('#buPlistIdError').css('display', 'none');
    $('#titleIdError').css('display', 'none');
    $('#startDateIdError').css('display', 'none');
    $('#startIdError').css('display', 'none');
    $('#endIdError').css('display', 'none');
    $('#custGroupIdError').css('display', 'none');
    $('#provinceIdError').css('display', 'none');
    $('#cityIdError').css('display', 'none');

}


function eventClickCompute(clickedTask, timeDiff, date) {

    $("#cityId").prop('disabled', true);
    $("#accountId").prop('disabled', true);

    var startDateVar;
    var startTimeVar;
    var endTimeVar;
    var endDateVar;

    var lastDayMonthWhole = ((new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0)).toString()).split(" ");
    var lastDateCompVar = lastDayMonthWhole[3] + "-" + getnumMonth(lastDayMonthWhole[1]) + "-" + lastDayMonthWhole[2];

    var lastDatetimeDiff = (new Date(date + " 00:00:00") - new Date(lastDateCompVar + " 00:00:00")) / 1000 / 60 / 60;
    if ("display: block;" == $("#doneId").attr("style")) {
        if (("Submitted" == clickedTask.status || clickedTask.numOfAccounts
                //&& (timeDiff <= 0 || lastDatetimeDiff > 0)
                //&& lastDatetimeDiff <= 0
            ) ||
            ("Approved" == clickedTask.status
                //&& (timeDiff <= 0 || lastDatetimeDiff > 0)
                //&& lastDatetimeDiff <= 0
            ) ||
            $('#userpickListId').val() != userDetailsClass.currUser
        ) {
            $("#doneId").attr("style", "display: none;");
            $("#removeEventId").attr("style", "display: none;");
            disableModalFields();
            $("#deleteEventId").hide();
            $("#removeEventId").hide();
        } else {
            $("#doneId").attr("style", "display: block;");
            $("#removeEventId").attr("style", "display: block;");
            if (userDetailsClass.currentUserRecord.Business_Unit__c != 'GFS' && ($("#monStatId").text() == "Edited After Approval" || $("#monStatId").text() == "Approved") ||
                timeDiff < 0
            ) {
                $("#removeEventId").hide();
            }
            $("#deleteEventId").show();
            enableModalFields();
        }

        console.log('--2068--', $("#cityReqId").is(":visible"));
    }

    if ($('#userpickListId').val() != userDetailsClass.currUser) {
        $("#copyEventId").hide();
    } else {
        $("#copyEventId").show();
    }

    if (clickedTask.start) {
        startTimeVar = (clickedTask.start).length > 10 ?
            (clickedTask.start).split("T")[1] :
            (clickedTask.start);

        startDateVar = (clickedTask.start).length > 10 ?
            (clickedTask.start).split("T")[0] :
            (clickedTask.start);
    }

    if (clickedTask.end) {
        console.log('---222--', clickedTask.end);
        endDateVar = (clickedTask.end).split("T")[0];

        endTimeVar = (clickedTask.end).length > 10 ?
            (clickedTask.end).split("T")[1] :
            (clickedTask.end);
    }

    console.log('-95--', clickedTask);

    var accName = clickedTask.custGroupSetName ?
        clickedTask.custGroupSetName :
        clickedTask.accountName;



    setModalParameters( clickedTask.userName
                      , clickedTask.userId
                      , clickedTask.taskTitle
                      , clickedTask.halfDay
                      , clickedTask.leaveReason
                      , clickedTask.halfDayType
                      , clickedTask.description
                      , startDateVar
                      , startTimeVar
                      , endTimeVar
                      , clickedTask.accountId
                      , accName
                      , clickedTask.numOfAccounts
                      , clickedTask.potCustDetails
                      , clickedTask.workOption
                      , clickedTask.activityTypeOption
                      , clickedTask.taskTypeOption
                      , clickedTask.city
                      , clickedTask.cityName
                      , clickedTask.province
                      , clickedTask.provinceName
                      , clickedTask.customerGroup
                      , clickedTask.customerGroupName
                      , clickedTask.customerGroupDescription
                      , clickedTask.targetRevenue
                      );

    if (clickedTask.sfId) {
        $("#goToId").attr("style", "display: block;");
    } else {
        console.log("599--", clickedTask)
        $("#goToId").attr("style", "display: none;");
    }

    if (dayClickEventClass &&
        dayClickEventClass.taskmap &&
        dayClickEventClass.taskmap[clickedTaskId] &&
        dayClickEventClass.taskmap[clickedTaskId].eformUrl &&
        dayClickEventClass.taskmap[clickedTaskId].status &&
        dayClickEventClass.taskmap[clickedTaskId].status.toUpperCase() == 'APPROVED'
    ) {
        $("#goToEform").show();
    } else {
        $("#goToEform").hide();
    }

    if (  dayClickEventClass
       && dayClickEventClass.configTaskMap
       && dayClickEventClass.configTaskMap[clickedName]
       && dayClickEventClass.configTaskMap[clickedName][clickedTaskId]
       && dayClickEventClass.configTaskMap[clickedName][clickedTaskId].eformUrl
       ) {
        $("#goToConfigEform").show();
    } else {
        $("#goToConfigEform").hide();
    }

    setEndTime($("#startId").val());

    ////////////////////////////////////////////////////        Compute BU      /////////////////////////////////////////////

    typeOfActivityCompute();

    if ($("#selTaskTypeSpanId").text()) {
        $("#buTypePlistId").val($("#selTaskTypeSpanId").text());
    } else {
        $("#buTypePlistId").val("--none--");
    }

    editEventBUCompute();

    if ($("#selTaskSpanId").text()) {
        $("#buPlistId").val($("#selTaskSpanId").text());
    } else {
        $("#buPlistId").val("--none--");
    }

    if ($('#numOfAccId').val().trim() || userDetailsClass.currentUserRecord.Business_Unit__c.toLowerCase() == 'GFS'.toLowerCase()) {
        $("#provinceReqId, #cityReqId").hide();
        if($('#numOfAccId').val().trim()) {
            $("#bu-AccSectionId>#activityDivId").hide();
        } else {
            $("#bu-AccSectionId>#activityDivId").show();
        }
    } else {
        $("#provinceReqId, #cityReqId").show();
        $("#bu-AccSectionId>#activityDivId").show();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    if ($("#monStatId").text() == "Submitted" ||
        (new Date().toString().split(" ")[0] != EDIT_APPROVED_TASKS_ON && ($("#monStatId").text() == "Approved" || $("#monStatId").text() == "Edited After Approval") && userDetailsClass.currentUserRecord.Business_Unit__c == 'GFS') ||
        (timeDiff < 0) ||
        (moment(dayClickEventClass._d).format("YYYY-MM-DD") <= moment(NEXT_MONDAY).format("YYYY-MM-DD") && userDetailsClass.currentUserRecord.Business_Unit__c == 'GFS') ||
        (userDetailsClass.currentUserRecord.Business_Unit__c != 'GFS' && ($("#monStatId").text() == "Approved"))
    ) {
        //alert('!!!!!!!!!!!!!!!!!');
        disableModalFields();
        $("#copyEventId").show();
        $("#deleteEventId").hide();
        $("#removeEventId").hide();
        $("#doneId").hide();
        $("#numOfAccId").prop('disabled', true);
    } else if (!clickedTask.numOfAccounts || (userDetailsClass.currentUserRecord.Business_Unit__c == 'GFS' && $("#monStatId").text() != "Submitted")) {
        enableModalFields();
        $("#copyEventId").show();
        $("#deleteEventId").show();
        //$("#deleteEventId").hide();
        $("#doneId").show();
        $("#numOfAccId").prop('disabled', false);
    }
    if (userDetailsClass.currentUserRecord.Business_Unit__c == 'GFS' && $("#monStatId").text() == "Not Submitted" && $('#userpickListId').val() == userDetailsClass.currUser) {
        enableModalFields();
        $("#copyEventId").show();
        $("#deleteEventId").show();
        $("#doneId").show();
        $("#removeEventId").hide();
        $("#numOfAccId").prop('disabled', false);
        //$("#sendApprovalId").prop('disabled', false);
    }

    if ((userDetailsClass.currentUserRecord.Business_Unit__c == 'GFS' &&
            ($("#monStatId").text() == "Edited After Approval" || $("#monStatId").text() == "Approved") &&
            (moment(startDateVar).format("YYYY-MM-DD") >= moment(NEXT_MONDAY).format("YYYY-MM-DD")) &&
            !clickedTask.numOfAccounts &&
            timeDiff >= 0 && $('#userpickListId').val() == userDetailsClass.currUser) ||
        (($("#monStatId").text() == "Not Submitted" || $("#monStatId").text() == "Rejected") && timeDiff >= 0 && $('#userpickListId').val() == userDetailsClass.currUser)
    ) {
        enableModalFields();
        $("#copyEventId").show();
        $("#deleteEventId").show();
        $("#doneId").show();
        $("#removeEventId").hide();
        $("#numOfAccId").prop('disabled', false);
        //$("#sendApprovalId").prop('disabled', false);
    } else {
        disableModalFields();

        $("#copyEventId").show();
        $("#deleteEventId").hide();
        $("#doneId").hide();
        $("#removeEventId").show();
        if (userDetailsClass.currentUserRecord.Business_Unit__c != 'GFS' && ($("#monStatId").text() == "Edited After Approval" || $("#monStatId").text() == "Approved") ||
            timeDiff < 0
        ) {
            $("#removeEventId").hide();
        }
        $("#numOfAccId").prop('disabled', true);
        if($("#monStatId").text() == "Submitted") {
            $("#removeEventId").hide();
        }
    }

    if( userDetailsClass.currentUserRecord.Business_Unit__c == 'GFS' //GFS
    &&  ($("#monStatId").text() == "Edited After Approval" || $("#monStatId").text() == "Approved") //Approved
    &&  new Date().toString().split(" ")[0] != EDIT_APPROVED_TASKS_ON //Thursday
    ){
        disableModalFields();
        $('#removeEventId').hide();
        $('#createActivityId').prop('disabled', true).css('opacity',0.5);
        $("#deleteEventId").hide();
        $("#doneId").hide();
    }


    if(  userDetailsClass.currentUserRecord.Business_Unit__c != 'GFS'
      || $("#monStatId").text() == "Submitted"
      ||(  userDetailsClass.currentUserRecord.Business_Unit__c == 'GFS'
        && ($("#monStatId").text() == "Approved" || $("#monStatId").text() == "Edited After Approval")
        && (moment(dayClickEventClass.start).format("YYYY-MM-DD") < moment(NEXT_MONDAY).format("YYYY-MM-DD"))
        && new Date().toString().split(" ")[0] == EDIT_APPROVED_TASKS_ON
        )
      ) {
        $('#removeEventId').hide();
    }

    showTargetRevenue();

    if ($('#userpickListId').val() != userDetailsClass.currUser) {
        $("#copyEventId").hide();
        $("#removeEventId").hide();
        $("#deleteEventId").hide();
    }
    return;

}


function openConfigAccounts() {

    $("#copyEventId").css("display", "");

    var startDateVar;
    var startTimeVar;
    var endTimeVar;
    var endDateVar;


    // Check if event on day clicked is before curr date
    var dateWhole = ((dayClickEventClass.start._d).toString()).split(" ");

    var date = dateWhole[3] + "-" + getnumMonth(dateWhole[1]) + "-" + dateWhole[2];

    var currDateWhole = ((new Date()).toString()).split(" ");
    var currDateCompVar = currDateWhole[3] + "-" + getnumMonth(currDateWhole[1]) + "-" + currDateWhole[2];

    console.log(":::377:::", currDateCompVar, "::::", date);

    var timeDiff = (new Date(date + " 00:00:00") - new Date(currDateCompVar + " 00:00:00")) / 1000 / 60 / 60;

    console.log("383:::", timeDiff);

    if (timeDiff < 0) {
        $("#doneId").attr("style", "display: none;");
        $("#removeEventId").attr("style", "display: none;");
        disableModalFields();
    } else {
        $("#doneId").attr("style", "display: block;");
        $("#removeEventId").attr("style", "display: block;");
        enableModalFields();
    }

    $("#addEventModalId").modal("toggle");

    $("#accountModal").modal({
        "backdrop": "static"
    });

    $("#taskListId").empty();

    $("#clickAccNameId").text(" " + dayClickEventClass.title);

    var dateSortArr = [];

    $.each(Object.keys(dayClickEventClass.configTaskMap), function(index, value) {

        $("#taskListId").append("<div>Account Name: <a href='#' id=" + value.replace(/\s/g,'') + "Id>" + value + "</a></div>");

        var idVarClick = value.replace(/\s/g,'') + 'Id';

        $("[id='"+idVarClick+"']").click(function() {

            $("#taskListId").empty();

            $.each(Object.keys(dayClickEventClass.configTaskMap[value]), function(index2, value2) {
                console.log('--1299--', value2, '---', value);
                $("#taskListId").append("<div>Event: <a href='#' id=" + value2.replace(/\s/g,'') + "Id>" + dayClickEventClass.configTaskMap[value][value2].taskTitle + "</a></div>");

                var idVarClick2 = value2.replace(/\s/g,'') + 'Id';

                $("[id='"+idVarClick2+"']").click(function() {

                    //eventClickExtend(dayClickEventClass.configTaskMap[value][value2]);

                    CONFIG_EDIT = true;

                    clearModalErrorMessages();
                    $("#addEventModalId").modal({
                        "backdrop": "static"
                    });

                    $("#accountModal").modal("toggle");


                    $('#goToAccounts').hide();

                    if (Object.keys(dayClickEventClass.configTaskMap[value])) {
                        clickedTaskId = value2;
                        clickedName = value;
                        console.log('--clickedTask2332--', clickedTaskId);
                        eventClickCompute(dayClickEventClass.configTaskMap[value][value2], timeDiff, date);
                        $('#copyEventId').hide();
                    }

                });
            });
        });
    });
}

//---------------------------------End of calendarEvents method-------------------------------------------------------------------------


function fetchUserRecords() {

    $("#blurDivId").attr("style", "display: block");
    $('#userpickListId').find('option').remove()

    var userIdStr = getURLParameter("ownerId");
    if (!userIdStr) {
        userIdStr = null;
    }

    Visualforce.remoting.Manager.invokeAction(
        'CalendarController.fetchUserRecords',
        userIdStr,
        function(result, event) {
            if (event.status) {
                console.log('success in remoting498--');


                var resultJSON = JSON.parse(result);
                userDetailsClass = resultJSON;
                EDIT_APPROVED_TASKS_ON = resultJSON.dilVar.GFS_Editable_Day__c.substring(0, 3);

                console.log('--now see the result494--', resultJSON);
                $('#userpickListId').empty();

                $('#userpickListId').append('<option value="' + resultJSON.loggedUserId + '">' + resultJSON.loggedUserName + '</option>');

                var showUserPLStatus = 0;
                $.each(resultJSON.userList, function(index, value) {

                    ++showUserPLStatus;

                    // To select the current user on top
                    console.log("---726--", value.Id, "----", resultJSON.currUser);
                    if (value.Id == resultJSON.loggedUserId) {
                        $("#userpickListId").val(value.Id);
                        if (value.Business_Unit__c == 'GFS') {
                            userGFSClass = true;
                        } else {
                            userGFSClass = false;
                        }

                        return;
                    }

                    $('#userpickListId').append('<option value="' + value.Id + '">' + value.Name + '</option>');

                });

                $('#userpickListId').val(resultJSON.currUser);

                $('#leaveReasonSelectId').empty();
                $('#leaveReasonSelectId').append('<option value="--none--">--none--</option>');

                $.each(resultJSON.leaveReasonList, function(index, value) {

                    $('#leaveReasonSelectId').append('<option value="' + value + '">' + value + '</option>');

                });

                console.log("--see the user selected---", $("#userpickListId").val());

                if (showUserPLStatus > 1) {

                    $("#userPickDivId").attr("style", "display: block");
                    $("#monthRedId").attr("style", "");

                    if(  userDetailsClass
                      && userDetailsClass.currentUserRecord
                      && userDetailsClass.currentUserRecord.Profile
                      && (  userDetailsClass.currentUserRecord.Profile.Name == 'Executive User'
                         || userDetailsClass.currentUserRecord.Profile.Name == 'Sales User'
                         )
                      ) {
                        $('#editAsmMessage').show();
                    } else {
                        $('#editAsmMessage').hide();
                    }

                } else {
                    $('#editAsmMessage').hide();
                }


                $("#blurDivId").attr("style", "display: none");

                fetchUserData();

            } else if (event.type === 'exception') {
                $("#blurDivId").attr("style", "display: none");
            } else {
                $("#blurDivId").attr("style", "display: none");
            }
        }, {
            escape: false
        }
    );

}

function typeOfActivityCompute() {

    $('#buTypePlistId').find('option').remove();
    $('#buTypePlistId').append('<option>--none--</option>');

    showHideModalElements();

    if ("Leave" == $("#Work").val()) {
        return;
    }

    var activityTypeSet = new Set();

    _.forEach(metaDataVarClassVar, function(value, index) {


        if ($("#Work").val().toLowerCase() == value.Work_Type__c.toLowerCase()) {
            activityTypeSet.add(value.MasterLabel);
        }

    });

    for (let item of Array.from(activityTypeSet).sort()) {
        item = item.trim();
        if (!item) {
            continue;
        }
        $('#buTypePlistId').append('<option value="' + item + '">' + item + '</option>');
    }

}

//---------------------------------End of typeOfActivityCompute method-------------------------------------------------------------------------

function showHideModalElements() {

    if ("Leave" == $("#Work").val()) {

        hideSectionsForLeave("none");
        showTypeOfLeave("block");
        $('#titleDivId').hide();
        if ($('#leaveSelectId').val() == 'Half Day') {
            showTypeOfHalfLeave("block");
        } else {
            showTypeOfHalfLeave('none');
        }
        return;

    } else {
        hideSectionsForLeave("");
        showTypeOfLeave("none");
        $('#titleDivId').show();
        showTypeOfHalfLeave('none');
    }

    if ("Office work" == $("#Work").val()) {

        $("#accountDivId").attr("style", "display: none");
        $("#provinceDivId").attr("style", "display: none");
        $("#cityDivId").attr("style", "display: none");
        $("#customerGroupDivId").attr("style", "display: none");
        $("#potCustDivId").attr("style", "display: none");
        $("#bu-AccSectionId>#activityDivId").show();
        $('#numOfAccDivId').hide();

    } else {

        if (userDetailsClass.currentUserRecord.Business_Unit__c.toLowerCase() == 'GFS'.toLowerCase()) {
            $("#customerGroupDivId").attr("style", "display: block");
            $("#provinceDivId").attr("style", "display: block");
            $("#cityDivId").attr("style", "display: block");
            $("#accountDivId").attr("style", "display: block");
            $('#cityReqId').hide();
            $('#provinceReqId').hide();
            $("#bu-AccSectionId>#activityDivId").show();
            $('#numOfAccDivId').hide();
        } else if (userDetailsClass.currentUserRecord.Business_Unit__c.toLowerCase() == 'Feeds'.toLowerCase() ||
            userDetailsClass.currentUserRecord.Business_Unit__c.toLowerCase() == 'Poultry'.toLowerCase()
            //|| userDetailsClass.currentUserRecord.Meats__c
        ) {
            $("#customerGroupDivId").attr("style", "display: none");
            $("#provinceDivId").attr("style", "display: block");
            $("#cityDivId").attr("style", "display: block");
            $("#accountDivId").attr("style", "display: block");
            if ($('#numOfAccId').val().trim()) {
                $("#provinceReqId, #cityReqId").hide();
            } else {
                $("#provinceReqId, #cityReqId").show();
            }
            if ($('#numOfAccId').val()) {
                $("#bu-AccSectionId>#activityDivId").hide();
            } else {
                $("#bu-AccSectionId>#activityDivId").show();
            }
            $('#numOfAccDivId').show();
        } else {
            $("#customerGroupDivId").attr("style", "display: block");
            $("#provinceDivId").attr("style", "display: block");
            $("#cityDivId").attr("style", "display: block");
            $("#accountDivId").attr("style", "display: block");
            $('#cityReqId').hide();
            $('#provinceReqId').hide();
            $("#bu-AccSectionId>#activityDivId").hide();
            $('#numOfAccDivId').hide();
        }

        if ($("#accountId").attr("data") &&
            (UNPLANNED_ACCOUNT.toLowerCase() == ($("#accountId").attr("data")).toLowerCase() ||
                MULTIPLE_ACCOUNT.toLowerCase() == ($("#accountId").attr("data")).toLowerCase()
            )
        ) {
            $("#potCustDivId").attr("style", "");
        } else {
            $("#potCustDivId").attr("style", "display: none");
        }
    }

}

function hideSectionsForLeave(attrBute) {

    $('[data="dateTimeSectionAttr"]').attr("style", "display: " + attrBute);
    $("#bu-AccSectionId").attr("style", "display: " + attrBute);

}

function showTypeOfLeave(attrBute) {
    $("#leaveTypeId").attr("style", "display: " + attrBute);
    $("#leaveReasonDivId").attr("style", "display: " + attrBute);
}

function showTypeOfHalfLeave(attrBute) {
    $("#halfLeaveTypeId").attr("style", "display: " + attrBute);
}


function editEventBUCompute() {

    $('#buPlistId').find('option').remove();
    $('#buPlistId').append('<option>--none--</option>');

    showHideModalElements();

    if ("Leave" == $("#Work").val()) {
        return;
    }

    if ("--none--" == $("#buTypePlistId").val()) {
        $('#buPlistId').val("--none--");
        console.log("returning 409");
        return;
    }

    var taskTypeSet = new Set();

    _.forEach(metaDataVarClassVar, function(value, index) {

        if ($("#Work").val().toLowerCase() == value.Work_Type__c.toLowerCase() &&
            $("#buTypePlistId").val() &&
            $("#buTypePlistId").val().toLowerCase() == value.MasterLabel.toLowerCase()
        ) {
            if (value.Activity__c) {
                taskTypeSet.add(value.Activity__c);
            }

        }

    });

    for (let item of Array.from(taskTypeSet).sort()) {
        item = item.trim();
        if (!item) {
            continue;
        }
        $('#buPlistId').append('<option value="' + item + '">' + item + '</option>');
    }

    return taskTypeSet;

}

//---------------------------------End of editEventBUCompute method-------------------------------------------------------------------------



function fetchBUMetaData() {


    $("#blurDivId").attr("style", "display: block");

    Visualforce.remoting.Manager.invokeAction(
        'CalendarController.getBUMetaData',
        $('#userpickListId').val(),
        function(result, event) {
            if (event.status) {
                console.log('success in remoting254--');


                var resultJSON = JSON.parse(result);
                console.log('--now see the result254--', resultJSON);

                metaDataVarClassVar = JSON.parse(resultJSON.metaDataList);
                $("#blurDivId").attr("style", "display: none");

                //console.log("--189--", userListClassVar);

            } else if (event.type === 'exception') {
                $("#blurDivId").attr("style", "display: none");
            } else {
                $("#blurDivId").attr("style", "display: none");
            }
        }, {
            escape: false
        }
    );

}

//---------------------------------End of fetchBUMetaData method-------------------------------------------------------------------------

function fetchAccountRecords() {

    var accRecList = [];

    $("#accountId").autocomplete({
        minLength: 2,
        source: function(request, response) {

            if ($("#cityId").val().trim() &&
                $("#cityId").attr("data") != $("#cityId").val().trim()
            ) {
                alert("Please select the City from the dropdown when typing !");
                return;
            }

            if ($("#customerGroupId").val().trim() &&
                $("#customerGroupId").attr("data") != $("#customerGroupId").val().trim()
            ) {
                alert("Please select the Customer Group from the dropdown when typing !");
                return;
            }

            var cityIdVar;
            var countryGroupVar;

            if ($("#cityId").attr("name")) {
                cityIdVar = $("#cityId").attr("name");
            } else {
                cityIdVar = '';
            }

            if ($("#customerGroupId").attr("data")) {
                countryGroupVar = $("#customerGroupId").attr("data");
            } else {
                countryGroupVar = '';
            }

            if (($("#customerGroupId").val().trim() && $("#customerGroupId").val().toLowerCase() != 'Others'.toLowerCase()) ||
                (userDetailsClass.currentUserRecord.Business_Unit__c != 'GFS' && !($('#numOfAccId').val().trim()))
            ) {

                Visualforce.remoting.Manager.invokeAction(
                    'CalendarController.fetchAccountData',
                    $("#accountId").val(),
                    String(cityIdVar),
                    String(countryGroupVar),
                    function(result, event) {
                        if (event.status) {

                            accRecList = [];

                            $.each(JSON.parse(result), function(index, value) {

                                var record = {};
                                record.label = value.text;
                                record.value = value.text;
                                record.Id = value.value;

                                accRecList.push(record);
                            });

                            console.log("I cant believe", accRecList);
                            response(accRecList);

                        } else if (event.type === 'exception') {
                            //$("#blurDivId").attr("style", "display: none");
                        } else {
                            //$("#blurDivId").attr("style", "display: none");
                        }
                    }, {
                        escape: false
                    }
                );
            }
        },
        select: function(event, ui) {

            console.log("--yoyyo", ui, "---", ui.item.label, '-----', ui.item.Id);
            $("#accountId").attr("name", ui.item.Id);
            $("#accountId").attr("data", ui.item.label);
            //$(this).val(ui.item.label);

            if (UNPLANNED_ACCOUNT.toLowerCase() == ui.item.label.toLowerCase() || MULTIPLE_ACCOUNT.toLowerCase() == ui.item.label.toLowerCase()) {
                $("#potCustDivId").attr("style", "");
            } else {
                $("#potCustDivId").attr("style", "display: none");
            }

            var activityStr = $("#buPlistId").val() == "--none--" ?
                "" :
                $("#buPlistId").val();

            if (activityStr) {
                $("#titleId").val(ui.item.label + " - " + activityStr);
            } else {
                $("#titleId").val(ui.item.label);
            }

            $('#numOfAccId').val('');

            accountChangedClass = true;

        },
        appendTo: "#ui-widgetAcc"

    });
}

//---------------------------------End of fetchAccountRecords method-------------------------------------------------------------------------

function fetchCustomerGroupRecords() {

    var customerGroupRecList = [];

    $("#customerGroupId").autocomplete({
        minLength: 2,
        source: function(request, response) {

            Visualforce.remoting.Manager.invokeAction(
                'CalendarController.fetchCustomerRecordData',
                $("#customerGroupId").val(),
                function(result, event) {
                    if (event.status) {

                        cityRecList = [];

                        $.each(JSON.parse(result), function(index, value) {

                            var record = {};
                            record.label = value.Description__c;
                            record.value = value.Description__c;
                            record.Id = value.Id;
                            record.concat = value.Name + ' - ' + value.Description__c;

                            cityRecList.push(record);
                        });

                        console.log("I cant believe city", cityRecList);
                        response(cityRecList);

                    } else if (event.type === 'exception') {
                        //$("#blurDivId").attr("style", "display: none");
                    } else {
                        //$("#blurDivId").attr("style", "display: none");
                    }
                }, {
                    escape: false
                }
            );
        },
        select: function(event, ui) {

            console.log("--yoyyo city", ui, "---", ui.item.label, '-----', ui.item.Id);
            $("#customerGroupId").attr("name", ui.item.Id);
            $("#customerGroupId").attr("data", ui.item.label);
            $("#customerGroupId").attr("data2", ui.item.concat);
            //$(this).val(ui.item.label);

            $('#accountId').val('');
            $("#accountId").removeAttr("data");
            $("#accountId").removeAttr("name");

        },
        appendTo: "#ui-widgetCustomerGroup"

    });
}

function fetchProvinceRecords() {

    var provinceRecList = [];

    $("#provinceId").autocomplete({
        minLength: 2,
        source: function(request, response) {

            Visualforce.remoting.Manager.invokeAction(
                'CalendarController.fetchProvinceData',
                $("#provinceId").val(),
                function(result, event) {
                    if (event.status) {

                        provinceRecList = [];

                        $.each(JSON.parse(result), function(index, value) {

                            var record = {};
                            record.label = value.text;
                            record.value = value.text;
                            record.Id = value.value;

                            provinceRecList.push(record);
                        });

                        console.log("I cant believe province", provinceRecList);
                        response(provinceRecList);

                    } else if (event.type === 'exception') {
                        //$("#blurDivId").attr("style", "display: none");
                    } else {
                        //$("#blurDivId").attr("style", "display: none");
                    }
                }, {
                    escape: false
                }
            );
        },
        select: function(event, ui) {

            console.log("--yoyyo city", ui, "---", ui.item.label, '-----', ui.item.Id);
            $("#provinceId").attr("name", ui.item.Id);
            $("#provinceId").attr("data", ui.item.label);
            //$(this).val(ui.item.label);

            if (!$('#numOfAccId').val().trim()) {
                $('#accountId').val('');
                $("#accountId").removeAttr("data");
                $("#accountId").removeAttr("name");
            }
            $("#cityId").val();
            $("#cityId").removeAttr("data");
            $("#cityId").removeAttr("name");

        },
        appendTo: "#ui-widgetProvince"

    });
}

function fetchCityRecords() {

    var cityRecList = [];

    $("#cityId").autocomplete({
        minLength: 2,
        source: function(request, response) {

            Visualforce.remoting.Manager.invokeAction(
                'CalendarController.fetchCityData',
                $("#cityId").val(),
                $('#provinceId')[0].name,
                function(result, event) {
                    if (event.status) {

                        cityRecList = [];

                        $.each(JSON.parse(result), function(index, value) {

                            var record = {};
                            record.label = value.text;
                            record.value = value.text;
                            record.Id = value.value;

                            cityRecList.push(record);
                        });

                        console.log("I cant believe city", cityRecList);
                        response(cityRecList);

                    } else if (event.type === 'exception') {
                        //$("#blurDivId").attr("style", "display: none");
                    } else {
                        //$("#blurDivId").attr("style", "display: none");
                    }
                }, {
                    escape: false
                }
            );
        },
        select: function(event, ui) {

            console.log("--yoyyo city", ui, "---", ui.item.label, '-----', ui.item.Id);
            $("#cityId").attr("name", ui.item.Id);
            $("#cityId").attr("data", ui.item.label);
            //$(this).val(ui.item.label);
            if (!$('#numOfAccId').val().trim()) {
                $('#accountId').val('');
                $("#accountId").removeAttr("data");
                $("#accountId").removeAttr("name");
            }

        },
        appendTo: "#ui-widgetCity"

    });
}

//---------------------------------End of fetchAccountRecords method-------------------------------------------------------------------------



//---------------------------------End of hideSectionsForLeave method-------------------------------------------------------------------------

function alertToChangeUserData() {

    var flag = false;

    if (_.find($('#calendar').fullCalendar('clientEvents'), 'edited') ||
        (!removedEventsSfIds && removedEventsSfIds.length != 0)
    ) {
        flag = true;
    }

    if (flag) {
        $("#slctUsrPopUpId").modal()
    } else {
        fetchUserData();
    }

}

//---------------------------------End of alertToChangeUserData method-------------------------------------------------------------------------

function alertBeforeRedirect() {

    $("#slctRedirect").modal();

}

//---------------------------------End of alertBeforeRedirect method-------------------------------------------------------------------------

// To fetch the event data on change of user pick list.
function fetchUserData() {

    $("#blurDivId").attr("style", "display: block");

    var selectedMonth = getSelectedMonth();
    selectedMonth = JSON.stringify(selectedMonth);

    var selectedUserID = $("#userpickListId").val();

    console.log('--2070--', selectedUserID, '---', selectedMonth);

    var numMonthToBeRemoved = getnumMonth(getSelectedMonth().month);
    var yearToBeRemoved = getSelectedMonth().year;

    $('#calendar').fullCalendar("removeEvents", function(event) {
        if ((event.start._i.split('-')[1] == numMonthToBeRemoved ||
                event.start._i.split("T")[0].split('-')[1] == numMonthToBeRemoved
            ) &&
            (event.start._i.split('-')[0] == yearToBeRemoved ||
                event.start._i.split("T")[0].split('-')[0] == yearToBeRemoved
            )
        ) {
            return true;
        } else {
            return false;
        }
    });

    var useSegregated = false;

    if ($('button[class$="active"]').text() == 'day' ||
        $('button[class$="active"]').text() == 'week'
    ) {
        useSegregated = true;
    } else {
        useSegregated = false;
    }

    Visualforce.remoting.Manager.invokeAction(
        'CalendarController.fetchTaskData',
        selectedMonth,
        selectedUserID,
        useSegregated,
        function(result, event) {
            if (event.status) {
                console.log('success in remoting2083--', result);


                var resultJSON = '';
                try {
                    resultJSON = JSON.parse(result);
                } catch (err) {}
                console.log('--now see the result688--', resultJSON);

                $.each(resultJSON.calendarValues, function(index, value) {
                    $('#calendar').fullCalendar('renderEvent', value, stick = true);
                });

                if(resultJSON && resultJSON.asmData && resultJSON.asmData.asmRecordManager) {
                    userDetailsClass.asmRecordManager = resultJSON.asmData.asmRecordManager;
                } else {
                    userDetailsClass.asmRecordManager = null;
                }
                //userDetailsClass.asmRecordSelf = resultJSON.asmData.asmRecordSelf;

                if(  userDetailsClass
                  && $("#userpickListId").val() == userDetailsClass.loggedUserId
                  && $("#userpickListId > option").length > 1
                  && userDetailsClass.currentUserRecord
                  && userDetailsClass.currentUserRecord.Profile
                  && (  userDetailsClass.currentUserRecord.Profile.Name == 'Executive User'
                     || userDetailsClass.currentUserRecord.Profile.Name == 'Sales User'
                     )
                  ) {
                    $('#editAsmMessage').show();
                } else {
                    $('#editAsmMessage').hide();
                }

                fetchBUMetaData();

                fetchMonthStatus(selectedUserID);

                colorCompute();

                getHolidayRecords();

                computeBanner();

                $("#blurDivId").attr("style", "display: none");

            } else if (event.type === 'exception') {
                $("#blurDivId").attr("style", "display: none");
            } else {
                $("#blurDivId").attr("style", "display: none");
            }
        }, {
            escape: false
        }
    );

}

//---------------------------------End of fetchUserData method-------------------------------------------------------------------------


function fetchMonthStatus(selectedUserID) {

    $("#blurDivId").attr("style", "display: block");
    var selectedMonth = getSelectedMonth();
    selectedMonth = JSON.stringify(selectedMonth);

    Visualforce.remoting.Manager.invokeAction(
        'CalendarController.fetchMonthStatus',
        selectedMonth,
        $("#userpickListId").val(),
        function(result, event) {
            if (event.status) {
                console.log('success in remoting monthfetch--');

                var resultJSON = JSON.parse(result);
                monthJSONClass = resultJSON;
                console.log('--now see the result on hold--', resultJSON.Total_Work_Days_Completed__c, ":::", selectedMonth);

                setMonthStatusParameters(resultJSON, selectedMonth, selectedUserID);

                $("#blurDivId").attr("style", "display: none");

            } else if (event.type === 'exception') {
                $("#blurDivId").attr("style", "display: none");
            } else {
                $("#blurDivId").attr("style", "display: none");
            }
        }, {
            escape: false
        }
    );

}

function setMonthStatusParameters(resultJSON, selectedMonth, selectedUserID) {

    var selectedMonth = JSON.parse(selectedMonth);

    var totalWorkDaysComVar = (resultJSON && resultJSON[0] && resultJSON[0].Total_Work_Days_Completed__c) ?
        resultJSON[0].Total_Work_Days_Completed__c :
        "0";

    var totalWorkDaysVar = (resultJSON && resultJSON[0] && resultJSON[0].Total_Work_Days__c) ?
        resultJSON[0].Total_Work_Days__c :
        "0";

    var totalWorkHrsComVar = (resultJSON && resultJSON[0] && resultJSON[0].Total_Work_Hours_Completed__c) ?
        resultJSON[0].Total_Work_Hours_Completed__c :
        "0";

    var totalWorkHrsVar = (resultJSON && resultJSON[0] && resultJSON[0].Total_Work_Hours__c) ?
        resultJSON[0].Total_Work_Hours__c :
        "0";

    var totalOfficeWorkVar = (resultJSON && resultJSON[0] && resultJSON[0].Total_Office_Work_Hours__c) ?
        resultJSON[0].Total_Office_Work_Hours__c :
        "0";

    var totalFieldWorkVar = (resultJSON && resultJSON[0] && resultJSON[0].Total_Field_Work_Hours__c) ?
        resultJSON[0].Total_Field_Work_Hours__c :
        "0";

    var totalLeaveVar = (resultJSON && resultJSON[0] && resultJSON[0].Total_Leave_Hours__c) ?
        resultJSON[0].Total_Leave_Hours__c :
        "0";

    var totalOfficeWorkDaysVar = (resultJSON && resultJSON[0] && resultJSON[0].Office_Work_Days__c) ?
        resultJSON[0].Office_Work_Days__c :
        "0";

    var totalFieldWorkDaysVar = (resultJSON && resultJSON[0] && resultJSON[0].Field_Work_Days__c) ?
        resultJSON[0].Field_Work_Days__c :
        "0";

    var totalLeaveWorkDaysVar = (resultJSON && resultJSON[0] && resultJSON[0].Leave_Days__c) ?
        resultJSON[0].Leave_Days__c :
        "0";

    var totalTime = totalWorkDaysVar * 8;
    var workDaysPer = 0;
    var workHrsPer = 0;
    var officeDaysPer = 0;
    var officeHrsPer = 0;
    var fieldHrsPer = 0;
    var leaveHrsPer = 0;
    var outletsCovered = (resultJSON && resultJSON[0] && resultJSON[0].Outlets_Covered__c) ?
        resultJSON[0].Outlets_Covered__c :
        "0";

    if (totalWorkDaysVar && totalWorkDaysVar != 0) {
        workDaysPer = (totalWorkDaysComVar / totalWorkDaysVar) * 100;
        workHrsPer = (totalWorkHrsComVar / totalWorkHrsVar) * 100;
        officeDaysPer = (totalOfficeWorkDaysVar / totalWorkDaysVar) * 100;
        officeHrsPer = (totalOfficeWorkVar / totalTime) * 100;
        fieldDaysPer = (totalFieldWorkDaysVar / totalWorkDaysVar) * 100;
        fieldHrsPer = (totalFieldWorkVar / totalTime) * 100;
        leaveDaysPer = (totalLeaveWorkDaysVar / totalWorkDaysVar) * 100;
        leaveHrsPer = (totalLeaveVar / totalTime) * 100;
    } else {
        workDaysPer = 0;
        workHrsPer = 0;
        officeDaysPer = 0;
        officeHrsPer = 0;
        fieldDaysPer = 0;
        fieldHrsPer = 0;
        leaveDaysPer = 0;
        leaveHrsPer = 0;
    }


    var setterJSON = {};
    setterJSON = {
        totalWorkDaysComVar: totalWorkDaysComVar,
        totalWorkDaysVar: totalWorkDaysVar,
        totalWorkHrsComVar: totalWorkHrsComVar,
        totalWorkHrsVar: totalWorkHrsVar,
        workDaysPer: workDaysPer,
        workHrsPer: workHrsPer,
        totalOfficeWorkDaysVar: totalOfficeWorkDaysVar,
        totalOfficeWorkVar: totalOfficeWorkVar,
        officeDaysPer: officeDaysPer,
        officeHrsPer: officeHrsPer,
        totalFieldWorkDaysVar: totalFieldWorkDaysVar,
        totalFieldWorkVar: totalFieldWorkVar,
        fieldDaysPer: fieldDaysPer,
        fieldHrsPer: fieldHrsPer,
        totalLeaveWorkDaysVar: totalLeaveWorkDaysVar,
        totalLeaveVar: totalLeaveVar,
        leaveDaysPer: leaveDaysPer,
        leaveHrsPer: leaveHrsPer,
        outletsCovered: outletsCovered
    };

    setMonthDivValues(setterJSON);

    var dateVar = new Date().toString();
    var timeDiff = (new Date(dateVar.split(" ")[3] + "-" + getnumMonth(dateVar.split(" ")[1]) + "-01" + " 00:00:00") -
        new Date(getSelectedMonth().year + "-" + getnumMonth(getSelectedMonth().month) + "-01" + " 00:00:00")) / 1000 / 60 / 60;

    if (resultJSON &&
        resultJSON[0]
    ) {
        if ((userDetailsClass.currentUserRecord.Business_Unit__c.toLowerCase() == 'Feeds'.toLowerCase() ||
                userDetailsClass.currentUserRecord.Business_Unit__c.toLowerCase() == 'Poultry'.toLowerCase()
                //|| userDetailsClass.currentUserRecord.Meats__c
            ) &&
            resultJSON[0].Work_Days_Completed__c
        ) {

            $("#sendApprovalId").removeAttr("disabled").css('opacity', 1);
            console.log('Showing approval');

        } else if (resultJSON[0].Work_Days_Completed__c
            //&& resultJSON[0].Work_Hours_Completed__c
            &&
            userDetailsClass.currentUserRecord.Business_Unit__c.toLowerCase() == 'GFS'.toLowerCase()
        ) {
            $("#sendApprovalId").removeAttr("disabled").css('opacity', 1);
            console.log('Showing approval');
        } else {
            $('#sendApprovalId').prop('disabled', true).css('opacity', 0.5);
            console.log('Disbled approval button-----', resultJSON[0]);
        }

        if (resultJSON[0].Status__c) {
            $("#monStatId").text(resultJSON[0].Status__c);
            if ("Submitted" == resultJSON[0].Status__c ||
                "Approved" == resultJSON[0].Status__c ||
                $('#userpickListId').val() != userDetailsClass.currUser
            ) {
                if (new Date().toString().split(" ")[0] != EDIT_APPROVED_TASKS_ON) {
                    $('#sendApprovalId').prop('disabled', true).css('opacity', 0.5);
                }
                //Cancel Functionality
                $("#removeEventId").show();

                //$("#activityDivId").attr("style", "display: none");
                $('#createActivityId').prop('disabled', true).css('opacity', 0.5);
                $('#copyTasksId').prop('disabled', true).css('opacity', 0.5);
                if(EDIT_APPROVED_TASKS_ON == new Date().toString().split(" ")[0] && userDetailsClass.currentUserRecord.Business_Unit__c == 'GFS') {
                    $('#copyTasksId').removeAttr("disabled").css('opacity', 1);
                    $('#createActivityId').removeAttr('disabled').css('opacity', 1);
                }
            } else {
                //$("#activityDivId").attr("style", "");

                if (timeDiff <= 0) {
                    $("#createActivityId").removeAttr("disabled").css('opacity', 1);
                    $("#copyTasksId").removeAttr("disabled").css('opacity', 1);
                } else {
                    $('#createActivityId').prop('disabled', true).css('opacity', 0.5);
                    $("#copyTasksId").prop('disabled', true).css('opacity', 0.5);
                }
                //$("#copyTasksId").removeAttr("disabled").css('opacity',1);
            }
            console.log("--1276--");
        } else {
            $("#monStatId").text("Not Submitted");
            console.log("--1279--");
        }

        if (new Date().toString().split(" ")[0] != EDIT_APPROVED_TASKS_ON &&
            ($("#monStatId").text() == "Approved" || $("#monStatId").text() == "Edited After Approval") &&
            userDetailsClass.currentUserRecord.Business_Unit__c.toLowerCase() == 'GFS'.toLowerCase()) {
            $('#copyTasksId').prop('disabled', true).css('opacity', 0.5);
            $('#createActivityId').prop('disabled', true).css('opacity', 0.5);
        } else if (($("#monStatId").text() == "Approved" || $("#monStatId").text() == "Edited After Approval" || $("#monStatId").text() == "Submitted") &&
            userDetailsClass.currentUserRecord.Business_Unit__c.toLowerCase() != 'GFS'.toLowerCase()) {
            $('#copyTasksId').prop('disabled', true).css('opacity', 0.5);
            $('#createActivityId').prop('disabled', true).css('opacity', 0.5);
        } else {
            $('#copyTasksId').prop('disabled', false).css('opacity', 1);
            $('#createActivityId').prop('disabled', false).css('opacity', 1);
        }

        if ($("#monStatId").text() == "Submitted") {
            $('#copyTasksId').prop('disabled', true).css('opacity', 0.5);
            $('#createActivityId').prop('disabled', true).css('opacity', 0.5);
        }

        if($('#userpickListId').val() != userDetailsClass.currUser) {
            $('#createActivityId').prop('disabled', true).css('opacity', 0.5);
            $("#copyTasksId").prop('disabled', true).css('opacity', 0.5);
        }

    } else {
        $('#sendApprovalId').prop('disabled', true).css('opacity', 0.5);
        $("#monStatId").text("Not Submitted");
        if ($('#userpickListId').val() != userDetailsClass.currUser) {
            $('#createActivityId').prop('disabled', true).css('opacity', 0.5);
            $('#copyTasksId').prop('disabled', true).css('opacity', 0.5);
        } else {
            if (timeDiff <= 0) {
                $("#createActivityId").removeAttr("disabled").css('opacity', 1);
                $("#copyTasksId").removeAttr("disabled").css('opacity', 1);
            } else {
                $('#createActivityId').prop('disabled', true).css('opacity', 0.5);
                $('#copyTasksId').prop('disabled', true).css('opacity', 0.5);
            }
        }
        //$('#copyTasksId').prop('disabled',true).css('opacity',0.5);
        console.log('Disbled approval button');
    }
}

function setMonthDivValues(setterJSON) {

    if (setterJSON) {

        ////////////////////////////////////Month/////////////////////////////////////////////////////
        if (setterJSON.totalWorkDaysComVar && setterJSON.totalWorkDaysVar) {
            $("#workDaysId").html(setterJSON.totalWorkDaysComVar + "/" + setterJSON.totalWorkDaysVar);
        } else {
            $("#workDaysId").html("");
        }

        if (setterJSON.totalWorkHrsComVar && setterJSON.totalWorkHrsVar) {
            $("#workHoursId").html(setterJSON.totalWorkHrsComVar + "/" + setterJSON.totalWorkHrsVar);
        } else {
            $("#workHoursId").html("");
        }

        if (setterJSON.workDaysPer) {
            $("#perDaysId").text((setterJSON.workDaysPer).toFixed(2) + " %");
        } else {
            $("#perDaysId").text("0 %");
        }

        if (setterJSON.workHrsPer) {
            $("#perHoursId").text((setterJSON.workHrsPer).toFixed(2) + " %");
        } else {
            $("#perHoursId").text("0 %");
        }

        //////////////////////////////////////////Office///////////////////////////////////////////////

        if (setterJSON.totalOfficeWorkDaysVar) {
            $("#officeDaysId").text((setterJSON.totalOfficeWorkDaysVar));
        } else {
            $("#officeDaysId").text("");
        }

        if (setterJSON.totalOfficeWorkVar) {
            $("#officeHoursId").text((setterJSON.totalOfficeWorkVar));
        } else {
            $("#officeHoursId").text("");
        }

        if (setterJSON.officeDaysPer) {
            $("#officePerDaysId").text((setterJSON.officeDaysPer).toFixed(2) + " %");
        } else {
            $("#officePerDaysId").text("0 %");
        }

        if (setterJSON.officeHrsPer) {
            $("#officePerHoursId").text((setterJSON.officeHrsPer).toFixed(2) + " %");
        } else {
            $("#officePerHoursId").text("0 %");
        }


        //////////////////////////////////////////Field///////////////////////////////////////////////


        if (setterJSON.totalFieldWorkDaysVar) {
            $("#fieldDaysId").text((setterJSON.totalFieldWorkDaysVar));
        } else {
            $("#fieldDaysId").text("");
        }

        if (setterJSON.totalFieldWorkVar) {
            $("#fieldHoursId").text((setterJSON.totalFieldWorkVar));
        } else {
            $("#fieldHoursId").text("");
        }

        if (setterJSON.fieldDaysPer) {
            $("#fieldPerDaysId").text((setterJSON.fieldDaysPer).toFixed(2) + " %");
        } else {
            $("#fieldPerDaysId").text("0 %");
        }

        if (setterJSON.fieldHrsPer) {
            $("#fieldPerHoursId").text((setterJSON.fieldHrsPer).toFixed(2) + " %");
        } else {
            $("#fieldPerHoursId").text("0 %");
        }

        if (setterJSON.outletsCovered) {
            $("#outletsCoveredId").text((setterJSON.outletsCovered));
        } else {
            $("#outletsCoveredId").text('0');
        }

        if (userDetailsClass.currentUserRecord.Business_Unit__c == 'Feeds' || userDetailsClass.currentUserRecord.Business_Unit__c == 'Poultry') {
            $('#outletDivId').show();
        } else {
            $('#outletDivId').hide();
        }

        ///////////////////////////////////////Leave//////////////////////////////////////////////


        if (setterJSON.totalLeaveWorkDaysVar) {
            $("#leaveDaysId").text((setterJSON.totalLeaveWorkDaysVar));
        } else {
            $("#leaveDaysId").text("");
        }

        if (setterJSON.totalLeaveVar) {
            $("#leaveHoursId").text((setterJSON.totalLeaveVar));
        } else {
            $("#leaveHoursId").text("");
        }

        if (setterJSON.leaveDaysPer) {
            $("#leavePerDaysId").text((setterJSON.leaveDaysPer).toFixed(2) + " %");
        } else {
            $("#leavePerDaysId").text("0 %");
        }

        if (setterJSON.leaveHrsPer) {
            $("#leavePerHoursId").text((setterJSON.leaveHrsPer).toFixed(2) + " %");
        } else {
            $("#leavePerHoursId").text("0 %");
        }

    }
}

function getHolidayRecords() {

    $("#blurDivId").attr("style", "display: block");
    var selectedMonth = getSelectedMonth();
    selectedMonth = JSON.stringify(selectedMonth);

    Visualforce.remoting.Manager.invokeAction(
        'CalendarController.fetchHolidays',
        selectedMonth,
        function(result, event) {
            if (event.status) {
                console.log('success in remoting monthfetch--');

                //holidayList = JSON.parse(result);
                console.log('--1454--', JSON.parse(result));
                holidayList = [];

                $.each(JSON.parse(result), function(index, value) {

                    holidayList.push(value.Date_of_Holiday__c);
                    $('#' + value.Date_of_Holiday__c + 'hol').remove();

                    if (value.Reason_of_Holiday__c) {
                        $("td[data-date=" + value.Date_of_Holiday__c + "][class*='fc-day-top']").append('<a class="holidayText" id="' +
                            value.Date_of_Holiday__c +
                            'hol"><b>' +
                            value.Reason_of_Holiday__c +
                            '</b></a>'
                        );
                    }

                    $("td[data-date|=" + value.Date_of_Holiday__c + "]").attr("style", "background-color: darkgrey");

                });

                $("#blurDivId").attr("style", "display: none");

            } else if (event.type === 'exception') {
                $("#blurDivId").attr("style", "display: none");
            } else {
                $("#blurDivId").attr("style", "display: none");
            }
        }, {
            escape: false
        }
    );

}


function checkAllDay(elementRef) {

    if (elementRef.checked) {
        $("#datesSectionId").attr("style", "display: none");
    } else {
        $("#datesSectionId").attr("style", "display: block");
    }

}


//---------------------------------End of checkAllDay method-------------------------------------------------------------------------

function getnumMonth(strMonth) {

    if (!strMonth) {
        return "01";
    }

    var numMonth = "01";

    switch (strMonth) {

        case "Jan":
            numMonth = "01";
            break;

        case "Feb":
            numMonth = "02";
            break;

        case "Mar":
            numMonth = "03";
            break;

        case "Apr":
            numMonth = "04";
            break;

        case "May":
            numMonth = "05";
            break;

        case "Jun":
            numMonth = "06";
            break;

        case "Jul":
            numMonth = "07";
            break;

        case "Aug":
            numMonth = "08";
            break;

        case "Sep":
            numMonth = "09";
            break;

        case "Oct":
            numMonth = "10";
            break;

        case "Nov":
            numMonth = "11";
            break;

        case "Dec":
            numMonth = "12";
            break;

        default:
            numMonth = "01";
    }

    return numMonth;

}

//---------------------------------------------------End of getnumMonth method-----------------------------------------------------


function createEventData(refEventPar) {

    if (refEventPar) {

        var workOptionVar = refEventPar.workOption;

        var eventName = refEventPar.taskTitle;
        console.log('--see--', eventName);

        if (!eventName) {
            if ("Leave" == workOptionVar) {
                eventName = 'Leave';
            } else {
                copyTaskErrorObj.push(refEventPar.start.split("T")[0] + "---" + Math.random() + "-----" + "Enter Valid Task Title");
                return true;
            }
        }

        var startDateVar             = refEventPar.start.split("T")[0];
        var startTimeVar             = refEventPar.start.split("T")[1];
        var endTimeVar               = refEventPar.end.split("T")[1];
        var accountName              = refEventPar.custGroupSetName;
        var accountId                = refEventPar.accountId;
        var taskTypeOptionVar        = refEventPar.taskTypeOption;
        var actTypeOptionVar         = refEventPar.activityTypeOption;
        var userId                   = refEventPar.userId;
        var potCustVar               = refEventPar.potCustDetails;
        var descriptionVar           = refEventPar.description;
        var leaveTypeVar             = refEventPar.halfDay;
        var leaveReasonVar           = refEventPar.leaveReason;
        var halfleaveTypeVar         = refEventPar.halfDayType;
        var custGroupSetName         = refEventPar.custGroupSetName;
        var numOfAccounts            = refEventPar.numOfAccounts;
        var targetRevenue            = refEventPar.targetRevenue;
        var custGroupId              = refEventPar.customerGroup;
        var customerGroupDescription = refEventPar.customerGroupDescription;
        var cityId                   = refEventPar.city;
        var provinceId               = refEventPar.province;

        console.log("929--", actTypeOptionVar);

        var startDate;
        var endDate;
        var eventType = "";

        if ("Leave" == workOptionVar) {
            if (leaveTypeVar) {
                if (leaveTypeVar == 'Full Day') {
                    startDate = startDateVar + 'T' + '09:00:00';
                    endDate = startDateVar + 'T' + '17:00:00';
                } else if (leaveTypeVar == 'Half Day') {

                    if (halfleaveTypeVar) {

                        if (halfleaveTypeVar == 'Morning') {
                            startDate = startDateVar + 'T' + '09:00:00';
                            endDate = startDateVar + 'T' + '14:00:00';
                        } else {
                            startDate = startDateVar + 'T' + '14:00:00';
                            endDate = startDateVar + 'T' + '18:00:00';
                        }

                    }

                }
            }
        } else if (startTimeVar) {

            startDate = startDateVar + 'T' + startTimeVar;
            endDate = startDateVar + 'T' + endTimeVar;
        }

        console.log('--' + startDate + '--' + endDate);


        if (!(moment(startDate, "YYYY-MM-DDTHH:mm:ss", true).isValid())) {
            copyTaskErrorObj.push(refEventPar.start.split("T")[0] + "---" + eventName + "-----" + "Enter Valid Start Date / Time");
            return true;
        }

        if (!(moment(endDate, "YYYY-MM-DDTHH:mm:ss", true).isValid())) {
            copyTaskErrorObj.push(refEventPar.start.split("T")[0] + "---" + eventName + "-----" + "Enter Valid End Date / Time");
            return true;
        }

        if (!moment(endDate).isAfter(startDate)) {
            copyTaskErrorObj.push(refEventPar.start.split("T")[0] + "---" + eventName + "-----" + "Enter Start Time less than End Time");
            return true;
        }

        if(  $('#numOfAccId').val().trim()
          && $('#numOfAccId').val().trim() < 2
          ) {
            alert('Multiple accounts cannot be less than 2');
            $('#numOfAccId').val('');
            numOfAccIdKeyUp();
            return true;
        }


        var currDateWhole = ((new Date()).toString()).split(" ");
        var currDateCompVar = currDateWhole[3] + "-" + getnumMonth(currDateWhole[1]) + "-" + currDateWhole[2];
        var timeDiff = (new Date(startDateVar + " 00:00:00") - new Date(currDateCompVar + " 00:00:00")) / 1000 / 60 / 60;

        if (timeDiff < 0) {

            copyTaskErrorObj.push(refEventPar.start.split("T")[0] + "---" + eventName + "-----" + "Start Date cannot be less than today !!");

            return true;
        }

        if ("Leave" == workOptionVar) {

            accountId = "Leave";
            accountName = "Leave";
            eventType = "Leave";

            console.log("--1590--", startDateVar);

            if (leaveTypeVar == "Full Day") {
                if (removeAllEvents(startDateVar, null)) {
                    copyTaskErrorObj.push(refEventPar.start.split("T")[0] + "---" + eventName + "-----" + "Other Tasks present on this day !!");
                    return true;
                }
            } else {

                if (removeLeaveFromEvents(startDateVar, null)) {
                    copyTaskErrorObj.push(refEventPar.start.split("T")[0] + "---" + eventName + "-----" + "Leaves present on this day !!");
                    return true;
                }

                if (checkOverlapTime(startDate, endDate, null)) {
                    copyTaskErrorObj.push(refEventPar.start.split("T")[0] +
                        "---" + eventName + "-----" + "Time slot already booked. Please change time slot to proceed !!"
                    );
                    return true;
                }

            }

        } else if ("Office work" == workOptionVar) {

            accountId = ++uiTaskId;
            accountName = taskTypeOptionVar;
            eventType = "Office work";

            if (removeLeaveFromEvents(startDateVar, null)) {
                copyTaskErrorObj.push(refEventPar.start.split("T")[0] + "---" + eventName + "-----" + "Leaves present on this day !!");
                return true;
            }

            if (checkOverlapTime(startDate, endDate, null)) {
                copyTaskErrorObj.push(refEventPar.start.split("T")[0] + "---" + eventName + "-----" + "Time slot already booked. Please change time slot to proceed !!");
                return true;
            }

        } else {

            if (userDetailsClass.currentUserRecord.Business_Unit__c.toLowerCase() == 'GFS'.toLowerCase()) {
                eventType = "acc";
            } else {
                eventType = 'accConfig';
            }

            if (removeLeaveFromEvents(startDateVar, null)) {
                copyTaskErrorObj.push(refEventPar.start.split("T")[0] + "---" + eventName + "-----" + "Leaves present on this day !!");
                return true;
            }

            if (checkOverlapTime(startDate, endDate, null)) {
                copyTaskErrorObj.push(refEventPar.start.split("T")[0] + "---" + eventName + "-----" + "Time slot already booked. Please change time slot to proceed !!");
                return true;
            }
        }

        var addedNewElement = false;

        $.each($('#calendar').fullCalendar("clientEvents", function(event) {
            if ((event.start._i == startDateVar || event.start._i.split("T")[0] == startDateVar) &&
                event.accId == accountId
            ) {
                return true;
            } else {
                return false;
            }
        }), function(index, value) {

            value.taskmap[++uiTaskId] = {
                title: accountName,
                start: startDate,
                end: endDate,
                userId: userId,
                edited: true,
                color: "#0070d2",
                accountId: accountId,
                accountName: accountName,
                potCustDetails: potCustVar,
                workOption: workOptionVar,
                taskTypeOption: taskTypeOptionVar,
                activityTypeOption: actTypeOptionVar,
                taskTitle: eventName,
                eventType: eventType,
                description: descriptionVar,
                halfDay: leaveTypeVar,
                leaveReason: leaveReasonVar,
                halfDayType: halfleaveTypeVar,
                province: provinceId,
                city: cityId,
                customerGroup: custGroupId,
                custGroupSetName: customerGroupDescription,
                numOfAccounts: numOfAccounts,
                targetRevenue: targetRevenue
            };
            console.log("val--", value.taskmap);
            addedNewElement = true;
            return false;

        });

        if (!addedNewElement) {

            var innertask = {};
            innertask[++uiTaskId] = {
                title: accountName,
                start: startDate,
                end: endDate,
                userId: userId,
                edited: true,
                color: "#0070d2",
                accountId: accountId,
                accountName: accountName,
                potCustDetails: potCustVar,
                workOption: workOptionVar,
                taskTypeOption: taskTypeOptionVar,
                activityTypeOption: actTypeOptionVar,
                taskTitle: eventName,
                eventType: eventType,
                description: descriptionVar,
                halfDay: leaveTypeVar,
                leaveReason: leaveReasonVar,
                halfDayType: halfleaveTypeVar,
                province: provinceId,
                city: cityId,
                customerGroup: custGroupId,
                custGroupSetName: customerGroupDescription,
                numOfAccounts: numOfAccounts,
                targetRevenue: targetRevenue
            };

            $("#calendar").fullCalendar("renderEvent", {
                accId: accountId,
                start: startDateVar,
                taskmap: innertask,
                title: accountName,
                eventType: eventType,
                color: '#378400',
                textColor: "#ffffff"
            }, stick = true);
        }

        return false;

    } else { /////////////////////////////////////////////////////////////////////////////////////////////////////////////// main else

        var eventName = $("#titleId").val();
        var workOptionVar = $("#Work").val();
        console.log('--see--', eventName);

        if (!(eventName.trim())) {
            if ("Leave" == workOptionVar) {
                eventName = "Leave";
            } else {
                alert("Enter Valid Task Title");
                return true;
            }

        }

        if (eventName.length > 250) {
            alert("Title too long");
            return true;
        }

        var startDateVar = moment($("#startDateId").val()).format('YYYY-MM-DD');
        var startTimeVar = $("#startId").val();
        var endTimeVar = $("#endId").val();

        var accountName = $("#accountId").val();
        var accountSetName = $("#accountId").attr("data");
        var accountId = "";

        var custGroupName = $("#customerGroupId").val();
        var custGroupSetName = $("#customerGroupId").attr("data");
        var custGroupId = "";

        var provinceName = $("#provinceId").val();
        var provinceSetName = $("#provinceId").attr("data");
        var provinceId = "";

        var cityName = $("#cityId").val();
        var citySetName = $("#cityId").attr("data");
        var cityId = "";

        var potCustVar = $("#potCustId").val();
        var descriptionVar = $("#commentId").val();
        var leaveTypeVar = $('#leaveSelectId').val();
        var leaveReasonVar = $('#leaveReasonSelectId').val();
        var halfleaveTypeVar = $('#halfLeaveSelectId').val();
        var numOfAccounts = $('#numOfAccId').val();
        var targetRevenue = $('#targetRevenueId').val();

        if (accountName) {
            accountId = $("#accountId").get(0).name;
        } else {
            $("#accountId").attr("name", "");
        }

        if (custGroupName) {
            custGroupId = $("#customerGroupId").get(0).name;
        } else {
            $("#customerGroupId").attr("name", "");
        }

        if (provinceName) {
            provinceId = $("#provinceId").get(0).name;
        } else {
            $("#provinceId").attr("name", "");
        }

        if (cityName) {
            cityId = $("#cityId").get(0).name;
        } else {
            $("#cityId").attr("name", "");
        }

        var taskTypeOptionVar = $('#buPlistId').val();
        var actTypeOptionVar = $('#buTypePlistId').val();
        var userId = _.find($("select[id$=userpickListId]").children(), 'selected').value;

        console.log("929--", actTypeOptionVar);

        var startDate;
        var endDate;
        var eventType = "";

        if ("Leave" == workOptionVar) {
            if (leaveTypeVar) {
                if (leaveTypeVar == 'Full Day') {
                    startDate = startDateVar + 'T' + '09:00:00';
                    endDate = startDateVar + 'T' + '17:00:00';
                } else if (leaveTypeVar == 'Half Day') {

                    if (halfleaveTypeVar) {

                        if (halfleaveTypeVar == 'Morning') {
                            startDate = startDateVar + 'T' + '09:00:00';
                            endDate = startDateVar + 'T' + '14:00:00';
                        } else {
                            startDate = startDateVar + 'T' + '14:00:00';
                            endDate = startDateVar + 'T' + '18:00:00';
                        }

                    }

                }
            }
        } else if (startTimeVar) {

            if ("Field work" == workOptionVar) {

                if (accountChangedClass) {

                    if (accountId &&
                        accountName &&
                        accountName == accountSetName &&
                        (UNPLANNED_ACCOUNT.toLowerCase() == accountName.toLowerCase() || MULTIPLE_ACCOUNT.toLowerCase() == accountName.toLowerCase())
                    ) {} else {

                        if (userDetailsClass.currentUserRecord.Business_Unit__c.toLowerCase() == 'Feeds'.toLowerCase() ||
                            userDetailsClass.currentUserRecord.Business_Unit__c.toLowerCase() == 'Poultry'.toLowerCase()
                            //|| userDetailsClass.currentUserRecord.Meats__c
                        ) { // Agro

                            if (!$('#numOfAccId').val().trim()) {
                                if (!provinceId ||
                                    !provinceName ||
                                    provinceName != provinceSetName
                                ) {
                                    alert("Please select the Province from the dropdown when typing !!");
                                    return true;
                                }

                                if (!cityId ||
                                    !cityName ||
                                    cityName != citySetName
                                ) {
                                    alert("Please select the City from the dropdown when typing !!");
                                    return true;
                                }
                            }

                        } else if (userDetailsClass.currentUserRecord.Business_Unit__c.toLowerCase() == 'GFS'.toLowerCase()) { // GFS

                            if (!custGroupId ||
                                !custGroupName ||
                                custGroupName != custGroupSetName
                            ) {
                                alert("Please select the Customer Group from the dropdown when typing !!");
                                return true;
                            }

                        }
                    }
                }

                if ((userDetailsClass.currentUserRecord.Business_Unit__c.toLowerCase() == 'Feeds'.toLowerCase() ||
                        userDetailsClass.currentUserRecord.Business_Unit__c.toLowerCase() == 'Poultry'.toLowerCase()
                        //|| userDetailsClass.currentUserRecord.Meats__c
                    )) {
                    if (!accountName) {

                        alert("Please add Account !!");
                        return true;
                    }

                    if (accountName &&
                        !numOfAccounts &&
                        (!accountId ||
                            accountName != accountSetName
                        )
                    ) {

                        alert("Please select the Account from the dropdown when typing !!");
                        return;
                    }

                } else if (userDetailsClass.currentUserRecord.Business_Unit__c.toLowerCase() == 'GFS'.toLowerCase() &&
                    custGroupSetName.toLowerCase() != CUSTGROUP_OTHERS.toLowerCase() &&
                    accountName &&
                    (!accountId ||
                        accountName != accountSetName
                    )
                ) {
                    alert("Please select the Account from the dropdown when typing !!");
                    return true;
                }
            }

            if (userDetailsClass.currentUserRecord.Business_Unit__c.toLowerCase() == 'GFS'.toLowerCase() ||
                ((userDetailsClass.currentUserRecord.Business_Unit__c.toLowerCase() == 'Feeds'.toLowerCase() ||
                        userDetailsClass.currentUserRecord.Business_Unit__c.toLowerCase() == 'Poultry'.toLowerCase()
                    )
                    //&& "Office work" == workOptionVar
                )
            ) { // If the user is GFS OR the the user is (AGRO with Field Work)
                if ("Office work" == workOptionVar || !numOfAccounts) {
                    if (!actTypeOptionVar ||
                        "--none--" == actTypeOptionVar
                    ) {
                        alert("Please select the Type of Activity !!");
                        return true;
                    }

                    if (!taskTypeOptionVar ||
                        "--none--" == taskTypeOptionVar
                    ) {
                        alert("Please select the Activity !!");
                        return true;
                    }
                }
            }

            try {
                startTimeVar = convert12to24Hrs(startTimeVar)
            } catch (err) {
                alert("Enter Valid Start Date / Time");
                return true;
            }

            try {
                endTimeVar = convert12to24Hrs(endTimeVar)
            } catch (err) {
                alert("Enter Valid End Date / Time");
                return true;
            }


            startDate = startDateVar + 'T' + startTimeVar;
            endDate = startDateVar + 'T' + endTimeVar;
        }

        console.log('--' + startDate + '--' + endDate);


        if (!(moment(startDate, "YYYY-MM-DDTHH:mm:ss", true).isValid())) {
            alert("Enter Valid Start Date / Time");
            return true;
        }

        if (!(moment(endDate, "YYYY-MM-DDTHH:mm:ss", true).isValid())) {
            alert("Enter Valid End Date / Time");
            return true;
        }

        if (!moment(endDate).isAfter(startDate)) {
            alert("Enter Start Time less than End Time");
            return true;
        }

        if(  $('#numOfAccId').val().trim()
          && $('#numOfAccId').val().trim() < 2
          ) {
            alert('Multiple accounts cannot be less than 2');
            $('#numOfAccId').val('');
            numOfAccIdKeyUp();
            return true;
        }


        var currDateWhole = ((new Date()).toString()).split(" ");
        var currDateCompVar = currDateWhole[3] + "-" + getnumMonth(currDateWhole[1]) + "-" + currDateWhole[2];
        var timeDiff = (new Date(startDateVar + " 00:00:00") - new Date(currDateCompVar + " 00:00:00")) / 1000 / 60 / 60;

        if (timeDiff < 0) {

            alert("Start Date cannot be less than today !!");
            return true;
        }

        if ("Leave" == workOptionVar) {

            if (leaveTypeVar == 'Full Day') {
                accountId = "Leave";
                accountName = "Leave";
            } else if (leaveTypeVar == 'Half Day') {
                if (halfleaveTypeVar == 'Morning') {
                    accountId = "Morning Half Day Leave";
                    accountName = "Morning Half Day Leave";
                } else {
                    accountId = "Afternoon Half Day Leave";
                    accountName = "Afternoon Half Day Leave";
                }
            }

            //leaveTimeDiff = 8;
            eventType = "Leave";

            console.log("--1590--", startDateVar);

            if (leaveTypeVar == "Full Day") {
                if (removeAllEvents(startDateVar, true)) {
                    return true;
                }
            } else {

                if (removeLeaveFromEvents(startDateVar, true)) {
                    return true;
                }

                if (checkOverlapTime(startDate, endDate, null)) {
                    alert("Time slot already booked. Please change time slot to proceed !!");
                    return true;
                }

            }

        } else if ("Office work" == workOptionVar) {

            accountId = ++uiTaskId;
            accountName = taskTypeOptionVar;
            eventType = "Office work";

            //officeTimeDiff = ( new Date("1970-1-1 " + endTimeVar) - new Date("1970-1-1 " + startTimeVar) ) / 1000 / 60 / 60;

            if (removeLeaveFromEvents(startDateVar, true)) {
                return true;
            }

            if (checkOverlapTime(startDate, endDate, null)) {
                alert("Time slot already booked. Please change time slot to proceed !!");
                return true;
            }

        } else {
            //fieldTimeDiff = ( new Date("1970-1-1 " + endTimeVar) - new Date("1970-1-1 " + startTimeVar) ) / 1000 / 60 / 60;
            if (userDetailsClass.currentUserRecord.Business_Unit__c.toLowerCase() == 'GFS'.toLowerCase()) {
                eventType = "acc";
            } else {
                eventType = 'accConfig';
            }

            if (removeLeaveFromEvents(startDateVar, true)) {
                return true;
            }

            if (checkOverlapTime(startDate, endDate, null)) {
                alert("Time slot already booked. Please change time slot to proceed !!");
                return true;
            }
        }

        var addedNewElement = false;

        $.each($('#calendar').fullCalendar("clientEvents", function(event) {
            if ((event.start._i == startDateVar ||
                    event.start._i.split("T")[0] == startDateVar
                ) &&
                event.accId == accountId
            ) {
                return true;
            } else {
                return false;
            }
        }), function(index, value) {

            value.taskmap[++uiTaskId] = {
                title: accountName,
                start: startDate,
                end: endDate,
                userId: userId,
                edited: true,
                color: "#0070d2",
                accountId: accountId,
                accountName: accountName,
                potCustDetails: potCustVar,
                workOption: workOptionVar,
                taskTypeOption: taskTypeOptionVar,
                activityTypeOption: actTypeOptionVar,
                taskTitle: eventName,
                eventType: eventType,
                description: descriptionVar,
                halfDay: leaveTypeVar,
                leaveReason: leaveReasonVar,
                halfDayType: halfleaveTypeVar,
                province: provinceId,
                city: cityId,
                customerGroup: custGroupId,
                custGroupSetName: custGroupSetName,
                numOfAccounts: numOfAccounts,
                targetRevenue: targetRevenue
            };
            console.log("val--", value.taskmap);
            addedNewElement = true;
            return false;

        });

        if (!addedNewElement) {

            var innertask = {};
            innertask[++uiTaskId] = {
                title: accountName,
                start: startDate,
                end: endDate,
                userId: userId,
                edited: true,
                color: "#0070d2",
                accountId: accountId,
                accountName: accountName,
                potCustDetails: potCustVar,
                workOption: workOptionVar,
                taskTypeOption: taskTypeOptionVar,
                activityTypeOption: actTypeOptionVar,
                taskTitle: eventName,
                eventType: eventType,
                description: descriptionVar,
                halfDay: leaveTypeVar,
                leaveReason: leaveReasonVar,
                halfDayType: halfleaveTypeVar,
                province: provinceId,
                city: cityId,
                customerGroup: custGroupId,
                custGroupSetName: custGroupSetName,
                numOfAccounts: numOfAccounts,
                targetRevenue: targetRevenue
            };

            $("#calendar").fullCalendar("renderEvent", {
                accId: accountId,
                start: startDateVar,
                taskmap: innertask,
                title: accountName,
                eventType: eventType
            }, stick = true);
        }

    }

    var daysRemovedCount = removeEventsWithNoTasks();
    daysRemovedCount = daysRemovedCount * -1;

    $("#finalDoneId").click();
    return false;

}


//---------------------------------------end of createEventData method-----------------------------------------------------

function editEventData(deleteVar, newDateVar) {

    var newEventStr = $("#titleId").val();
    var workOptionVar = $("#Work").val();

    if (!(newEventStr.trim())) {
        if ("Leave" == workOptionVar) {
            newEventStr = "Leave"
        } else {
            alert("Enter Valid Task Title");
            return true;
        }
    }

    if (newEventStr.length > 250) {
        alert("Title too long");
        return true;
    }

    console.log('--dayClickEventClass--', dayClickEventClass);

    var fullDate = ((dayClickEventClass.start._d.toString()).split(" "));

    var dateStr = fullDate[3] + "-" + getnumMonth(fullDate[1]) + "-" + fullDate[2];

    var startDateVar = newDateVar ? moment(newDateVar).format('YYYY-MM-DD') : moment($("#startDateId").val()).format('YYYY-MM-DD');
    var startTimeVar = $("#startId").val();
    var endTimeVar = $("#endId").val();

    var accountSetName = $("#accountId").attr("data");
    var accountName = $("#accountId").val();
    var accountId = "";

    var custGroupName = $("#customerGroupId").val();
    var custGroupSetName = $("#customerGroupId").attr("data");
    var custGroupId = "";

    var provinceName = $("#provinceId").val();
    var provinceSetName = $("#provinceId").attr("data");
    var provinceId = "";

    var cityName = $("#cityId").val();
    var citySetName = $("#cityId").attr("data");
    var cityId = "";

    var potCustVar = $("#potCustId").val();
    var descriptionVar = $("#commentId").val();
    var leaveTypeVar = $('#leaveSelectId').val();
    var leaveReasonVar = $('#leaveReasonSelectId').val();
    var halfleaveTypeVar = $('#halfLeaveSelectId').val();
    var numOfAccounts = $('#numOfAccId').val().trim();
    var targetRevenue = $('#targetRevenueId').val();;

    if (accountName) {
        accountId = $("#accountId").get(0).name;
    } else {
        $("#accountId").attr("name", "");
    }

    if (custGroupName) {
        custGroupId = $("#customerGroupId").get(0).name;
    } else {
        $("#customerGroupId").attr("name", "");
    }

    if (provinceName) {
        provinceId = $("#provinceId").get(0).name;
    } else {
        $("#provinceId").attr("name", "");
    }

    if (cityName) {
        cityId = $("#cityId").get(0).name;
    } else {
        $("#cityId").attr("name", "");
    }

    var taskTypeOptionVar;
    var actTypeOptionVar;

    if (newDateVar) {

        taskTypeOptionVar = $("#selTaskSpanId").text();
        actTypeOptionVar = $("#selTaskTypeSpanId").text();

    } else {
        taskTypeOptionVar = $('#buPlistId').val();
        actTypeOptionVar = $('#buTypePlistId').val();
    }

    if ("Leave" == workOptionVar) {

        if (leaveTypeVar) {
            if (leaveTypeVar == 'Full Day') {
                startTimeVar = startDateVar + 'T' + '09:00:00';
                endTimeVar = startDateVar + 'T' + '17:00:00';
            } else if (leaveTypeVar == 'Half Day') {

                if (halfleaveTypeVar) {

                    if (halfleaveTypeVar == 'Morning') {
                        startTimeVar = startDateVar + 'T' + '09:00:00';
                        endTimeVar = startDateVar + 'T' + '14:00:00';
                    } else {
                        startTimeVar = startDateVar + 'T' + '14:00:00';
                        endTimeVar = startDateVar + 'T' + '18:00:00';
                    }

                }

            } else {
                alert('Please select type of Half Day Leave !!');
                return;
            }
        } else {
            alert('Please select type of Leave !!');
            return;
        }

    } else if (startTimeVar) {

        if ("Field work" == workOptionVar) {

            if (accountChangedClass) {

                if (accountId &&
                    accountName &&
                    accountName == accountSetName &&
                    (UNPLANNED_ACCOUNT.toLowerCase() == accountName.toLowerCase() || MULTIPLE_ACCOUNT.toLowerCase() == accountName.toLowerCase())
                ) {} else {
                    if (userDetailsClass.currentUserRecord.Business_Unit__c.toLowerCase() == 'Feeds'.toLowerCase() ||
                        userDetailsClass.currentUserRecord.Business_Unit__c.toLowerCase() == 'Poultry'.toLowerCase()
                        //|| userDetailsClass.currentUserRecord.Meats__c
                    ) { // Agro

                        if (!$('#numOfAccId').val().trim()) {

                            if (!provinceId ||
                                !provinceName ||
                                provinceName != provinceSetName
                            ) {
                                alert("Please select the Province from the dropdown when typing !!");
                                return true;
                            }

                            if (!cityId ||
                                !cityName ||
                                cityName != citySetName
                            ) {
                                alert("Please select the City from the dropdown when typing !!");
                                return true;
                            }
                        }

                    } else if (userDetailsClass.currentUserRecord.Business_Unit__c.toLowerCase() == 'GFS'.toLowerCase()) { // GFS

                        if (!custGroupId ||
                            !custGroupName ||
                            custGroupName != custGroupSetName
                        ) {
                            alert("Please select the Customer Group from the dropdown when typing !!");
                            return true;
                        }

                    }
                }
            }

            if (!newDateVar) {
                if ((userDetailsClass.currentUserRecord.Business_Unit__c.toLowerCase() == 'Feeds'.toLowerCase() ||
                        userDetailsClass.currentUserRecord.Business_Unit__c.toLowerCase() == 'Poultry'.toLowerCase()
                        //|| userDetailsClass.currentUserRecord.Meats__c
                    )) {
                    if (!accountName) {

                        alert("Please add Account !!");
                        return true;
                    }

                    if (accountName &&
                        !numOfAccounts &&
                        (!accountId ||
                            accountName != accountSetName
                        )
                    ) {

                        alert("Please select the Account from the dropdown when typing !!");
                        return;
                    }

                } else if (userDetailsClass.currentUserRecord.Business_Unit__c.toLowerCase() == 'GFS'.toLowerCase() &&
                    custGroupSetName.toLowerCase() != CUSTGROUP_OTHERS.toLowerCase() &&
                    accountName &&
                    (!accountId ||
                        accountName != accountSetName
                    )
                ) {
                    alert("Please select the Account from the dropdown when typing !!");
                    return true;
                }
            }
        }

        if (userDetailsClass.currentUserRecord.Business_Unit__c.toLowerCase() == 'GFS'.toLowerCase() ||
            ((userDetailsClass.currentUserRecord.Business_Unit__c.toLowerCase() == 'Feeds'.toLowerCase() ||
                    userDetailsClass.currentUserRecord.Business_Unit__c.toLowerCase() == 'Poultry'.toLowerCase()
                )
                //&& "Office work" == workOptionVar
            )
        ) { // If the user is GFS OR the the user is (AGRO with Field Work)

            if ("Office work" == workOptionVar || !numOfAccounts) {
                if (!actTypeOptionVar ||
                    "--none--" == actTypeOptionVar
                ) {
                    alert("Please select the Type of Activity !!");
                    return true;
                }

                if (!taskTypeOptionVar ||
                    "--none--" == taskTypeOptionVar
                ) {
                    alert("Please select the Activity !!");
                    return true;
                }
            }
        }

        try {
            startTimeVar = convert12to24Hrs(startTimeVar)
        } catch (err) {
            alert("Enter Valid Start Date / Time");
            return true;
        }

        try {
            endTimeVar = convert12to24Hrs(endTimeVar)
        } catch (err) {
            alert("Enter Valid End Date / Time");
            return true;
        }

        startTimeVar = startDateVar + 'T' + startTimeVar;
        endTimeVar = startDateVar + 'T' + endTimeVar;
    }

    console.log('--see--', startTimeVar);

    if (!(moment(startTimeVar, "YYYY-MM-DDTHH:mm:ss", true).isValid())) {
        alert("Enter Valid Start Time");
        return true;
    }

    if (!(moment(endTimeVar, "YYYY-MM-DDTHH:mm:ss", true).isValid())) {
        alert("Enter Valid End Date / Time");
        return true;
    }

    if (!moment(endTimeVar).isAfter(startTimeVar)) {
        alert("Enter Start Time less than End Time");
        return true;
    }

    if(  $('#numOfAccId').val().trim()
      && $('#numOfAccId').val().trim() < 2
      ) {
        alert('Multiple accounts cannot be less than 2');
        $('#numOfAccId').val('');
        numOfAccIdKeyUp();
        return true;
    }

    var currDateWhole = ((new Date()).toString()).split(" ");
    var currDateCompVar = currDateWhole[3] + "-" + getnumMonth(currDateWhole[1]) + "-" + currDateWhole[2];
    var timeDiff = (new Date(startDateVar + " 00:00:00") - new Date(currDateCompVar + " 00:00:00")) / 1000 / 60 / 60;

    if (timeDiff < 0) {

        alert("Start Date cannot be less than today !!");
        return true;
    }

    if (newEventStr.length < 251) {

        // Copy the parameters of the event to be deleted in a ref var.
        var event_ref;
        var taskmapKey;

        if ("acc" == dayClickEventClass.eventType) {
            event_ref = dayClickEventClass.taskmap[clickedTaskId];
        } else {
            event_ref = dayClickEventClass.taskmap[(Object.keys(dayClickEventClass.taskmap)[0])];
        }

        if (event_ref.sfId) {
            taskmapKey = event_ref.sfId;
        } else {
            taskmapKey = ++uiTaskId;
        }

        if(CONFIG_EDIT) {
            event_ref.sfId = clickedTaskId;
        }

        var officeTimeDiff = 0;
        var fieldTimeDiff = 0;
        var leaveTimeDiff = 0;

        officeTimeDiff = 0;
        fieldTimeDiff = 0;
        leaveTimeDiff = 0;

        var sfIdVar;
        if(!CONFIG_EDIT) {
            sfIdVar = newDateVar ? '' : event_ref.sfId;
        } else {
            sfIdVar = newDateVar ? '' : clickedTaskId;
        }



        if ("Leave" == workOptionVar) {
            accountId = "Leave";
            accountName = "Leave";
            leaveTimeDiff = 8;
            eventType = "Leave";

            if (leaveTypeVar == "Full Day") {
                if (removeAllEvents(startDateVar, true)) {
                    return true;
                }
            } else {
                console.log("--endTimeVar2238--", endTimeVar, "--startTimeVar--", startTimeVar);
                officeTimeDiff = (new Date("1970-1-1 " + endTimeVar.split("T")[1]) - new Date("1970-1-1 " + startTimeVar.split("T")[1])) / 1000 / 60 / 60;
                console.log("--2230--", officeTimeDiff);

                if (removeLeaveFromEvents(startDateVar, true)) {
                    return true;
                }

                if (checkOverlapTime(startTimeVar, endTimeVar, sfIdVar)) {
                    alert("Time slot already booked. Please change time slot to proceed !!");
                    return true;
                }
            }

        } else if ("Office work" == workOptionVar) {
            accountId = taskmapKey;
            accountName = taskTypeOptionVar;
            console.log("--endTimeVar--", endTimeVar, "--startTimeVar--", startTimeVar);
            officeTimeDiff = (new Date("1970-1-1 " + endTimeVar.split("T")[1]) - new Date("1970-1-1 " + startTimeVar.split("T")[1])) / 1000 / 60 / 60;
            console.log("--1648--", officeTimeDiff);
            eventType = "Office work";

            if (removeLeaveFromEvents(startDateVar, true)) {
                return true;
            }

            if (checkOverlapTime(startTimeVar, endTimeVar, sfIdVar)) {
                alert("Time slot already booked. Please change time slot to proceed !!");
                return true;
            }



        } else {
            fieldTimeDiff = (new Date("1970-1-1 " + endTimeVar.split("T")[1]) - new Date("1970-1-1 " + startTimeVar.split("T")[1])) / 1000 / 60 / 60;
            if (userDetailsClass.currentUserRecord.Business_Unit__c.toLowerCase() == 'GFS'.toLowerCase()) {
                eventType = "acc";
            } else {
                eventType = 'accConfig';
            }

            if (removeLeaveFromEvents(startDateVar, true)) {
                return true;
            }

            if (checkOverlapTime(startTimeVar, endTimeVar, sfIdVar)) {
                alert("Time slot already booked. Please change time slot to proceed !!");
                return true;
            }
        }

        // Remove the event to be edited.
        if (deleteVar) {
            console.log("--clickedTaskId 1294--", clickedTaskId)
            delete dayClickEventClass.taskmap[clickedTaskId];

        } else {
            event_ref.sfId = null;
        }

        var addedNewElement = false;

        $.each($('#calendar').fullCalendar("clientEvents", function(event) {
            if ((event.start._i == startDateVar || event.start._i.split("T")[0] == startDateVar) &&
                event.accId == accountId
            ) {
                return true;
            } else {
                return false;
            }
        }), function(index, value) {


            value.taskmap[taskmapKey] = {
                title: accountName,
                start: startTimeVar,
                end: endTimeVar,
                userId: event_ref.userId,
                edited: true,
                _id: event_ref._id,
                sfId: event_ref.sfId,
                accountId: accountId,
                accountName: accountName,
                potCustDetails: potCustVar,
                workOption: workOptionVar,
                taskTypeOption: taskTypeOptionVar,
                activityTypeOption: actTypeOptionVar,
                color: event_ref.color,
                textColor: event_ref.textColor,
                borderColor: event_ref.borderColor,
                taskTitle: newEventStr,
                eventType: eventType,
                description: descriptionVar,
                halfDay: leaveTypeVar,
                leaveReason: leaveReasonVar,
                halfDayType: halfleaveTypeVar,
                province: provinceId,
                city: cityId,
                customerGroup: custGroupId,
                custGroupSetName: custGroupSetName,
                numOfAccounts: numOfAccounts,
                targetRevenue: targetRevenue
            };
            console.log("val--", value.taskmap);
            addedNewElement = true;
            return false;

        });

        if (!addedNewElement) {

            var innertask = {};
            innertask[taskmapKey] = {
                title: accountName,
                start: startTimeVar,
                end: endTimeVar,
                userId: event_ref.userId,
                edited: true,
                _id: event_ref._id,
                sfId: event_ref.sfId,
                accountId: accountId,
                accountName: accountName,
                potCustDetails: potCustVar,
                workOption: workOptionVar,
                taskTypeOption: taskTypeOptionVar,
                activityTypeOption: actTypeOptionVar,
                color: event_ref.color,
                textColor: event_ref.textColor,
                borderColor: event_ref.borderColor,
                taskTitle: newEventStr,
                eventType: eventType,
                description: descriptionVar,
                halfDay: leaveTypeVar,
                leaveReason: leaveReasonVar,
                halfDayType: halfleaveTypeVar,
                province: provinceId,
                city: cityId,
                customerGroup: custGroupId,
                custGroupSetName: custGroupSetName,
                numOfAccounts: numOfAccounts,
                targetRevenue: targetRevenue
            };

            $("#calendar").fullCalendar("renderEvent", {
                accId: accountId,
                start: startDateVar,
                taskmap: innertask,
                title: accountName,
                eventType: eventType
            }, stick = true);
        }

        console.log('--updating--', dayClickEventClass);

    } else {
        alert('Event name too long.....');
    }

    var daysRemovedCount = removeEventsWithNoTasks();
    daysRemovedCount = daysRemovedCount * -1;

    $("#finalDoneId").click();

    return false;
}

function convert12to24Hrs(timePar) {

    var time = timePar;
    var hours = Number(time.match(/^(\d+)/)[1]);
    var minutes = Number(time.match(/:(\d+)/)[1]);
    var AMPM = time.match(/\s(.*)$/)[1];
    if (AMPM == "PM" && hours < 12) hours = hours + 12;
    if (AMPM == "AM" && hours == 12) hours = hours - 12;
    var sHours = hours.toString();
    var sMinutes = minutes.toString();
    if (hours < 10) sHours = "0" + sHours;
    if (minutes < 10) sMinutes = "0" + sMinutes;
    timePar = sHours + ":" + sMinutes + ":00";

    return timePar;

}

function convert24to12Hrs(timePar) {
    var time = timePar;
    time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

    if (time.length > 1) { // If time format correct
        time = time.slice(1); // Remove full string match value
        time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
        time[0] = +time[0] % 12 || 12; // Adjust hours
    }
    startTimePar = time.join('');
    return startTimePar;
}

//---------------------------------------end of editEventData method-----------------------------------------------------

function checkOverlapTime(startTime, endTime, event_refsfId) {

    var overLapFound = false;
    startTimeSplit = (startTime.split("T")[1]);
    endTimeSplit = (endTime.split("T")[1]);

    console.log("--2043see--", startTime);

    $.each($('#calendar').fullCalendar("clientEvents", function(event) {
        if (event.start._i == (startTime.split("T")[0]) ||
            event.start._i.split("T")[0] == (startTime.split("T")[0])
        ) {
            return true;
        } else {
            return false;
        }
    }), function(index2, value2) { // Date check

        $.each(Object.keys(value2.taskmap), function(index, value) { // Time check

            console.log("--2058--", value, ":::", event_refsfId);

            if (event_refsfId && event_refsfId == value) {
                console.log("wwoooowoowowos");
                return;
            }

            console.log("Start time:::", value2.taskmap[value].start.split("T")[1]);
            console.log("End time:::", value2.taskmap[value].end.split("T")[1]);

            var valueStartTime = value2.taskmap[value].start.length > 16 ?
                new Date("1970-1-1 " + value2.taskmap[value].start.split("T")[1]) :
                new Date("1970-1-1 " + "09:00:00");

            var valueEndTime = value2.taskmap[value].end.length > 16 ?
                new Date("1970-1-1 " + value2.taskmap[value].end.split("T")[1]) :
                new Date("1970-1-1 " + "09:00:00");

            var startTimeConst = new Date("1970-1-1 " + startTimeSplit);
            var endTimeConst = new Date("1970-1-1 " + endTimeSplit);

            console.log("startTimeConst:::", startTimeConst);
            console.log("endTimeConst:::", endTimeConst);
            console.log("valueStartTime:::", valueStartTime);
            console.log("valueEndTime:::", valueEndTime);

            if ((valueStartTime < endTimeConst) && (startTimeConst < valueEndTime)) {
                overLapFound = true;
                return false;
            }

        });

        if (overLapFound) {
            return false;
        }
    });

    console.log("--2167--", overLapFound);

    return overLapFound;
}

function populateCopyTaskVals() {

    $('#selectCopyMonthId').append('<option value="Jan">Jan</option>');
    $('#selectCopyMonthId').append('<option value="Feb">Feb</option>');
    $('#selectCopyMonthId').append('<option value="Mar">Mar</option>');
    $('#selectCopyMonthId').append('<option value="Apr">Apr</option>');
    $('#selectCopyMonthId').append('<option value="May">May</option>');
    $('#selectCopyMonthId').append('<option value="Jun">Jun</option>');
    $('#selectCopyMonthId').append('<option value="Jul">Jul</option>');
    $('#selectCopyMonthId').append('<option value="Aug">Aug</option>');
    $('#selectCopyMonthId').append('<option value="Sep">Sep</option>');
    $('#selectCopyMonthId').append('<option value="Oct">Oct</option>');
    $('#selectCopyMonthId').append('<option value="Nov">Nov</option>');
    $('#selectCopyMonthId').append('<option value="Dec">Dec</option>');

    var startYear = (new Date()).getFullYear() - 1;

    for (var i = 0; i < 3; i++) {
        $('#selectCopyYearId').append('<option value="' + startYear + '">' + startYear + '</option>');
        ++startYear;
    }

    $('#selectCopyYearId').val((new Date()).getFullYear());
}

function openASMEditModal() {
    if (userDetailsClass && userDetailsClass.asmRecordSelf) {
        if (userDetailsClass.asmRecordSelf.Message__c && userDetailsClass.asmRecordSelf.Start_Date__c && userDetailsClass.asmRecordSelf.End_Date__c) {
            $('#asmNewMessage').val(userDetailsClass.asmRecordSelf.Message__c);
            $('#asmStartDate').val(moment(userDetailsClass.asmRecordSelf.Start_Date__c).format('MM-DD-YYYY'));
            $('#asmEndDate').val(moment(userDetailsClass.asmRecordSelf.End_Date__c).format('MM-DD-YYYY'));
        } else {
            $('#asmNewMessage').val('');
            $('#asmStartDate').val('');
            $('#asmEndDate').val('');
        }
    } else {
        $('#asmNewMessage').val('');
        $('#asmStartDate').val('');
        $('#asmEndDate').val('');
    }
    $('#asmEditModalModal').modal();
}

function openCopyTaskModal() {

    var selectedMonthRec = getSelectedMonth();
    var monthNum = parseInt(getnumMonth($("#selectCopyMonthId").val())) - 1;
    var yearNum = $("#selectCopyYearId").val();

    var date = new Date(yearNum, monthNum, 1);
    var days = [];
    while (date.getMonth() === monthNum) {

        var loopMomentDate = moment(new Date(date));
        days.push({
            dayMonthYear: loopMomentDate.format('YYYY-MM-DD'),
            dayOfWeek: loopMomentDate.format('dddd')
        });
        date.setDate(date.getDate() + 1);
    }

    $("#daysHolderId").empty();

    console.log("236---", days);

    $.each(days, function(index, value) {

        $("#daysHolderId").append('<div class="container">' +
            '<div class="row">' +
            '<div class="col-sm-6">' +
            '<label class="customcheck">' +
            '<input type="checkbox" name="" value="" id="src' + value.dayMonthYear.trim() + '">' +
            '<span class="checkmark"></span>' +
            '<label for="copyTaskDate" class="fieldLabel">' + value.dayMonthYear + '</label>' +
            '</label>' +
            '<label for="copyTaskDay" class="fieldLabel">' + value.dayOfWeek + '</label>' +
            '</div>' +
            '<div id="' + value.dayMonthYear.trim() + 'destHolderId" style="display: none;" class="col-sm-6">' +
            '<label for="copyTaskDate" class="fieldLabel">Destination Date</label>' +
            '<input type="text" class="form-control2" id="dest' + value.dayMonthYear.trim() + '"/>' +
            '</div>' +
            '</div>' +
            '</div>'
        );

        $("#src" + value.dayMonthYear.trim()).change(function() {
            $("#dest" + value.dayMonthYear.trim()).val("")
            if ($("#src" + value.dayMonthYear.trim()).prop("checked")) {
                $("#" + value.dayMonthYear.trim() + "destHolderId").attr("style", "");
            } else {
                $("#" + value.dayMonthYear.trim() + "destHolderId").attr("style", "display: none");
            }
        });

        $("#dest" + value.dayMonthYear.trim()).datepicker({
            dateFormat: 'mm-dd-yy', //check change
            defaultDate: date,
            changeMonth: true,
            changeYear: true,
            //minDate: dateMin
        });
        //scroll issue
        $("#copyTaskModal").scroll(function() {
            $('#ui-datepicker-div').hide();
            $("#dest" + value.dayMonthYear.trim()).blur();
        });
        $("#dest" + value.dayMonthYear.trim()).click(function() {
            $('#ui-datepicker-div').show();
        });


    });
}


function copyTaskTime() {

    var copySuccess = false;

    $.each($("input[id^='src']"), function(index, value) {

        if ($(value).prop("checked")) {
            console.log(value.id.split("src")[1]);
            console.log("weeee", $("#dest" + value.id.split("src")[1]).val());

            var srcStartDate = value.id.split("src")[1];
            var destStartDate = $("#dest" + value.id.split("src")[1]).val();
            destStartDate = moment(destStartDate).format('YYYY-MM-DD');

            $.each($('#calendar').fullCalendar("clientEvents", function(event) {
                if (event.start._i == srcStartDate ||
                    event.start._i.split("T")[0] == srcStartDate
                ) {
                    return true;
                } else {
                    return false;
                }
            }), function(index2, value2) {

                $.each(Object.keys(value2.taskmap), function(index, value) {

                    var tempTime = value2.taskmap[value].start.split("T")[1];
                    value2.taskmap[value].start = destStartDate + "T" + tempTime;

                    console.log(value2.taskmap[value].start.split("T")[0]);

                    tempTime = value2.taskmap[value].end.split("T")[1];
                    value2.taskmap[value].end = destStartDate + "T" + tempTime;

                    createEventData(value2.taskmap[value]);

                    copySuccess = true;
                });
            });
        }
    });

    $("#finalDoneCopyId").click();

    return copySuccess;
}

function removeLeaveFromEvents(startDateVar, showAlert) {

    var leavePresent = false;

    $.each($('#calendar').fullCalendar("clientEvents", function(event) {
        if ((event.start._i == startDateVar ||
                event.start._i.split("T")[0] == startDateVar
            ) &&
            event.eventType == "Leave"
        ) {
            return true;
        } else {
            return false;
        }
    }), function(index2, value2) {
        leavePresent = true;
        return false;

    });

    if (showAlert) {
        if (leavePresent) {
            if (confirm("Leaves present on this date, Are you sure you want to cancel Leave ??")) {} else {
                return true;
            }
        }
    } else {
        if (leavePresent) {
            return true;
        }
    }



    $.each($('#calendar').fullCalendar("clientEvents", function(event) {
        if ((event.start._i == startDateVar ||
                event.start._i.split("T")[0] == startDateVar
            ) &&
            event.eventType == "Leave"
        ) {
            return true;
        } else {
            return false;
        }
    }), function(index2, value2) {


        $.each(Object.keys(value2.taskmap), function(index, value) {
            console.log("--me--", value2.taskmap[value].sfId);
            if (value2.taskmap[value].sfId) {
                removedEventsSfIds.push(value2.taskmap[value].sfId);
            }
        });

        $('#calendar').fullCalendar('removeEvents', value2._id);

    });

    return false;
}


function removeAllEvents(startDateVar, showAlert) {

    var taskPresent = false;

    $.each($('#calendar').fullCalendar("clientEvents", function(event) {
        if ((event.start._i == startDateVar ||
                event.start._i.split("T")[0] == startDateVar
            ) &&
            event.eventType != "Leave"
        ) {
            return true;
        } else {
            return false;
        }
    }), function(index2, value2) {
        taskPresent = true;
        return false;
    });

    if (showAlert) {
        if (taskPresent) {
            if (confirm("Another Task or Leaves present on this date, Are you sure you want to cancel them ??")) {} else {
                return true;
            }
        }
    } else {
        if (taskPresent) {
            return true;
        }
    }

    $.each($('#calendar').fullCalendar("clientEvents", function(event) {
        if (event.start._i == startDateVar ||
            event.start._i.split("T")[0] == startDateVar
        ) {
            return true;
        } else {
            return false;
        }
    }), function(index2, value2) {

        $.each(Object.keys(value2.taskmap), function(index, value) {
            console.log("--me--", value2.taskmap[value].sfId);
            if (value2.taskmap[value].sfId) {
                removedEventsSfIds.push(value2.taskmap[value].sfId);
            }
        });

        $('#calendar').fullCalendar('removeEvents', value2._id);

        console.log("---2072--", value2);
    });

    return false;
}


// To send data to controller to save data and delete data on click of submit.
function sendEventDataToController() {

    console.log("---calling sendEventDataToController--");

    var listOfEvents = [];

    $.each($("#calendar").fullCalendar('clientEvents'), function(index2, value2) {

        $.each(Object.keys(value2.taskmap), function(index, value) {

            var taskItr = value2.taskmap[value];

            if (taskItr.edited) {

                var eventJson = {
                    userId: taskItr.userId,
                    userName: taskItr.userName,
                    taskTitle: taskItr.taskTitle,
                    start: taskItr.start,
                    end: taskItr.end,
                    sfId: taskItr.sfId,
                    accountId: taskItr.accountId,
                    accountName: taskItr.accountName,
                    potCustDetails: taskItr.potCustDetails,
                    activityTypeOption: taskItr.activityTypeOption,
                    taskTypeOption: taskItr.taskTypeOption,
                    workOption: taskItr.workOption,
                    description: taskItr.description,
                    halfDay: taskItr.halfDay,
                    leaveReason: taskItr.leaveReason,
                    halfDayType: taskItr.halfDayType,
                    province: taskItr.province,
                    city: taskItr.city,
                    customerGroup: taskItr.customerGroup,
                    custGroupSetName: taskItr.custGroupSetName,
                    numOfAccounts: taskItr.numOfAccounts,
                    targetRevenue: taskItr.targetRevenue
                };

                console.log("--1356-", taskItr);

                listOfEvents.push(eventJson);

            }
        });

    });

    console.log("2265---", removedEventsSfIds);

    if (listOfEvents.length > 0 ||
        removedEventsSfIds
    ) {

        listOfEvents = JSON.stringify(listOfEvents);
        var removedDataString = removedEventsSfIds.toString();
        console.log('--parsing--', listOfEvents, "*----", removedDataString);

        $("#blurDivId").attr("style", "display: block");
        Visualforce.remoting.Manager.invokeAction(
            'CalendarController.insertTaskData',
            listOfEvents,
            removedDataString,
            cancelReasonMessage,
            function(result, event) {
                if (event.status) {
                    console.log('success in remoting--');
                    $("#blurDivId").attr("style", "display: none");
                    fetchUserData();

                    if (copyTaskErrorObj.length > 0) {
                        $.each(copyTaskErrorObj, function(index, value) {
                            alert(value);
                        });
                    }

                    copyTaskErrorObj = [];
                    $.each(result, function(index, value) {
                        alert(value);
                    });
                } else if (event.type === 'exception') {
                    alert( 'Status: '    + event.status
                         + '\nType: '    + event.type
                         + '\nMessage: ' + event.message
                         + '\nWhere: '   + event.where
                         );
                    fetchUserData();
                } else {
                    alert( 'Status: '    + event.status
                         + '\nType: '    + event.type
                         + '\nMessage: ' + event.message
                         + '\nWhere: '   + event.where
                         );
                    fetchUserData();
                }
            }, {
                escape: false
            }
        );
    }

    removedEventsSfIds = [];

}

//---------------------------------------end of sendEventDataToController method-----------------------------------------------------

function removeEventsWithNoTasks() {

    var daysRemovedCount = 0;
    var daysPresent = 0;
    var dateList = [];

    $.each($("#calendar").fullCalendar('clientEvents'), function(index, value) {
        if (!Object.keys(value.taskmap).length ||
            0 == Object.keys(value.taskmap).length
        ) {
            $('#calendar').fullCalendar('removeEvents', value._id);
            console.log("1903:::", value);
            dateList.push(value.start._i);
        }
    });

    $.each(dateList, function(index2, value2) {
        $.each($('#calendar').fullCalendar("clientEvents", function(event) {
            if (event.start._i == value2) {
                return true;
            } else {
                return false;
            }
        }), function(index, value) {
            console.log("::::", value);
            ++daysPresent;
        });
    });

    daysRemovedCount = dateList.length - daysPresent;

    return daysRemovedCount;
}


// To send data to controller to approve click of submit.
function sendForApprovalController() {

    var selectedMonth = getSelectedMonth();

    if (selectedMonth) {
        selectedMonth = JSON.stringify(selectedMonth);

        $("#blurDivId").attr("style", "display: block");
        Visualforce.remoting.Manager.invokeAction(
            'CalendarController.sendMonthsForApproval',
            selectedMonth,
            $("#userpickListId").val(),
            function(result, event) {
                if (event.status) {

                    console.log("--2609--", JSON.parse(result));
                    if (JSON.parse(result) &&
                        JSON.parse(result).errors &&
                        JSON.parse(result).errors[0] &&
                        JSON.parse(result).errors[0].message
                    ) {
                        alert("" + JSON.parse(result).errors[0].message);
                    } else {
                        alert("Submitted for Approval !!");
                    }
                    $("#blurDivId").attr("style", "display: none");

                    fetchUserData();
                } else if ('exception' === event.type) {
                    alert(event.message);
                    alert(event.where);
                } else {
                    alert(event.message);
                    alert(event.where);
                }
            }, {
                escape: false
            }
        );

    }

}

//---------------------------------------end of sendForApprovalController method-----------------------------------------------------


function getSelectedMonth() {

    var selectedMonth;

    if ($("#calendar").fullCalendar('getDate') &&
        $("#calendar").fullCalendar('getDate')._d &&
        $("#calendar").fullCalendar('getDate')._d.toString() &&
        $("#calendar").fullCalendar('getDate')._d.toString().split(" ")
    ) {

        selectedMonth = {
            month: $("#calendar").fullCalendar('getDate')._d.toString().split(" ")[1],
            year: $("#calendar").fullCalendar('getDate')._d.toString().split(" ")[3]
        };

    }

    return selectedMonth;

}


function removeCalEvents() {



    if (!dayClickEventClass) {
        return;
    }

    var removedEventSfId = dayClickEventClass.taskmap[clickedTaskId].sfId;
    console.log("---111----", dayClickEventClass);
    console.log("---222----", dayClickEventClass.taskmap);
    console.log("---333----", dayClickEventClass.taskmap[clickedTaskId]);
    console.log("---444----", dayClickEventClass.taskmap[clickedTaskId].sfId);
    //Cancel Reason
    //dayClickEventClass.taskmap[clickedTaskId].cancelReason = cancelReasonMessage;

    if (removedEventSfId) {
        removedEventsSfIds.push(removedEventSfId);
    }

    var startTimeJSONCAL = dayClickEventClass.taskmap[clickedTaskId].start.split("T")[1];
    var endTimeJSONCAL = dayClickEventClass.taskmap[clickedTaskId].end.split("T")[1];
    var workOptionJSONCAL = dayClickEventClass.taskmap[clickedTaskId].workOption;
    var officeTimeDiff = 0;
    var fieldTimeDiff = 0;
    var leaveTimeDiff = 0;

    if ("Leave" == workOptionJSONCAL) {
        leaveTimeDiff = 8;
        leaveTimeDiff = -1 * leaveTimeDiff;
    } else if ("Office work" == workOptionJSONCAL) {
        officeTimeDiff = (new Date("1970-1-1 " + endTimeJSONCAL) - new Date("1970-1-1 " + startTimeJSONCAL)) / 1000 / 60 / 60;
        officeTimeDiff = -1 * officeTimeDiff;
    } else {
        fieldTimeDiff = (new Date("1970-1-1 " + endTimeJSONCAL) - new Date("1970-1-1 " + startTimeJSONCAL)) / 1000 / 60 / 60;
        fieldTimeDiff = -1 * fieldTimeDiff;
    }

    delete dayClickEventClass.taskmap[clickedTaskId];

    var daysRemovedCount = removeEventsWithNoTasks();
    daysRemovedCount = daysRemovedCount * -1;
}

//---------------------------------------end of removeCalEvents method-----------------------------------------------------


//---------------------------------------end of getAccountsSF method-----------------------------------------------------



function setfutureYears() {
    var curDateArr = ((new Date()).toString()).split(" ");
    var curYear = parseInt(curDateArr[3]);
    console.log('curYear ' + curYear);

    $('#selectYearId').append('<option value="' + curYear + '">' + curYear + '</option>');
    $('#selectYearId').val(curYear);
    for (var itr = 1; itr <= 20; itr++) {
        var year = curYear + itr;
        $('#selectYearId').append('<option value="' + year + '">' + year + '</option>');
    }
}

//---------------------------------------end of setfutureYears method-----------------------------------------------------


// To send data to controller to copy previous month Tasks.
function sendToCopyPreviousMonthTasks() {

    var year = $('#selectYearId').val();
    var month = $('#selectMonthId').val();

    CalendarController.copyPreviousMonthTasks(year, month,
        function(result, event) {
            if (event.status) {
                location.reload(true);
            } else if ('exception' === event.type) {
                // code.
            } else {
                // code.
            }
        }, {
            escape: false
        });

}

//---------------------------------------end of sendForApprovalController method-----------------------------------------------------

function disableModalFields() {

    $("#accountId").prop('disabled', true);
    $("#titleId").prop('disabled', true);
    $("#startDateId").prop('disabled', true);
    $("#startId").prop('disabled', true);
    $("#endId").prop('disabled', true);
    $("#numOfAccId").prop('disabled', true);
    $("#potCustId").prop('disabled', true);
    $("#provinceId").prop('disabled', true);
    $("#cityId").prop('disabled', true);
    $("#customerGroupId").prop('disabled', true);
    $("#commentId").prop('disabled', true);

    $("#Work").prop('disabled', true);

    $("#buTypePlistId").prop('disabled', true);
    $("#leaveSelectId").prop('disabled', true);
    $("#leaveReasonSelectId").prop('disabled', true);
    $("#halfLeaveSelectId").prop('disabled', true);

    $("#buPlistId").prop('disabled', true);

}

function enableModalFields() {

    $("#accountId").removeAttr("disabled");
    $("#titleId").removeAttr("disabled");
    $("#startDateId").removeAttr("disabled");
    $("#startId").removeAttr("disabled");
    $("#endId").removeAttr("disabled");
    $("#numOfAccId").removeAttr("disabled");
    $("#potCustId").removeAttr("disabled");
    $("#provinceId").removeAttr("disabled");
    $("#cityId").removeAttr("disabled");
    $("#customerGroupId").removeAttr("disabled");
    $("#commentId").removeAttr("disabled");
    $("#Work").removeAttr("disabled");
    $("#leaveSelectId").removeAttr("disabled");
    $("#leaveReasonSelectId").removeAttr('disabled');
    $("#halfLeaveSelectId").removeAttr("disabled");
    $("#buTypePlistId").removeAttr("disabled");
    $("#buPlistId").removeAttr("disabled");

}

// 14 parameters
// Updated to 21 parameters to city, province and customer Group
function setModalParameters( userNamePar
                           , userIdPar
                           , titlePar
                           , leaveSelectPar
                           , leaveReasonSelectPar
                           , halfLeaveSelectPar
                           , commentPar
                           , startDatePar
                           , startTimePar
                           , endTimePar
                           , accountIdVar
                           , accountNameVar
                           , accNumVar
                           , potCustDetailsVar
                           , workTypeVar
                           , actTypeVar
                           , taskTypeSet
                           , cityId
                           , cityName
                           , provinceId
                           , provinceName
                           , customerGroupId
                           , customerGroupName
                           , customerGroupDescription
                           , targetRevenuePar
                           ) {

    $('#accountIdError').css('display', 'none');
    $('#buTypePlistIdError').css('display', 'none');
    $('#buPlistIdError').css('display', 'none');
    $('#titleIdError').css('display', 'none');
    $('#startDateIdError').css('display', 'none');
    $('#startIdError').css('display', 'none');
    $('#endIdError').css('display', 'none');

    $("#accountId").val("");
    $("#userSpanId").html(userNamePar);
    $("#userSpanId").attr("class", userIdPar);
    $("#titleId").val(titlePar);

    $('#leaveSelectId').val(leaveSelectPar);
    $('#leaveReasonSelectId').val(leaveReasonSelectPar);
    $('#halfLeaveSelectId').val(halfLeaveSelectPar)

    startTimePar = convert24to12Hrs(startTimePar);
    endTimePar = convert24to12Hrs(endTimePar);


    $("#startDateId").val(moment(startDatePar).format('MM-DD-YYYY'));
    $("#startId").val(startTimePar);
    $("#endId").val(endTimePar);
    $("#accountId").val(accountNameVar);
    $("#potCustId").val(potCustDetailsVar);

    $('#numOfAccId').val(accNumVar);

    $("#provinceId").val("");
    $("#cityId").val("");
    $("#customerGroupId").val("");
    $("#targetRevenueId").val(targetRevenuePar);


    $("#commentId").val(commentPar);

    if (accountIdVar &&
        18 == accountIdVar.length
    ) {
        $("#accountId").attr("name", accountIdVar);
        $("#accountId").attr("data", accountNameVar);
    } else {
        $("#accountId").attr("name", "");
        $("#accountId").attr("data", "");
    }
    //City, Province, Customer Group
    if (cityName &&
        18 == cityId.length
    ) {
        $("#cityId").attr("name", cityId);
        $("#cityId").attr("data", cityName);
        $("#cityId").val(cityName);
    } else {
        $("#cityId").attr("name", "");
        $("#cityId").attr("data", "");
    }

    if (provinceName &&
        18 == provinceId.length
    ) {
        $("#provinceId").attr("name", provinceId);
        $("#provinceId").attr("data", provinceName);
        $("#provinceId").val(provinceName);
    } else {
        $("#provinceId").attr("name", "");
        $("#provinceId").attr("data", "");
    }

    if (customerGroupName &&
        18 == customerGroupId.length
    ) {
        $("#customerGroupId").attr("name", customerGroupId);
        $("#customerGroupId").attr("data", customerGroupDescription);
        $("#customerGroupId").attr("data2", customerGroupName + ' - ' + customerGroupDescription);
        $("#customerGroupId").val(customerGroupDescription);
    } else {
        $("#customerGroupId").attr("name", "");
        $("#customerGroupId").attr("data", "");
        $("#customerGroupId").attr("data2", "");
    }

    if (workTypeVar) {
        $("input[value='" + workTypeVar + "']").prop('checked', true);
        $("#Work").val(workTypeVar);
    } else {
        $("input[value='Office work']").prop('checked', true);
        $("#Work").val("Office work");
    }

    $("#selTaskTypeSpanId").html(actTypeVar);
    $("#selTaskSpanId").html(taskTypeSet);

    if (actTypeVar) {
        actTypeVar = actTypeVar.trim();
    }

    if (taskTypeSet) {
        taskTypeSet = taskTypeSet.trim();
    }

    $("#buTypePlistId").val(actTypeVar);

    $("#buPlistId").val(taskTypeSet);


}


//---------------------------------------end of setModalParameters method-----------------------------------------------------