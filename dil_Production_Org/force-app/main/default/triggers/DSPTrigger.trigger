/*-------------------------------------------------------------------------------------------
Author       :   Adjell Ian Pabayos
Created Date :   November 27 2017
Definition   :   
History      :   
-------------------------------------------------------------------------------------------*/
trigger DSPTrigger on DSP__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    TriggerDispatcher.Run(new DSPTriggerHandler());
}