trigger AccountConfigurationTrigger on Account_Configuration__c ( before insert
                                                                , before update
                                                                , after delete
                                                                , after insert
                                                                , after update) {

    if(StaticResources.byPassAccConfigTrigger) {
        return;
    }

    if(trigger.isBefore) {
        if(trigger.isinsert || trigger.isUpdate) {
            new AccountConfigurationTriggerHandler().computeMonthData(trigger.new);
        }

        if(trigger.isUpdate) {
            new AccountConfigurationTriggerHandler().computeOutletsCovered(trigger.new);
        }
    }

    if(trigger.isAfter) {
        if(trigger.isDelete) {
            new AccountConfigurationTriggerHandler().computeOutletsCovered(trigger.old);
        }
        if(trigger.isinsert || trigger.isUpdate) {
            new AccountConfigurationTriggerHandler().computeWorkHours(trigger.new);
        }
    }
}