trigger PopuateKGConversion on Conversion__c (before Insert, before Update) {
    SET<Id> allproductId = new SET<id>();
    List<Conversion__c > conversionList = new List<Conversion__c>();
    Map<id,Conversion__c > productConversionMap = new Map<id,Conversion__c >();
    Map<id,Conversion__c > productCSConversionMap = new Map<id,Conversion__c >();
    Conversion__c  tempConversionRec = new Conversion__c ();
    Conversion__c  tempCSConversionRec = new Conversion__c ();
    
    for(Conversion__c  nc :trigger.new){
        allproductId.add(nc.Product__c);
    }
    system.debug('## allproductId:'+allproductId);
    

    conversionList = [SELECT name, product__r.name, product__r.id, KG_Conversion_Rate__c, Numerator__c, Denominator__c FROM Conversion__c WHERE (name LIKE 'KG%' OR name LIKE 'CS%') AND Product__c IN: allproductId];
    system.debug('## conversionList :'+conversionList );
    
    for(Conversion__c  cRec: conversionList ){

        if(cRec.Name.contains('CS')) {
            productCSConversionMap.put(cRec.product__r.id,cRec);
        }
        else {
            productConversionMap.put(cRec.product__r.id,cRec);
        }
    }
    system.debug('## productConversionMap:'+productConversionMap);
    
    
    for(Conversion__c  nc :trigger.new){
        if(productConversionMap.get(nc.product__c)!=null){
            tempConversionRec =productConversionMap.get(nc.product__c);
            nc.KG_Conversion_Rate__c = (1/(tempConversionRec.Numerator__c/tempConversionRec.Denominator__c))*(nc.Numerator__c/nc.Denominator__c);
            system.debug('## nc.KG_Conversion_Rate__c:'+nc.KG_Conversion_Rate__c);
        }else{
            nc.KG_Conversion_Rate__c = null;
        }
        
        if(productCSConversionMap.containsKey(nc.product__c)) {
            tempCSConversionRec  = productCSConversionMap.get(nc.product__c);
            nc.CS_Conversion_Rate__c = (1/(tempCSConversionRec.Numerator__c/tempCSConversionRec.Denominator__c))*(nc.Numerator__c/nc.Denominator__c);
        }
        else {
            nc.CS_Conversion_Rate__c = 0;
        }
    }
}