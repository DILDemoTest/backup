/*==========================================================================
    Author       :   Glen Cubinar
    Created Date :   6.26.2017
    Definition   :   trigger to upload Staging records into the Sales Information Form
    History      :   Glen Cubinar: Created
                 :   10.02.2017 - Adjell Pabayos: added check if trigger is to be bypassed due to conflict with Transactions controller
===========================================================================*/

trigger OrderStagingTrigger on Order_Staging__c (After Insert) { 
    
    if(Trigger.isAfter) {
        if(Trigger.isInsert) {
            OrderStagingTrigger_Handler.uploadSalesOrder(Trigger.newMap);
        }       
    }
}