trigger EmployeeCategoryTrigger on Employees_Category__c (After Insert, After Update) {
    
    if(Trigger.isAfter){
        if(Trigger.isInsert || Trigger.isUpdate){
            EmployeeCategoryTriggerHandler.setPrimaryCategory(Trigger.new);
        }
    }
}