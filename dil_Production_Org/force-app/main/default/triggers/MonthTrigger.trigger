/*
-----------------------------------------------------------------------------------------------------------
Modified By                 Date          Version         Description
-----------------------------------------------------------------------------------------------------------
Eternus Solutions           22/06/2018    Initial         Trigger to assign task records related to Month__c with Account
-----------------------------------------------------------------------------------------------------------

*/
trigger MonthTrigger on Month__c (before update, after update, after insert) {

    if(StaticResources.byPassMonthTrigger) {
        return;
    }

    StaticResources.byPassStopSubmittedTaskDML = true;

    if(trigger.isBefore) {

        if(trigger.isInsert || trigger.isUpdate) {
            new MonthTriggerHandler().populateApprovalDates(trigger.oldMap, trigger.new);
        }
        if(trigger.isUpdate) {
            new MonthTriggerHandler().revertTasksIfRejected(trigger.newMap, trigger.new);
            new MonthTriggerHandler().stampResubmissionDate(trigger.oldMap, trigger.new);
        }

    }

    if(trigger.isAfter) {
        if(trigger.isUpdate) {
            new  MonthTriggerHandler().updateTasks(trigger.oldMap, trigger.newMap);
            //if(MonthTriggerHandler.runOnce()) {
                StaticResources.byPassMonthTrigger = true;
                new MonthTriggerHandler().computeWorkingHours(Trigger.new);
                
                //new  MonthTriggerHandler().updateApprovalComments(Trigger.new);

                //deletes all backup files of tasks of approved months
                new  MonthTriggerHandler().deleteBackupFilesOnApproval(trigger.new);
                StaticResources.byPassMonthTrigger = false;
            //}
            new  MonthTriggerHandler().returnMapUserToTypeOfActivityToUniqueAccount(Trigger.new);
        }
        
        if(trigger.isInsert) {
            //new MonthTriggerHandler().updateTasks(trigger.oldMap, trigger.newMap);
            new MonthTriggerHandler().updateWorkingHours(Trigger.new);
        }
       

    }

    StaticResources.byPassStopSubmittedTaskDML = false;
}