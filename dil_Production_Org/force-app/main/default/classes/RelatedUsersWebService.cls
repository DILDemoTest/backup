@RestResource(urlMapping='/getSubordinates/*')
global with sharing class RelatedUsersWebService {
    
    //public static Set<String> setMgrRoles = new Set<String>{'USM_RMDEALERS_01','USM_RMDEALERS_02','USM_RMDEALERS_03','USM_RMDEALERS_05'};
    
    public static Integer subRoleHierCount = 2;
    
    // To get all sub roles.
    public static Set<ID> getAllSubRoleIds(Set<ID> roleIds) {

        Set<ID> currentRoleIds = new Set<ID>();

        // get all of the roles underneath the passed roles
        for(UserRole userRole :[SELECT Id FROM UserRole WHERE ParentRoleId
                                    IN :roleIds AND ParentRoleID != null
                               ]) {
            currentRoleIds.add(userRole.Id);
        }

        // go fetch some more roles!
        if(currentRoleIds.size() > 0 && --subRoleHierCount > 0) {
            System.debug('--------' + subRoleHierCount);
            currentRoleIds.addAll(getAllSubRoleIds(currentRoleIds));
        }

        return currentRoleIds;
    }
    
    @HttpGet
    global static void doGet() {
        RestRequest req = RestContext.request;
        System.debug('req--->'+req);
        RestResponse res = RestContext.response;
        String userId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        User objUser = new User();
        try{
            objUser = [SELECT UserRoleId, UserRole.Name FROM User WHERE Id = :userId];    
        }
        catch(QueryException qe){
            res.responseBody = Blob.valueOf(qe.getMessage());
            return;
        }
        
        Set<ID> currentRoleIds = new Set<ID>();
        try{
            Manager_Role__mdt obhMgrRole = [SELECT DeveloperName FROM Manager_Role__mdt WHERE DeveloperName = :objUser.UserRole.Name];
            /*
            if(setMgrRoles.contains(objUser.UserRole.Name)){
                currentRoleIds = getAllSubRoleIds(new Set<Id>{objUser.UserRoleId});
            }
            else{
                return;
            }
            */
            currentRoleIds = getAllSubRoleIds(new Set<Id>{objUser.UserRoleId});
        }
        catch(QueryException qe){
            res.responseBody = Blob.valueOf(Json.serialize(new Set<Id>{userId}));
            return;
        }
        
        
        Set<Id> setUserIds = new Set<Id>();
        for(User objUsr : [SELECT Id FROM User WHERE UserRoleId IN :currentRoleIds]){
            setUserIds.add(objUsr.Id);
        }
        setUserIds.add(userId);
        res.addHeader('Content-Type', 'application/json');
        res.responseBody = Blob.valueOf(JSON.serialize(setUserIds));
        res.statusCode = 200;
    }
}