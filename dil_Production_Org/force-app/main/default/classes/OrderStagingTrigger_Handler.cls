/*==========================================================================
    Author       :   Glen Cubinar
    Created Date :   6.26.2017
    Definition   :   upload Staging records into the Sales Information Form
    History      :   Glen Cubinar: Created
                 :   09.29.2017 - Adjell Pabayos: Updated to handle new order items, and create error logs to be passed to the controller
                 :   10.03.2017 - Adjell Pabayos: Updated to handle duplicate products on a single order, handle Products using Salesforce Id/SKU Code, and Products with no Base UOMs or no conversions
===========================================================================*/

global with sharing class OrderStagingTrigger_Handler {

    public static void uploadSalesOrder(Map<Id, Order_Staging__c> newMap) {
        Map<String, RecordTypeInfo> orderRecordTypes = Schema.SObjectType.Order__c.getRecordTypeInfosByName();
        Map<String, RecordTypeInfo> orderItemRecordTypes = Schema.SObjectType.Order_Item__c.getRecordTypeInfosByName();
        Map<Id, String> errorStagingMap = new Map<Id, String>();
        Map<Id, Order_Staging__c> orderRecordStagingMap = new Map<Id, Order_Staging__c>();
        Map<String, Order_Staging__c> orderProductStagingMap = new Map<String, Order_Staging__c>();
        Map<String, Custom_Product__c> productsCodeMap = new Map<String, Custom_Product__c>();
        Map<String, Conversion__c> productConversions = new Map<String, Conversion__c>();
        Map<String, Order_Staging__c> orderDuplicateMapCheck = new Map<String, Order_Staging__c>();
        Map<Id, Order__c> orderMapToInsert = new Map<Id, Order__c>();
        Map<Id, Custom_Product__c> productsMap;
        Map<Id, Order__c> oMap = new Map<Id, Order__c>();
        Map<Id, Order_Item__c> oiMap = new Map<Id, Order_Item__c>();
        Set<String> productCodes = new Set<String>();
        Set<Id> accountIds = new Set<Id>();
        Set<Id> productBaseConversionIDs = new Set<Id>();
        Set<ID> oIdSet = new Set<ID>();
        Set<ID> oiIdSet = new Set<ID>();
        Set<Id> stagingsToDelete = new Set<Id>();
        List<Custom_Product__c> productsList = new List<Custom_Product__c>();
        List<Order_Item__c> oItemList = new List<Order_Item__c>();
        List<Database.SaveResult> orderSaveResults = new List<Database.SaveResult>();
        List<Database.UpsertResult> orderItemSaveResults = new List<Database.UpsertResult>();
        List<Order_Item__c> tempOrderItemList = new List<Order_Item__c>();
        List<Order__c> orderListToInsert = new List<Order__c>();
        ExtractionUpdateUtility extractUpdateUtility = new ExtractionUpdateUtility();
        String tempQuery = '';

        for(Order_Staging__c os : newMap.values()){
            productCodes.add(os.Product_Code__c);
        }

        productsMap = new Map<Id, Custom_Product__c>([select Id, Name, Base_UOM__c, Base_UOM__r.Name, SKU_Code__c from Custom_Product__c where SKU_Code__c in :productCodes or Id in :productCodes]);

        for(Custom_Product__c product : productsMap.values()){
            productsCodeMap.put(product.SKU_Code__c, product);
        }

        List<Conversion__c> conversions = [select Id, Product__c, Product__r.Base_UOM__r.Name, UOM__r.Name from Conversion__c where Product__c in :productsMap.keySet()];

        for(Conversion__c conversion : conversions){
            productConversions.put(conversion.Product__c+'-'+conversion.UOM__r.Name, conversion);
        }

        for(Order_Staging__c os : newMap.values()) {
            oiIdSet.add(os.Order_Item_ID__c);
            oIdSet.add(os.SIF_ID__c);
        }

        //AARP: 11-27-2017: Added Querying of Poultry for Over Invoice
        oMap = new Map<Id, Order__c>([SELECT Account__r.Name, Account__r.BU_Poultry__c, DSP__c, SAS__c, Invoice_No__c, Invoice_Date__c, Requested_Delivery_Date__c, Payment_Term1__c, RecordTypeId  FROM Order__c WHERE Id IN: oIdSet]);
        tempQuery = extractUpdateUtility.createQuery('Order_Item__c', new Set<String>(), '', new List<String>());
        tempQuery += ' where Id in :oiIdSet';

        tempOrderItemList = Database.query(tempQuery);
        oiMap = new Map<Id, Order_Item__c>(tempOrderItemList);
        // Map<Id, Order_Item__c> oiMap = new Map<Id, Order_Item__c>([SELECT Order_Form__c, Order_Discount_4_N__c, Order_Discount_3_N__c, Order_Discount_2_N__c, Order_Discount_1_N__c, Invoice_Quantity__c, Invoice_Discount__c, Reason_Order_Invoice__c, RecordTypeId, Filename__c, isUpdatebyBatch__c  FROM Order_Item__c WHERE Id IN: oiIdSet]);

        for(Order_Staging__c os : newMap.values()){
            try {
                Boolean tempHasError = false;
                Order__c tempOrder = oMap.get(os.SIF_ID__c);
                Order_Item__c tempOrderItem = (!String.isEmpty(os.Order_Item_ID__c)) ? oiMap.get(os.Order_Item_ID__c) : new Order_Item__c();
                String tempCombinationKey;
                String tempInvoiceDistributorCombinationKey;

                tempOrder.DSP__c = os.DSP_ID__c;
                tempOrder.SAS__c = os.SAS_ID__c;
                tempOrder.Invoice_No__c = os.Invoice_Number__c;
                tempOrder.Invoice_Date__c = os.Invoice_Date__c;
                tempOrder.Payment_Term1__c = os.Payment_Term__c;
                tempOrder.Requested_Delivery_Date__c = os.Requested_Delivery_Date__c;
                tempOrder.RecordTypeId = orderRecordTypes.get('Actual Sales').getRecordTypeId();
                //tempOrder.SIF_Record_Type__c = 'Pre-booked Locked';

                if(String.isEmpty(os.Order_Item_ID__c)){
                    tempOrderItem.Order_Form__c = os.SIF_ID__c;
                    tempOrderItem.Order_Discount_1_N__c = os.Order_Total_Product_Discount__c;
                    // tempOrderItem.Order_Discount_1_N__c = os.Order_Total_Product_Discount__c / os.Order_Quantity__c;
                }

                tempOrderItem.Invoice_Discount_1__c = os.Invoice_Total_Product_Discount__c;
                // tempOrderItem.Invoice_Discount_1__c = os.Invoice_Total_Product_Discount__c / os.Invoice_Quantity__c;
                tempOrderItem.Order_Quantity__c = os.Order_Quantity__c;
                tempOrderItem.Order_Price__c = os.Order_Price__c;
                tempOrderItem.Invoice_Quantity__c = os.Invoice_Quantity__c;
                tempOrderItem.Invoice_Price__c = os.Invoice_Price__c;
                tempOrderItem.FileName__c = os.FileName__c;
                //tempOrderItem.RecordTypeId = orderItemRecordTypes.get('Pre-booked').getRecordTypeId();
                tempOrderItem.RecordTypeId = orderItemRecordTypes.get('Actual Sales').getRecordTypeId();
                tempOrderItem.isUpdatebyBatch__c = true;

                //AARP: 11-27-2017: Reason for Over Invoice will default to Product Conversion if Account is Poultry
                if(tempOrderItem.Invoice_Quantity__c > tempOrderItem.Order_Quantity__c && tempOrder.Account__r.BU_Poultry__c){
                    tempOrderItem.Reason_Order_Invoice__c = 'Product Conversion';
                }else{
                    tempOrderItem.Reason_Order_Invoice__c = os.Reason_For_Over_Invoice__c;
                }
                //AAR: 11-27-2017: END

                String tempCode = String.escapeSingleQuotes(os.Product_Code__c);
                if((tempCode.length() == 15 || tempCode.length() == 18) && Pattern.matches('^[a-zA-Z0-9]*$', tempCode)){
                    tempOrderItem.Product__c = os.Product_Code__c;
                }else{
                    if(productsCodeMap.containsKey(os.Product_Code__c)){
                        tempOrderItem.Product__c = productsCodeMap.get(os.Product_Code__c).Id;
                    }else{
                        errorStagingMap.put(os.Id, processErrorMessage(errorStagingMap.get(os.Id), 'The product ' + os.Product_Code__c + ' does not exist.'));
                        tempHasError = true;
                    }
                }

                if(!String.isEmpty(tempOrderItem.Product__c)){
                    String tempUOM = os.UOM__c;
                    String tempBaseUOMMatchKey = tempOrderItem.Product__c + '-' + productsMap.get(tempOrderItem.Product__c).Base_UOM__r.Name;

                    if(productConversions.containsKey(tempOrderItem.Product__c+'-'+tempUOM)){
                        tempOrderItem.Conversion__c = productConversions.get(tempOrderItem.Product__c+'-'+tempUOM).Id;
                    }else if(productConversions.containsKey(tempBaseUOMMatchKey)){
                        tempOrderItem.Conversion__c = productConversions.get(tempBaseUOMMatchKey).Id;
                    }else{
                        errorStagingMap.put(os.Id, processErrorMessage(errorStagingMap.get(os.Id), 'The product ' + os.Product_Code__c + ' does not have a valid Conversion to use. (if unable to provide a UOM Name, the Product must have a Base UOM.)'));
                        tempHasError = true;
                    }
                }

                orderRecordStagingMap.put(tempOrder.Id, os);

                tempCombinationKey = String.valueOf(tempOrder.Id) + tempOrderItem.Product__c;
                if(!orderProductStagingMap.containsKey(tempCombinationKey)){
                    orderProductStagingMap.put(tempCombinationKey, os);
                }else{
                    errorStagingMap.put(os.Id, processErrorMessage(errorStagingMap.get(os.Id), 'Duplicate product on Order ' + os.Order_Number__c));
                    tempHasError = true;
                }

                tempInvoiceDistributorCombinationKey = os.Invoice_Number__c + oMap.get(os.SIF_ID__c).Account__r.Name;
                if(!orderDuplicateMapCheck.containsKey(tempInvoiceDistributorCombinationKey)){
                    orderDuplicateMapCheck.put(tempInvoiceDistributorCombinationKey, os);
                }else{
                    Order_Staging__c tempOrderStaging = orderDuplicateMapCheck.get(tempInvoiceDistributorCombinationKey);

                    if(tempOrderStaging.SIF_ID__c != os.SIF_ID__c){
                        errorStagingMap.put(os.Id, processErrorMessage(errorStagingMap.get(os.Id), 'Duplicate Invoice Number on Order ' + os.Order_Number__c));
                        tempHasError = true;
                    }
                }

                if(!tempHasError){
                    orderMapToInsert.put(tempOrder.Id, tempOrder);
                    oItemList.add(tempOrderItem);
                }
            }
            catch(exception e) {
                System.debug(e.getLineNumber() + '-----' + e.getMessage());
                errorStagingMap.put(os.Id, processErrorMessage(errorStagingMap.get(os.Id), e.getMessage()));
            }
        }

        orderListToInsert = new List<Order__c>(orderMapToInsert.values());

        TriggerUtility.BY_PASS_ORDER_FILECOUNTER_UPDATE = true;

        if(!oItemList.isEmpty())
            orderItemSaveResults = database.upsert(oItemList, false);

        if(!orderListToInsert.isEmpty()) {
            System.debug('orderListToInsert :: '+orderListToInsert);
            orderSaveResults = database.update(orderListToInsert, false);
        }

        if(!orderSaveResults.isEmpty()){
            for(Integer x = 0; x < orderSaveResults.size(); x++){
                Order__c tempOrder = orderListToInsert[x];
                Order_Staging__c tempStaging = orderRecordStagingMap.get(tempOrder.Id);

                if(!orderSaveResults[x].isSuccess()){
                    for(Database.Error error : orderSaveResults[x].getErrors()){
                        errorStagingMap.put(tempStaging.Id, processErrorMessage(errorStagingMap.get(tempStaging.Id), 'The field(s): ' + error.getFields() + ' have caused the error: ' + error.getMessage()));
                    }
                }else{
                    stagingsToDelete.add(tempStaging.Id);
                }
            }
        }

        if(!orderItemSaveResults.isEmpty()){
            for(Integer x = 0; x < orderItemSaveResults.size(); x++){
                Order_Item__c tempOrderItem = oItemList[x];
                String tempCombination = String.valueOf(tempOrderItem.Order_Form__c)+tempOrderItem.Product__c;
                Order_Staging__c tempStaging = orderProductStagingMap.get(tempCombination);

                if(!orderItemSaveResults[x].isSuccess()){
                    for(Database.Error error : orderItemSaveResults[x].getErrors()){
                        errorStagingMap.put(tempStaging.Id, processErrorMessage(errorStagingMap.get(tempStaging.Id), 'The field(s): ' + error.getFields() + ' have caused the error: ' + error.getMessage()));
                    }
                }else{
                    stagingsToDelete.add(tempStaging.Id);
                }
            }
        }

        for(Id stagingId : errorStagingMap.keySet()){
            String errorMessage = errorStagingMap.get(stagingId);
            Order_Staging__c tempStaging = newMap.get(stagingId);

            tempStaging.Id.addError(errorMessage.escapeCSV(), true);
        }

        database.delete(new List<Id>(stagingsToDelete));
    }

    public static String processErrorMessage(String errorMessages, String errorString){
        if(String.isEmpty(errorMessages)){
            return errorString;
        }else{
            String tempString = errorMessages;

            return tempString + '*' + errorString;
        }
    }
}