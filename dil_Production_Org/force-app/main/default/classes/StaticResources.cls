public class StaticResources {

    public static Boolean byPasspopulateExternalSystemId = false;
    public static Boolean byPassAccConfigTrigger         = false;
    public static Boolean byPasscheckStockAllocation     = false;
    public static Boolean byPassComputeStatus            = false;
    public static Boolean byPassTaskTrigger              = false;
    public static Boolean byPassCreateEForm              = false;
    public static Boolean byPassMonthTrigger             = false;
    public static Boolean byPassStartOrEndOfDayTrigger   = false;
    public static Boolean byPassStopSubmittedTaskDML     = false;
    public static Boolean changesMadeByWebService        = false;
}