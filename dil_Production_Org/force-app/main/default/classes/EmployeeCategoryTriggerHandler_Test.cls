@isTest
private class EmployeeCategoryTriggerHandler_Test {

    @isTest static void insertEmpCategory() {
        
        String accoundDistributorRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distributor/ Direct').getRecordTypeId();
        String sasRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('SAS').getRecordTypeId();
        
        //Insert UOM
        UOM__c uom = new UOM__c(
        Unique_UOM__c = 'KG',
        Name = 'KG',
        SAP_Code__c = 'KG'
        );
        Insert uom;
        
        //Insert Category
        Category__c cat = new Category__c(
        Category_Code__c = '0001',
        Dataloader_ID__c = '0001',
        EIS_Category__c = 'Pork',
        SAP_Code__c = '0001',
        UOM__c = uom.Id
        );
        Insert cat;
        
        //Insert Account Record for Customer And Distributor Records.
        Account disAcc = new Account(
        Active__c = TRUE,
        Name = 'distributorAccount',
        AccountNumber = '12345',
        General_Trade__c = TRUE,
        Delivery_Time_From__c = '5:00 AM',
        Delivery_Time_To__c = '6:00 PM',
        RecordTypeId = accoundDistributorRecordTypeId
        );
        Insert disAcc;
        
        Contact con = new Contact(
        FirstName = 'First1',
        LastName = 'Last1',
        AccountId = disAcc.id,
        Is_Primary__c = FALSE,
        RecordTypeId = sasRecordTypeId
        );
        Insert con;
        
        Employees_Category__c empCat1 = new Employees_Category__c(
        Active__c = TRUE,
        Category__c = cat.Id,
        Contact__c = con.Id,
        Primary_Category__c = TRUE,
        Unique__c = '000001'
        );
        
        Employees_Category__c empCat2 = new Employees_Category__c(
        Active__c = TRUE,
        Category__c = cat.Id,
        Contact__c = con.Id,
        Primary_Category__c = TRUE,
        Unique__c = '000002'
        );
        
        Insert empCat1;
        Insert empCat2;
    }
}