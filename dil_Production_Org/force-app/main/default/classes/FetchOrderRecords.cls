@RestResource(urlMapping='/FetchOrderRecords/*')
global with sharing class FetchOrderRecords {


    @HttpPost
    global static List<WebServiceResponse.OrderResponse> doPost() {

        String requestJSON  = RestContext.request.requestBody.toString();

        List<String> poNumCompleteList = new List<String>();
        List<String> accountIdList     = new List<String>();
        List<WebServiceResponse.OrderResponse> OrderResponseList = new List<WebServiceResponse.OrderResponse>();
        Map<Integer, WebServiceResponse.OrderResponse> IntegerVsResponseMap = new Map<Integer, WebServiceResponse.OrderResponse>();

        try { // DeSerialize the input String.

            OrderResponseList = (List<WebServiceResponse.OrderResponse>) JSON.deserialize( requestJSON
                                                                                         , List<WebServiceResponse.OrderResponse>.class
                                                                                         );
        } catch(Exception e) { // If Exception create error response and return.

            System.debug('exception :' + e);
            OrderResponseList.add(new WebServiceResponse.OrderResponse( null
                                                                      , new List<Id>()
                                                                      , false
                                                                      , new List<String>{e.getMessage()}
                                                                      ));
            return OrderResponseList;

        }

        Integer countOfRecords = 0;

        // Create Map of count vs WebServiceResponse.OrderResponse Class
        for(WebServiceResponse.OrderResponse orderResponseItr : OrderResponseList) {

            IntegerVsResponseMap.put(countOfRecords++, orderResponseItr);

        }

        // Create poNumList and accountIdList to be passed in "IN" clause of query.
        for(Integer countItr : IntegerVsResponseMap.keySet()) {

            if(String.isNotBlank(IntegerVsResponseMap.get(countItr).poNumber)) {

                poNumCompleteList.addAll( new List<String>{  IntegerVsResponseMap.get(countItr).poNumber
                                                          , (IntegerVsResponseMap.get(countItr).poNumber) + '-A'
                                                          }
                                        );
            }

            if(String.isNotBlank(IntegerVsResponseMap.get(countItr).accountId)) {
                accountIdList.add(IntegerVsResponseMap.get(countItr).accountId);
            }

        }


        List<Order__c> orderQueryList;
        try { // Query on Order__c to find matching records.
            orderQueryList = [SELECT Id
                                   , Order_Reference_No__c
                                   , LastModifiedDate
                                   , Status__c
                                   , PO_No__c
                                   , Account__c
                                   , Soup_Entry_Id__c
                                FROM Order__c
                               WHERE PO_No__c   IN :poNumCompleteList
                                 AND Account__c IN :accountIdList
                             ];
        } catch(Exception e) { // If Exception create error response and return.

            System.debug('exception :' + e);
            OrderResponseList.add(new WebServiceResponse.OrderResponse( null
                                                                      , new List<Id>()
                                                                      , false
                                                                      , new List<String>{e.getMessage()}
                                                                      ));
            return OrderResponseList;

        }

        List<WebServiceResponse.OrderResponse> OrderResponseRetList = new List<WebServiceResponse.OrderResponse>();

        if(  null != orderQueryList
          && !orderQueryList.isEmpty()
          ) {

            for(Integer countItr : IntegerVsResponseMap.keySet()) {

                for(Order__c orderItr : orderQueryList) {

                    if(  (  IntegerVsResponseMap.get(countItr).poNumber == orderItr.PO_No__c
                         || IntegerVsResponseMap.get(countItr).poNumber + '-A' == orderItr.PO_No__c
                         )
                      && IntegerVsResponseMap.get(countItr).accountId == orderItr.Account__c
                      ) {

                        WebServiceResponse.OrderResponse resObj = new WebServiceResponse.OrderResponse( orderItr.Id
                                                                                                      , new List<Id>()
                                                                                                      , true
                                                                                                      , new List<String>()
                                                                                                      , orderItr.Order_Reference_No__c
                                                                                                      , String.valueOf(DateTime.now())
                                                                                                      , String.valueOf(orderItr.LastModifiedDate)
                                                                                                      , orderItr.Status__c
                                                                                                      , orderItr.Account__c
                                                                                                      , orderItr.PO_No__c
                                                                                                      , orderItr.Soup_Entry_Id__c
                                                                                                      );
                        OrderResponseRetList.add(resObj);

                    }

                }

            }

        } else { // If no records found.
            OrderResponseRetList.add(new WebServiceResponse.OrderResponse( null
                                                                         , new List<Id>()
                                                                         , false
                                                                         , new List<String>{'No match found !!!'}
                                                                         , null
                                                                         , String.valueOf(DateTime.now())
                                                                         , null
                                                                         , null
                                                                         , null
                                                                         , null
                                                                         , null
                                                                         ));
        }



        return OrderResponseRetList;
    }
}