/**********************************************************
* @Author       Rozelle Villanueva
* @Date         10-20-2016
* @Description  Test class for batchDeleteSIFOrderItem
* @Revision(s)
**********************************************************/
@isTest
public class batchDeleteSIFOrderItem_Test {
    static testmethod void testDelete() {
        Id customerRecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Distributor Customer').getRecordTypeId();
        TestDataFactory tdf = new TestDataFactory();
        // account record, 'Distributor Direct'
        list <Account> accList = tdf.createTestAccount(1, True, True, 'Distributor/ Direct',null);
        
        //AARP: Added Customer Account
        Account customerAccount = new Account(
            Name = 'TestDistributor',
            RecordTypeId = customerRecordTypeId ,
            Sales_Organization__c  = 'Feeds',
            BU_Feeds__c = true,
            Branded__c = true,
            GFS__c = true,
            BU_Poultry__c = true,
            Active__c = true,
            AccountNumber = 'ACT11',
            Distributor__c = accList[0].Id
        );        
        
        insert customerAccount;        
        
        String contDSPRT = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Distributor Personnel').getRecordTypeId();
        String contSASRT = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('SAS').getRecordTypeId();
        // contact record
        list <Contact> contList = TestDataFactory.createContact(2, accList[0].Id);
        contList[0].RecordtypeId = contSASRT ;
        contList[1].RecordtypeId = contDSPRT ;
        insert contList;
        
        Date mnth = System.Today().addMonths(-3);
        Integer last3Month = mnth.month();
        Date invoiceDate = Date.newInstance(System.Today().year(), last3Month, 30);
        
         //AARP: set Account used to Customer Account
        // sales information record
        list <Order__c> sif = tdf.createTestTPOrder(1, customerAccount.Distributor__c, customerAccount.Id);
        sif[0].RecordTypeId = Schema.SObjectType.Order__c.getRecordTypeInfosByName().get('Actual Sales').getRecordTypeId();
        sif[0].Invoice_Date__c = invoiceDate;
        sif[0].Export_and_Delete__c = true;
        update sif;       
        
        Test.startTest();
           batchDeleteSIFOrderItem bc = new batchDeleteSIFOrderItem();   
           Database.executeBatch(bc, 2000); 
        Test.stopTest(); 
        System.assertEquals(accList.size() > 0, true);   
        System.assertEquals(contList.size() > 0, true);   
        System.assertEquals(sif.size() > 0, true);   
    }
    
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';

    static testmethod void testShedule() {
       Test.startTest();

       // Schedule the test job
       String jobId = System.schedule('batchDeleteSIFOrderItem',
                        CRON_EXP, 
                        new batchDeleteSIFOrderItem());
         
       // Get the information from the CronTrigger API object
       CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
                         NextFireTime
                         FROM CronTrigger WHERE id = :jobId];

       // Verify the expressions are the same
       System.assertEquals(CRON_EXP, 
          ct.CronExpression);

       // Verify the job has not run
       System.assertEquals(0, ct.TimesTriggered);

       // Verify the next time the job will run
       System.assertEquals('2022-03-15 00:00:00', 
       String.valueOf(ct.NextFireTime));
       Test.stopTest();
    }
}