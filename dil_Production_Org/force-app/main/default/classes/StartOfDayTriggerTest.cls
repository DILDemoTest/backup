@isTest
public class StartOfDayTriggerTest {
	@isTest
    public static void StartOfDayTriggerTest_1() {
        StaticResources.byPassStartOrEndOfDayTrigger = true;
        Start_of_Day__c sodObj = new Start_of_Day__c();
        sodObj.Field_Value_Mapping__c = '';
        Test.startTest();
        insert sodObj;
        Test.stopTest();
    }
    @isTest
    public static void StartOfDayTriggerTest_2() {
        Start_of_Day__c sodObj = new Start_of_Day__c();
        sodObj.Field_Value_Mapping__c = '';
        Test.startTest();
        insert sodObj;
        Test.stopTest();
    }
}