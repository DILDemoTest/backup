public with sharing class SIVStagingTrigger_Handler {
    
    
    
    public static void onAfterInsert(List<SIV_Staging__c> newSIVStagingList){
        TriggerFlagControl__c settings = TriggerFlagControl__c.getInstance();
        if(settings!=null){
            if(settings.SIV_CreateInventoryLineFromStaging__c == true){
                Map<String,Inventory_Item__c> inventoryItemMap = new Map<String,Inventory_Item__c>();
                List<Inventory_Line__c> inventoryLineForUpsert = new List<Inventory_Line__c>();
                Map<String,Inventory_Line__c> inventoryLineMap = new Map<String,Inventory_Line__c>();
                Set<String> stagingKey = new Set<String>();
                Set<String> skuCodes = new Set<String>();
                Set<Id> invItemIds = new Set<Id>();
                Set<String> dataloaderIds = new Set<String>();
                Map<String,Id> conversionLoaderIds = new Map<String,Id>();
                Set<String> customerIds = new Set<String>();
                Map<String,Id> inventoryIdsPerCustomerMap = new Map<String,Id>();
                Map<String,Id> customProductMap = new Map<String,Id>();
                
                for(SIV_Staging__c siv : newSIVStagingList){
                    String key = String.valueOf(transposeDevuelto(siv.Customer__c)) + '/' + String.valueOf(transposeDevuelto(siv.Material__c));
                    System.debug('\n\n\nSTAGING KEY PER SIV : ' + key + '\n\n\n');
                    stagingKey.add(key);
                    skuCodes.add(String.valueOf(transposeDevuelto(siv.Material__c)));
                    System.debug('\n\n\nSKU CODE : ' + String.valueOf(transposeDevuelto(siv.Material__c)) + '\n\n\n');
                    dataloaderIds.add(String.valueOf(transposeDevuelto(siv.Material__c)) + '/' + siv.UOM__c);
                    System.debug('\n\n\nSKU CODE : ' + String.valueOf(transposeDevuelto(siv.Material__c)) + '/' + siv.UOM__c + '\n\n\n');
                    customerIds.add(String.valueOf(transposeDevuelto(siv.Customer__c)));
                    System.debug('\n\n\nCUSTOMER NUMBER : ' + String.valueOf(transposeDevuelto(siv.Customer__c)) + '\n\n\n');
                }
                if(stagingKey.size()>0){
                    
                    for(Inventory__c inv : [SELECT Id,Account_Number__c FROM Inventory__c WHERE Account_Number__c IN : customerIds]){
                        inventoryIdsPerCustomerMap.put(inv.Account_Number__c,inv.Id);
                    }
                    
                    Map<String,Conversion__c> conversionMap = new Map<String,Conversion__c>();
                    for(Conversion__c conv : [Select Dataloader_ID__c, UOM_name__c, Product__c,Conversion_Rate__c From Conversion__c Where Dataloader_ID__c IN :dataloaderIds]){
                        conversionMap.put(conv.Dataloader_ID__c,conv);
                        conversionLoaderIds.put(conv.Dataloader_ID__c,conv.Id);
                        
                    }
                    System.debug('\n\n\nCONVERSION MAP : ' + conversionMap + '\n\n\n');
                    System.debug('\n\n\nCONVERSION LOADER IDS : ' + conversionLoaderIds + '\n\n\n');
                    for(Custom_Product__c cprod : [SELECT Id,SKU_Code__c FROM Custom_Product__c WHERE SKU_Code__c IN : skuCodes]){
                        customProductMap.put(cprod.SKU_Code__c,cprod.Id);
                    }
                    
                    for(Inventory_Item__c invItem : [SELECT Id,SIV_Staging_Key__c,SKU_Code__c FROM Inventory_Item__c WHERE SIV_Staging_Key__c IN : stagingKey]){
                        inventoryItemMap.put(invItem.SIV_Staging_Key__c,invItem);
                        invItemIds.add(invItem.Id);
                    }
                    List<Inventory_Item__c> inventoryItemForInsert = new List<Inventory_Item__c>();
                    for(String str : stagingKey){
                        if(!inventoryItemMap.containsKey(str)){
                            //SPLIT STAGING KEY TO CUSTOMER AND MATERIAL 
                            List<String> stagingKeyList = str.split('/');
                            if(stagingKeyList.size()==2){
                                //CREATE INVENTORY ITEM
                                String customerNo = stagingKeyList[0];
                                String materialCode = stagingKeyList[1];
                                if(String.isNotBlank(customerNo) && String.isNotBlank(materialCode)){
                                    Inventory_Item__c invItem = new Inventory_Item__c();
                                    if(customProductMap.containsKey(materialCode) && inventoryIdsPerCustomerMap.containsKey(customerNo)){
                                        invItem.Inventory__c = inventoryIdsPerCustomerMap.get(customerNo);
                                        invItem.Product__c = customProductMap.get(materialCode);
                                        inventoryItemForInsert.add(invItem);
                                    }
                                }
                            }
                            
                        }
                    }
                    if(inventoryItemForInsert.size()>0){
                        insert inventoryItemForInsert;
                    }
                    
                    inventoryItemMap.clear();
                    invItemIds.clear();
                    for(Inventory_Item__c invItem : [SELECT Id,SIV_Staging_Key__c,SKU_Code__c FROM Inventory_Item__c WHERE SIV_Staging_Key__c IN : stagingKey]){
                        inventoryItemMap.put(invItem.SIV_Staging_Key__c,invItem);
                        invItemIds.add(invItem.Id);
                    }
                    
                    for(Inventory_Line__c invLine: [SELECT Id,Account__c,Age1__c,Age__c,Alt_UOM__c,ALT_UOM_Name__c,Batch_Code__c,Begining_Balance__c,Date_Manufactured__c,Date_Validated__c,Depleted__c,Ending_Balance__c,Incoming_Quantity__c,Inventory_Health__c,Inventory_Item__c,Inventory_Item_ID__c,Inventory_Item_Name__c,Inventory_Line_ID__c,Inventory_Rate_Accuracy__c,LineKey__c,MAA__c,MEGAKEY__c,Product__c,Safety_Stock_Level__c,Shelf_Life_Days__c,SIF_Quantity__c,SI_No__c,SIV_Value__c,So_No__c,UOM__c,UOM__r.Dataloader_ID__c,UOM_Conversion_Name__c,Updated__c,UpdatedBegBalance__c,Validated_Incoming_Quantity__c,Validated_Quantity__c,Validate_Incoming_Quantity__c,Validate_Quantity__c,Weight_Quantity__c,Weight_UOM__c,Weight_UOM_Name__c FROM Inventory_Line__c WHERE Inventory_Item__c IN : invItemIds]){
                        inventoryLineMap.put(invLine.MEGAKEY__c,invLine);
                    }

                    for(SIV_Staging__c siv : newSIVStagingList){
                        String megaKey = String.valueOf(transposeDevuelto(siv.Customer__c)) + '/' + String.valueOf(transposeDevuelto(siv.Material__c)) + '/' + String.valueOf(transposeDevuelto(siv.Sales_Document__c));
                        String sivKey = String.valueOf(transposeDevuelto(siv.Customer__c)) + '/' + String.valueOf(transposeDevuelto(siv.Material__c));
                        String convDataloaderId = String.valueOf(transposeDevuelto(siv.Material__c)) + '/' + siv.UOM__c;
                        if(inventoryLineMap.containsKey(megaKey)){
                            //GET INVENTORY LINE AND UPDATE
                            Inventory_Line__c invLine = inventoryLineMap.get(megaKey);
                            if(String.isNotBlank(siv.Billing_Date__c)){
                                invLine.Billing_Date__c = stringToDate(siv.Billing_Date__c);
                                if(invLine.Date_Manufactured__c == null){
                                    invLine.Date_Manufactured__c = invLine.Billing_Date__c;
                                }
                            }
                            if(String.isBlank(invLine.SI_No__c)){
                               invLine.SI_No__c = String.valueOf(transposeDevuelto(siv.Reference_Doc_No__c)); 
                            }
                            if(invLine.UOM__r.Dataloader_ID__c == convDataloaderId){
                                
                                if(invLine.SIV_Value__c!=null){
                                    invLine.SIV_Value__c += transposeDevuelto(siv.SIV_Value__c);
                                }else{
                                    invLine.SIV_Value__c = transposeDevuelto(siv.SIV_Value__c);
                                }
                                if(invLine.Incoming_Quantity__c != null){
                                    invLine.Incoming_Quantity__c += transposeDevuelto(siv.Sales_Qty__c);
                                }else{
                                    invLine.Incoming_Quantity__c = transposeDevuelto(siv.Sales_Qty__c);
                                }
                                    
                            }else{
                                if(conversionMap.containsKey(convDataloaderId)){
                                    Decimal conversionRate = conversionMap.get(convDataloaderId).Conversion_Rate__c;
                                    if(siv.SIV_Value__c!=null){
                                        invLine.SIV_Value__c += transposeDevuelto(siv.SIV_Value__c) * conversionRate;
                                    }else{
                                        invLine.SIV_Value__c = transposeDevuelto(siv.SIV_Value__c) * conversionRate;
                                    }
                                    if(siv.Sales_Qty__c!=null){
                                        invLine.Incoming_Quantity__c += transposeDevuelto(siv.Sales_Qty__c) * conversionRate;
                                    }else{
                                        invLine.Incoming_Quantity__c = transposeDevuelto(siv.Sales_Qty__c) * conversionRate;
                                    }
                                    
                                }
                            }
                            inventoryLineMap.put(megaKey,invLine);
                            
                        }else{
                            //GET INVENTORY ITEM AND CREATE INVENTORY LINE
                            if(inventoryItemMap.containsKey(sivKey)){
                                if(inventoryLineMap.containsKey(megaKey)){
                                    //GET INVENTORY LINE AND UPDATE
                                    Inventory_Line__c invLine = inventoryLineMap.get(megaKey);
                                    if(String.isNotBlank(siv.Billing_Date__c)){
                                        invLine.Billing_Date__c = stringToDate(siv.Billing_Date__c);
                                        if(invLine.Date_Manufactured__c == null){
                                            invLine.Date_Manufactured__c = invLine.Billing_Date__c;
                                        }
                                    }
                                    if(String.isBlank(invLine.SI_No__c)){
                                       invLine.SI_No__c = String.valueOf(transposeDevuelto(siv.Reference_Doc_No__c)); 
                                    }
                                    if(conversionLoaderIds.containsKey(convDataloaderId)){
                                        Id UOMId = conversionLoaderIds.get(convDataloaderId);
                                        if(invLine.UOM__c != null){
                                            if(invLine.UOM__c == UOMId){
                                                if(invLine.SIV_Value__c!=null){
                                                    invLine.SIV_Value__c += transposeDevuelto(siv.SIV_Value__c);
                                                }else{
                                                    invLine.SIV_Value__c = transposeDevuelto(siv.SIV_Value__c);
                                                }
                                                if(invLine.Incoming_Quantity__c != null){
                                                    invLine.Incoming_Quantity__c += transposeDevuelto(siv.Sales_Qty__c);
                                                }else{
                                                    invLine.Incoming_Quantity__c = transposeDevuelto(siv.Sales_Qty__c);
                                                }    
                                            }else{
                                                Decimal conversionRate = conversionMap.get(convDataloaderId).Conversion_Rate__c;
                                                if(siv.SIV_Value__c!=null){
                                                    invLine.SIV_Value__c += transposeDevuelto(siv.SIV_Value__c) * conversionRate;
                                                }else{
                                                    invLine.SIV_Value__c = transposeDevuelto(siv.SIV_Value__c) * conversionRate;
                                                }
                                                if(siv.Sales_Qty__c!=null){
                                                    invLine.Incoming_Quantity__c += transposeDevuelto(siv.Sales_Qty__c) * conversionRate;
                                                }else{
                                                    invLine.Incoming_Quantity__c = transposeDevuelto(siv.Sales_Qty__c) * conversionRate;
                                                }    
                                            }
                                            
                                        }
                                    }
                                    inventoryLineMap.put(megaKey,invLine);
                                }else{
                                    Inventory_Line__c invLineForUpsert = new Inventory_Line__c();
                                    invLineForUpsert.Inventory_Item__c =  inventoryItemMap.get(sivKey).Id;
                                    invLineForUpsert.SIV_Value__c = transposeDevuelto(siv.SIV_Value__c);
                                    invLineForUpsert.Incoming_Quantity__c = transposeDevuelto(siv.Sales_Qty__c);
                                    invLineForUpsert.SI_No__c = String.valueOf(transposeDevuelto(siv.Reference_Doc_No__c));
                                    invLineForUpsert.SO_No__c = siv.Sales_Document__c;
                                    
                                    if(conversionMap.containsKey(convDataloaderId)){
                                        invLineForUpsert.UOM__c = conversionMap.get(convDataloaderId).Id;    
                                    }
                                    if(String.isNotBlank(siv.Billing_Date__c)){
                                        invLineForUpsert.Billing_Date__c = stringToDate(siv.Billing_Date__c);
                                        invLineForUpsert.Date_Manufactured__c = invLineForUpsert.Billing_Date__c;
                                    }
                                    if(invLineForUpsert.UOM__c!=null){
                                        inventoryLineMap.put(megaKey,invLineForUpsert);    
                                    }else{
                                        String uomStr = siv.UOM__c;
                                        String matNum = siv.Material__c;
                                        siv.addError('Missing UOM : ' + uomStr + ' for Material : ' + matNum);
                                    }
                                    
                                }
                            }
                        }
                    }
                    if(inventoryLineMap.size()>0){
                        upsert inventoryLineMap.values();
                        
                    }
                }
            }    
        }
        
    }
    
    
    public static Decimal transposeDevuelto(String stringToTranspose){
        if(stringToTranspose!=null){
            if(String.isNotBlank(stringToTranspose)){
                if(stringToTranspose.endsWith('-')){
                    String traValue = stringToTranspose.remove('-');
                    traValue = '-' + traValue;
                    return Decimal.valueOf(traValue);
                }else{
                    return Decimal.valueOf(stringToTranspose);
                }
            }    
        }
        return 0;
    }
    
    public static Date stringToDate(String strDate){
        //ASSUMES THE FORMAT YYYYMMDD
        if(String.isNotBlank(strDate)){
            if(strDate.length() == 8){
                Integer yearx = Integer.valueOf(strDate.substring(0,4));
                Integer monthx = Integer.valueOf(strDate.substring(4,6));
                Integer dayx = Integer.valueOf(strDate.substring(6,8));
                return Date.newInstance(yearx, monthx,dayx);
            }
            return null;
        }
        return null;
    }

    
    
}