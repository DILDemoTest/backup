/**
    * AUTHOR        : LENNARD PAUL M SANTOS(DELOITTE)
    * DESCRIPTION   : HANDLES ALL TRIGGER EVENT PROCESS.
    * HISTORY       : (ANY UPDATES TO THE CODE MUST BE LOGGED HERE)
                    : JUN.14.2016 - CREATED.
**/
public with sharing class CustomProductTrigger_Handler {
    /**
    * AUTHOR        : LENNARD PAUL M SANTOS(DELOITTE)
    * DESCRIPTION   : HANDLES BEFORE INSERT TRIGGER EVENT
    * PARAM         : TRIGGER.NEW(LIST<Custom_Product__c>)
    * RETURN        : NONE
    * HISTORY       : (ANY UPDATES TO THE CODE MUST BE LOGGED HERE)
                    : JUN.14.2016 - CREATED.
    **/
    public static void onBeforeInsert(List<Custom_Product__c> customProductList){
        List<Market__c> marketList = new List<Market__c>();
        Set<Id> BUIds = new Set<Id>();
        TriggerFlagControl__c settings = TriggerFlagControl__c.getInstance();
        //CHECK CUSTOM SETTING VALUE IF ENABLED
        if(settings != null){
            if(settings.reflectPicklistValue__c == true){
                for(Custom_Product__c prod : customProductList){
                    if(prod.Market__c != null){
                        BUIds.add(prod.Market__c);
                    }
                }
                for(Market__c m : [SELECT Id, Name FROM Market__c WHERE Id IN : BUIds]){
                    marketList.add(m);
                }
                for(Custom_Product__c prod : customProductList){
                    if(prod.Market__c!=null){
                        for(Market__c m : marketList){
                            if(prod.Market__c == m.Id){
                                prod.Business_UnitReportChart__c = m.Name;
                            }
                        }
                    }
                }
            }
        }
    }
    /**
    * AUTHOR        : LENNARD PAUL M SANTOS(DELOITTE)
    * DESCRIPTION   : HANDLES BEFORE UPDATE TRIGGER EVENT
    * PARAM         : TRIGGER.NEW(LIST<Custom_Product__c>), TRIGGER.OLDMAP(MAP<Id,Custom_Product__c>)
    * RETURN        : NONE
    * HISTORY       : (ANY UPDATES TO THE CODE MUST BE LOGGED HERE)
                    : JUN.14.2016 - CREATED.
    **/
    public static void onBeforeUpdate(List<Custom_Product__c> customProductList, Map<Id,Custom_Product__c> oldCustomProductMap){
        List<Market__c> marketList = new List<Market__c>();
        Set<Id> BUIds = new Set<Id>();
        TriggerFlagControl__c settings = TriggerFlagControl__c.getInstance();
        //CHECK CUSTOM SETTING VALUE IF ENABLED
        if(settings != null){
            if(settings.reflectPicklistValue__c == true){
                for(Custom_Product__c prod : customProductList){
                    Custom_Product__c oldProduct = oldCustomProductMap.get(prod.Id);
                    if(oldProduct.Market__c != prod.Market__c){
                        if(prod.Market__c != null){
                            BUIds.add(prod.Market__c);
                        }    
                    }
                }
                for(Market__c m : [SELECT Id, Name FROM Market__c WHERE Id IN : BUIds]){
                    marketList.add(m);
                }
                for(Custom_Product__c prod : customProductList){
                    if(prod.Market__c!=null){
                        for(Market__c m : marketList){
                            if(prod.Market__c == m.Id){
                                prod.Business_UnitReportChart__c = m.Name;
                            }
                        }
                    }
                }
            }
        }
    }
}