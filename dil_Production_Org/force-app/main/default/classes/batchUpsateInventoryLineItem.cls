global class batchUpsateInventoryLineItem implements Database.Batchable<sObject>, Schedulable{

    global void execute(SchedulableContext SC) {
      Database.executeBatch(new batchUpsateInventoryLineItem (), 2000);
   }


   global Database.querylocator start(Database.BatchableContext BC){         
       return Database.getQueryLocator([Select UpdatedBegBalance__c, Begining_Balance__c, Ending_Balance__c, Validate_Quantity__c, Validated_Quantity__c, SIF_Quantity__c, 
                                            Incoming_Quantity__c, Validate_Incoming_Quantity__c, Validated_Incoming_Quantity__c
                                            From Inventory_Line__c
                                            Where SystemModStamp >= LAST_N_DAYS:1]);
   }//end method  
   
   global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<Inventory_Line__c> lineItemList = new List<Inventory_Line__c>();
        for(sObject s : scope){
            lineItemList.add((Inventory_Line__c)s);        
        }  
        
        for(Inventory_Line__c item : lineItemList){
        
        if (item.UpdatedBegBalance__c == false){
            if (item.Validate_Quantity__c){
                item.Begining_Balance__c = item.Validated_Quantity__c;
                item.UpdatedBegBalance__c = true;
                item.SIF_Quantity__c = 0;
                item.Validate_Quantity__c = false;
                item.Validated_Quantity__c = 0;
            }else{
                 item.Begining_Balance__c = item.Ending_Balance__c;
                  item.UpdatedBegBalance__c = true;
                    item.SIF_Quantity__c = 0;
                    item.Validate_Quantity__c = false;
                    item.Validated_Quantity__c = 0;
            }
        }
        
        }       
        
        if (lineItemList.size() > 0)
            update lineItemList;
            
   }  // end method
   
   global void finish(Database.BatchableContext BC){     
    System.debug('FINISH');
   }//end method 
   
}//end class