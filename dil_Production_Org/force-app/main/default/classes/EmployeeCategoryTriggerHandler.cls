/**
 * AUTHOR       : THIRD PILLAR
 * DESCRIPTION  : PREVENT MORETHAN ONE PRIMARY CATEGORY PER CONTACT
**/
public class EmployeeCategoryTriggerHandler {

    public static void setPrimaryCategory(List<Employees_Category__c> empCatList){
            
        List<String> contList = new List<String>();
        List<String> catList = new List<String>();
        List<String> empCatIdList = new List<String>();
        List<Employees_Category__c> empCatResList = new List<Employees_Category__c>();
        List<Employees_Category__c> empCatUpdateList = new List<Employees_Category__c>();
        
        for(Employees_Category__c ec : empCatList) {
            
            if(ec.Primary_Category__c) {
                contList.add(ec.Contact__c);
                catList.add(ec.Category__c);
                
                if(ec.Id != NULL) {
                    empCatIdList.add(ec.Id);
                }
            }
        }
        
        empCatResList = [SELECT Id, Contact__c, Category__c FROM Employees_Category__c WHERE Contact__c IN: contList AND Primary_Category__c =: TRUE AND Active__c =: TRUE AND Id NOT IN:empCatIdList];

        for(Employees_Category__c ecp : empCatResList) {
        
                ecp.Primary_Category__c = FALSE;
                empCatUpdateList.add(ecp);
        }

        if(!empCatUpdateList.isEmpty()) {
            Update empCatUpdateList;
        }
    }
}