@isTest
private class MOMFormControllerTest {

    private static testMethod void fetchUsersTest() {
        //List<MOMFormController.SearchWrapper>lstSrchWrap = MOMFormController.fetchUsers();
        new MOMFormController(null);
    }

    private static testMethod void  getExistingMOMTest() {
         Id momId = DIL_TestDataFactory.getMoms(1,true)[0].Id;
         MOM__c objMom = MOMFormController.getExistingMOM(momId);
    }

    private static testMethod void saveMOMTest() {
        Id momId = DIL_TestDataFactory.getMoms(1,true)[0].Id;
        List<Month__c> monthList = DIL_TestDataFactory.getMonth(1, true);
        List<User> userList = [SELECT Id FROM USER WHERE isActive = true LIMIT 2];
        String momObjStr = '{"Id":"'+ momId +'","Attendees__c":"0050l000000EH2IAAW;0050l000000Ed46AAC","Meeting_Date__c":"09/28/2018","Minutes_By__c":"'+userList.get(0).Id+'","Month__c":"'+monthList.get(0).Id+'","Status__c":"Saved","Time_to_Finish__c":"07:00 AM","Time_to_Start__c":"07:00 AM","Venue__c":"Test Venue","Copies_to__c":"Test Copies to","Objective__c":"Test Objective"}';
        String momDetailsStr = '[{"Date_Raised__c":"10/17/2018","Deadline__c":"10/14/2018","Topic__c":"Test  TOPIC","Aggrements__c":"Test TOPIC","Person_Group_Responsible__c":"Test PERSON/GROUP RESPONSIBLE"}]';
        momId = MOMFormController.saveMOM(momObjStr,momDetailsStr);
    }
}