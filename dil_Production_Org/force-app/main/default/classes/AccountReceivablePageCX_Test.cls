@isTest
private class AccountReceivablePageCX_Test {
    
    @testSetup static void setupData(){
        List<Pricing_Condition__c> pricingConditionList = new List<Pricing_Condition__c>();
        List<Market__c> marketList = new List<Market__c>();
        List<Custom_Product__c> parentProductList = new List<Custom_Product__c>();
        Id distributorRecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Distributor/ Direct').getRecordTypeId();
        Id customerRecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Distributor Customer').getRecordTypeId();
        Id actualSalesRecordTypeId = Schema.sObjectType.Order__c.getRecordTypeInfosByName().get('Actual Sales').getRecordTypeId();
        Pricing_Condition__c nationalPricing = new Pricing_Condition__c(
            Name = 'NATIONAL',
            SAP_Code__c = '12345'
            );
        pricingConditionList.add(nationalPricing);
        Pricing_Condition__c regionalPricing = new Pricing_Condition__c(
            Name = 'REGIONAL',
            SAP_Code__c = '89798'
            );
        pricingConditionList.add(regionalPricing);
        Pricing_Condition__c segmentPricing = new Pricing_Condition__c(
            Name = 'SEGMENT',
            SAP_Code__c = '098098'
            );
        pricingConditionList.add(segmentPricing);
        insert pricingConditionList;
        
        
        Account distributorAccount = new Account(
            Name = 'TestDistributor',
            RecordTypeId = distributorRecordTypeId,
            Sales_Organization__c  = 'Feeds',
            General_Trade__c = true,
            BU_Feeds__c = true,
            Branded__c = true,
            GFS__c = true,
            BU_Poultry__c = true,
            Active__c = true,
            AccountNumber = 'ACT11'
            );
        insert distributorAccount;
        

        Market__c meatMarket = new Market__c(
            Name = 'Meats',
            Active__c = true,
            Market_Code__c = '01'
            );
        marketList.add(meatMarket);
        Market__c gfsMarket = new Market__c(
            Name = 'GFS',
            Active__c = true,
            Market_Code__c = '02'
            );
        marketList.add(gfsMarket);
        Market__c poultryMarket = new Market__c(
            Name = 'Poultry',
            Active__c = true,
            Market_Code__c = '03'
            );
        marketList.add(poultryMarket);
        insert marketList;
        
        Warehouse__c wr = new Warehouse__c();
        wr.Account__c = distributorAccount.Id;
        wr.Address__c = distributorAccount.Name + distributorAccount.Id;
        wr.Warehouse_No__c = String.valueOf(distributorAccount.Id) + String.valueOf(System.now().millisecond());
        insert wr;
        distributorAccount.Warehouse__c = wr.Id;
        update distributorAccount;
                
        List<Account> accountForInsertList = new List<Account>();
        Account acc = new Account();
        acc.Name = 'Sample Grocery2';
        acc.RecordTypeId = customerRecordTypeId;
        acc.Sales_Organization__c  = 'Feeds';
        acc.BU_Feeds__c = true;
        acc.Branded__c = true;
        acc.GFS__c = true;
        acc.BU_Poultry__c = true;
        acc.Market__c = marketList.get(0).Id;
        acc.Distributor__c = distributorAccount.Id;
        acc.Warehouse__c = wr.Id;
        acc.AccountNumber = 'ACT11';
        acc.General_Trade__c  = true;
        acc.Active__c = true;
        accountForInsertList.add(acc);
        
        Account acc2 = new Account();
        acc2.Name = 'Sample Grocery3';
        acc2.RecordTypeId = customerRecordTypeId;
        acc2.Sales_Organization__c  = 'Feeds';
        acc2.BU_Feeds__c = true;
        acc2.Branded__c = true;
        acc2.GFS__c = true;
        acc2.BU_Poultry__c = true;
        acc2.Market__c = marketList.get(0).Id;
        acc2.Distributor__c = distributorAccount.Id;
        acc2.Warehouse__c = wr.Id;
        acc2.AccountNumber = 'ACT21';
        acc2.General_Trade__c  = true;
        acc2.Active__c = true;
        accountForInsertList.add(acc2);
        
        insert accountForInsertList;
        
        TriggerFlagControl__c settings = new TriggerFlagControl__c(
            SetupOwnerId = UserInfo.getOrganizationId(),
            reflectPicklistValue__c = true
            );
        insert settings;
        Custom_Product__c prodParent1 = new Custom_Product__c();
        prodParent1.Name = 'ParentProductFeeds';
        prodParent1.Business_Unit__c = 'Feeds';
        prodParent1.Feeds__c = true;
        prodParent1.Market__c = marketList.get(0).Id;
        prodParent1.SKU_Code__c = '111222333';
        parentProductList.add(prodParent1);
        
        Custom_Product__c prodParent2 = new Custom_Product__c();
        prodParent2.Name = 'ParentProductGFS';
        prodParent2.Business_Unit__c = 'GFS';
        prodParent2.Feeds__c = true;
        prodParent2.Market__c = marketList.get(1).Id;
        prodParent2.SKU_Code__c = '444555666';
        parentProductList.add(prodParent2);
        
        List<UOM__c> uomList = new List<UOM__c>();
        UOM__c uom1 = new UOM__c();
        uom1.Name = 'KG';
        
        UOM__c uom2 = new UOM__c();
        uom2.Name = 'PC';
        
        uomList.add(uom1);
        uomList.add(uom2);
        
        Insert uomList;
        
        if(parentProductList.size()>0){
            insert parentProductList;
        }
        List<Conversion__c> ConversionforInsertList = new List<Conversion__c>();
        for(Custom_Product__c customProduct : parentProductList){
            Conversion__c conversionRecord = new Conversion__c(
                Name = 'KG',
                Product__c = customProduct.Id,
                Numerator__c = 1,
                UOM__c = uomList[0].Id,
                Denominator__c = 1
                );
                
            ConversionforInsertList.add(conversionRecord);
            Conversion__c conversionRecordPC = new Conversion__c(
                Name = 'PC',
                Product__c = customProduct.Id,
                Numerator__c = 1,
                UOM__c = uomList[1].Id,
                Denominator__c = 1
                );
            ConversionforInsertList.add(conversionRecordPC);
        }
        if(ConversionforInsertList.size()>0){
            insert ConversionforInsertList;
        }
        
        //AARP: Added DSP Contact
        //CREATE SAS- Contact
        Id SASrecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('SAS').getRecordTypeId();
        Contact SASvar = new Contact(RecordTypeId=SASrecordTypeId, FirstName = 'xyzFirst', LastName = 'XyZLast', AccountId = distributorAccount.Id);
        insert SASvar;
        
        //CREATE DSP- Contact
        Id DSPrecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Distributor Personnel').getRecordTypeId();
        Contact DSPvar = new Contact(RecordTypeId=DSPrecordTypeId , FirstName = 'xyzSecond', LastName = 'XyZSecond', AccountId = distributorAccount.Id);
        insert DSPvar;        
        
        //AARP: Added DSP Contact to insert in Order
        Order__c orderRecord = new Order__c(
            Account__c = accountForInsertList.get(0).Id,
            RecordTypeId = actualSalesRecordTypeId,
            Invoice_No__c = '11111',
            SAS__c = SASvar.Id,
            DSP__c = DSPvar.Id
            );
        insert orderRecord;
        
        Order_Item__c orderItem = new Order_Item__c(
            Order_Form__c = orderRecord.Id,
            Product__c = prodParent1.Id,
            Conversion__c = ConversionforInsertList[0].Id,
            Invoice_Net_Total__c = 1000,
            Invoice_Quantity__c = 5
            );
         
        insert orderItem;
        
        //AARP: Added DSP Contact to insert in Order
        List<Order__c> orderForInsertList = new List<Order__c>();
        List<Order_Item__c> orderItemListForInsert = new List<Order_Item__c>();
        for(integer i = 0; i<=20; i++){
            Order__c ord = new Order__c(
                Account__c = accountForInsertList.get(0).Id,
                RecordTypeId = actualSalesRecordTypeId,
                Invoice_No__c = '11111-' + String.valueOf(i),
                SAS__c = SASvar.Id,
                DSP__c = DSPvar.Id
                );
            orderForInsertList.add(ord);
        }
        insert orderForInsertList;
        for(Order__c ord : orderForInsertList){
            Order_Item__c oli = new Order_Item__c(
                Order_Form__c = ord.Id,
                Product__c = prodParent1.Id,
                Conversion__c = ConversionforInsertList[0].Id,
                Invoice_Net_Total__c = 1000,
                Invoice_Quantity__c = 1
                );
            orderItemListForInsert.add(oli);
        }
        insert orderItemListForInsert;
        
        Order__c orderForUpdate = [SELECT Id,Outstanding_Amount1__c FROM Order__c WHERE Id = : orderRecord.Id];
        orderForUpdate.Payment_Made__c = 100;
        orderForUpdate.Returned_Item_Amount__c = 50;
        update orderForUpdate;
    }

    private static testMethod void test() {
        Id accountId = [SELECT Id FROM Account WHERE Name = : 'Sample Grocery2'].Id;
        Order__c orderRecordTest = [SELECT Id,Name,Invoice_No__c FROM Order__c WHERE Invoice_No__c = '11111'];
        Test.startTest();
            Order__c orderRecord = new Order__c();
            ApexPages.StandardController con = new ApexPages.StandardController(orderRecord);
            AccountReceivablePageCX ext = new AccountReceivablePageCX(con);
            ext.currentSIFInfo.Account__c = accountId;
            ext.initializePage();
            //ext.currentSIFInfo.Name = orderRecordTest.Name;
            ext.currentSIFInfo.Invoice_No__c = orderRecordTest.Invoice_No__c;
            ext.searchSIFs();
            ext.saveSIF2();
            ext.saveAndNewSIF2();
            
        Test.stopTest();
    }
    
    private static testMethod void testPagination() {
        Id accountId = [SELECT Id FROM Account WHERE Name = : 'Sample Grocery2'].Id;
        Order__c orderRecordTest = [SELECT Id,Name,Invoice_No__c FROM Order__c WHERE Invoice_No__c = '11111'];
        
        Test.startTest();
            Order__c orderRecord = new Order__c();
            ApexPages.StandardController con = new ApexPages.StandardController(orderRecord);
            AccountReceivablePageCX ext = new AccountReceivablePageCX(con);
            ext.currentSIFInfo.Account__c = accountId;
            ext.initializePage();
            ext.currentSIFInfo.Invoice_No__c = orderRecordTest.Invoice_No__c;
            ext.searchSIFs();
            ext.nextList();
            ext.previousList();
            ext.saveSIF2();
            ext.saveAndNewSIF2();
            
        Test.stopTest();
    }

}