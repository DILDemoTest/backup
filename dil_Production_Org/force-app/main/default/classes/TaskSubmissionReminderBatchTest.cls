@isTest
public class TaskSubmissionReminderBatchTest {
    // Method to create test Users records
    public static List<User> createUsers(Integer num) {
        Profile pf = [Select Id from profile where Name='Sales User'];
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','') ;
        Integer RandomId = Integer.valueOf(Math.rint(Math.random()*1000000));
        String uniqueName = orgId+dateString+RandomId;
        List<User> userList = new List<User>();
        for(integer i=0; i<= num; i++) {
            User user = new User(firstname = 'ABC'+i,
                         lastName = 'XYZ'+i,
                         email = uniqueName +i+ '@test' + orgId + '.org',
                         Username = uniqueName +i+ '@test' + orgId + '.org',
                         EmailEncodingKey = 'ISO-8859-1',
                         Alias = uniqueName.substring(18, 23),
                         TimeZoneSidKey = 'America/Los_Angeles',
                         LocaleSidKey = 'en_US',
                         LanguageLocaleKey = 'en_US',
                         ProfileId = pf.Id
                        );
            userList.add(user);
        }
        return userList;
    }

    // Method to create test Month records
    public static List<Month__c> createMonth(Integer num, List<User> userList) {
        List<Month__c> monthList = new List<Month__c>();
        for(Integer i=0; i<=num-2; i++) {
            monthList.add(new Month__c(Month__c=System.now().addMonths(1).format('MMMM'), Year__c=System.now().addMonths(1).format('YYYY'), OwnerId = userList[i].Id, Submitted_for_Approval__c = true));
        }
        return monthList;
    }

    // Test Method to check batch functionality
    @isTest
    public static void testBatchFailed_MCP_reminder_sent_True() {
        /*
        List<User> userList = createUsers(5);
        insert userList;
        List<Month__c> monthList = createMonth(5,userList);
        insert monthList;

        Test.startTest();
            TaskSubmissionReminderBatch obj = new TaskSubmissionReminderBatch();
            DataBase.executeBatch(obj);
            Integer invocations = Limits.getEmailInvocations();
        Test.stopTest();
        System.debug('invocations : '+invocations);
        */
        User testUser = DIL_TestDataFactory.createBulkUsers(1,true)[0];
        List<Month__c> lstMonths = DIL_TestDataFactory.getMonth(1,true);

        DateTime currentTime = datetime.now().addMonths(1);
        String nextMonth = currentTime.format('MMMMM');
        String year = currentTime.format('YYYY');

        for(Month__c objMonth : lstMonths){
            objMonth.OwnerId = testUser.Id;
            objMonth.Submitted_for_Approval__c = True;
            objMonth.Month__c = nextMonth;
            objMonth.Year__c = year;
        }
        update lstMonths;
        Database.executeBatch(new TaskSubmissionReminderBatch(Integer.valueOf(System.Label.Second_Reminder_Date + 1)));
    }

     @isTest
    public static void testBatchFailed_MCP_reminder_sent_False() {
        User testUser = DIL_TestDataFactory.createBulkUsers(1,true)[0];
        List<Month__c> lstMonths = DIL_TestDataFactory.getMonth(1,true);

        DateTime currentTime = datetime.now().addMonths(1);
        String nextMonth = currentTime.format('MMMMM');
        String year = currentTime.format('YYYY');

        for(Month__c objMonth : lstMonths){
            objMonth.OwnerId = testUser.Id;
            objMonth.Submitted_for_Approval__c = True;
            objMonth.Month__c = nextMonth;
            objMonth.Year__c = year;
        }
        update lstMonths;
        Database.executeBatch(new TaskSubmissionReminderBatch(Integer.valueOf(System.Label.Third_Reminder_Date)));
    }
}