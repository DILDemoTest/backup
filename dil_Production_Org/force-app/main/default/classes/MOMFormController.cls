public with sharing class MOMFormController {

    public MOMFormController(ApexPages.StandardController controller) {

    }

    public static final String TIME_AM = 'AM';
    public static final String TIME_PM = 'PM';

    @RemoteAction
    public static MOM__c getExistingMOM(Id momId) {
        List<MOM__c> momRecords = [
                                    SELECT Id
                                         , Attendees__c
                                         , Copies_to__c
                                         , Meeting_Date__c
                                         , Minutes_By__c
                                         , Minutes_By__r.Name
                                         , Month__c
                                         , Objective__c
                                         , Status__c
                                         , Time_to_Finish__c
                                         , Time_to_Start__c
                                         , Venue__c
                                         , eForm__r.OwnerId
                                         , CreatedBy.Name
                                         , CreatedById
                                         , eForm__c
                                         , (
                                             SELECT Id
                                                  , Aggrements__c
                                                  , Date_Raised__c
                                                  , Deadline__c
                                                  , Person_Group_Responsible__c
                                                  , Topic__c
                                              FROM MOM_Details__r
                                          ORDER BY CreatedDate
                                         )
                                     FROM MOM__c
                                    WHERE Id = :momId
                                ];

        List<Task> taskList = [
                                SELECT Id
                                     , Check_in__c
                                  FROM Task
                                 WHERE eForm__c = :momRecords[0].eForm__c
                              ];
        //System.debug('check in date :: '+taskList[0].Check_in__c);

        if( true == momRecords.isEmpty() || (taskList.size() > 0 && (taskList[0].Check_in__c == null || String.valueOf(taskList[0].Check_in__c) == ''))) {
            return new MOM__c();
        }
        return momRecords[0];
    }

    @RemoteAction
    public static String saveMOM(String momObjStr, String momDetailsStr) {
        
        StaticResources.changesMadeByWebService = true;
        System.debug('...momObjStr: ' + momObjStr);
        System.debug('...momDetailsStr: ' + momDetailsStr);
        Map<String, String> momObj = (Map<String, String>)JSON.deserialize(momObjStr, Map<String, String>.class);

        MOM__c mom = new MOM__c();
        if( String.isNotBlank(momObj.get('Meeting_Date__c'))) {
            mom.Meeting_Date__c = Date.parse(momObj.get('Meeting_Date__c'));
        }
        mom.Time_to_Start__c = convertStringToTime(momObj.get('Time_to_Start__c'));
        mom.Time_to_Finish__c = convertStringToTime(momObj.get('Time_to_Finish__c'));
        mom.Venue__c = momObj.get('Venue__c');
        mom.Copies_to__c = momObj.get('Copies_to__c');
        mom.Objective__c = momObj.get('Objective__c');
        mom.Status__c = momObj.get('Status__c');
        mom.Minutes_By__c = momObj.get('Minutes_By__c');
        mom.Attendees__c = momObj.get('Attendees__c');
        System.debug('Status__c :: '+mom.Status__c);
        if( momObj.containsKey('Id') ) {
            mom.Id = momObj.get('Id');
        }

        if( mom.Status__c == Label.Status_Submitted ) {
            mom.Submitted_Date_Time__c = System.now();
            Id eformId = [SELECT eForm__c FROM MOM__c WHERE Id = :mom.Id][0].eForm__c;
            System.debug('eformId :: '+eformId);
            Task objTask = [SELECT Id FROM Task WHERE eForm__c = :eformId];
            objTask.Task_Completion_Datetime__c = System.now();
            update objTask;
        }
        update mom;
        insertMOMDetails(mom.Id, momDetailsStr);
        StaticResources.changesMadeByWebService = false;

        return mom.Id;
    }

    public static void insertMOMDetails(Id momId, String momDetailsStr) {
        
        StaticResources.changesMadeByWebService = true;
        List<Map<String,String>> detailsList = (List<Map<String,String>>)JSON.deserialize(momDetailsStr, List<Map<String,String>>.class);
        System.debug('...detailsList: ' + detailsList);

        List<MOM_Details__c> momDetailsList = new List<MOM_Details__c>();
        for(Map<String, String> momDetailObj : detailsList) {

            System.debug('...momDetailObj: ' + momDetailObj);

            MOM_Details__c momDetail = new MOM_Details__c();
            momDetail.MOM__c = momId;
            if( String.isNotBlank(momDetailObj.get('Date_Raised__c')) ) {
                momDetail.Date_Raised__c = momDetailObj.get('Date_Raised__c');
            }
            if( String.isNotBlank(momDetailObj.get('Deadline__c')) ) {
                momDetail.Deadline__c = momDetailObj.get('Deadline__c');
            }

            momDetail.Aggrements__c = momDetailObj.get('Aggrements__c');
            momDetail.Topic__c = momDetailObj.get('Topic__c');
            momDetail.Person_Group_Responsible__c = momDetailObj.get('Person_Group_Responsible__c');
            momDetailsList.add(momDetail);
        }

        insert momDetailsList;
        StaticResources.changesMadeByWebService = false;
    }

    public static Time convertStringToTime(String timeString) {

        if(true == String.isBlank(timeString)) {
            return null;
        }
       String dateStr = String.valueOf(timeString);
       Boolean isPM = false;
       if( dateStr.endsWithIgnoreCase(TIME_PM) ) {
           isPM = true;
       }

       dateStr = dateStr.removeEndIgnoreCase(TIME_AM);
       dateStr = dateStr.removeEndIgnoreCase(TIME_PM);
       List<String> timeStr = dateStr.split(':');
       if(isPM) {
           Integer timeInt = Integer.valueOf(timeStr[0].trim());
           timeInt = timeInt < 12 ? timeInt + 12 : timeInt;
           timeStr[0] = String.valueOf( timeInt );
       }
       System.debug('...timeStr: ' + timeStr);
       Time myTime;
       switch on timeStr.size() {
           when 1 {  myTime = Time.newInstance(Integer.valueOf(timeStr[0].trim()), 0, 0, 0); }
           when 2 { myTime = Time.newInstance( Integer.valueOf(timeStr[0].trim()), Integer.valueOf(timeStr[1].trim()), 0, 0); }
           when 3 {
               myTime = Time.newInstance( Integer.valueOf(timeStr[0].trim()), Integer.valueOf(timeStr[1].trim()), Integer.valueOf(timeStr[2].trim()), 0);
           }
       }
       System.debug('...myTime: ' + myTime);
       return myTime;
    }

    public class SearchWrapper {
        public String text;
        public String id;
    }
}