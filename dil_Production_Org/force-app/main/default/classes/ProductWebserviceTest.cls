/* * * * * * * * * * * * *
* Class Name  : ProductWebserviceTest
* Purpose     : This test class is written for covering the functionality of the class 'ProductWebservice'.
* Author      : Purushottam Bhaigade
* * * * * * * * * * * * */
@IsTest
public class ProductWebserviceTest {

    /* * * * * * * * * * * * *
	* Class Name  : doGetTest
	* Purpose     : This test method is written for covering the functionality of the 'doGet' method.
	* * * * * * * * * * * * */
	@IsTest
    public static void doGetTest(){
        List<Custom_Product__c> customProductList = TestDataFactory.createTestProductsForWebService(5, true);

        Test.startTest();
        List<WebServiceResponse.ProductResponse> lstProductResponse = ProductWebservice.doGet();
        Test.stopTest();
    }

    /* * * * * * * * * * * * *
	* Class Name  : doPostTest1
	* Purpose     : This test method is written for covering the functionality of the 'doPost' method by scenario 'Unique PO Number'.
	* * * * * * * * * * * * */
 	/*@IsTest
    public static void doPostTest1(){
        string jsonResponseString='';

        Id contRecordTypeId = Schema.SObjectType.Order__c.getRecordTypeInfosByName().get('Purchase Order').getRecordTypeId();

        List<Custom_Product__c> customProductList = TestDataFactory.createTestProductsForWebService(2, true);

        List<Order__c> orderList = TestDataFactory.createTestOrderForWebService(2, true, true);
        List<OrderWrapper> orderWrapperList = new List<OrderWrapper>();
        if(contRecordTypeId != NULL && customProductList != NULL && orderList!= NULL){
            for(Integer rowCtr = 0; rowCtr<orderList.Size(); rowCtr++){
                orderWrapper orderWrapperObj = new OrderWrapper();
                orderWrapperObj.RecordTypeId=contRecordTypeId;
				orderWrapperObj.Price='';
				orderWrapperObj.Product=customProductList[rowCtr].Id;
				orderWrapperObj.Estimated_Amount='200.6';
				orderWrapperObj.Order_Form=orderList[rowCtr].Id;
				orderWrapperObj.Order_Quantity='12.0';

                orderWrapperList.add(orderWrapperObj);
            }
        }

        jsonResponseString = JSON.serialize(orderWrapperList);
        jsonResponseString = jsonResponseString.replace('Price', 'Price__c').replace('Product', 'Product__c').replace('Estimated_Amount', 'Estimated_Amount__c').replace('Order_Form', 'Order_Form__c').replace('Order_Quantity', 'Order_Quantity__c');

        if(!String.isEmpty(jsonResponseString)){
            Test.startTest();
        	WebServiceResponse.OrderItemResponse orderItemResponse = ProductWebservice.doPost(jsonResponseString);
        	Test.stopTest();
        }
    }*/

    /* * * * * * * * * * * * *
	* Class Name  : doPostTest2
	* Purpose     : This test method is written for covering the functionality of the 'doPost' method by scenario 'Duplicate PO Number'.
	* * * * * * * * * * * * */
    /*@IsTest
    public static void doPostTest2(){
        string jsonResponseString='';

        Id contRecordTypeId = Schema.SObjectType.Order__c.getRecordTypeInfosByName().get('Purchase Order').getRecordTypeId();

        List<Custom_Product__c> customProductList = TestDataFactory.createTestProductsForWebService(2, true);

        List<Order__c> orderList = TestDataFactory.createTestOrderForWebService(2, true, false);
        List<OrderWrapper> orderWrapperList = new List<OrderWrapper>();
        if(contRecordTypeId != NULL && customProductList != NULL && orderList!= NULL){
            for(Integer rowCtr = 0; rowCtr<orderList.Size(); rowCtr++){
                orderWrapper orderWrapperObj = new OrderWrapper();
                orderWrapperObj.RecordTypeId=contRecordTypeId;
				orderWrapperObj.Price='';
				orderWrapperObj.Product=customProductList[rowCtr].Id;
				orderWrapperObj.Estimated_Amount='200.6';
				orderWrapperObj.Order_Form=orderList[rowCtr].Id;
				orderWrapperObj.Order_Quantity='12.0';

                orderWrapperList.add(orderWrapperObj);
            }
        }

        jsonResponseString = JSON.serialize(orderWrapperList);
        jsonResponseString = jsonResponseString.replace('Price', 'Price__c').replace('Product', 'Product__c').replace('Estimated_Amount', 'Estimated_Amount__c').replace('Order_Form', 'Order_Form__c').replace('Order_Quantity', 'Order_Quantity__c');

        if(!String.isEmpty(jsonResponseString)){
            Test.startTest();
        	WebServiceResponse.OrderItemResponse orderItemResponse = ProductWebservice.doPost(jsonResponseString);
        	Test.stopTest();
        }
    }*/

    /* a wrapper class created to be used to JSONStringify for sending it to webservice methods */
	private class orderWrapper{
        public Id RecordTypeId {get; set;}
        public String Price {get; set;}
        public Id Product {get; set;}
        public String Estimated_Amount {get; set;}
        public Id Order_Form {get; set;}
        public String Order_Quantity {get; set;}
	}
}