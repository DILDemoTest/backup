@isTest
    private class OrderProductPagePDFCX_Test{
        
        @testSetup static void setupData(){
        List<Pricing_Condition__c> pricingConditionList = new List<Pricing_Condition__c>();
        List<Market__c> marketList = new List<Market__c>();
        List<Custom_Product__c> parentProductList = new List<Custom_Product__c>();
        Id distributorRecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Distributor/ Direct').getRecordTypeId();
        Id customerRecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Distributor Customer').getRecordTypeId();
        Pricing_Condition__c nationalPricing = new Pricing_Condition__c(
            Name = 'NATIONAL',
            SAP_Code__c = '12345'
            );
        pricingConditionList.add(nationalPricing);
        Pricing_Condition__c regionalPricing = new Pricing_Condition__c(
            Name = 'REGIONAL',
            SAP_Code__c = '89798'
            );
        pricingConditionList.add(regionalPricing);
        Pricing_Condition__c segmentPricing = new Pricing_Condition__c(
            Name = 'SEGMENT',
            SAP_Code__c = '098098'
            );
        pricingConditionList.add(segmentPricing);
        insert pricingConditionList;
        Account distributorAccount = new Account(
            Name = 'TestDistributor',
            RecordTypeId = distributorRecordTypeId,
            Sales_Organization__c  = 'Feeds',
            General_Trade__c = true,
            BU_Feeds__c = true,
            Branded__c = true,
            GFS__c = true,
            BU_Poultry__c = true,
            Active__c = true,
            AccountNumber = 'TestDistributor11'
            );
        insert distributorAccount;
        Market__c meatMarket = new Market__c(
            Name = 'Meats',
            Active__c = true,
            Market_Code__c = '01'
            );
        marketList.add(meatMarket);
        Market__c gfsMarket = new Market__c(
            Name = 'GFS',
            Active__c = true,
            Market_Code__c = '02'
            );
        marketList.add(gfsMarket);
        Market__c poultryMarket = new Market__c(
            Name = 'Poultry',
            Active__c = true,
            Market_Code__c = '03'
            );
        marketList.add(poultryMarket);
        insert marketList;
        
        Warehouse__c wr = new Warehouse__c();
        wr.Account__c = distributorAccount.Id;
        wr.Address__c = distributorAccount.Name + distributorAccount.Id;
        wr.Warehouse_No__c = String.valueOf(distributorAccount.Id) + String.valueOf(System.now().millisecond());
        insert wr;
        
        distributorAccount.Warehouse__c = wr.Id;
        update distributorAccount;
        
        List<Account> accountForInsertList = new List<Account>();
        Account acc = new Account();
        acc.Name = 'Sample Grocery2';
        acc.RecordTypeId = customerRecordTypeId;
        acc.Sales_Organization__c  = 'Feeds';
        acc.BU_Feeds__c = true;
        acc.Branded__c = true;
        acc.GFS__c = true;
        acc.BU_Poultry__c = false;
        acc.Market__c = marketList.get(0).Id;
        acc.Distributor__c = distributorAccount.Id;
        acc.Warehouse__c = wr.Id;
        acc.AccountNumber = 'ACT21';
        accountForInsertList.add(acc);
        
        Account acc2 = new Account();
        acc2.Name = 'Sample Grocery3';
        acc2.RecordTypeId = customerRecordTypeId;
        acc2.Sales_Organization__c  = 'Feeds';
        acc2.BU_Feeds__c = true;
        acc2.Branded__c = true;
        acc2.GFS__c = true;
        acc2.BU_Poultry__c = true;
        acc2.Market__c = marketList.get(0).Id;
        acc2.Distributor__c = distributorAccount.Id;
        acc2.Warehouse__c = wr.Id;
        acc2.AccountNumber = 'ACT31';
        accountForInsertList.add(acc2);
        
        insert accountForInsertList;
        
        TriggerFlagControl__c settings = new TriggerFlagControl__c(
            SetupOwnerId = UserInfo.getOrganizationId(),
            reflectPicklistValue__c = true
            );
        insert settings;
        Custom_Product__c prodParent1 = new Custom_Product__c();
        prodParent1.Name = 'ParentProductFeeds';
        prodParent1.Business_Unit__c = 'Feeds';
        prodParent1.Feeds__c = true;
        prodParent1.Market__c = marketList.get(0).Id;
        prodParent1.SKU_Code__c = '111';
        parentProductList.add(prodParent1);
        
        Custom_Product__c prodParent2 = new Custom_Product__c();
        prodParent2.Name = 'ParentProductGFS';
        prodParent2.Business_Unit__c = 'GFS';
        prodParent2.Feeds__c = true;
        prodParent2.Market__c = marketList.get(1).Id;
        prodParent2.SKU_Code__c = '222';
        parentProductList.add(prodParent2);
        
        if(parentProductList.size()>0){
            insert parentProductList;
        }
        List<Conversion__c> ConversionforInsertList = new List<Conversion__c>();
        for(Custom_Product__c customProduct : parentProductList){
            Conversion__c conversionRecord = new Conversion__c(
                Name = 'KG',
                Product__c = customProduct.Id,
                Numerator__c = 1,
                Denominator__c = 1
                );
            ConversionforInsertList.add(conversionRecord);
            Conversion__c conversionRecordPC = new Conversion__c(
                Name = 'PC',
                Product__c = customProduct.Id,
                Numerator__c = 1,
                Denominator__c = 1
                );
            ConversionforInsertList.add(conversionRecordPC);
        }
        if(ConversionforInsertList.size()>0){
            insert ConversionforInsertList;
        }
    }
    
    static testmethod void testGeneratePDF(){
        Id customerRecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Distributor Customer').getRecordTypeId();
        Id actualSalesRecordTypeId = Schema.sObjectType.Order__c.getRecordTypeInfosByName().get('Actual Sales').getRecordTypeId();
        Account distAccount = [SELECT Id FROM Account WHERE Name ='TestDistributor'];
        List<Market__c> marketList = [SELECT Id,Name FROM Market__c LIMIT 20];
        Custom_Product__c prodSMIS = [SELECT Id FROM Custom_Product__c WHERE Name = : 'ParentProductFeeds'];
        Conversion__c uom = [SELECT Id FROM Conversion__c WHERE Product__c = : prodSMIS.Id LIMIT 1];
        
         //CREATE SAS- Contact
        Id SASrecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('SAS').getRecordTypeId();
        Contact SASvar = new Contact(RecordTypeId=SASrecordTypeId, FirstName = 'xyzFirst', LastName = 'XyZLast', AccountId = distAccount.Id);
        insert SASvar;
        
        //AARP: Insert DSP Record
         //CREATE DSP- Contact
        Id DSPrecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Distributor Personnel').getRecordTypeId();
        Contact DSPvar = new Contact(RecordTypeId=DSPrecordTypeId, FirstName = 'xyzSecond', LastName = 'XyZSecond', AccountId = distAccount.Id);
        insert DSPvar;        
        
        Test.startTest();
        Account acc = [SELECT Id FROM Account WHERE Name = 'Sample Grocery2'];
        Order__c orderRecord = new Order__c(
            Account__c = acc.Id,
            RecordTypeId = actualSalesRecordTypeId,
            Invoice_No__c = 'Invoiceno211',
            SAS__c = SASvar.Id,
            DSP__c = DSPvar.Id
            );
        insert orderRecord;
        Order_Item__c orderItem = new Order_Item__c(
            Order_Form__c = orderRecord.Id,
            Product__c = prodSMIS.Id,
            Conversion__c = uom.Id,
            Invoice_Quantity__c = 5
            );
        insert orderItem;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(orderRecord);
        OrderProductPagePDFCX pdfCX = new OrderProductPagePDFCX(sc);
        Test.stopTest();
        
    }
    
    
        
        // static testMethod void generatePdf() {
        //     TestDataFactory data = new TestDataFactory();
                   
        //     List<Account> distributorAccount = data.createTestAccount(1, True, false, 'Distributor/ Direct',null);
        //     List<Account> customerAccount = data.createTestAccount(1, True, True, 'Distributor Customer',distributorAccount[0].id);
        //     Id pricingDataType = Schema.SObjectType.Pricing_Condition__c.getRecordTypeInfosByName().get('Pricing').getRecordTypeId();
        //     Id discountDataType = Schema.SObjectType.Pricing_Condition__c.getRecordTypeInfosByName().get('Discount').getRecordTypeId();
        //     Id listPriceDatype = Schema.SObjectType.Pricing_Condition_Data__c.getRecordTypeInfosByName().get('List Price').getRecordTypeId();
        //     Pricing_Condition__c prcingCondition = new Pricing_Condition__c(Name = 'TetPricingCondition');
        //     Order__c newOrderRec = new Order__c();
        //     DSP__c dsp = new DSP__c();

        //     List<Custom_Product__c> productList = data.createTestProducts(5);
        //     Pricing_Condition__c gmaPricingCondition = new Pricing_Condition__c(name='test');
        //     insert gmaPricingCondition;
        //     List<Pricing_Condition_Data__c> gmaPricigConditionDataList = new List<Pricing_Condition_Data__c>();
        //     Pricing_Condition_Data__c tempPricingConditionData = new Pricing_Condition_Data__c();
        //     for(Custom_Product__c cp :productList){
        //         tempPricingConditionData = new Pricing_Condition_Data__c();
        //         tempPricingConditionData.Pricing_Condition__c = gmaPricingCondition.id;
        //         tempPricingConditionData.Product__c = cp.id;
        //         tempPricingConditionData.RecordTypeId = listPriceDatype;
        //         gmaPricigConditionDataList.add(tempPricingConditionData);
        //     }
            
        //     /*
        //     Contact contact1 = new Contact();
        //     contact1.lastName = 'test Contact';
        //     contact1.Account = distributorAccount[0];
        //     insert contact1;
            
        //     dsp.Name = 'DSP-000001';
        //     dsp.Account__c = distributorAccount[0].id;
        //     dsp.Distributor_Sales_Personnel__c=contact1.id;
        //     insert dsp;
        //     */
            
        //     insert gmaPricigConditionDataList;
        //     List<Pricing_Sequence_Condition__c> newPricingSeqCondition = new List<Pricing_Sequence_Condition__c>();
        //     Pricing_Sequence_Condition__c tempPricingSequenceCondition = new Pricing_Sequence_Condition__c();
        //     tempPricingSequenceCondition.Pricing_Condition__c= gmaPricingCondition.id;
        //     tempPricingSequenceCondition.Sequence__c = 1;
        //     newPricingSeqCondition.add(tempPricingSequenceCondition);
        //     insert newPricingSeqCondition;

            
            
        //     PageReference pageRef = Page.orderProductPage;
        //     Test.setCurrentPage(pageRef);
        //     ApexPages.Standardcontroller pageController = new ApexPages.Standardcontroller(newOrderRec);
        //     SalesInformationPageCX pagExtension = new SalesInformationPageCX(pageController);
            
        //     pagExtension.InitializeProductTable();

            
        //     pagExtension.currentOrderRec.Account__c = customerAccount[0].id;
        //     //pagExtension.currentOrderRec.dsp1__c = dsp.id;
        //     pagExtension.InitializeProductTable();
        //     system.assertEquals(pagExtension.searchResultWrapper.size()>0,true);
            
        //     pagExtension.searchResultWrapper[0].isSelected = true;
        //     pagExtension.searchResultWrapper[0].selectedPrice = '200';
        //     pagExtension.searchResultWrapper[0].selectedDiscount = '5';
        //     pagExtension.searchResultWrapper[0].selectedQuantity = '5';
        //     pagExtension.searchResultWrapper[0].orderQuantity = '5';
        //     //pagExtension.searchResultWrapper[0].misOrderReasonsOption = t;
        //     //pagExtension.searchResultWrapper[0].isSelected = true;
        //     pagExtension.nextPage();
        //     pagExtension.previousPage();
        //     pagExtension.proceedProcess();
            
        //     pageRef.getParameters().put('saveAction', 'saveOnly');
        //     pagExtension.SaveOrderRecord();
            
        //     List<Order__c> allOrderRecord = new List<Order__c> ();
        //         allOrderRecord = [SELECT id,Account__c,Sales_Rep__c, DSP1__c,Invoice_Date__c FROM Order__c];
        //         System.debug('\n\n\nALL ORDER RECORD : ' + allOrderRecord + '\n\n\n');
        //         List<Order_Item__c> orderItemList =  [SELECT Order_Quantity__c, id, Conversion__c, Product__c,Reason_for_Missed_Order__c,
        //                                         Order_Form__c ,Price__c ,Discount_PHP__c, Invoice_Quantity__c, Product__r.name
        //                                     FROM Order_Item__c ];
            
        //     // system.assertEquals(allOrderRecord.size(),1);
        //     // system.assertEquals(orderItemList.size(),1);
            
        //     Test.StartTest();
        //         pageRef = Page.OrderProductPagePDF;
        //         Test.setCurrentPage(pageRef);
        //         pageController = new ApexPages.Standardcontroller(allOrderRecord[0]);
        //         OrderProductPagePDFCX pdfPagExtension = new OrderProductPagePDFCX(pageController);
        //     Test.StopTest();    
        //     //create pricing condition record 
        // }
    }