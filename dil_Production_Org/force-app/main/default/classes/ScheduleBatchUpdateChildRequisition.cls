global class ScheduleBatchUpdateChildRequisition implements Schedulable 
{
    global void execute(SchedulableContext SC) 
    {
        UpdateChildRequisition BatchUpdateChildRequisition = new UpdateChildRequisition();
        Database.executeBatch(BatchUpdateChildRequisition, 2000);
    }
}