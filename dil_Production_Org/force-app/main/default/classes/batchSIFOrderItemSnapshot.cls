/*********************************************************************
Author          Deloitte (RVillanueva)
Description     Batch class to run monthly and create SIF and order Item record
Date Created:   09/29/2016
*********************************************************************/
global class batchSIFOrderItemSnapshot implements Database.Batchable<sObject> {

    public Integer sMonth;  
    public Integer sYear; 
    global batchSIFOrderItemSnapshot (Integer ssMonth, Integer ssYear){
        this.sMonth = ssMonth;
        this.sYear = ssYear;
    }
    
    global Database.querylocator start(Database.BatchableContext BC){   
        System.debug('sMonth---'+ sMonth);
        System.debug('sYear---'+ sYear);
        String sifActualSalesRT = Schema.SObjectType.Order__c.getRecordTypeInfosByName().get('Actual Sales').getRecordTypeId();
        String sifReturnOrderFormRT = Schema.SObjectType.Order__c.getRecordTypeInfosByName().get('Return Order Form').getRecordTypeId();
        
        //GAAC: 11/07/2017 - Add Invoice No;
        //String queryString = 'SELECT Id, Monthly_order__c, Delete_Only__c, Account__c, DSP__c, SAS__c FROM Order__c'; 
        String queryString = 'SELECT Id, Monthly_order__c, Delete_Only__c, Account__c, DSP__c, SAS__c, Invoice_No__c FROM Order__c'; 
        //GAAC: 11/07/2017 - END;
        
        queryString += ' WHERE CALENDAR_MONTH(Invoice_Date__c) =: sMonth AND CALENDAR_YEAR(Invoice_Date__c) =: sYear AND (RecordTypeId =: sifActualSalesRT OR RecordTypeId =: sifReturnOrderFormRT) AND Export_and_Delete__c = false';
        //queryString += ' WHERE CALENDAR_MONTH(Invoice_Date__c) =: sMonth AND CALENDAR_YEAR(Invoice_Date__c) =: sYear AND (RecordTypeId =: sifActualSalesRT OR RecordTypeId =: sifReturnOrderFormRT) AND Export_and_Delete__c = false AND CreatedDate = 2016-10-01T00:00:00Z';
        ////queryString += ' WHERE CALENDAR_MONTH(Invoice_Date__c) =: sMonth AND CALENDAR_YEAR(Invoice_Date__c) =: sYear AND (RecordTypeId =: sifActualSalesRT OR RecordTypeId =: sifReturnOrderFormRT) AND Export_and_Delete__c = false AND Account__c = \'0012800000tcrbM\'';
        
        System.debug('queryString---'+ queryString);
        return Database.getQueryLocator(queryString);
  
    }
   
    global void execute(Database.BatchableContext BC, List<sObject> scope){
    
        System.debug('scope---'+ scope);
        String oiActualSalesRT = Schema.SObjectType.Order_Item__c.getRecordTypeInfosByName().get('Actual Sales').getRecordTypeId();
        String oiOrderReturnRT = Schema.SObjectType.Order_Item__c.getRecordTypeInfosByName().get('Order Return').getRecordTypeId();
        list <Order_Item__c> orderItemList = new list <Order_Item__c> ();
        map <Id, list <Order_Item__c>> orderItemToSIFMap = new map <Id,list<Order_Item__c>>();
        map <String, Order__c> sifCodeMap = new map <String, Order__c>();
        map <String, map<String, Order_Item__c>> itemMap = new map<String, map<String, Order_Item__c>>();
        set <Id> sifIdSet = new set <Id>();
        SIFOrderItemBatchSwitch__c snapshotSwitch = SIFOrderItemBatchSwitch__c.getInstance();
        snapshotSwitch.RunByBatch__c  = true;
        upsert snapshotSwitch;
         // set the date to 30th of snapshot month
        Date invoiceDate = Date.newInstance(sYear, sMonth, 5);
        System.debug('invoiceDate ----'+ invoiceDate );
        
        for (sObject obj : scope) {
            Order__c sif = (Order__c)obj;
            sifIdSet.add(sif.Id);
        }
        for (Order_Item__c oi : [SELECT Id, Name, Order_Form__c, Product__c, Conversion__c, 
                                        Price__c, Order_Discount__c, Discount_PHP__c, Invoice_Net_Total__c, 
                                        Invoice_Quantity__c, Order_Quantity__c,
                                        Branded_UOM_Conversion_Rate__c,FEEDS_UOM__c, Invoice_Net_of_Discount__c,
                                        Invoice_Gross_amount__c, Invoice_Gross_Total__c, Invoice_Net_Amount__c,
                                        Invoice_Net_Total_F__c, Order_VAT__c, Reporting_UOM_Quantity_KG__c,
                                        Reporting_UOM_Quantity__c, VAT_Amount__c,
                                        Discount_Amount_Snapshot__c,
                                        Invoice_Net_Total2_Snapshot__c,
                                        Branded__c,
                                        Feeds_UOM_Snapshot__c,
                                        Invoice_Amount_Net_of_Discount_Snapshot__c,
                                        Invoice_Gross_Amount_Snapshot__c,
                                        Invoice_Gross_Total_Snapshot__c,
                                        Invoice_Net_Amount_Snapshot__c,
                                        Invoice_Net_Total_Snapshot__c,
                                        Order_VAT_Snapshot__c,
                                        Reporting_UOM_Quantity_KG_Snapshot__c,
                                        Reporting_UOM_Quantity_Snapshot__c,
                                        VAT_Amount_Snapshot__c
                                 FROM Order_Item__c 
                                 WHERE (RecordTypeId =: oiActualSalesRT OR RecordTypeId =: oiOrderReturnRT) AND Order_Form__c IN: sifIdSet]) {
            if (!orderItemToSIFMap.containskey(oi.Order_Form__c)) {
                orderItemToSIFMap.put(oi.Order_Form__c, new List<Order_Item__c>{oi});
            } else {
                orderItemToSIFMap.get(oi.Order_Form__c).add(oi);
            }
        }
        
        map <String, List<Decimal>> priceMap = new map<String, list<Decimal>>();
        map <String, List<Decimal>> orderDiscountMap = new map<String, list<Decimal>>();
        map <String, List<Decimal>> discountMap = new map<String, list<Decimal>>();
        map <String, List<Decimal>> invNetTotalMap = new map<String, list<Decimal>>();
        map <String, List<Decimal>> invQuantityMap = new map<String, list<Decimal>>();
        map <String, List<Decimal>> orderQuantityMap = new map<String, list<Decimal>>();
        
        // added map for snapshot total
        map <String, List<Decimal>> brandedSnapshotMap = new map<String, list<Decimal>>();
        map <String, List<Decimal>> feedsSnapshotMap = new map<String, list<Decimal>>();
        map <String, List<Decimal>> invAmountNetSnapshotMap = new map<String, list<Decimal>>();
        map <String, List<Decimal>> invGrossAmountSnapshotMap = new map<String, list<Decimal>>();
        map <String, List<Decimal>> invGrossTotalSnapshotMap = new map<String, list<Decimal>>();
        map <String, List<Decimal>> invNetAmountSnapshotMap = new map<String, list<Decimal>>();
        map <String, List<Decimal>> invNetTotalFSnapshotMap = new map<String, list<Decimal>>();
        map <String, List<Decimal>> orderVATSnapshotMap = new map<String, list<Decimal>>();
        map <String, List<Decimal>> reportingUOMKGSnapshotMap = new map<String, list<Decimal>>();
        map <String, List<Decimal>> reportingUOMSnapshotMap = new map<String, list<Decimal>>();
        map <String, List<Decimal>> vatAmountSnapshotMap = new map<String, list<Decimal>>();
        
        Decimal priceAve, orderDiscSum, discountSum, invNetTotalSum, invQuantitySum, orderQuantitySum,brandedSum,feedSum,invAmountNetSum,invGrossAmtSum,invGrossTotalSum,invNetAmountSum,intNetTotalFSum,orderVATSum,reportingUOMkgSum,reportingUOMSum,vatAmountSum;      
        list <Order__c> sifToUpdate = new list <Order__c>();
        list <Order_Item__c> orderToUpdate = new list <Order_Item__c>();
        
        // Group per same Account > SIF > DSP > SAS > PRODUCT > UOM
        // create new sif and order item record
        for (sObject obj : scope) {
            Order__c sif = (Order__c)obj;
            sif.Export_and_Delete__c = true;
            if (sif.Monthly_order__c == true) {
                sif.Delete_Only__c = true;
            }
            if (!sifCodeMap.containskey(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c)) {
                
                //GAAC: 11/07/2017 - Added Invoice No;
                //sifCodeMap.put(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c, new Order__c(Account__c = sif.Account__c, DSP__c = sif.DSP__c, SAS__c = sif.SAS__c, Monthly_order__c = true, Invoice_Date__c = invoiceDate));
                sifCodeMap.put(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c, new Order__c(Account__c = sif.Account__c, DSP__c = sif.DSP__c, SAS__c = sif.SAS__c, Monthly_order__c = true, Invoice_Date__c = invoiceDate, Invoice_No__c = sif.Invoice_No__c));
                //GAAC: 11/07/2017 - END;
            }
            if (orderItemToSIFMap.containskey(sif.Id)) {
                for (Order_Item__c oi : orderItemToSIFMap.get(sif.Id)) {
    
                    oi.Export_and_Delete__c = true;
                    
                    if(!itemMap.containsKey(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c)){
                        itemMap.put(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c, new map<String, Order_Item__c>{oi.Product__c + ':' + oi.Conversion__c => new Order_Item__c(Product__c = oi.Product__c, Conversion__c = oi.Conversion__c, Invoice_Quantity__c = 1)});
                        
                    }else{
                        itemMap.get(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c).put(oi.Product__c + ':' + oi.Conversion__c, new Order_Item__c(Product__c = oi.Product__c, Conversion__c = oi.Conversion__c, Invoice_Quantity__c = 1));
                    } 
                    
                    if (sif.Monthly_order__c == true) {
                        oi.Delete_Only__c = true;
                        if (!priceMap.containskey(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c)) {
                            priceMap.put(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c, new list<Decimal>{oi.Price__c == null? 0:oi.Price__c});
                            orderDiscountMap.put(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c, new list<Decimal>{oi.Order_Discount__c == null? 0:oi.Order_Discount__c});
                            discountMap.put(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c, new list<Decimal>{oi.Discount_Amount_Snapshot__c == null? 0:oi.Discount_Amount_Snapshot__c});
                            invNetTotalMap.put(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c, new list<Decimal>{oi.Invoice_Net_Total2_Snapshot__c == null? 0:oi.Invoice_Net_Total2_Snapshot__c});
                            invQuantityMap.put(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c, new list<Decimal>{oi.Invoice_Quantity__c == null? 0:oi.Invoice_Quantity__c});
                            orderQuantityMap.put(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c, new list<Decimal>{oi.Order_Quantity__c == null? 0:oi.Order_Quantity__c});
                            
                            brandedSnapshotMap.put(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c, new list<Decimal>{oi.Branded__c == null? 0:oi.Branded__c});
                            feedsSnapshotMap.put(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c, new list<Decimal>{oi.Feeds_UOM_Snapshot__c == null? 0:oi.Feeds_UOM_Snapshot__c});
                            invAmountNetSnapshotMap.put(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c, new list<Decimal>{oi.Invoice_Amount_Net_of_Discount_Snapshot__c == null? 0:oi.Invoice_Amount_Net_of_Discount_Snapshot__c});
                            invGrossAmountSnapshotMap.put(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c, new list<Decimal>{oi.Invoice_Gross_Amount_Snapshot__c == null? 0:oi.Invoice_Gross_Amount_Snapshot__c});
                            invGrossTotalSnapshotMap.put(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c, new list<Decimal>{oi.Invoice_Gross_Total_Snapshot__c == null? 0:oi.Invoice_Gross_Total_Snapshot__c});
                            invNetAmountSnapshotMap.put(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c, new list<Decimal>{oi.Invoice_Net_Amount_Snapshot__c == null? 0:oi.Invoice_Net_Amount_Snapshot__c});
                            invNetTotalFSnapshotMap.put(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c, new list<Decimal>{oi.Invoice_Net_Total_Snapshot__c == null? 0:oi.Invoice_Net_Total_Snapshot__c});
                            orderVATSnapshotMap.put(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c, new list<Decimal>{oi.Order_VAT_Snapshot__c == null? 0:oi.Order_VAT_Snapshot__c});
                            reportingUOMKGSnapshotMap.put(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c, new list<Decimal>{oi.Reporting_UOM_Quantity_KG_Snapshot__c == null? 0:oi.Reporting_UOM_Quantity_KG_Snapshot__c});
                            reportingUOMSnapshotMap.put(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c, new list<Decimal>{oi.Reporting_UOM_Quantity_Snapshot__c == null? 0:oi.Reporting_UOM_Quantity_Snapshot__c});
                            vatAmountSnapshotMap.put(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c, new list<Decimal>{oi.VAT_Amount_Snapshot__c == null? 0:oi.VAT_Amount_Snapshot__c});
                        
                        } else {
                            priceMap.get(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c).add(oi.Price__c == null? 0:oi.Price__c);
                            orderDiscountMap.get(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c).add(oi.Order_Discount__c == null? 0:oi.Order_Discount__c);
                            discountMap.get(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c).add(oi.Discount_Amount_Snapshot__c == null? 0:oi.Discount_Amount_Snapshot__c);
                            invNetTotalMap.get(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c).add(oi.Invoice_Net_Total2_Snapshot__c == null? 0:oi.Invoice_Net_Total2_Snapshot__c);
                            invQuantityMap.get(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c).add(oi.Invoice_Quantity__c == null? 0:oi.Invoice_Quantity__c);
                            orderQuantityMap.get(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c).add(oi.Order_Quantity__c == null? 0:oi.Order_Quantity__c);
                            
                            brandedSnapshotMap.get(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c).add(oi.Branded__c == null? 0:oi.Branded__c);
                            feedsSnapshotMap.get(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c).add(oi.Feeds_UOM_Snapshot__c == null? 0:oi.Feeds_UOM_Snapshot__c);
                            invAmountNetSnapshotMap.get(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c).add(oi.Invoice_Amount_Net_of_Discount_Snapshot__c == null? 0:oi.Invoice_Amount_Net_of_Discount_Snapshot__c);
                            invGrossAmountSnapshotMap.get(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c).add(oi.Invoice_Gross_Amount_Snapshot__c == null? 0:oi.Invoice_Gross_Amount_Snapshot__c);
                            invGrossTotalSnapshotMap.get(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c).add(oi.Invoice_Gross_Total_Snapshot__c == null? 0:oi.Invoice_Gross_Total_Snapshot__c);
                            invNetAmountSnapshotMap.get(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c).add(oi.Invoice_Net_Amount_Snapshot__c == null? 0:oi.Invoice_Net_Amount_Snapshot__c );
                            invNetTotalFSnapshotMap.get(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c).add(oi.Invoice_Net_Total_Snapshot__c == null? 0:oi.Invoice_Net_Total_Snapshot__c);
                            orderVATSnapshotMap.get(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c).add(oi.Order_VAT_Snapshot__c == null? 0:oi.Order_VAT_Snapshot__c);
                            reportingUOMKGSnapshotMap.get(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c).add(oi.Reporting_UOM_Quantity_KG_Snapshot__c == null? 0:oi.Reporting_UOM_Quantity_KG_Snapshot__c);
                            reportingUOMSnapshotMap.get(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c).add(oi.Reporting_UOM_Quantity_Snapshot__c  == null? 0:oi.Reporting_UOM_Quantity_Snapshot__c);
                            vatAmountSnapshotMap.get(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c).add(oi.VAT_Amount_Snapshot__c == null? 0:oi.VAT_Amount_Snapshot__c);
                            
                        }
                    } else {
                        if (!priceMap.containskey(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c)) {
                            priceMap.put(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c, new list<Decimal>{oi.Price__c == null? 0:oi.Price__c});
                            orderDiscountMap.put(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c, new list<Decimal>{oi.Order_Discount__c == null? 0:oi.Order_Discount__c});
                            discountMap.put(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c, new list<Decimal>{oi.Discount_PHP__c == null? 0:oi.Discount_PHP__c});
                            invNetTotalMap.put(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c, new list<Decimal>{oi.Invoice_Net_Total__c == null? 0:oi.Invoice_Net_Total__c});
                            invQuantityMap.put(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c, new list<Decimal>{oi.Invoice_Quantity__c == null? 0:oi.Invoice_Quantity__c});
                            orderQuantityMap.put(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c, new list<Decimal>{oi.Order_Quantity__c == null? 0:oi.Order_Quantity__c});
                            
                            brandedSnapshotMap.put(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c, new list<Decimal>{oi.Branded_UOM_Conversion_Rate__c == null? 0:oi.Branded_UOM_Conversion_Rate__c});
                            feedsSnapshotMap.put(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c, new list<Decimal>{oi.FEEDS_UOM__c == null? 0:oi.FEEDS_UOM__c});
                            invAmountNetSnapshotMap.put(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c, new list<Decimal>{oi.Invoice_Net_of_Discount__c == null? 0:oi.Invoice_Net_of_Discount__c});
                            invGrossAmountSnapshotMap.put(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c, new list<Decimal>{oi.Invoice_Gross_amount__c == null? 0:oi.Invoice_Gross_amount__c});
                            invGrossTotalSnapshotMap.put(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c, new list<Decimal>{oi.Invoice_Gross_Total__c == null? 0:oi.Invoice_Gross_Total__c});
                            invNetAmountSnapshotMap.put(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c, new list<Decimal>{oi.Invoice_Net_Amount__c == null? 0:oi.Invoice_Net_Amount__c});
                            invNetTotalFSnapshotMap.put(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c, new list<Decimal>{oi.Invoice_Net_Total_F__c == null? 0:oi.Invoice_Net_Total_F__c});
                            orderVATSnapshotMap.put(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c, new list<Decimal>{oi.Order_VAT__c == null? 0:oi.Order_VAT__c});
                            reportingUOMKGSnapshotMap.put(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c, new list<Decimal>{oi.Reporting_UOM_Quantity_KG__c == null? 0:oi.Reporting_UOM_Quantity_KG__c});
                            reportingUOMSnapshotMap.put(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c, new list<Decimal>{oi.Reporting_UOM_Quantity__c == null? 0:oi.Reporting_UOM_Quantity__c});
                            vatAmountSnapshotMap.put(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c, new list<Decimal>{oi.VAT_Amount__c == null? 0:oi.VAT_Amount__c});
                        
                        } else {
                            priceMap.get(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c).add(oi.Price__c == null? 0:oi.Price__c);
                            orderDiscountMap.get(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c).add(oi.Order_Discount__c == null? 0:oi.Order_Discount__c);
                            discountMap.get(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c).add(oi.Discount_PHP__c == null? 0:oi.Discount_PHP__c);
                            invNetTotalMap.get(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c).add(oi.Invoice_Net_Total__c == null? 0:oi.Invoice_Net_Total__c);
                            invQuantityMap.get(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c).add(oi.Invoice_Quantity__c == null? 0:oi.Invoice_Quantity__c);
                            orderQuantityMap.get(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c).add(oi.Order_Quantity__c == null? 0:oi.Order_Quantity__c);
                            
                            brandedSnapshotMap.get(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c).add(oi.Branded_UOM_Conversion_Rate__c == null? 0:oi.Branded_UOM_Conversion_Rate__c);
                            feedsSnapshotMap.get(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c).add(oi.FEEDS_UOM__c == null? 0:oi.FEEDS_UOM__c);
                            invAmountNetSnapshotMap.get(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c).add(oi.Invoice_Net_of_Discount__c == null? 0:oi.Invoice_Net_of_Discount__c);
                            invGrossAmountSnapshotMap.get(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c).add(oi.Invoice_Gross_amount__c == null? 0:oi.Invoice_Gross_amount__c);
                            invGrossTotalSnapshotMap.get(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c).add(oi.Invoice_Gross_Total__c == null? 0:oi.Invoice_Gross_Total__c);
                            invNetAmountSnapshotMap.get(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c).add(oi.Invoice_Net_Amount__c == null? 0:oi.Invoice_Net_Amount__c );
                            invNetTotalFSnapshotMap.get(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c).add(oi.Invoice_Net_Total_F__c == null? 0:oi.Invoice_Net_Total_F__c);
                            orderVATSnapshotMap.get(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c).add(oi.Order_VAT__c == null? 0:oi.Order_VAT__c);
                            reportingUOMKGSnapshotMap.get(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c).add(oi.Reporting_UOM_Quantity_KG__c == null? 0:oi.Reporting_UOM_Quantity_KG__c);
                            reportingUOMSnapshotMap.get(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c).add(oi.Reporting_UOM_Quantity__c  == null? 0:oi.Reporting_UOM_Quantity__c);
                            vatAmountSnapshotMap.get(sif.Account__c + ':' + sif.DSP__c + ':' + sif.SAS__c + ':' + oi.Product__c + ':' + oi.Conversion__c).add(oi.VAT_Amount__c == null? 0:oi.VAT_Amount__c);
                            
                        }
                    }
                    
                    orderToUpdate.add(oi);
                }
                sifToUpdate.add(sif);
                
            }
        }
        
        // update existing sif and order item record
        if (sifToUpdate.size()>0) {
            update sifToUpdate;
        }
        if (orderToUpdate.size()>0) {
            update orderToUpdate;
        }
        // insert monthly snapshot of sif
        if(!sifCodeMap.isEmpty()){
            insert sifCodeMap.values();
            System.debug('sifCodeMap.values()----'+ sifCodeMap.values());
        }

        for(String key : itemMap.keySet()){
            if(sifCodeMap.containsKey(key)){
                for(Order_Item__c oi : itemMap.get(key).values()){
                    if (priceMap.containskey(key+ ':' + oi.Product__c + ':' + oi.Conversion__c)) {
                        priceAve = computeAverage(priceMap.get(key+ ':' + oi.Product__c + ':' + oi.Conversion__c));
                        oi.Price__c = priceAve;
                    }
                    if (orderDiscountMap.containskey(key+ ':' + oi.Product__c + ':' + oi.Conversion__c)) {
                        orderDiscSum = computeSum(orderDiscountMap.get(key+ ':' + oi.Product__c + ':' + oi.Conversion__c));
                        oi.Order_Discount__c = orderDiscSum;
                    }
                    if (discountMap.containskey(key+ ':' + oi.Product__c + ':' + oi.Conversion__c)) {
                        discountSum = computeSum(discountMap.get(key+ ':' + oi.Product__c + ':' + oi.Conversion__c));
                        oi.Discount_PHP__c = discountSum;
                        System.debug('oi.Discount_Amount_Snapshot__c---'+oi.Discount_Amount_Snapshot__c);
                        oi.Discount_Amount_Snapshot__c = discountSum;
                    }
                    if (invNetTotalMap.containskey(key+ ':' + oi.Product__c + ':' + oi.Conversion__c)) {
                        invNetTotalSum = computeSum(invNetTotalMap.get(key+ ':' + oi.Product__c + ':' + oi.Conversion__c));
                        oi.Invoice_Net_Total__c = invNetTotalSum; 
                        oi.Invoice_Net_Total2_Snapshot__c = invNetTotalSum;
                    }
                    if (invQuantityMap.containskey(key+ ':' + oi.Product__c + ':' + oi.Conversion__c)) {
                        invQuantitySum = computeSum(invQuantityMap.get(key+ ':' + oi.Product__c + ':' + oi.Conversion__c));
                        oi.Invoice_Quantity__c = invQuantitySum; 
                    }
                    if (orderQuantityMap.containskey(key+ ':' + oi.Product__c + ':' + oi.Conversion__c)) {
                        orderQuantitySum = computeSum(orderQuantityMap.get(key+ ':' + oi.Product__c + ':' + oi.Conversion__c));
                        oi.Order_Quantity__c = orderQuantitySum; 
                    }
                    
                    if (brandedSnapshotMap.containskey(key+ ':' + oi.Product__c + ':' + oi.Conversion__c)) {
                        brandedSum = computeSum(brandedSnapshotMap.get(key+ ':' + oi.Product__c + ':' + oi.Conversion__c));
                        oi.Branded__c = brandedSum; 
                    }
                    if (feedsSnapshotMap.containskey(key+ ':' + oi.Product__c + ':' + oi.Conversion__c)) {
                        feedSum = computeSum(feedsSnapshotMap.get(key+ ':' + oi.Product__c + ':' + oi.Conversion__c));
                        System.debug('feedSum ---'+feedSum );
                        oi.Feeds_UOM_Snapshot__c = feedSum; 
                    }
                    if (invAmountNetSnapshotMap.containskey(key+ ':' + oi.Product__c + ':' + oi.Conversion__c)) {
                        invAmountNetSum = computeSum(invAmountNetSnapshotMap.get(key+ ':' + oi.Product__c + ':' + oi.Conversion__c));
                        System.debug('invAmountNetSum ---'+invAmountNetSum );
                        oi.Invoice_Amount_Net_of_Discount_Snapshot__c = invAmountNetSum; 
                    }
                    if (invGrossAmountSnapshotMap.containskey(key+ ':' + oi.Product__c + ':' + oi.Conversion__c)) {
                        invGrossAmtSum = computeSum(invGrossAmountSnapshotMap.get(key+ ':' + oi.Product__c + ':' + oi.Conversion__c));
                        System.debug('invGrossAmtSum ---'+invGrossAmtSum );
                        oi.Invoice_Gross_Amount_Snapshot__c = invGrossAmtSum; 
                    }
                    if (invGrossTotalSnapshotMap.containskey(key+ ':' + oi.Product__c + ':' + oi.Conversion__c)) {
                        invGrossTotalSum = computeSum(invGrossTotalSnapshotMap.get(key+ ':' + oi.Product__c + ':' + oi.Conversion__c));
                        System.debug('invGrossTotalSum ---'+invGrossTotalSum );
                        oi.Invoice_Gross_Total_Snapshot__c = invGrossTotalSum; 
                    }
                    if (invNetAmountSnapshotMap.containskey(key+ ':' + oi.Product__c + ':' + oi.Conversion__c)) {
                        invNetAmountSum = computeSum(invNetAmountSnapshotMap.get(key+ ':' + oi.Product__c + ':' + oi.Conversion__c));
                        System.debug('invNetAmountSum ---'+invNetAmountSum );
                        oi.Invoice_Net_Amount_Snapshot__c = invNetAmountSum; 
                    }
                    if (invNetTotalFSnapshotMap.containskey(key+ ':' + oi.Product__c + ':' + oi.Conversion__c)) {
                        intNetTotalFSum = computeSum(invNetTotalFSnapshotMap.get(key+ ':' + oi.Product__c + ':' + oi.Conversion__c));
                        System.debug('intNetTotalFSum ---'+intNetTotalFSum );
                        oi.Invoice_Net_Total_Snapshot__c = intNetTotalFSum; 
                    }
                    if (orderVATSnapshotMap.containskey(key+ ':' + oi.Product__c + ':' + oi.Conversion__c)) {
                        orderVATSum = computeSum(orderVATSnapshotMap.get(key+ ':' + oi.Product__c + ':' + oi.Conversion__c));
                        System.debug('orderVATSum ---'+orderVATSum );
                        oi.Order_VAT_Snapshot__c = orderVATSum; 
                    }
                    if (reportingUOMKGSnapshotMap.containskey(key+ ':' + oi.Product__c + ':' + oi.Conversion__c)) {
                        reportingUOMkgSum = computeSum(reportingUOMKGSnapshotMap.get(key+ ':' + oi.Product__c + ':' + oi.Conversion__c));
                        System.debug('reportingUOMkgSum ---'+reportingUOMkgSum );
                        oi.Reporting_UOM_Quantity_KG_Snapshot__c = reportingUOMkgSum; 
                    }
                    if (reportingUOMSnapshotMap.containskey(key+ ':' + oi.Product__c + ':' + oi.Conversion__c)) {
                        reportingUOMSum = computeSum(reportingUOMSnapshotMap.get(key+ ':' + oi.Product__c + ':' + oi.Conversion__c));
                        System.debug('reportingUOMSum ---'+reportingUOMSum );
                        oi.Reporting_UOM_Quantity_Snapshot__c = reportingUOMSum; 
                    }
                    if (vatAmountSnapshotMap.containskey(key+ ':' + oi.Product__c + ':' + oi.Conversion__c)) {
                        vatAmountSum = computeSum(vatAmountSnapshotMap.get(key+ ':' + oi.Product__c + ':' + oi.Conversion__c));
                        System.debug('vatAmountSum ---'+vatAmountSum );
                        oi.VAT_Amount_Snapshot__c = vatAmountSum; 
                    }
                    
                    oi.Order_Form__c = sifCodeMap.get(key).Id;
                    
                    orderItemList.add(oi);
                    
                }
            }
        }
        //insert monthly snapshot of order item
        if(!orderItemList.isEmpty()){
            insert orderItemList;
            System.debug('orderItemList-----'+ orderItemList);
        }
        snapshotSwitch.RunByBatch__c = false;    
        upsert snapshotSwitch;    
        System.debug('snapshotSwitch.RunByBatch__c end---'+snapshotSwitch.RunByBatch__c);
    }  
   
    global void finish(Database.BatchableContext BC){     
    }
    
    // Compute average for Order Item fields
    global Decimal computeAverage(list<Decimal> addendList) {
        System.debug('computeAve---addendList '+ addendList);
        Integer aveDivisor = 0;
        Decimal totalAve = 0;
        
        for (Decimal d : addendList){
            totalAve = totalAve + d;
            aveDivisor++;
        }
         
        totalAve = totalAve/aveDivisor;
        System.debug('totalAve ---'+ totalAve );
        return totalAve;
    }
    
    // Compute sum for Order Item fields
    global Decimal computeSum(list<Decimal> addendList) {
        System.debug('computeSum---addendList '+ addendList);
        Decimal total = 0;
        if (addendList.size()>0) {
            for (Decimal d : addendList){
                total = total + d;
            }
        }
        System.debug('total ---'+ total );
        return total;
    }
}