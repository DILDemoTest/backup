//    Author      :  Adjell Ian Pabayos
//    Description :  Utility Class for the Sales Information Object
//    History     :  Created  -  12.11.2017
@RestResource(urlMapping='/Order__c/*')
global class OrderUtils {

    //    Author      :  Adjell Ian Pabayos
    //    Description :  Returns a List of Select Options for Distribution Channel using the Account Id
    //    History     :  Created  -  12.11.2017
    public static List<SelectOption> getDistributionChannels(Id accountId){
        List<SelectOption> distributionChannelsList = new List<SelectOption>();
        ExtractionUpdateUtility eupUtility = new ExtractionUpdateUtility();
        String whereClause = ' Id =  \''+accountId+'\'';
        String query = eupUtility.createQuery('Account', new Set<String>(), whereClause, new List<String>());
        List<Account> tempAccounts = Database.query(query);
        
        if(!tempAccounts.isEmpty()){
            Account tempAccount = tempAccounts[0];
            
            List<Distribution_Channel__c> distributionChannels = [select Name, Distribution_Channel__c, Account_Field_Name__c from Distribution_Channel__c order by Name];
            
            for(Distribution_Channel__c distributionChannel : distributionChannels){
                if((Boolean)tempAccount.get(distributionChannel.Account_Field_Name__c)){
                    String tempValue = distributionChannel.Name + ' - ' + distributionChannel.Distribution_Channel__c;
                    distributionChannelsList.add(new SelectOption(tempValue, tempValue));
                }
            }
        }       
        return distributionChannelsList;
    }
    
    /********************************************
    Author       :   Kimiko Roberto
    Created Date :   12.12.2017
    Definition   :   Returns a list of SelectOption for salesorgs
    History      :   12.12.2017 - Kiko Roberto:  Created
    *********************************************/
    public static List<SelectOption> getSalesOrgs(Order__c orderRec, Map<String, List<String>> categoryBusinessUnitMap){
         List<SelectOption> salesOrgs = new List<SelectOption>();
         
         if(!String.isEmpty(orderRec.Product_Category__c) && categoryBusinessUnitMap.containsKey(orderRec.Product_Category__c)){
                List<String> businessUnits = categoryBusinessUnitMap.get(orderRec.Product_Category__c);
                List<Sales_Org_Division__c> availableSalesOrgs = [select Code__c, Business_Unit__c from Sales_Org_Division__c where Business_Unit__c like :businessUnits];
                Set<SelectOption> tempSelectOptions = new Set<SelectOption>();
                
                for(Sales_Org_Division__c salesOrg : availableSalesOrgs){
                    String tempBusinessUnit = salesOrg.Business_Unit__c.toLowerCase().capitalize();
                    String tempString = (!String.isEmpty(salesOrg.Business_Unit__c)) ? salesOrg.Code__c + ' - ' + tempBusinessUnit : salesOrg.Code__c;
                    tempSelectOptions.add(new SelectOption(tempString, tempString));
                }
                
                salesOrgs.addAll(tempSelectOptions);
                
                if(salesOrgs.size() == 1){
                    orderRec.Sales_Org__c = salesOrgs[0].getValue();
                }
            }
            
        return salesOrgs;
    }
    
    /********************************************
    Author       :   Adjell Pabayos
    Created Date :   12.21.2017
    Definition   :   Global Class for Webservice for Product Price
    History      :   12.21.2017 - Adjell Pabayos:  Created
    *********************************************/
    global class ProductPrice{
        webservice String skuCode;
        webservice Double price;
    }
    
    /********************************************
    Author       :   Adjell Pabayos
    Created Date :   12.21.2017
    Definition   :   Webservice method for retrieving the national price
    History      :   12.21.2017 - Adjell Pabayos:  Created
    *********************************************/
    @HttpPost
    global static Map<String, Double> getNationalPricing(Id accountId, List<String> productIds){
        Map<String, Double> nationalProductPrices = new Map<String, Double>(); //AIRP 1/7/2018 - Changed to Map
        ProductPrice tempProductPrice = new ProductPrice(); //GAAC: 12/22/2017 - Moved variable.
        ExtractionUpdateUtility eupUtility = new ExtractionUpdateUtility();
        Set<String> recordId = new Set<String>(productIds); //GAAC: 12/22/2017 - pass SKU codes into a set for dynamic query.

        List<Pricing_Sequence_Condition__c> pscList = [SELECT Pricing_Condition__c, Sequence__c FROM Pricing_Sequence_Condition__c WHERE Account__c = :accountId order by Sequence__c asc];
        List<String> rFields = new List<String>();
        
        String whereClause = !pscList.isEmpty() ? 'Pricing_Condition__c = \''+pscList[0].Pricing_Condition__c+'\'' : '(Selling_Account__c = \''+accountId+'\' OR Account__c = \''+accountId+'\')';
        whereClause += ' AND Active__c = TRUE and Product__c '; //GAAC: 12/22/2017 - Changed conditon from Product__c IN: skuCodes into Product__r.SKU_Code__c.
        
        List<Pricing_Condition_Data__c> pcdList = eupUtility.dynamicQuery('Pricing_Condition_Data__c', recordId , whereClause, rFields);
                
        for(Pricing_Condition_Data__c pcd : pcdList){
            // tempProductPrice = new ProductPrice();
            // tempProductPrice.skuCode = pcd.Product__r.SKU_Code__c;
            // tempProductPrice.price = pcd.Price__c;
            
            nationalProductPrices.put(pcd.Product__c, pcd.Price__c);
        }

        return nationalProductPrices;
    }
    
    /********************************************
    Author       :   Adjell Pabayos
    Created Date :   1.7.2017
    Definition   :   Gets the Prices for the Product (Current Order: List Price, Customer Specific Price)
    History      :   1.7.2017 - Adjell Pabayos:  Created
    *********************************************/
    public static Map<String, Map<Id, Double>> getProductPrices(Id accountId, Set<Id> productIds){
        Map<String, Map<Id, Double>> productPrices = new Map<String, Map<Id, Double>>();
        List<Pricing_Condition_Data__c> tempPriceCondList = [SELECT Price__c, UOM_1__r.Id, UOM_1__r.UOM__c, Product__c, Account__c, RecordType.Name FROM Pricing_Condition_Data__c WHERE Active__c = TRUE AND Product__c IN :productIds and Account__c = :accountId];
        
        if(!tempPriceCondList.isEmpty()) {
            for(Pricing_Condition_Data__c pcd : tempPriceCondList) {
                Map<Id, Double> tempPriceMap = (productPrices.containsKey(pcd.RecordType.Name)) ? productPrices.get(pcd.RecordType.Name) : new Map<Id, Double>();
                
                if(!tempPriceMap.containsKey(pcd.Product__c))
                    tempPriceMap.put(pcd.Product__c, pcd.Price__c);
            }
        }        
        
        return productPrices;
    }    
    
    /********************************************
    Author       :   Kimiko Roberto
    Created Date :   12.21.2017
    Definition   :   Returns a map of custom products identified by plant determination records where status is not available
    History      :   12.21.2017 - Kiko Roberto:  Created
    *********************************************/
    public static Map<Id,Custom_Product__c> plantDeterminationIdentifier(Map<Order__c,List<Custom_Product__c>> orderProductMap){

        List<Plant_Determination__c> plantDeterminationList = new List<Plant_Determination__c>();
        List<Account> acctList = new List<Account>();
        Set<Id> acctIdSet = new Set<Id>();
        Set<Id> custProdIdSet = new Set<Id>();
        List<Custom_Product__c> custProdList = new List<Custom_Product__c>();
        Set<String> shipToPartySet = new Set<String>();
        Set<String> salesOrgSet = new Set<String>();
        Set<String> salesGroupSet = new Set<String>();
        Set<String> salesOfficeSet = new Set<String>();
        Set<String> distChannelSet = new Set<String>();
        Set<String> custGroupSet = new Set<String>();
        Map<Id, Custom_Product__c> custProductMap = new Map<Id, Custom_Product__c>();

        //collect account sales org and distribution channel fro order
        for(Order__c o : orderProductMap.keySet()){
            acctIdSet.add(o.Account__c);
            salesOrgSet.add(o.Sales_Org__c);
            distChannelSet.add(o.Distribtution_Channel__c);
        }

        for(List<Custom_Product__c> cpList: orderProductMap.values()){
            
            for(Custom_Product__c cp : cpList){
                custProdIdSet.add(cp.Id);
                custProdList.add(cp);
            }
        }

        //collect ship to, sales group, sales office and customer group from acct
        if(acctIdSet != null && !acctIdSet.isEmpty()){
            acctList = [SELECT Id,
                               Ship_To__c,
                               Sales_Group__c,
                               Sales_Office__c,
                               Customer_Group__c
                        FROM Account
                        WHERE Id IN: acctIdSet];

            for(Account a : acctList){
                shipToPartySet.add(a.Ship_To__c);
                salesGroupSet.add(a.Sales_Group__c);
                salesOfficeSet.add(a.Sales_Office__c);
                custGroupSet.add(a.Customer_Group__c);
            }
        }

        //collect plant determination record where product status is not available from the gathered data above
        plantDeterminationList = [SELECT Id,
                                         Account__c,
                                         Customer_Group_1__c,
                                         CustomProduct__c,
                                         Distribution_Channel__c,
                                         Product_Status__c,
                                         Sales_Group__c,
                                         Sales_Office__c,
                                         Sales_Org__c,
                                         Ship_to_party__c
                                  FROM Plant_Determination__c
                                  WHERE Product_Status__c !='Available'
                                  AND CustomProduct__c IN: custProdIdSet
                                  AND /*(*/Account__c IN: acctIdSet
                                  /*OR Ship_to_party__c IN: shipToPartySet
                                  OR Sales_Org__c IN: salesOrgSet
                                  OR Sales_Group__c IN: salesGroupSet
                                  OR Sales_Office__c IN: salesOfficeSet
                                  OR Distribution_Channel__c IN: distChannelSet
                                  OR Customer_Group_1__c IN: custGroupSet)*/];    

        //create mapping of custom product record
        if(plantDeterminationList != null && !plantDeterminationList.isEmpty()){
            for(Custom_Product__c cp : custProdList){
                
                for(Plant_Determination__c pd : plantDeterminationList){
                    
                    if(pd.CustomProduct__c == cp.Id && !custProductMap.containsKey(pd.CustomProduct__c)){
                        custProductMap.put(pd.CustomProduct__c, cp);
                    }   
                }   
            }
        }

        return custProductMap;
    }
    
    /********************************************
    Author       :   Kimiko Roberto
    Created Date :   01.26.2018
    Definition   :   Returns a List of Category__c base on the SAS parameter recieved
    History      :   01.26.2018 - Kiko Roberto:  Created
    *********************************************/
    /*public static List<Category__c> getProductCatBasedFromBU(Id sasId){

        List<Category__c> catListToReturn = new List<Category__c>();
        system.Debug('***sasId: '+sasId);
        if(sasId != null){
            String empNumber = [SELECT Employee_Number__c FROM Contact WHERE Id =: sasId].Employee_Number__c;
            system.Debug('***empNumber: '+empNumber);
            if(empNumber != null && empNumber != ''){

                String businessUnit = [SELECT Business_Unit__c
                                       FROM User
                                       WHERE EmployeeNumber =: empNumber
                                       AND Business_Unit__c != 'Branded'].Business_Unit__c;
                system.Debug('***businessUnit: '+businessUnit);
                if((businessUnit != null || businessUnit != '') && businessUnit != 'All'){
                    return catListToReturn = [SELECT Id,
                                                     Name,
                                                     Business_Unit__c
                                              FROM Category__c
                                              WHERE Business_Unit__c =: businessUnit
                                              ORDER BY Name];
                } else if(businessUnit == 'All'){
                    return catListToReturn = [SELECT Id,
                                                     Name,
                                                     Business_Unit__c
                                              FROM Category__c
                                              ORDER BY Name];
                }
            }         
        }

        return null;
    }*/

    /********************************************
    Author       :   Kimiko Roberto
    Created Date :   01.26.2018
    Definition   :   Returns a List of Category__c base on the SAS parameter recieved
    History      :   01.26.2018 - Kiko Roberto:  Created
    *********************************************/
    public static List<Category__c> getProductCatBasedFromBU(String businessUnit){

        List<Category__c> catListToReturn = new List<Category__c>();
        system.Debug('***businessUnit: '+businessUnit);
        if(businessUnit != null && businessUnit != ''){
            
                if(businessUnit == 'GFS'){
                    return catListToReturn = [SELECT Id,
                                                     Name,
                                                     Business_Unit__c
                                              FROM Category__c
                                              WHERE Business_Unit__c INCLUDES (:businessUnit)
                                              AND Business_Unit__c != 'Feeds'
                                              AND Active__c = TRUE
                                              ORDER BY Name];
                } else{
                    return catListToReturn = [SELECT Id,
                                                     Name,
                                                     Business_Unit__c
                                              FROM Category__c
                                              WHERE Business_Unit__c INCLUDES (:businessUnit)
                                              AND Active__c = TRUE
                                              ORDER BY Name];
                }
        }         

        return null;
    }
    
    /********************************************
    Author       :   Kimiko Roberto
    Created Date :   01.28.2018
    Definition   :   Returns a List of Plant_Determination__c base on the Order and Products Recieved
    History      :   01.28.2018 - Kiko Roberto:  Created
    *********************************************/
    public static List<Plant_Determination__c> returnPlantDetermination(Map<Order__c,List<Custom_Product__c>> orderProductMap){
        List<Plant_Determination__c> plantDeterminationList = new List<Plant_Determination__c>();
        List<Account> acctList = new List<Account>();
        Set<Id> acctIdSet = new Set<Id>();
        Set<Id> custProdIdSet = new Set<Id>();
        List<Custom_Product__c> custProdList = new List<Custom_Product__c>();
        Set<String> shipToPartySet = new Set<String>();
        Set<String> salesOrgSet = new Set<String>();
        Set<String> orderTypeSet = new Set<String>();
        Set<String> salesGroupSet = new Set<String>();
        Set<String> salesOfficeSet = new Set<String>();
        Set<String> shipCondSet = new Set<String>();
        Set<String> distChannelSet = new Set<String>();
        Set<String> custGroupSet = new Set<String>();
        Set<String> loadingGroupSet = new Set<String>();
        Map<Id, Custom_Product__c> custProductMap = new Map<Id, Custom_Product__c>();

        //collect account sales org and distribution channel fro order
        for(Order__c o : orderProductMap.keySet()){
        
            if(o.Sales_Org__c != NULL && o.Sales_Org__c != '') {
                List<String> tempStringSalesOrg = o.Sales_Org__c.split(' - ');
                salesOrgSet.add(tempStringSalesOrg[0]);
            }
            acctIdSet.add(o.Account__c);
            //salesOrgSet.add(o.Sales_Org__c);
            distChannelSet.add(o.Distribtution_Channel__c);
            orderTypeSet.add(o.Order_Type__c);
            
        }

        for(List<Custom_Product__c> cpList: orderProductMap.values()){
            
            for(Custom_Product__c cp : cpList){
                custProdIdSet.add(cp.Id);
                custProdList.add(cp);
                loadingGroupSet.add(cp.Loading_Group__c);
            }
        }

        //collect ship to, sales group, sales office and customer group from acct
        if(acctIdSet != null && !acctIdSet.isEmpty()){
            acctList = [SELECT Id,
                               Ship_To__c,
                               Sales_Group__c,
                               Sales_Office__c,
                               Customer_Group__c,
                               Shipping_Condition__c
                               //Sales_Organization__c
                        FROM Account
                        WHERE Id IN: acctIdSet];

            for(Account a : acctList){
                shipToPartySet.add(a.Ship_To__c);
                salesGroupSet.add(a.Sales_Group__c);
                salesOfficeSet.add(a.Sales_Office__c);
                custGroupSet.add(a.Customer_Group__c);
                shipCondSet.add(a.Shipping_Condition__c);
                //salesOrgSet.add(a.Sales_Organization__c);
            }
        }
        
        system.Debug('***custProdIdSet: '+custProdIdSet);
        system.Debug('***salesOrgSet: '+salesOrgSet);
        system.Debug('***shipCondSet: '+shipCondSet);
        system.Debug('***loadingGroupSet: '+loadingGroupSet);

        //collect plant determination record where product status is not available from the gathered data above
        plantDeterminationList = [SELECT Id,
                                         Account__c,
                                         Customer_Group_1__c,
                                         CustomProduct__c,
                                         CustomProduct__r.Name,
                                         Distribution_Channel__c,
                                         Product_Status__c,
                                         Sales_Group__c,
                                         Sales_Office__c,
                                         Sales_Org__c,
                                         Ship_to_party__c,
                                         Loading_Group__c,
                                         Shipping_Condition__c,
                                         Plant__c,
                                         Plant_Description__c
                                  FROM Plant_Determination__c
                                  WHERE Product_Status__c !='Available'
                                  AND CustomProduct__c IN: custProdIdSet
                                  AND Sales_Org__c IN: salesOrgSet
                                  AND Order_Type__c IN: orderTypeSet
                                  AND Shipping_Condition__c IN: shipCondSet
                                  AND Loading_Group__c IN: loadingGroupSet];
           
        system.Debug('***plantDeterminationList : '+plantDeterminationList );
        //create mapping of custom product record
        if(plantDeterminationList != null && !plantDeterminationList.isEmpty()){
            return plantDeterminationList;
        }

        return null;
    }
    
    /********************************************
    Author       :   Kimiko Roberto
    Created Date :   01.31.2018
    Definition   :   Gets the Order Type for the Order
    History      :   1.31.2018 - Kiko Roberto:  Created
    *********************************************/
    public static void getOrderType(Order__c ord, Id accntId, Id categoryId){
        //AIRP: 12/11/2017 - Added copying of related Order fields from Parent Records
        Map<Id, Account> parentAccounts = new Map<Id, Account>([select Name from Account where Id = :accntId]);
        Map<Id, Category__c> parentCategories = new Map<Id, Category__c>([select Name from Category__c where Id = :categoryId]);   
        //AIRP: 12/11/2017 - END
               
        //AIRP: 12/11/2017 - Added copying of related Order fields from Parent Records
        String tempSalesOrg = (String.isNotBlank(ord.Sales_Org__c)) ? ord.Sales_Org__c.toUpperCase() : '';
        Account tempParentAccount = parentAccounts.get(ord.Account__c);
        ord.Account_Name__c = tempParentAccount.Name;
        
        Category__c tempParentCategory = parentCategories.get(ord.Product_Category__c);
        ord.Product_Category_Name__c = (tempParentCategory != null) ? tempParentCategory.Name : '';   
        //AIRP: 12/11/2017 - END
        
        If(ord.Account_Name__c.contains('AVS') || ord.Account_Name__c.contains('F SUPER SM') || ord.Account_Name__c.contains('F SANFORD') || ord.Account_Name__c.contains('F ALFAMART') || ord.Account_Name__c.contains('F FREE CHOICE')) {
            
            if(ord.Product_Category_Name__c.toUpperCase() == 'REFRIGERATED MEATS' && tempSalesOrg.contains('PHC')) {
                ord.Order_Type__c = 'ZKB';
            }
        }
        
        If(tempSalesOrg.contains('PHC') || tempSalesOrg.contains('BRANDED') || tempSalesOrg.contains('MAGNOLIA') || tempSalesOrg.contains('SMSCCI') || tempSalesOrg.contains('GFS') || tempSalesOrg.contains('FEEDS')) {
            ord.Order_Type__c = 'ZOR'; 
        }
        else {
            ord.Order_Type__c = 'ZOCO'; //GAAC: TEMPORARY ORDER TYPE FOR POULTRY 1/22/2018
        }              
    }
    
    public static DateTime getSystemNow() {
        
        if(Test.isRunningTest()) {
            DateTime myDateTime = DateTime.newInstance(2018, 1, 31, 12, 8, 16);
            return myDateTime;
        }
        return System.Now();
    }
}