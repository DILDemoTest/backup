public class ParseJsonForRecordDML {
    
    public void createRecord(List<sObject> sobjListForJson){
        List<sObject> lstToupdate = new List<sObject>();
        
        for(SObject objsobj : sobjListForJson){
            if(objsobj.get('Field_Value_Mapping__c') != null){
                lstToupdate.add((SObject)Json.deserialize(String.valueOf(objsobj.get('Field_Value_Mapping__c')), Sobject.class));    
            }
        } 
        StaticResources.byPassStartOrEndOfDayTrigger = true;
        update lstToupdate;
        StaticResources.byPassStartOrEndOfDayTrigger = false;
    }
    
}