/*-------------------------------------------------------------------------------------------
Author       :   Kimiko Roberto
Created Date :   05.15.2017
Definition   :   Test Data Factory
History      :   05.15.2017 - Kiko Roberto:  Created
-------------------------------------------------------------------------------------------*/
@isTest
public class TP_TestDataFactory {
    
    public static Map<String, RecordType> contactRecordTypes;
    public static Map<String, RecordType> accountRecordTypes;
    public static Map<String, RecordType> orderRecordTypes;
    
    static{
        contactRecordTypes = new Map<String, RecordType>();
        accountRecordTypes = new Map<String, RecordType>();
        orderRecordTypes = new Map<String, RecordType>();
        
        List<RecordType> tempContactRecordTypes = [select Id, Name, DeveloperName from RecordType where SobjectType = 'Contact'];
        List<RecordType> tempAccountRecordTypes = [select Id, Name, DeveloperName from RecordType where SobjectType = 'Account'];
        List<RecordType> tempOrderRecordTypes = [select Id, Name, DeveloperName from RecordType where SobjectType = 'Order__c'];
        
        for(RecordType tempContactRecordType : tempContactRecordTypes){
            contactRecordTypes.put(tempContactRecordType.DeveloperName, tempContactRecordType);
        }
        
        for(RecordType tempAccountRecordType : tempAccountRecordTypes){
            accountRecordTypes.put(tempAccountRecordType.DeveloperName, tempAccountRecordType);
        }
        
        for(RecordType tempOrderRecordType : tempOrderRecordTypes){
            orderRecordTypes.put(tempOrderRecordType.DeveloperName, tempOrderRecordType);
        }
    }
    
    /*-------------------------------------------------------------------------------------------
    Author       :   Adjell Pabayos
    Created Date :   09.26.2017
    Definition   :   returns a single Business Unit record
    History      :   09.26.2017 - Adjell Pabayos: Created
    -------------------------------------------------------------------------------------------*/    
    public static Market__c createSingleBusinessUnit(String name){
        return createMultipleBusinessUnits(name, 1)[0];
    }
    
    /*-------------------------------------------------------------------------------------------
    Author       :   Adjell Pabayos
    Created Date :   09.26.2017
    Definition   :   returns a list of Business Unit records
    History      :   09.26.2017 - Adjell Pabayos: Created
    -------------------------------------------------------------------------------------------*/  
    public static List<Market__c> createMultipleBusinessUnits(String name, Integer num){
        List<Market__c> businessUnits = new List<Market__c>();
        
        for(Integer x = 0; x < num; x++){
            Market__c tempBusinessUnit = new Market__c();
            tempBusinessUnit.Name = name + String.valueOf(x);
            businessUnits.add(tempBusinessUnit);
        }
        
        return businessUnits;
    }
    
    /*-------------------------------------------------------------------------------------------
    Author       :   Kimiko Roberto
    Created Date :   05.15.2017
    Definition   :   returns single account record
    History      :   05.15.2017 - Kiko Roberto:  Created
                     09.26.2017 - Adjell Pabayos: Deprecated. create Single account with specific recordType is now used
    -------------------------------------------------------------------------------------------*/
    public static Account createSingleAccount(String acctName) {
        return new Account(Name = acctName);
    }    
    
    /*-------------------------------------------------------------------------------------------
    Author       :   Adjell Pabayos
    Created Date :   09.26.2017
    Definition   :   returns an Account created with a specific recordType
    History      :   09.26.2017 - Adjell Pabayos: created
    -------------------------------------------------------------------------------------------*/    
    public static Account createSingleAccount(String acctName, String recordType, Id businessUnitId){
        Account newTestAccount = createSingleAccount(acctName);
        newTestAccount.RecordTypeId = accountRecordTypes.get(recordType).Id;
        newTestAccount.Business_Unit__c = businessUnitId;
        newTestAccount.AccountNumber = String.valueOf(Math.random() * 1000000);
        return newTestAccount;
    }
    
    /*-------------------------------------------------------------------------------------------
    Author       :   Adjell Pabayos
    Created Date :   09.26.2017
    Definition   :   returns multiple children Accounts
    History      :   09.26.2017 - Adjell Pabayos: created
    -------------------------------------------------------------------------------------------*/    
    public static List<Account> createChildrenAccounts(Account parentAccount, Integer num){
        List<Account> tempAccounts = new List<Account>();
        
        for(Integer x = 0; x < num; x++){
            Account tempAccount = createSingleAccount(parentAccount.Name + String.valueOf(x), 'Distributor_Customer', parentAccount.Business_Unit__c);
            tempAccount.Distributor__c = parentAccount.Id;
            
            tempAccounts.add(tempAccount);
        }
        
        return tempAccounts;
    }  
    
    /*-------------------------------------------------------------------------------------------
    Author       :   Adjell Pabayos
    Created Date :   09.26.2017
    Definition   :   returns contacts based on the recordType
    History      :   09.26.2017 - Adjell Pabayos: created
    -------------------------------------------------------------------------------------------*/
    public static List<Contact> createMultipleContacts(String contactName, boolean isPrimary, Integer num, String recordType, Id accountId){
        List<Contact> tempContacts = new List<Contact>();
        
        for(Integer x = 0; x < num; x++){
            Contact tempContact = new Contact();
            tempContact.LastName = contactName + String.valueOf(x);
            tempContact.FirstName = contactName + String.valueOf(x);
            tempContact.Is_Primary__c = isPrimary;
            tempContact.AccountId = accountId;
            tempContact.RecordTypeId = contactRecordTypes.get(recordType).Id;
            
            tempContacts.Add(tempContact);
        }
        
        return tempContacts;
    }    
    
    /*-------------------------------------------------------------------------------------------
    Author       :   Adjell Pabayos
    Created Date :   09.26.2017
    Definition   :   returns a single contact
    History      :   09.26.2017 - Adjell Pabayos: created
    -------------------------------------------------------------------------------------------*/
    public static Contact createSingleContact(String contactName, boolean isPrimary, String recordType, Id accountId){
        return createMultipleContacts(contactName, isPrimary, 1, recordType, accountId).get(0);
    }    

    /*-------------------------------------------------------------------------------------------
    Author       :   Kimiko Roberto
    Created Date :   05.15.2017
    Definition   :   returns a list of account record depending on parameter numberOfRecords
    History      :   05.15.2017 - Kiko Roberto:  Created
    -------------------------------------------------------------------------------------------*/
    public static List<Account> createBulkAccount(String acctName, Integer numberOfRecords) {
        List<Account> testAccountList = new List<Account>();

        for(Integer x = 0; x < numberOfRecords; x++) {
            testAccountList.add( new Account(Name = acctName + ' ' + String.valueOf(x)));
        }

        return testAccountList;
    }

    /*-------------------------------------------------------------------------------------------
    Author       :   Kimiko Roberto
    Created Date :   05.15.2017
    Definition   :   returns a user record with System Amdmin Profile
    History      :   05.15.2017 - Kiko Roberto:  Created
    -------------------------------------------------------------------------------------------*/
    public static User createUserAdmin() {
        
        Id sysAdId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;

        return new User(LastName = 'Test System Admin',
                        Alias = 'tsysad',
                        Email = 'test.sysad@test.com',
                        Username = 'test.sysad@test.com',
                        ProfileId = sysAdId,
                        TimeZoneSidKey = 'GMT',
                        LanguageLocaleKey = 'en_US',
                        EmailEncodingKey = 'UTF-8',
                        LocaleSidKey = 'en_US');
    }

    /*-------------------------------------------------------------------------------------------
    Author       :   Kimiko Roberto
    Created Date :   05.15.2017
    Definition   :   returns a user record depending on the parameters
    History      :   05.15.2017 - Kiko Roberto:  Created
    -------------------------------------------------------------------------------------------*/
    public static User createUser(String profileName,
                                  String uName,
                                  String uAlias,
                                  String uEmail,
                                  String uUserName) {
        
        Id uId = [SELECT Id FROM Profile WHERE Name =: profileName].Id;

        return new User(LastName = uName,
                        Alias = uAlias,
                        Email = uEmail,
                        Username = uUserName,
                        ProfileId = uId,
                        TimeZoneSidKey = 'GMT',
                        LanguageLocaleKey = 'en_US',
                        EmailEncodingKey = 'UTF-8',
                        LocaleSidKey = 'en_US');
    }
    
    /*-------------------------------------------------------------------------------------------
    Author       :   Kimiko Roberto
    Created Date :   05.15.2017
    Definition   :   returns single custom product record
    History      :   05.15.2017 - Kiko Roberto:  Created
    -------------------------------------------------------------------------------------------*/
    public static Custom_Product__c createSingleCustomProduct(String prodName, String materialCode) {
        return new Custom_Product__c(Name = prodName, SKU_Code__c = materialCode);
    }
    
    /*-------------------------------------------------------------------------------------------
    Author       :   Adjell Pabayos
    Created Date :   09.26.2017
    Definition   :   returns single custom product record with Base UOM
    History      :   09.26.2017 - Adjell Pabayos: Created
    -------------------------------------------------------------------------------------------*/  
    public static Custom_Product__c createSingleCustomProduct(String prodName, String materialCode, Id baseUOMId){
        return createMultipleCustomProducts(prodName, materialCode, baseUOMId, 1).get(0);
    }  

    /*-------------------------------------------------------------------------------------------
    Author       :   Adjell Pabayos
    Created Date :   09.26.2017
    Definition   :   returns multiple custom product records with Base UOM
    History      :   09.26.2017 - Adjell Pabayos: Created
    -------------------------------------------------------------------------------------------*/     
    public static List<Custom_Product__c> createMultipleCustomProducts(String prodName, String materialCode, Id baseUOMId, Integer num){
        List<Custom_Product__c> tempProducts = new List<Custom_Product__c>();
        
        for(Integer x = 0; x < num; x++){
            Custom_Product__c tempProduct = new Custom_Product__c();
            tempProduct.Name = prodName + String.valueOf(x);
            tempProduct.SKU_Code__c = materialCode+String.valueOf(x);
            tempProduct.Base_UOM__c = baseUOMId;
            tempProducts.add(tempProduct);
        }
        
        return tempProducts;
    }
    
    /*-------------------------------------------------------------------------------------------
    Author       :   Adjell Pabayos
    Created Date :   09.26.2017
    Definition   :   returns mulitple UOM based on num
    History      :   09.26.2017 - Adjell Pabayos: created
    -------------------------------------------------------------------------------------------*/    
    public static List<UOM__c> createMultipleUOMs(String uomName, Integer num){
        List<UOM__c> tempUOMs = new List<UOM__c>();
        
        for(Integer x =0; x < num; x++){
            UOM__c tempUOM = new UOM__c();
            tempUOM.Name = uomName + String.valueOf(x);
            tempUOMs.add(tempUOM);
        }
        
        return tempUOMs;
    }
    
    /*-------------------------------------------------------------------------------------------
    Author       :   Adjell Pabayos
    Created Date :   09.26.2017
    Definition   :   returns a single UOM
    History      :   09.26.2017 - Adjell Pabayos: created
    -------------------------------------------------------------------------------------------*/
    public static UOM__c createSingleUOM(String uomName){
        return createMultipleUOMs(uomName, 1).get(0);
    }
    
    /*-------------------------------------------------------------------------------------------
    Author       :   Adjell Pabayos
    Created Date :   09.126.2017
    Definition   :   returns a single conversion using a single product and uom
    History      :   09.26.2017 - Adjell Pabayos: Created
    -------------------------------------------------------------------------------------------*/
    public static Conversion__c createSingleConversion(Id productId, Id uomId){
        Custom_Product__c tempProduct = new Custom_Product__c(Id = productId);
        UOM__c tempUOM = new UOM__c(Id = uomId);
        
        return createMultipleConversions(new List<Custom_Product__c>{tempProduct}, new List<UOM__c>{tempUOM})[0];
    }    
    
    /*-------------------------------------------------------------------------------------------
    Author       :   Adjell Pabayos
    Created Date :   09.26.2017
    Definition   :   returns multiple conversions using a single product and multiple uom
    History      :   09.26.2017 - Adjell Pabayos: Created
    -------------------------------------------------------------------------------------------*/
    public static List<Conversion__c> createMultipleConversions(Id productId, List<UOM__c> uoms){
        Custom_Product__c tempProduct = new Custom_Product__c(Id = productId);
        return createMultipleConversions(new List<Custom_Product__c>{tempProduct}, uoms);
    }  
    
    /*-------------------------------------------------------------------------------------------
    Author       :   Adjell Pabayos
    Created Date :   09.26.2017
    Definition   :   returns multiple conversions using multiple products and a single uom
    History      :   09.26.2017 - Adjell Pabayos: Created
    -------------------------------------------------------------------------------------------*/
    public static List<Conversion__c> createMultipleConversions(List<Custom_Product__c> products, Id uomId){
        UOM__c tempUOM = new UOM__c(Id = uomId);
        return createMultipleConversions(products, new List<UOM__c>{tempUOM});
    }        
    
    /*-------------------------------------------------------------------------------------------
    Author       :   Adjell Pabayos
    Created Date :   09.26.2017
    Definition   :   returns multiple conversions using multiple products and multiple uoms
    History      :   09.26.2017 - Adjell Pabayos: Created
    -------------------------------------------------------------------------------------------*/
    public static List<Conversion__c> createMultipleConversions(List<Custom_Product__c> products, List<UOM__c> uoms){
        List<Conversion__c> tempConversions = new List<Conversion__c>();
        
        for(Custom_Product__c product : products){
            for(UOM__c uom : uoms){
                Conversion__c tempConversion = new Conversion__c();
                tempConversion.Name = 'test';
                tempConversion.Product__c = product.Id;
                tempConversion.UOM__c = uom.Id;
                tempConversion.Numerator__c = 1;
                tempConversion.Denominator__c = 1;            
                
                tempConversions.add(tempConversion);            
            }
        }
        
        return tempConversions;
    }      
    
    /*-------------------------------------------------------------------------------------------
    Author       :   Kimiko Roberto
    Created Date :   05.15.2017
    Definition   :   returns single order record
    History      :   05.15.2017 - Kiko Roberto:  Created
    -------------------------------------------------------------------------------------------*/
    public static Order__c createSingleSalesOrderForm(Id acctId, String salesType) {
        return new Order__c(Sales_Type__c = salesType, Account__c = acctId);
    }
    
    /*-------------------------------------------------------------------------------------------
    Author       :   Kimiko Roberto
    Created Date :   05.15.2017
    Definition   :   returns single order line item record
    History      :   05.15.2017 - Kiko Roberto:  Created
    -------------------------------------------------------------------------------------------*/
    public static Order_Item__c createSingleSalesOrderLineItem(Id ordId, Id prodId) {
        return new Order_Item__c(Order_Form__c = ordId, Product__c = prodId);
    }    
}