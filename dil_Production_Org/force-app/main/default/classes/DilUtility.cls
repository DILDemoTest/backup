global without sharing class DilUtility {

    public static Integer subRoleHierCount = 6;
    public static Integer parRoleHierCount = 6;

    // To get all sub roles.
    public static Set<Id> getAllSubRoleIds(Set<Id> roleIds) {

        Set<Id> currentRoleIds = new Set<Id>();

        // get all of the roles underneath the passed roles
        for(UserRole userRole :[SELECT Id FROM UserRole WHERE ParentRoleId
                                    IN :roleIds AND ParentRoleID != null
                               ]) {
            currentRoleIds.add(userRole.Id);
        }

        // go fetch some more roles!
        if(currentRoleIds.size() > 0 && --subRoleHierCount > 0) {
            System.debug('--------' + subRoleHierCount);
            currentRoleIds.addAll(getAllSubRoleIds(currentRoleIds));
        }

        return currentRoleIds;
    }

    public static List<String> getPickListValuesIntoList(){
       List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = Task.Leave_Reason__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        }
        pickListValuesList.sort();
        return pickListValuesList;
    }

    global static Map<String, String> getBUMetaDataValues(Map<Id, User> userQueryMap) {
        List<String> clusterList = new List<String>();

        for(Id userIdItr : userQueryMap.keySet()) {

            if(userQueryMap.get(userIdItr).Business_Unit__c == 'GFS') {
                clusterList.add('GFS');
            }
            if(userQueryMap.get(userIdItr).Business_Unit__c == 'Feeds') {
                clusterList.add('AGRO FEEDS');
            }
            if(userQueryMap.get(userIdItr).Business_Unit__c == 'Poultry') {
                clusterList.add('AGRO PM');
            }
        }

        List<BU_Specific_Metadata__mdt> buMetaDataQueryList = [SELECT Id
                                                                    , DeveloperName
                                                                    , MasterLabel
                                                                    , BU_Cluster__c
                                                                    , Activity__c
                                                                    , Work_Type__c
                                                                    , eForm__c
                                                                 FROM BU_Specific_Metadata__mdt
                                                                WHERE BU_Cluster__c IN :clusterList 
                                                               	ORDER BY MasterLabel
                                                              ];


        Map<String, String> metaDataMap = new Map<String, String>();

        metaDataMap.put('metaDataList', JSON.serialize(buMetaDataQueryList));
        metaDataMap.put('userMap', JSON.serialize(userQueryMap));

        return metaDataMap;
    }

    global static String returnUniqueCryptoKey() {

        return (( System.Userinfo.getUserId()
                + '-'
                + System.now().getTime()
                + '-'
                + EncodingUtil.base64Encode(Crypto.generateAesKey(256))
                ).subString(0, 57)
               );
    }
}