@isTest
private class AccountTrigger_Test {
    
    @testSetup static void setupTestData(){
        List<Pricing_Condition__c> pricingConditionList = new List<Pricing_Condition__c>();
        List<Market__c> marketList = new List<Market__c>();
        Pricing_Condition__c nationalPricing = new Pricing_Condition__c(
            Name = 'National List Price',
            SAP_Code__c = '12345'
            );
        pricingConditionList.add(nationalPricing);
        Pricing_Condition__c regionalPricing = new Pricing_Condition__c(
            Name = 'REGIONAL',
            SAP_Code__c = '89798'
            );
        pricingConditionList.add(regionalPricing);
        Pricing_Condition__c segmentPricing = new Pricing_Condition__c(
            Name = 'SEGMENT',
            SAP_Code__c = '098098'
            );
        pricingConditionList.add(segmentPricing);
        insert pricingConditionList;
        Market__c meatMarket = new Market__c(
            Name = 'Meats',
            Active__c = true,
            Market_Code__c = '01'
            );
        marketList.add(meatMarket);
        Market__c gfsMarket = new Market__c(
            Name = 'GFS',
            Active__c = true,
            Market_Code__c = '02'
            );
        marketList.add(gfsMarket);
        Market__c poultryMarket = new Market__c(
            Name = 'Poultry',
            Active__c = true,
            Market_Code__c = '03'
            );
        marketList.add(poultryMarket);
        insert marketList;
        TriggerFlagControl__c settings = new TriggerFlagControl__c(
            SetupOwnerId = UserInfo.getOrganizationId(),
            reflectPicklistValue__c = true,
            Account_CreateWarehouseForDistributor__c = true,
            Account_CreatePricingCondition__c = true,
            Account_PricingConditionName__c = 'National List Price'
            );
        insert settings;
    }
    static testMethod void testAccountInsertBranded() {
        List<Market__c> marketList = [SELECT Id,Name FROM Market__c LIMIT 20];
        Id distributorAccountId = Schema.sobjectType.Account.getRecordTypeInfosByName().get('Distributor/ Direct').getRecordTypeId();
        Test.startTest();
        Account acc = new Account();
        acc.Name = 'Sample Grocery1';
        acc.Sales_Organization__c  = 'Branded';
        acc.BU_Feeds__c = true;
        acc.Branded__c = true;
        acc.GFS__c = true;
        acc.BU_Poultry__c = true;
        acc.Market__c = marketList.get(0).Id;
        acc.RecordTypeId = distributorAccountId;
        acc.AccountNumber = 'ACT11';
        insert acc;
        Test.stopTest();
    }
    
    static testMethod void testAccountInsertGFS() {
        List<Market__c> marketList = [SELECT Id,Name FROM Market__c LIMIT 20];
        Id distributorAccountId = Schema.sobjectType.Account.getRecordTypeInfosByName().get('Distributor/ Direct').getRecordTypeId();
        Test.startTest();
        Account acc = new Account();
        acc.Name = 'Sample Grocery2';
        acc.Sales_Organization__c  = 'GFS';
        acc.BU_Feeds__c = true;
        acc.Branded__c = true;
        acc.GFS__c = true;
        acc.BU_Poultry__c = true;
        acc.Market__c = marketList.get(0).Id;
        acc.RecordTypeId = distributorAccountId;
        acc.AccountNumber = 'ACT21';
        insert acc;
        Test.stopTest();
    }
    
    static testMethod void testAccountInsertFeeds() {
        List<Market__c> marketList = [SELECT Id,Name FROM Market__c LIMIT 20];
        Id distributorAccountId = Schema.sobjectType.Account.getRecordTypeInfosByName().get('Distributor/ Direct').getRecordTypeId();
        Test.startTest();
        Account acc = new Account();
        acc.Name = 'Sample Grocery3';
        acc.Sales_Organization__c  = 'Feeds';
        acc.BU_Feeds__c = true;
        acc.Branded__c = true;
        acc.GFS__c = true;
        acc.BU_Poultry__c = true;
        acc.Market__c = marketList.get(0).Id;
        acc.RecordTypeId = distributorAccountId;
        acc.AccountNumber = 'ACT31';
        insert acc;
        Test.stopTest();
    }
    
    static testMethod void testAccountInsertPoultry() {
        List<Market__c> marketList = [SELECT Id,Name FROM Market__c LIMIT 20];
        Id distributorAccountId = Schema.sobjectType.Account.getRecordTypeInfosByName().get('Distributor/ Direct').getRecordTypeId();
        Test.startTest();
        Account acc = new Account();
        acc.Name = 'Sample Grocery4';
        acc.Sales_Organization__c  = 'Poultry';
        acc.BU_Feeds__c = true;
        acc.Branded__c = true;
        acc.GFS__c = true;
        acc.BU_Poultry__c = true;
        acc.Market__c = marketList.get(0).Id;
        acc.RecordTypeId = distributorAccountId;
         acc.AccountNumber = 'ACT41';
        insert acc;
        Account accountForUpdate = [SELECT Id,Market__c FROM Account WHERE Id = : acc.Id];
        accountForUpdate.Market__c = marketList.get(1).Id;
        update accountForUpdate;
        Test.stopTest();
    }
    
    
}