/**
    * AUTHOR        : LENNARD PAUL M SANTOS(DELOITTE)
    * DESCRIPTION   : HANDLES ALL TRIGGER EVENT PROCESS.
    * HISTORY       : (ANY UPDATES TO THE CODE MUST BE LOGGED HERE)
                    : JUN.9.2016 - CREATED.
**/
public with sharing class AccountTrigger_Handler {
    /**
    * AUTHOR        : LENNARD PAUL M SANTOS(DELOITTE)
    * DESCRIPTION   : HANDLES BEFORE INSERT TRIGGER EVENT
    * PARAM         : TRIGGER.NEW(LIST<Account>)
    * RETURN        : NONE
    * HISTORY       : (ANY UPDATES TO THE CODE MUST BE LOGGED HERE)
                    : JUN.9.2016 - CREATED.
    **/
    public static void onBeforeInsert(List<Account> newAccountList){
        List<Market__c> marketList = new List<Market__c>();
        Set<Id> BUIds = new Set<Id>();
        TriggerFlagControl__c settings = TriggerFlagControl__c.getInstance();
        //CHECK CUSTOM SETTING VALUE IF ENABLED
        if(settings != null){
            if(settings.reflectPicklistValue__c == true){
                for(Account acc : newAccountList){
                    if(acc.Market__c != null){
                        BUIds.add(acc.Market__c);
                    }
                }
                for(Market__c m : [SELECT Id, Name FROM Market__c WHERE Id IN : BUIds]){
                    marketList.add(m);
                }
                for(Account acc : newAccountList){
                    if(acc.Market__c!=null){
                        for(Market__c m : marketList){
                            if(acc.Market__c == m.Id){
                                acc.Business_UnitReportChart__c = m.Name;
                            }
                        }
                    }
                }
            }
            
            
        }
    }
    /**
    * AUTHOR        : LENNARD PAUL M SANTOS(DELOITTE)
    * DESCRIPTION   : HANDLES BEFORE UPDATE TRIGGER EVENT
    * PARAM         : TRIGGER.NEW(LIST<Account>), TRIGGER.OLDMAP(MAP<Id,Account>)
    * RETURN        : NONE
    * HISTORY       : (ANY UPDATES TO THE CODE MUST BE LOGGED HERE)
                    : JUN.9.2016 - CREATED.
    **/
    public static void onBeforeUpdate(List<Account> newAccountList,Map<Id,Account> oldAccountMap){
        List<Market__c> marketList = new List<Market__c>();
        Set<Id> BUIds = new Set<Id>();
        TriggerFlagControl__c settings = TriggerFlagControl__c.getInstance();
        //CHECK CUSTOM SETTING VALUE IF ENABLED
        if(settings != null){
            if(settings.reflectPicklistValue__c == true){
                for(Account acc : newAccountList){
                    Account oldAccount = oldAccountMap.get(acc.Id);
                    if(oldAccount.Market__c != acc.Market__c){
                        if(acc.Market__c != null){
                            BUIds.add(acc.Market__c);
                        }    
                    }
                }
                for(Market__c m : [SELECT Id, Name FROM Market__c WHERE Id IN : BUIds]){
                    marketList.add(m);
                }
                for(Account acc : newAccountList){
                    if(acc.Market__c!=null){
                        for(Market__c m : marketList){
                            if(acc.Market__c == m.Id){
                                acc.Business_UnitReportChart__c = m.Name;
                            }
                        }
                    }
                }
            }
        }
    }
    
    public static void onAfterInsert(List<Account> newAccountList){
        Id distributorAccountId = Schema.sobjectType.Account.getRecordTypeInfosByName().get('Distributor/ Direct').getRecordTypeId();
        TriggerFlagControl__c settings = TriggerFlagControl__c.getInstance();
        if(settings!=null){
            //CREATE PRICING CONDITION
            if(settings.Account_CreatePricingCondition__c == true && String.isNotBlank(settings.Account_PricingConditionName__c)){
                List<Pricing_Sequence_Condition__c> seqList = new List<Pricing_Sequence_Condition__c>();
                Map<string, Pricing_Condition__c> mapCondition = new Map<string, Pricing_Condition__c>();
                for(Pricing_Condition__c pc : [Select Id,Name, SAP_Code__c From Pricing_Condition__c]){
                    mapCondition.put(pc.Name, pc);
                }
                for (Account acc : newAccountList){
                    if(acc.RecordTypeId == distributorAccountId){
                        Pricing_Sequence_Condition__c seq = new Pricing_Sequence_Condition__c();
                        seq.Account__c = acc.Id;
                        seq.Sequence__c = 1;
                        if(mapCondition.containsKey(settings.Account_PricingConditionName__c)){
                            seq.Pricing_Condition__c = mapCondition.get(settings.Account_PricingConditionName__c).Id;     
                        }
                        seqList.add(seq);
                    }
                    
                }
                
                if(seqList.size() > 0){
                    insert seqList;
                }    
            }
            //CREATE WAREHOUSE
            if(settings.Account_CreateWarehouseForDistributor__c == true){
                //CREATE WAREHOUSE RECORDS
                List<Account> accountForUpdateList = new List<Account>();
                List<Warehouse__c> warehouseForInsertList = new List<Warehouse__c>();
                Set<Id> accountIds = new Set<Id>();
                Map<Id,Account> accountMap = new Map<Id,Account>();
                for(Account acct : newAccountList){
                    if(acct.Warehouse__c == null && acct.RecordTypeId == distributorAccountId){
                        Warehouse__c wr = new Warehouse__c(
                            Account__c = acct.Id,
                            Name = acct.Name
                            );
                        warehouseForInsertList.add(wr);
                        accountIds.add(acct.Id);
                    }
                }
                if(warehouseForInsertList.size()>0){
                    insert warehouseForInsertList;
                }
                for(Account acc : [SELECT Id,Warehouse__c FROM Account WHERE Id IN : accountIds]){
                    for(Warehouse__c wr : warehouseForInsertList){
                        if(acc.Id == wr.Account__c){
                            acc.Warehouse__c = wr.Id;
                            accountForUpdateList.add(acc);
                        }
                    }
                }
                
                
                if(accountForUpdateList.size()>0){
                    update accountForUpdateList;
                }
                
            }    
        }
        
        
        
    }
}