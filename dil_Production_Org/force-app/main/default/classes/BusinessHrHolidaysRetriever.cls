public with sharing class BusinessHrHolidaysRetriever {

    public List<Holiday> retrieveHolidays(BusinessHours bh) {

        List<BusinessHours> bHrs = [SELECT Id FROM BusinessHours WHERE Id = :bh.Id];
        
        if(bHrs.isEmpty()) {
            throw new InvalidDataException('Failed to retrieve holidays for a given business hour. Input business hour is invalid');
        }
        PageReference pg = new PageReference(BusinessHrHolidaysRetrieverConstants.HOLIDAY_URL_PREFIX + bh.Id);
        Blob content = Blob.valueOf(BusinessHrHolidaysRetrieverConstants.BLOB_CONTENT_FOR_HOLIDAY_TEST);
        if(!System.Test.isRunningTest()) { content = pg.getContent(); }
        
        Set<String> holidayIds = retrieveBusinessHourHolidays(BusinessHrHolidaysRetrieverConstants.HOLIDAY_PREFIX, content);
        List<Holiday> holidays = [  SELECT SystemModstamp, StartTimeInMinutes, RecurrenceType, RecurrenceStartDate, RecurrenceMonthOfYear, 
                                        RecurrenceInterval, RecurrenceInstance, RecurrenceEndDateOnly, RecurrenceDayOfWeekMask, RecurrenceDayOfMonth, Name,
                                        LastModifiedDate, LastModifiedById, IsRecurrence, IsAllDay, Id, EndTimeInMinutes, Description, CreatedDate, CreatedById, ActivityDate 
                                    FROM Holiday 
                                    WHERE Id IN :holidayIds ];
        return holidays;
    }

    private static Set<String> retrieveBusinessHourHolidays(String keyPrefix, Blob content) {
    
        try {
            Integer tableStartIndex = content.toString().indexOf(BusinessHrHolidaysRetrieverConstants.PAGE_BODY_CLASS);
            String subStr = content.toString().substring(tableStartIndex);
            subStr = subStr.substring(subStr.indexOf(BusinessHrHolidaysRetrieverConstants.TABLE_ROW_END));
            
            String strWithBusinessHrHolidays = subStr.substring(subStr.indexOf(BusinessHrHolidaysRetrieverConstants.TABLE_HEADER_START));
            Set<String> recordIds = new Set<String>();
            while(strWithBusinessHrHolidays.contains(keyPrefix)) {
                String strWithRecordId = strWithBusinessHrHolidays.substring(strWithBusinessHrHolidays.indexOf(BusinessHrHolidaysRetrieverConstants.ANCHOR_TAG_START), strWithBusinessHrHolidays.indexOf(BusinessHrHolidaysRetrieverConstants.ANCHOR_TAG_END));
                String recordId = strWithRecordId.substring(strWithRecordId.indexOf(BusinessHrHolidaysRetrieverConstants.HREF_STRING) + BusinessHrHolidaysRetrieverConstants.HREF_STRING.length(),strWithRecordId.indexOf(BusinessHrHolidaysRetrieverConstants.CLOSING_TAG));
                String recordName = strWithRecordId.substring(strWithRecordId.indexOf(BusinessHrHolidaysRetrieverConstants.CLOSING_TAG) + BusinessHrHolidaysRetrieverConstants.CLOSING_TAG.length());
                if(!recordId.contains(BusinessHrHolidaysRetrieverConstants.ROWSPERPAGE)) {              
                    recordIds.add(recordId);             
                }   
                strWithBusinessHrHolidays = strWithBusinessHrHolidays.substring(strWithBusinessHrHolidays.indexOf(BusinessHrHolidaysRetrieverConstants.ANCHOR_TAG_END) + BusinessHrHolidaysRetrieverConstants.ANCHOR_TAG_END.length());
            }
            
            return recordIds;
        }
        catch(Exception e) { return null; }
    }
    public class InvalidDataException extends Exception {}
}