trigger EndOfDayTrigger on End_of_Day__c (after insert,before insert) {
    if(StaticResources.byPassStartOrEndOfDayTrigger){
       return;         
    }
    if(Trigger.isAfter) {
        new ParseJsonForRecordDML().createRecord(Trigger.new);
    }
    
    if(Trigger.isBefore) {
        new EndOfDayTriggerHandler().populateStartOfDay(Trigger.new);
    }
    
}