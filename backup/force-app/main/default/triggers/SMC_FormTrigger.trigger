trigger SMC_FormTrigger on ePAW_Form__c (after insert, after update, before insert, before update)
{
    if(Trigger.isAfter)
    {
        if(Trigger.isInsert)
        {
            System.debug('=== Initiating Trigger Event: Trigger.isInsert | Running: onAfterInsert() ===');
            SMC_FormEventTriggerHandler.onAfterInsert(Trigger.New);
            System.debug('>>> Terminating Trigger Event: Trigger.isInsert | Terminating: onAfterInsert() <<<');
        }
        else if(Trigger.isUpdate)
        {
            System.debug('Trigger Event: Trigger.isUpdate | Running: onAfterUpdate()');
			SMC_FormEventTriggerHandler.onAfterUpdate(Trigger.New, Trigger.OldMap, Trigger.NewMap);
			System.debug('>>> Terminating Trigger Event: Trigger.isUpdate | Terminating: onAfterUpdate() <<<');
        }
    }
    else if(Trigger.isBefore)
    {
        if(Trigger.isInsert)
        {
            System.debug('=== Executing [SMC_FormTrigger] isInsert Trigger Context ===');

            // Goal: Save Match Key Criteria in field: Match_Key_Code__c
            SMC_FormEventTriggerHandler.onInsertUpdateEpawMatchKey(Trigger.New, Trigger.oldMap, Trigger.newMap);
            System.debug('>>> Terminating [SMC_FormTrigger] isInsert Trigger Context <<<');
        }
        else if(Trigger.isUpdate)
        {
            System.debug('=== Executing [SMC_FormTrigger] isUpdate Trigger Context ===');

            // Goal: Save Match Key Criteria in field: Match_Key_Code__c
            SMC_FormEventTriggerHandler.onInsertUpdateEpawMatchKey(Trigger.New, Trigger.oldMap, Trigger.newMap);
            System.debug('>>> Terminating [SMC_FormTrigger] isUpdate Trigger Context <<<');
        }
    }
}