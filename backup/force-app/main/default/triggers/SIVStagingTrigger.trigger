trigger SIVStagingTrigger on SIV_Staging__c (after insert) {


    if(Trigger.isAfter){
        if(Trigger.isInsert){
            SIVStagingTrigger_Handler.onAfterInsert(Trigger.new);
        }
    }
    // List<SIV_Staging__c> listSIV = trigger.new; 
    // List<Inventory__c> inventoryList = new List<Inventory__c>(); 
    // List<Inventory_Item__c> itemList = new List<Inventory_Item__c> ();
    // List<Inventory_Line__c> lineList = new List<Inventory_Line__c> ();

    // //string in this Map will contain unique id. Combination of material code + / + Sales Order No
    // Map <string, SIVWrapper > mapSIV = new Map <string, SIVWrapper>();
    
    // //string in this Map will contain unique id. Combination of material code + / + Sales Order No
    // Map <string, SIVWrapper > mapSIVReturn = new Map <string, SIVWrapper>();
    
    // //string in  this set is a combination of distributor code + / + BU code 
    // Set<string> accountDataLoaderIds = new Set<string>(); 
    
    // List<SIV_Staging__c> devueltoSIV = new List<SIV_Staging__c>();
    // Set<string> skuCodes = new Set<string>(); 

    // //Populate the wrapper
    // //From SIV Staging, create Inventory, Inventory Item, and Inventory Line     
    // for(SIV_Staging__c  siv : listSIV){
        
    //     if(siv.Sales_Qty__c.endsWith('-') || siv.SIV_Value__c.endsWith('-')){
    //         skuCodes.add(siv.Material__c);
    //         devueltoSIV.add(siv);
    //     }else{            
        
    //         Inventory__c inv = new Inventory__c ();
    //         Inventory_Item__c item = new Inventory_Item__c ();
    //         Inventory_Line__c line = new Inventory_Line__c ();
            
    //         SIVWrapper wrapper = new SIVWrapper ();
            
    //         line.SI_No__c = siv.Reference_Doc_No__c;
    //         line.SO_No__c = siv.Sales_Document__c;
    //         line.Incoming_Quantity__c = decimal.valueOf(siv.Sales_Qty__c).setScale(2);
    //         line.SIV_Value__c = siv.SIV_Value__c;
            
    //         wrapper.Inv = inv;
    //         wrapper.Inv.SIV_Unique__c = siv.Material__c + '/' + siv.Sales_Document__c;
            
    //         wrapper.Item = item;
    //         wrapper.Item.SIV_Unique__c = siv.Material__c + '/' + siv.Sales_Document__c;
            
    //         wrapper.Line = line;
    //         wrapper.distributorCode = siv.Customer__c.replaceFirst('^0+(?!$)', '');
    //         wrapper.skuCode = siv.Material__c;
    //         wrapper.buCode = siv.Sales_Org__c;
    //         wrapper.uomName = siv.UOM__c;
    //         wrapper.channelCode = siv.Distribution_Channel__c;
            
    //         if (line.Validated_Incoming_Quantity__c > 0){
    //             mapSIV.put(siv.Material__c + '/' + siv.Sales_Document__c, wrapper); 
    //             accountDataLoaderIds.add(siv.Customer__c + '/' + siv.Sales_Org__c);
    //             skuCodes.add(siv.Material__c);
    //         }
    //         else{
    //             mapSIVReturn.put(siv.Material__c + '/' + siv.Sales_Document__c, wrapper);       
    //         }
    //     }

    // }
    
    // //Get accounts so we can use it for the look-up in Inventory
    // List<Account> customerList = [Select Distributor_Code__c, Dataloader_ID__c From Account
    //                                 Where Dataloader_ID__c IN  :accountDataLoaderIds];                            
    // Map<string, Account> mapAccount = new Map<string, Account>();                                
    // for (Account acc : customerList ){
    //     mapAccount.put(acc.Dataloader_ID__c, acc);
    // }
    
  
    // //Get products so we can use it for the look-up in Inventory Item 
    // List<Custom_Product__c> productList = [Select SKU_Code__c From Custom_Product__c
    //                                     Where SKU_Code__c IN :skuCodes];  
      
    //   system.debug('productList: '+productList );                                  
                                          
    // Map<string, Custom_Product__c> mapProduct = new Map<string, Custom_Product__c>();
    
    // for(Custom_Product__c p : productList ){
    //     mapProduct.put(p.SKU_Code__c, p);
    // }
    
       
    //  //try{  
    // //Update our wrapper by assigning the account Id to the Inventory account look-up
    // // and assigning the product Id to the Inventory Item product look-up
    // for (SIVWrapper wrapper : mapSIV.values()){
    //     if (mapAccount.get(wrapper.distributorCode + '/' + wrapper.buCode + '/' + wrapper.channelCode) != null){
    //         Account ac = mapAccount.get(wrapper.distributorCode + '/' + wrapper.buCode + '/' + wrapper.channelCode);
    //         wrapper.accountId = ac.Id;
    //         wrapper.Inv.Account__c = ac.Id;
    //     }
        
    //     if(mapProduct.get(wrapper.skuCode) != null){
    //         wrapper.Item.Product__c = mapProduct.get(wrapper.skuCode).Id;
    //         wrapper.productId  = mapProduct.get(wrapper.skuCode).Id;
            
    //         System.debug('wrapper.productId: '+wrapper.productId);
    //     }
        
    //     inventoryList.add(wrapper.Inv);
    
    // }
        
    // if (inventoryList.size() > 0)
    //     insert inventoryList; 
    
        
    //     System.debug('inventoryList: '+inventoryList);
        
    // for(Inventory__c invRes : inventoryList){
    //     if (mapSIV.get(invRes.SIV_Unique__c) != null){
    //         SIVWrapper  wrap = mapSIV.get(invRes.SIV_Unique__c);
    //         wrap.Item.Inventory__c = invRes.Id;
            
    //         itemList.add(wrap.Item);
    //     }
    // }
    
    //  if (itemList.size() > 0)
    //     insert itemList;
        
    //     System.debug('itemList: '+itemList);
        
        
    //   Set<Id> productIds = (new Map<Id,Custom_Product__c>(productList)).keySet();
    //   List<Conversion__c> uomList = [Select Dataloader_ID__c, UOM_name__c, Product__c From Conversion__c Where Product__c IN :productIds];
      
    //   //This is use to get the Id of Conversion (UOM)
    //   //I use the combination of product id + / + UOM Name
    //   Map<string, Conversion__c> uomMap = new Map<string, Conversion__c>();
    //   for (Conversion__c conv : uomList){
    //       uomMap.put(conv.Dataloader_ID__c, conv);
    //   }
      
    //   System.debug('uomMap: ' + uomMap);
        
    //   for(Inventory_Item__c itemRes : itemList){
    //     if (mapSIV.get(itemRes.SIV_Unique__c) != null){
    //         SIVWrapper  wrap = mapSIV.get(itemRes.SIV_Unique__c);
    //         wrap.Line.Inventory_Item__c = itemRes.Id;
            
    //         System.debug('&&&&&'+wrap.skuCode + '/' + wrap.uomName);

    //         if (uomMap.get(wrap.skuCode + '/' + wrap.uomName) != null){
    //             wrap.Line.UOM__c = uomMap.get(wrap.skuCode + '/' + wrap.uomName).Id; 
    //         }
            
    //         lineList.add(wrap.Line);
    //     }
    // }
    
    // if (lineList.size() > 0)
    //     insert lineList;
     
    //  //==========================================   
     
    // if (devueltoSIV.size() > 0){
       
    //     List<String> strList = new List<string>();
    //     for (SIV_Staging__c siv:  devueltoSIV){
    //         strList.add(siv.Material__c + '/' + siv.Sales_Document__c);
    //     }
        
    //     List<Inventory_Item__c> listDevuelto = [Select Id, SIV_Unique__c From Inventory_Item__c where SIV_Unique__c IN :strList ];
    //     List<Inventory_Line__c > listLineDevuelto = new List<Inventory_Line__c >();
        
    //     for (Inventory_Item__c item : listDevuelto ){
    //         for (SIV_Staging__c siv:  devueltoSIV){
    //              if(item.SIV_Unique__c == (siv.Material__c + '/' + siv.Sales_Document__c)){
    //                 Inventory_Line__c line = new Inventory_Line__c();
    //                 line.Inventory_Item__c = item.Id;
    //                 line.Incoming_Quantity__c = decimal.valueOf(siv.Sales_Qty__c.remove('-'));
                    
                    
    //                 System.debug('devuelto uom '+ uomMap.get(siv.Material__c + '/' + siv.UOM__c));
    //                  if (uomMap.get(siv.Material__c + '/' + siv.UOM__c) != null){
    //                         line.UOM__c = uomMap.get(siv.Material__c + '/' + siv.UOM__c).Id; 
    //                   }
                    
    //                 listLineDevuelto.add(line);
    //             }
    //         }
    //     }
        
    //     insert listLineDevuelto;
            


    // }//end if
    

    //Delete [Select Id From SIV_Staging__c ];
}