var atLeastOneError = false;
var contactReportForm = {};
contactReportForm.discObj = {};
contactReportForm.discObjList = [];
contactReportForm.nextStepObj = {};
contactReportForm.nextStepObjList = [];
contactReportForm.discObj.discussedItemDetails = [];
contactReportForm.discObj.nextItemDetails =[];

$(document).ready(function() {
	//Added new modal for discussed items and next steps
	contactReportForm.UserName = userName;
	contactReportForm.Id = idParam;
    console.log('...contactReportForm.Id: ', contactReportForm.Id);


	// $("#preparedBy").val(contactReportForm.UserName);

	 $(".addDiscussedItemBtn").click(function(){
        $('.addDiscussedItemModal').modal('show');
     });


	 $(".addNextStepsItemBtn").click(function(){
        $('.addNextStepsItemModal').modal('show');
     });


    $('#blurDivId').show();
    $('.datepicker').datepicker({
		startDate: new Date()
    });

    $(".nav-tabs a").click(function() {
        $(this).tab('show');
    });

    $("#saveId").click(function() {
        submitBool = false;
        //alert("!!!!!!!!!!!!");
        $('#blurDivId').show();
        highlightAllErrors();
        location. reload(true);
    });

    $("#submitId").click(function() {
        //alert("!!!!!!!!!!!!");
        submitBool = true;
        $('#blurDivId').show();
        highlightAllErrors();
        console.log('this should close');
    });

    $("#cancelId").click(function() {
        window.close();
    });

    //getUserList();
    //console.log('----idParam------',idParam);
    if (undefined != idParam && '' != idParam) {
        //alert('Running');
        fetchContatReport();
    }

    $("#dateId, #outletNameId, #placeId, #attendeesId").on('change blur', function() {
        validationOnBlur();
    });

	if(undefined != contactReportForm.Id && '' != contactReportForm.Id ) {
		//momForm.fetchMOMRecord();
    } else {
		alert('Record id is missing.!');
        $('#blurDivId').hide();
    }
});

function fetchContactReportData(){



}

function addDiscussedItemsOnForm(){
	//disscussedItemTopic
	//disscussedItemAgreements
	$('#blurDivId').show();
    var discObj = {};

    //momForm.populateMOMDetails();
	if(($('#disscussedItemTopic').val().trim() != "") || ($('#disscussedItemAgreements').val().trim() != "")){
	    discObj.Topic__c= $('#disscussedItemTopic').val();
		discObj.Agreements_Issues_Concerns__c= $('#disscussedItemAgreements').val();
		contactReportForm.discObj.discussedItemDetails.push(discObj);
		contactReportForm.discObjList.push(discObj);
		populateDiscussedItem();
	}
    $('.addDiscussedItemModal').modal('hide');
    $('#disscussedItemTopic').val('');
    $('#disscussedItemAgreements').val('');
    $('#blurDivId').hide();
}

function populateDiscussedItem(){
	if(!contactReportForm.discObj.discussedItemDetails){
        contactReportForm.discObj.discussedItemDetails = [];
    }
	var template = $("#discussedItemsDetailsTemplate").html();
    $("#discussedItemsDetailsTarget").html(
		_.template(
			template
			,{
				"items" : contactReportForm.discObj.discussedItemDetails
            }
        )
    );
}

function addNextItemsOnForm(){
	$('#blurDivId').show();
    var discObj = {};
	if($('#nextStepItemActivities').val().trim() != "" ||  $('#nextStepItemPersonInCharge').val().trim() != "" || $('#nextStepItemTargetDate').val().trim() != ""){
		discObj.Actvitivies__c = $('#nextStepItemActivities').val();
		discObj.Person_In_Charge__c = $('#nextStepItemPersonInCharge').val();
		discObj.Target_Date__c = $('#nextStepItemTargetDate').val();

		contactReportForm.discObj.nextItemDetails.push(discObj);
		contactReportForm.discObjList.push(discObj);
		//momForm.populateMOMDetails();
		populateNextItem();
	}
    $('.addNextStepsItemModal').modal('hide');
    $('#nextStepItemActivities').val('');
    $('#nextStepItemPersonInCharge').val('');
	$('#nextStepItemTargetDate').val('');
    $('#blurDivId').hide();
}

function populateNextItem(){
	if(!contactReportForm.discObj.nextItemDetails){
        contactReportForm.discObj.nextItemDetails = [];
    }else{

	}
	var template = $("#nextItemsDetailsTemplate").html();
    $("#nextStepsItemsDetailsTarget").html(
		_.template(
			template
			,{
				"items" : contactReportForm.discObj.nextItemDetails
            }
        )
    );
}

function formatDate(timestamp) {
	if(timestamp != null)
		return moment(timestamp).format('L')
	else
		return '';
}

function sendContactReporttDataToController() {

    //alert('!!!!!!!!!!!!');

    var contactReporttData = {};
    contactReporttData.territory = $("#territoryId").val();
    contactReporttData.crDate = $("#dateId").val();
    contactReporttData.outletName = $("#outletNameId").val();
    contactReporttData.place = $("#placeId").val();

    contactReporttData.objectives = $("#objectivesId").val();
    contactReporttData.issuesConcerns = $("#issuesConcernsId").val();
    contactReporttData.nextSteps = $("#nextStepsId").val();
    contactReporttData.conforme = $("#conformeId").val();

    contactReporttData.id = idParam;
    //console.log('-----submitBool-------->',submitBool);
    if (submitBool) {
        contactReporttData.status = 'Submitted';
    } else {
        contactReporttData.status = 'Saved';
    }
    //console.log('-----contactReporttData.status-------->', contactReporttData.status);
    console.log('--------Json String----->',JSON.stringify(contactReporttData));

    contactReporttData.attendees = $("#attendeesId").val();

    Visualforce.remoting.Manager.invokeAction(
        'ContactReportForm.insertContactReportData',
        JSON.stringify(contactReporttData),
		JSON.stringify(contactReportForm.discObj.discussedItemDetails),
		JSON.stringify(contactReportForm.discObj.nextItemDetails),
        function(result, event) {
            if (event.status) {
                $('#blurDivId').hide();
				if (!atLeastOneError && submitBool) {
					window.close();
				}
            }
        }, {
            escape: false
        }
    );
    //$('#blurDivId').hide();
}
function fetchContatReport() {
    Visualforce.remoting.Manager.invokeAction(
        'ContactReportForm.getExistingContactReport',
        idParam,
        function(result, event) {
            if (event.status) {
                console.log('yay---',result);
                populateContactReport(result.objCR, result.objTask);
				if(  result.objCR
				  && result.objCR.Contact_Report_Next_Steps__r
				  ){
				    contactReportForm.discObj.nextItemDetails = result.objCR.Contact_Report_Next_Steps__r;
				}
				if(  result.objCR
				  && result.objCR.Contact_Report_Discussed_Items__r
				  ){
				    contactReportForm.discObj.discussedItemDetails = result.objCR.Contact_Report_Discussed_Items__r;
				}
				//contactReportForm.eformId = result.objCr.eForm__c;
				populateDiscussedItem();
				populateNextItem();
				fetchAccountName(result.objCR.eForm__c);
            } else if (event.type === 'exception') {

            }
        }, {
            escape: false
        }
    );
}

function fetchAccountName(eformId) {
	Visualforce.remoting.Manager.invokeAction(
        'ContactReportForm.getAccountName',
        eformId,
        function(result, event) {
            if (event.status) {
                console.log('Account info---');
				console.log(result);
				if(result != null){
					$('#outletNameId').val(result.AccountName);
					$('#dateId').val(result.StartDate);
				}
            } else if (event.type === 'exception') {

            }
        }, {
            escape: false
        }
    );
}

function populateContactReport(contRepInstance, taskInstance) {
    console.log('contRepInstance----',contRepInstance);
    $("#territoryId").val(contRepInstance.Territory__c);
    //console.log('-------contRepInstance.Date__c---------', contRepInstance.Date__c);
    var dt = new Date(contRepInstance.Date__c);
    $("#dateId").val((dt.getMonth() + 1) + '/' + (dt.getDate()) + '/' + dt.getFullYear());
    $("#preparedBy").val(contRepInstance.CreatedBy.Name);
    //$("#dateId").val(dt);
    $("#outletNameId").val(contRepInstance.Outlet_Name__c);
    $("#placeId").val(contRepInstance.Venue__c);
    $("#objectivesId").val(contRepInstance.Objective__c);
    $("#issuesConcernsId").val(contRepInstance.Issues_and_Concerns__c);
    $("#nextStepsId").val(contRepInstance.Next_Step__c);
    $("#attendeesId").val(contRepInstance.Attendees__c);
    $("#conformeId").val(contRepInstance.Conforme__c);

    if (contRepInstance.Status__c == 'Submitted' || contRepInstance.eForm__r.OwnerId != loggedInUserID || !taskInstance.Check_in__c) {
        disableForm();
    }
    $('#blurDivId').hide();
}

function disableForm() {
    $('.btnSubmit').attr('disabled', 'disabled');
    $('.form-control, select').attr('disabled', 'disabled');
	$('.addDiscussedItemBtn').attr('disabled', 'disabled');
	$('.addNextStepsItemBtn').attr('disabled', 'disabled');
}

function validationOnBlur() {

    $('#attendeesIdError').css('display', 'none');
	$('#objectivesIdError').css('display', 'none');

	if (!$("#objectivesId").val() || !$("#objectivesId").val().trim()) {
        $('#objectivesIdError').css('display', '');
    } else {
        return;
    }

    if (!$("#attendeesId").val() || $("#attendeesId").val().length == 0) {
        $('#attendeesIdError').css('display', '');
    } else {
        return;
    }

}

function highlightAllErrors() {
    atLeastOneError = false;

	if (!$("#objectivesId").val() || $("#objectivesId").val().length == 0) {
        $('#objectivesIdError').css('display', '');
        atLeastOneError = true;
    }

    if (!$("#attendeesId").val() || $("#attendeesId").val().length == 0) {
        $('#attendeesIdError').css('display', '');
        atLeastOneError = true;
    }

    if (atLeastOneError) {
        alert('Please enter valid details!!!');
    } else {
        sendContactReporttDataToController();
        alert('Data Saved Successfully !!!\n Please Refresh the Calendar to see updated data !!!');
    }
    $('#blurDivId').hide();
}
