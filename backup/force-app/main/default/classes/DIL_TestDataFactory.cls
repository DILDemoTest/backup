@isTest
public class DIL_TestDataFactory {

    public static List<Account> getAccounts(Integer count, Boolean isInsert){

        List<Account> accountList = new List<Account>();
        for(Integer i=0; i<count; i++){
            Account acc = new Account();
            acc.Name = 'Test Acc - '+i;
            acc.AccountNumber = '4';
            accountList.add(acc);
        }

        if(isInsert){
            insert accountList;
        }
        return accountList;
    }

    // Method to create Test City
    public static List<City__c> getCity(Integer count, Boolean isInsert) {

        List<City__c> cityList = new List<City__c>();
        for(Integer i = 1; i <= count; i++) {
            cityList.add(new City__c(Name = 'Test City - '+i,
                                     Dataloader_ID__c = 'Test'));
        }
        if(isInsert){
            insert cityList;
        }
        return cityList;
    }

    // Method to create Test Region
    public static List<Region__c> getRegion(Integer count, Boolean isInsert) {

        List<Region__c> regionList = new List<Region__c>();
        for(Integer i = 1; i <= count; i++) {
            regionList.add(new Region__c(Dataloader_Id__c = 'Test'+i));
        }
        if(isInsert){
            insert regionList;
        }
        return regionList;
    }

    // Method to create Test Province
    public static List<Province__c> getProvince(Integer count, Boolean isInsert) {

        List<Province__c> provinceList = new List<Province__c>();
        for(Integer i = 1; i <= count; i++) {
            provinceList.add(new Province__c(Name = 'Test Province - '+i,
                                     Dataloader_ID__c = 'Test'+i));
        }
        if(isInsert){
            insert provinceList;
        }
        return provinceList;
    }

    // Method to create Test Customer Group
    public static List<Chains__c> getCustomerGroup(Integer count, Boolean isInsert) {

        List<Chains__c> custGrpList = new List<Chains__c>();
        for(Integer i = 1; i <= count; i++) {
            custGrpList.add(new Chains__c(Description__c='Test CG - '+i,
                                          Group_No__c = i));
        }
        if(isInsert){
            insert custGrpList;
        }
        return custGrpList;
    }

    // Method to create Test Month
    public static List<Month__c> getMonth(Integer count, Boolean isInsert) {

        List<Month__c> monthList = new List<Month__c>();
        DateTime d = datetime.now();
        for(Integer i=0; i<count; i++) {
            String monthName = d.addMonths(i).format('MMMMM');
            monthList.add(new Month__c(Month__c = monthName,
                                       Year__c = String.valueOf(System.Today().addMonths(1).year()),
                                       OwnerId = UserInfo.getUserId()));
        }
        if(isInsert){
            insert monthList;
        }
        return monthList;
    }

    // Method to create Test Tasks
    public static List<Task> getTasks(Integer count, Boolean isInsert) {

        List<Task> lstTask = new List<Task>();
        for(Integer i=0; i<count; i++){
            lstTask.add(new Task(Subject = 'Task - '+i,
                                    StartDateTime__c = System.now().addMonths(1),
                                    EndDateTime__c = System.now().addMonths(1).addHours(1),
                                    Work_Type__c = 'Office work',
                                    User__c = UserInfo.getUserId()));
        }

        if(isInsert){
            insert lstTask;
        }
        return lstTask;
    }

    // Method to create Test Tasks
    public static List<Account_Configuration__c> getConfig(Integer count, Boolean isInsert) {

        List<Account_Configuration__c> lstConfig = new List<Account_Configuration__c>();
        for(Integer i=0; i<count; i++){
            lstConfig.add(new Account_Configuration__c(Subject__c = 'Config - '+i,
                                    StartDateTime__c = System.now().addMonths(1),
                                    EndDateTime__c = System.now().addMonths(1).addHours(1),
                                    User__c = UserInfo.getUserId()));
        }

        if(isInsert){
            insert lstConfig;
        }
        return lstConfig;
    }

    // Method to create Test Contact Reports
    public static List<Contact_Report__c> getContactReports(Integer count, Boolean isInsert) {

        List<Contact_Report__c> lstContReps = new List<Contact_Report__c>();

        eForm__c objEform = new eForm__c();
        insert objEform;

        for(Integer i=0; i<count; i++) {
            Contact_Report__c objContRep = new Contact_Report__c();
            objContRep.Territory__c = 'Test TERRITORY';
            objContRep.Date__c = Date.today();
            objContRep.Outlet_Name__c = 'Test OUTLET NAME';
            objContRep.Venue__c = 'Test PLACE';
            objContRep.Objective__c = 'Test OBJECTIVES';
            objContRep.Issues_and_Concerns__c = 'Test ISSUES & CONCERNS';
            objContRep.Next_Step__c = 'Test NEXT STEPS';
            objContRep.Conforme__c = 'Test CONFORME';
            objContRep.Attendees__c = 'User 1,User 2';
            objContRep.Status__c = 'Saved';
            objContRep.eForm__c = objEform.Id;
            lstContReps.add(objContRep);
        }
        if(isInsert){
            insert lstContReps;
        }
        return lstContReps;
    }

    //Method to create TradeVisit Record
    public static List<Trade_Visit__c> getTradeVisit(Integer count, Boolean isInsert) {
        List<Trade_Visit__c> lstTradeVisit = new List<Trade_Visit__c>();
        eForm__c objEform = new eForm__c();
        insert objEform;

        for(Integer i=0; i<count; i++) {
            Trade_Visit__c objTrade = new Trade_Visit__c();
            objTrade.eForm__c = objEform.Id;
            objTrade.Business_Address__c = 'Test Address';
            lstTradeVisit.add(objTrade);
        }

        if(isInsert){
            insert lstTradeVisit;
        }
        return lstTradeVisit;
    }

    //Method to create TradeVisit Record
    public static List<Service_Level__c> getServiceLevel(Integer count, Boolean isInsert) {
        List<Service_Level__c> lstServiceLevel = new List<Service_Level__c>();
        eForm__c objEform = new eForm__c();
        insert objEform;

        for(Integer i=0; i<count; i++) {
            Service_Level__c objService = new Service_Level__c();
            objService.eForm__c = objEform.Id;
            lstServiceLevel.add(objService);
        }

        if(isInsert){
            insert lstServiceLevel;
        }
        return lstServiceLevel;
    }

    public static Service_Level_Standards_Details__c getStandardDetail(Id objServiceId, String product){
        Service_Level_Standards_Details__c objServiceSD = new Service_Level_Standards_Details__c();
        objServiceSD.Service_Level__c = objServiceId;
        objServiceSD.Product__c = product;
        return objServiceSD;
    }

    //Method to create TradeVisit Record
    public static List<Service_Level_Standards_Details__c> getServiceLevelStandardDetails(Id objServiceId, Boolean isInsert) {
        List<Service_Level_Standards_Details__c> lstServiceLevelStandardDetails = new List<Service_Level_Standards_Details__c>();

        lstServiceLevelStandardDetails.add(getStandardDetail(objServiceId, 'Greets customer with a smile'));
        lstServiceLevelStandardDetails.add(getStandardDetail(objServiceId, 'Suggestive Selling'));

        if(isInsert){
            insert lstServiceLevelStandardDetails;
        }
        return lstServiceLevelStandardDetails;
    }

    public static Service_Level_Product_Details__c getProductDetail(Id objServiceId, Id recordTypeID, String product){
        Service_Level_Product_Details__c objServicePD = new Service_Level_Product_Details__c();
        objServicePD.Service_Level__c = objServiceId;
        objServicePD.Product__c = product;
        objServicePD.RecordTypeId = recordTypeID;
        return objServicePD;
    }

    public static List<Service_Level_Product_Details__c> getProductDetailList(Id objServiceId, Boolean isInsert) {
        List<Service_Level_Product_Details__c> lstServiceLevelProductDetails = new List<Service_Level_Product_Details__c>();

        Schema.DescribeSObjectResult desResult = Schema.SObjectType.Service_Level_Product_Details__c;
        Map<String,Schema.RecordTypeInfo> rtMapByName = desResult.getRecordTypeInfosByName();

        lstServiceLevelProductDetails.add(getProductDetail(objServiceId, rtMapByName.get(Label.RT_Display_Quality).getRecordTypeId(), 'Beef Timplados'));
        lstServiceLevelProductDetails.add(getProductDetail(objServiceId, rtMapByName.get(Label.RT_Product_Availability).getRecordTypeId(), 'Beef Timplados'));
        lstServiceLevelProductDetails.add(getProductDetail(objServiceId, rtMapByName.get(Label.RT_Product_Quality).getRecordTypeId(), 'Beef Timplados'));

        if(isInsert){
            insert lstServiceLevelProductDetails;
        }
        return lstServiceLevelProductDetails;
    }

    // Method to create Test Moms
    public static List<MOM__c> getMoms(Integer count, Boolean isInsert) {

        List<MOM__c> lstMoms = new List<MOM__c>();

        eForm__c objEform = new eForm__c();
        insert objEform;

        for(Integer i=0; i<count; i++) {
            MOM__c objMom = new MOM__c();
            objMom.eForm__c = objEform.Id;
            lstMoms.add(objMom);
            /*
            //objMom.Attendees__c =
            //objMom.Copies_to__c =
            //objMom.Meeting_Date__c =
            //objMom.Minutes_By__c =
            //objMom.Minutes_By__r.Name =
            //objMom.Month__c =
            //objMom.Objective__c =
            //objMom.Status__c =
            //objMom.Time_to_Finish__c =
            //objMom.Time_to_Start__c =
            //objMom.Venue__c =
            */
        }
        if(isInsert){
            insert lstMoms;
        }
        return lstMoms;
    }


    public static List<User> createBulkUsers(Integer numberOfUsers, Boolean isInsert){

        Id profileId = [SELECT Id FROM Profile WHERE Name =: 'System Administrator'].Id;
        List<User> userList = new List<User>();

        for(Integer x = 0; x <= numberOfUsers; x++){

            User u = new User(
                ProfileId = profileId,
                LastName = 'smpfctestuser'+ String.valueOf(x),
                Email = 'smpfcTestUser'+ String.valueOf(x) +'@smpfc.com',
                Username = 'smpfcTestUser@smpfc.com' + String.valueOf(x) + System.currentTimeMillis(),
                CompanyName = 'SMPFC',
                Title = 'testTitle',
                Alias = 'smcU',
                TimeZoneSidKey = 'Asia/Manila',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                isActive = true,
                Feeds__c = true,
                EmployeeNumber = 'EMP :' + String.valueOf(x)
            );
            userList.add(u);
        }
        if (isInsert) {
            insert userList;
        }
        return userList;
    }

}