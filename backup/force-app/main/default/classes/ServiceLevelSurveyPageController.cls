/*
 * Description - Controller class for Service Level Survey Page
 *
 * Version        Date            Author            Description
 * 1.0            25/09/18        Eternus Solutions     Initial Draft
 */
public class ServiceLevelSurveyPageController {

    public ServiceLevelSurveyPageController(ApexPages.StandardController controller) {
    }

    public List<String> getProductList() {
        return new List<String> {
            'Beef (Primals & IPCs)','Pork (Primals & IPCs)','Chicken (Primals & IPCs)','Beef Timplados','Pork Timplados','Chicken Timplados','Push Products','Promo Items','Others'
        };
    }

    @RemoteAction
    public static Service_Level__c getExistingServiceLevel(Id serviceId) {
        List<Service_Level__c> serviceRecords = [
            SELECT Id
                 , Name
                 , Account__c
                 , Account__r.Name
                 , Assessment_Date__c
                 , Availability__c
                 , Customer_Service__c
                 , Display_Quality__c
                 , eForm__c
                 , Final_Rating__c
                 , Franchisee_Service_Provider__c
                 , Name_of_Staff_on_Duty__c
                 , Name_of_Staff_on_Duty__r.Name
                 , Outlet_Code__c
                 , Outlet_Location__c
                 , Outlet_Name__c
                 , Product_Quality__c
                 , Proper_Grooming__c
                 , Status__c
                 , Submitted_Date_Time__c
                 , Sync_Date_Time__c
                 , Discussed_with__r.Name
                 , Franchisee_Team_Leader__r.Name
                 , Prepared_by__r.Name
                 , Reviewed_by__r.Name
                 , Sales_Account_Specialist__r.Name
                 , Unit_Sales_Manager__r.Name
                 , Product_Quality_Final_Rating__c
                 , Display_Quality_Final_Rating__c
                 , Availablity_Final_Rating__c
                 , Customer_Service_Final_Rating__c
                 , Proper_Grooming_Final_Rating__c
            	 , Date_of_Discussion__c
              FROM Service_Level__c
             WHERE Id = :serviceId
        ];

        if( true == serviceRecords.isEmpty() ) {
            return new Service_Level__c();
        }
        return serviceRecords[0];
    }

    @RemoteAction
    public static ProductDetailsWrapper fetchRelatedProductDetails(Id serviceId) {

        Schema.DescribeSObjectResult desResult = Schema.SObjectType.Service_Level_Product_Details__c;
        Map<Id,Schema.RecordTypeInfo> rtMapById = desResult.getRecordTypeInfosById();

        ProductDetailsWrapper productDetailsWrapper = new ProductDetailsWrapper();
        for(Service_Level_Product_Details__c productDetails: [
                SELECT Id
                     , Actual_Score__c
                     , Final_Rating__c
                     , Product__c
                     , RecordTypeId
                     , Others__c
                 FROM Service_Level_Product_Details__c
                WHERE Service_Level__c =:serviceId
        ]) {
            System.debug('..productDetails: ' + productDetails);
            String rtStr = rtMapById.get(productDetails.RecordTypeId).getName();
            //productDetailsWrapper.rtStr = rtStr;
            if(Label.RT_Display_Quality == rtStr) {
                productDetailsWrapper.displayQuality.put(productDetails.Product__c, productDetails);
            } else if (Label.RT_Product_Availability == rtStr) {
                productDetailsWrapper.availablity.put(productDetails.Product__c, productDetails);
            } else if(Label.RT_Product_Quality == rtStr) {
                productDetailsWrapper.productQuality.put(productDetails.Product__c, productDetails);
            }
        }

        return productDetailsWrapper;
    }

    public class ProductDetailsWrapper {
        public Map<String, Service_Level_Product_Details__c> productQuality;
        public Map<String, Service_Level_Product_Details__c> displayQuality;
        public Map<String, Service_Level_Product_Details__c> availablity;
        //public String rtStr;

        public ProductDetailsWrapper() {
            productQuality = new Map<String, Service_Level_Product_Details__c>();
            displayQuality = new Map<String, Service_Level_Product_Details__c>();
            availablity = new Map<String, Service_Level_Product_Details__c>();
            //rtStr = '';
        }
    }

    @RemoteAction
    public static Map<String, Service_Level_Standards_Details__c> fetchRelatedStandardDetails(Id serviceId) {

        Map<String, Service_Level_Standards_Details__c> productStandardMap = new Map<String, Service_Level_Standards_Details__c>();

        for(Service_Level_Standards_Details__c standardDetails: [
                SELECT Id
                     , Actual_Score__c
                     , Final_Rating__c
                     , Product__c
                     , RecordTypeId
                     , Others__c
                 FROM Service_Level_Standards_Details__c
                WHERE Service_Level__c =:serviceId
        ]){
            productStandardMap.put(standardDetails.Product__c, standardDetails);
        }

        return productStandardMap;
    }
}