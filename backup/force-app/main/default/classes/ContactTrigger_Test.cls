/*-------------------------------------------------------------------------------------------
Author       :   Adjell Pabayos
Created Date :   10.23.2017
Definition   :   Contact Trigger Methods Test class
History      :   10.23.2017 - Adjell Pabayos: Created
-------------------------------------------------------------------------------------------*/
@isTest
public class ContactTrigger_Test {
    private static Map<String, RecordTypeInfo> contactRecordTypes = Schema.SObjectType.Contact.getRecordTypeInfosByName();
    
    /*-------------------------------------------------------------------------------------------
    Author       :   Kimiko Roberto
    Created Date :   05.11.2017
    Definition   :   Method for creating test data (BU, Account, Contacts, Products, UOMs, Conversions, Orders, Order Items)
    History      :   05.11.2017 - Kiko Roberto:  Created
                     09.26.2017 - Adjell Pabayos: Updated
    -------------------------------------------------------------------------------------------*/    
    @testSetup static void initializeTestData() {
        Id dspRecordTypeId = contactRecordTypes.get('Distributor Personnel').getRecordTypeId();
        Map<Id, Market__c> businessUnits;
        Map<Id, Account> parentAccounts;
        Map<Id, Custom_Product__c> products;
        Map<Id, List<Account>> childrenAccounts;
        Map<Id, UOM__c> uoms;    
        //call your test data here using a test data factory
        User testUser1 = TP_TestDataFactory.createUserAdmin();
        insert testUser1;
        
        //init Business Units
        List<Market__c> tempBusinessUnits = TP_TestDataFactory.createMultipleBusinessUnits('testBU', 2);
        
        insert tempBusinessUnits;
        
        businessUnits = new Map<Id, Market__c>([select Id, Name from Market__c]);
        
        //init accounts
        List<Account> tempParentAccounts = new List<Account>();
        
        for(Integer x = 0; x < 5; x++){
            for(Id buId : businessUnits.keySet()){
                Account account = TP_TestDataFactory.createSingleAccount('Test Name', 'Distributor_Direct', buId);
                tempParentAccounts.add(account);
            }
        }
        
        insert tempParentAccounts;
        
        parentAccounts = new Map<Id, Account>([select Id, Name from Account]);  
        
        //init customer accounts
        List<Account> tempChildrenAccounts = new List<Account>();
        
        for(Account parentAccount : parentAccounts.values()){
            for(Integer x = 0; x < 2; x++){
                for(Id buId : businessUnits.keySet()){
                    Account account = TP_TestDataFactory.createSingleAccount('Test Customer Account', 'Distributor_Customer', buId);
                    account.Distributor__c = parentAccount.Id;
                    tempChildrenAccounts.add(account);
                }
            }
        }
        
        insert tempChildrenAccounts;  
        
        tempChildrenAccounts = [select Id, Name, Distributor__c from Account where Distributor__c != null];
        
        childrenAccounts = new Map<Id, List<Account>>();
        
        for(Account childAccount : tempChildrenAccounts){
            List<Account> tempChildren = (childrenAccounts.get(childAccount.Distributor__c) != null) ? childrenAccounts.get(childAccount.Distributor__c) : new List<Account>();
            
            tempChildren.add(childAccount);
            childrenAccounts.put(childAccount.Distributor__c, tempChildren);
        }
        
        //init contacts
        Map<String, Contact> childrenContacts = new Map<String, Contact>();
        List<DSP__c> childrenDSPs = new List<DSP__c>();
        
        for(Account parent : parentAccounts.values()){
            Contact tempSASContact = TP_TestDataFactory.createSingleContact('Test Name_SAS_'+parent.Name, true, 'SAS', parent.Id);
            Contact tempDSPContact = TP_TestDataFactory.createSingleContact('Test Name_DSP_'+parent.Name, true, 'Distributor_Personnel', parent.Id);
            
            childrenContacts.put(parent.Id+'_SAS', tempSASContact);
            childrenContacts.put(parent.Id+'_DSP', tempDSPContact);
        }
        
        insert childrenContacts.values();
        
        List<Contact> tempDSPContacts = new List<Contact>();
        Set<Id> contactIds = new Set<Id>();
        
        for(Contact tempContact : childrenContacts.values()){
            contactIds.add(tempContact.Id);
        }
    }
    
    /*-------------------------------------------------------------------------------------------
    Author       :   Adjell Pabayos
    Created Date :   10.23.2017
    Definition   :   After Insert trigger context
    History      :   10.23.2017 - Adjell Pabayos: Created
                 :   11.27.2017 - Modified to use DSP object instead of DSP Contact Record Type
    -------------------------------------------------------------------------------------------*/   
    @isTest static void test_afterInsert() {
        Account tempAccount = [select Id, Primary_SAS_Contacts__c, Primary_DSP_Contacts__c from Account limit 1];
        Id sasRecordTypeId = contactRecordTypes.get('SAS').getRecordTypeId();
        Id dspRecordTypeId = contactRecordTypes.get('Distributor Personnel').getRecordTypeId();
        
        test.startTest();
        Contact tempPrimarySAS = new Contact(AccountId = tempAccount.Id, LastName = 'testNewPrimary', RecordTypeId = sasRecordTypeId);
        
        insert tempPrimarySAS;
        
        tempAccount = [select Id, Primary_SAS_Contacts__c from Account where Id = :tempAccount.Id];
        
        System.assertEquals(1, tempAccount.Primary_SAS_Contacts__c);
        
        Contact tempPrimaryDSP = new Contact(AccountId = tempAccount.Id, LastName = 'testNewPrimary', RecordTypeId = dspRecordTypeId);
        
        insert tempPrimaryDSP;
        
        tempAccount = [select Id, Primary_DSP_Contacts__c from Account where Id = :tempAccount.Id];
        
        System.assertEquals(1, tempAccount.Primary_DSP_Contacts__c);
        
        test.stopTest();
    }
    
    /*-------------------------------------------------------------------------------------------
    Author       :   Adjell Pabayos
    Created Date :   10.23.2017
    Definition   :   After Update trigger context
    History      :   10.23.2017 - Adjell Pabayos: Created
                 :   11.27.2017 - Modified to use DSP object instead of DSP Contact Record Type
    -------------------------------------------------------------------------------------------*/   
    @isTest static void test_afterUpdate() {
        Account tempAccount = [select Id, Primary_SAS_Contacts__c, Primary_DSP_Contacts__c from Account where Id in (select AccountId from Contact where Is_Primary__c = true) limit 1];
        Id sasRecordTypeId = contactRecordTypes.get('SAS').getRecordTypeId();
        Id dspRecordTypeId = contactRecordTypes.get('Distributor Personnel').getRecordTypeId();
        Contact tempContactSAS = [select Id, Is_Primary__c from Contact where AccountId = :tempAccount.Id and RecordTypeId = :sasRecordTypeId and Is_Primary__c = true limit 1];
        Contact tempContactDSP = [select Id, Is_Primary__c from Contact where AccountId = :tempAccount.Id and RecordTypeId = :dspRecordTypeId and Is_Primary__c = true limit 1];
        
        test.startTest();
        tempContactSAS.Is_Primary__c = false;
        
        update tempContactSAS;
        
        tempAccount = [select Id, Primary_SAS_Contacts__c from Account where Id = :tempAccount.Id];
        
        System.assertEquals(0, tempAccount.Primary_SAS_Contacts__c);
        
        tempContactDSP.Is_Primary__c = false;
        
        update tempContactDSP;
        
        tempAccount = [select Id, Primary_DSP_Contacts__c from Account where Id = :tempAccount.Id];
        
        System.assertEquals(0, tempAccount.Primary_DSP_Contacts__c);
        
        test.stopTest();
    }
    
    /*-------------------------------------------------------------------------------------------
    Author       :   Adjell Pabayos
    Created Date :   10.23.2017
    Definition   :   After Delete trigger context
    History      :   10.23.2017 - Adjell Pabayos: Created
                 :   11.27.2017 - Modified to use DSP object instead of DSP Contact Record Type
    -------------------------------------------------------------------------------------------*/   
    @isTest static void test_afterDelete() {
        Account tempAccount = [select Id, Primary_SAS_Contacts__c, Primary_DSP_Contacts__c from Account where Id in (select AccountId from Contact where Is_Primary__c = true) limit 1];
        Id sasRecordTypeId = contactRecordTypes.get('SAS').getRecordTypeId();
        Id dspRecordTypeId = contactRecordTypes.get('Distributor Personnel').getRecordTypeId();
        Contact tempContactSAS = [select Id, Is_Primary__c from Contact where AccountId = :tempAccount.Id and RecordTypeId = :sasRecordTypeId and Is_Primary__c = true limit 1];
        Contact tempContactDSP = [select Id, Is_Primary__c from Contact where AccountId = :tempAccount.Id and RecordTypeId = :dspRecordTypeId and Is_Primary__c = true limit 1];
        
        test.startTest();
        delete tempContactSAS;
        
        tempAccount = [select Id, Primary_SAS_Contacts__c from Account where Id = :tempAccount.Id];
        
        System.assertEquals(0, tempAccount.Primary_SAS_Contacts__c);
        
        delete tempContactDSP;
        
        tempAccount = [select Id, Primary_DSP_Contacts__c from Account where Id = :tempAccount.Id];
        
        System.assertEquals(0, tempAccount.Primary_DSP_Contacts__c);
        
        test.stopTest();
    } 
    
    /*-------------------------------------------------------------------------------------------
    Author       :   Adjell Pabayos
    Created Date :   10.23.2017
    Definition   :   After Undelete trigger context
    History      :   10.23.2017 - Adjell Pabayos: Created
                 :   11.27.2017 - Modified to use DSP object instead of DSP Contact Record Type
    -------------------------------------------------------------------------------------------*/   
    @isTest static void test_afterUndelete() {
        Account tempAccount = [select Id, Primary_SAS_Contacts__c, Primary_DSP_Contacts__c from Account where Id in (select AccountId from Contact where Is_Primary__c = true) limit 1];
        Id sasRecordTypeId = contactRecordTypes.get('SAS').getRecordTypeId();
        Id dspRecordTypeId = contactRecordTypes.get('Distributor Personnel').getRecordTypeId();
        Contact tempContactSAS = [select Id, Is_Primary__c from Contact where AccountId = :tempAccount.Id and RecordTypeId = :sasRecordTypeId and Is_Primary__c = true limit 1];
        Contact tempContactDSP = [select Id, Is_Primary__c from Contact where AccountId = :tempAccount.Id and RecordTypeId = :dspRecordTypeId and Is_Primary__c = true limit 1];
        
        test.startTest();
        delete tempContactDSP;
        undelete tempContactDSP;
        
        tempAccount = [select Id, Primary_DSP_Contacts__c from Account where Id = :tempAccount.Id];
        
        System.assertEquals(1, tempAccount.Primary_DSP_Contacts__c);
        
        delete tempContactSAS;
        undelete tempContactSAS;
        
        tempAccount = [select Id, Primary_SAS_Contacts__c from Account where Id = :tempAccount.Id];
        
        System.assertEquals(1, tempAccount.Primary_SAS_Contacts__c);
        
        test.stopTest();
    } 
    
    /*-------------------------------------------------------------------------------------------
    Author       :   Adjell Pabayos
    Created Date :   10.23.2017
    Definition   :   Validation checking - SAS
    History      :   10.23.2017 - Adjell Pabayos: Created
    -------------------------------------------------------------------------------------------*/   
    @isTest static void test_failure() {
        Account tempAccount = [select Id, Primary_SAS_Contacts__c, Primary_DSP_Contacts__c from Account where Id in (select AccountId from Contact where Is_Primary__c = true) limit 1];
        Id sasRecordTypeId = contactRecordTypes.get('SAS').getRecordTypeId();
        Id dspRecordTypeId = contactRecordTypes.get('Distributor Personnel').getRecordTypeId();
        
        test.startTest();
        Contact tempPrimarySAS = new Contact(AccountId = tempAccount.Id, LastName = 'testNewPrimary', RecordTypeId = sasRecordTypeId, Is_Primary__c = true);
        
        try{
            insert tempPrimarySAS;
        }catch(Exception e){
            System.assertEquals(true, e.getMessage().contains('already has a Primary Contact'));
        }
        
        test.stopTest();
    }       
}