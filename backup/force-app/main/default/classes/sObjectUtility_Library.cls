public with sharing class sObjectUtility_Library {

    public static Map<String, Map<String, Schema.SObjectField>> fieldDescribeCache = 
                new Map<String, Map<String, Schema.SObjectField>>();
    public static Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
    
    public static Map<String, Schema.SObjectField> getFieldDescribe(Schema.DescribeSObjectResult sdo) {
        if (!fieldDescribeCache.containsKey(sdo.getName())) {
            fieldDescribeCache.put(sdo.getName(), sdo.fields.getMap());
        }
        return fieldDescribeCache.get(sdo.getName());
    }
    
    public static String buildQueryFields(Schema.DescribeSObjectResult sdo) {
        
        Map<String, Schema.SObjectField> objectFields_map = getFieldDescribe(sdo);
        //Added by Abhijeet
        String queryFields = '';
        Boolean firstFlag = true;
        for (string c : objectFields_map.keySet()) {
            if (!firstFlag) {
                queryFields = queryFields + ', ';
            }
            queryFields = queryFields + c;
            firstFlag = false;
        }
        return queryFields;
    }
    
    
    public static String buildQuery(Schema.DescribeSObjectResult sdo) {
        String queryFields = buildQueryFields(sdo);
        queryFields = 'SELECT ' + queryFields + ' FROM ' + sdo.getName() + ' ';
        return queryFields;
    }
}