public with sharing class ProductAssignmentCX{
    public List<Custom_Product__c> productResults {get;set;}
    public String filterByName {get;set;}
    public String filterByBU  {get;set;}
    public String filterBySKU {get;set;}
    public String filterByBrand {get;set;}
    public String filterByCategory {get;set;}
    public String pageInfo {get;set;}
    public Integer pageNum {get;set;}
    public Integer lastPageNum {get;set;}
    public Integer tableRowCount {get;set;}
    public List<ProductWrapper> productsToDisplay {get;set;}
    
    Product_Assignment__c currentProductAssignentRec = new Product_Assignment__c();
    Map<id,ProductWrapper> selectedProductsMap = new Map<id,ProductWrapper>();
    Map<id,ProductWrapper> assignedProducts = new Map<id,ProductWrapper>();
    Map<id,ProductWrapper> tempSelectedProductsMap = new Map<id,ProductWrapper>();
    ProductWrapper tempProductWrapper = new ProductWrapper();
    
    Boolean productListAboveLimit; 
    

    public ProductAssignmentCX(ApexPages.StandardController controller) {
        this.currentProductAssignentRec = (Product_Assignment__c)controller.getRecord();
        tableRowCount = 100;
        productResults = new List<Custom_Product__c>();
        InitializePage();

    }
    
    public void InitializePage(){

        productsToDisplay = new List<ProductWrapper>();
        assignedProducts = new Map<id,ProductWrapper>();
        pageNum = 0;
        SaveProgress();
        
        
        if(currentProductAssignentRec.Account__c!=null) {
            

            //Ceck numer of products if it exceed the limits.
            productListAboveLimit = [SELECT Count() FROM Custom_Product__c] >100000;

            if(productListAboveLimit){
                // remind user to search due to above limit number of products.
            }
            
            //Check Accounts product assignments.
            for(Product_Assignment__c pa: [SELECT id, Custom_Product__c, Custom_Product__r.id  FROM Product_Assignment__c WHERE Account__c = :currentProductAssignentRec.Account__c]){
                tempProductWrapper = new ProductWrapper();
                tempProductWrapper.isSelected = true;
                tempProductWrapper.isAssigned = true;
                assignedProducts.put(pa.Custom_Product__r.id, tempProductWrapper);
            }
            system.debug('&& assignedProducts['+assignedProducts.size()+']:'+assignedProducts);
            //Fetch all the produts available.
            productResults = [SELECT Id, Name, Brand__r.Name, Category1__r.name, SKU_Code__c, List_Price__c, 
                                Business_Unit__c, List_Price_2__c, Unit_of_Measurement__c, UOM2__c,VAT__c, 
                                Price__c, Alternative_UOM__c, Alternative_UOM__r.name, Category1__c, Brand__c
                            FROM Custom_Product__c 
                            ORDER BY Name ASC
                            LIMIT 10000];
            GenerateProductListTable (pageNum);
        }
    }


    public void GenerateProductListTable (Integer pageNumber){
        productsToDisplay = new List<ProductWrapper>();
        Id productId;

        lastPageNum = 0;
        lastPageNum = Integer.valueOf(Decimal.valueOf(productResults.size())/(Decimal.valueOf(tableRowCount)).setscale(0,RoundingMode.DOWN));
        if(math.mod(productResults.size(),tableRowCount)>0){
            lastPageNum++;
        }
        pageInfo = (pageNum+1) +' of '+lastPageNum;

        if(productResults.size()>0){
            Integer rowEndCount = tableRowCount + (tableRowCount * pageNumber);
            for(Integer i=(tableRowCount * pageNumber) ;i < rowEndCount && i < productResults.size(); i++){
                system.debug('$$$['+(tableRowCount * pageNumber)+'] i:'+i);
                productId = productResults[i].id;
                tempProductWrapper = new ProductWrapper();
                tempProductWrapper.product = productResults[i];
                if(assignedProducts.get(productId) !=null ){
                    system.debug('%% assigned - '+productResults[i].name);
                    tempProductWrapper.isAssigned = true;
                    tempProductWrapper.isSelected = true;
                }else if (selectedProductsMap.get(productId) != null){
                    tempProductWrapper.isSelected = true;
                }
                productsToDisplay.add(tempProductWrapper);
            }
        }
    }

    public void GoToNextPage(){
        pageNum ++;
        SaveProgress();
        GenerateProductListTable (pageNum);
    }

    public void GoToPreviousPage(){
        pageNum --;
        SaveProgress();
        GenerateProductListTable (pageNum);
    }

    public void SearchProducts (){
        String soqlCondition = '';
        String soqlClause = ' SELECT Id, Name, Brand__r.Name, Category1__r.name, SKU_Code__c, Category1__c, Brand__c  FROM Custom_Product__c ';

        SaveProgress();

        if (String.isNotBlank(filterByName))
            soqlCondition += ' AND Name LIKE \'%'+filterByName.replace('\'','\\\'')+'%\'';
        if (String.isNotBlank(filterByBU))
            soqlCondition += ' AND Business_Unit__c INCLUDES (\''+filterByBU.replace('\'','\\\'')+'\')';
        if (String.isNotBlank(filterBySKU))
            soqlCondition += ' AND  SKU_Code__c LIKE \'%'+filterBySKU.replace('\'','\\\'')+'%\'';  
        if (String.isNotBlank(filterByBrand))
            soqlCondition += ' AND Brand__r.Name LIKE \'%'+filterByBrand.replace('\'','\\\'')+'%\'';
        if (String.isNotBlank(filterByCategory))
            soqlCondition += ' AND  Category1__r.Name LIKE \'%'+filterByCategory.replace('\'','\\\'')+'%\'';
        system.debug('## soqlCondition:'+soqlCondition);
        if(String.isNotBlank(soqlCondition)){
            soqlCondition = ' WHERE id != null '+soqlCondition;
            soqlClause += soqlCondition;
        }
        soqlClause += ' ORDER BY Name ASC LIMIT 10000';
        system.debug('## soqlClause2:'+soqlClause);
        productResults = Database.query(soqlClause);

        pageNum = 0;
        GenerateProductListTable (pageNum);
    }

    public void SaveProgress (){
        system.debug('@@ productsToDisplay:'+productsToDisplay);
        for(ProductWrapper pw : productsToDisplay){
            //tempProductWrapper.product = pw.product;
            if(pw.isSelected && !pw.isAssigned){
                selectedProductsMap.put(pw.product.id, pw);
            }else if (selectedProductsMap.get(pw.product.id) != null){
                selectedProductsMap.put(pw.product.id, pw);
            }else{

            }
        }

        //Remove unselected fromselected Map
        for(ProductWrapper pwm : selectedProductsMap.values()){
            if(pwm.isSelected){
                tempSelectedProductsMap.put(pwm.product.id,pwm);
            }
        }
        //refresh selectedPrductMap.
        if(tempSelectedProductsMap.size()>0){
            selectedProductsMap = new Map<id,ProductWrapper>();
            selectedProductsMap = tempSelectedProductsMap;
        }
    }

    public PageReference SaveProductAssignments(){
        SaveProgress();
        PageReference destinationPage = new PageReference ('/'+currentProductAssignentRec.Account__c);
        ApexPages.getMessages().clear();
        list<Product_Assignment__c> newProductAssignmentList = new list<Product_Assignment__c>();
        Product_Assignment__c tempParoductAssignment;
        
        if (selectedProductsMap.size() <= 0) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select at least one Product.'));
            return null;
        }
        for(ProductWrapper pwRec: selectedProductsMap.values()){
            tempParoductAssignment = new Product_Assignment__c();
            tempParoductAssignment.Account__c = currentProductAssignentRec.Account__c;
            tempParoductAssignment.Custom_Product__c = pwRec.product.id;
            newProductAssignmentList.add(tempParoductAssignment);
            system.debug(' **** added:'+pwRec.product.name );
        }
        try{
            upsert (newProductAssignmentList);
        }catch (DmlException e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, String.valueOf(e)));
            return null;       
        }


        destinationPage.setRedirect(true);
        return destinationPage;
    }


    
    
    public class ProductWrapper{
        public Custom_Product__c product {get;set;}
        public Boolean isSelected {get;set;}
        public Boolean isAssigned {get;set;}
        public ProductWrapper(){
            product = new Custom_Product__c();
            isSelected = false;
            isAssigned = false;
        }
    }

}