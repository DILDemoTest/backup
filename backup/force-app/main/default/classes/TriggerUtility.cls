/**
    * AUTHOR        : EUGENEBASIANOMUTYA
    * DESCRIPTION   : GLOBAL VARIABLES USED FOR BYPASSING METHODS
    * HISTORY       : 
                    : MAY.06.2017 - CREATED.
                    6.26.2017   - UPDATE: Added Checker for SIF Creation
**/

Public class TriggerUtility {
    
    public static Boolean BY_PASS_ORDER_FILECOUNTER_UPDATE = false;
    public static Boolean BY_PASS_HASTRADEASSET = false;
    //GAAC-6.27.2017
    public static Boolean CREATE_NEW_SIF = false;
    public static Boolean BY_SPLIT_UPDATE = FALSE;
    
}