public class UpdateOrderStatusHandler {

    public void updateOrderStatus(List<Order_Item__c> orderItemTriggerList, Map<Id,Order_Item__c> orderItemOldMap) {

        Map<Id, List<Order_Item__c>> orderIdVsOrderItemListMap = new Map<Id, List<Order_Item__c>>();
        List<Order__c> orderUpdateList = new List<Order__c>();


        for(Order_Item__c orderItemItr : orderItemTriggerList) {
            
            if(orderItemOldMap != null && !orderItemOldMap.isEmpty()){
                Order_Item__c oldOi = orderItemOldMap.get(orderItemItr.Id);
                if(orderItemItr.Received__c != null && orderItemItr.Received__c != oldOi.Received__c){
                    orderIdVsOrderItemListMap.put(orderItemItr.Order_Form__c, new List<Order_Item__c>());
                }
                
            }
        }
        
        if(orderIdVsOrderItemListMap != null && !orderIdVsOrderItemListMap.isEmpty()){
            for(Order_Item__c orderItemItr : [SELECT Id, Order_Form__c, Received__c FROM Order_Item__c WHERE Order_Form__c IN :orderIdVsOrderItemListMap.keySet()]) {

                orderIdVsOrderItemListMap.get(orderItemItr.Order_Form__c).add(orderItemItr);

            }
    
            for(Id orderIdItr : orderIdVsOrderItemListMap.keySet()) {
    
                Boolean orderItemReceived = true;
    
                for(Order_Item__c orderItemItr : orderIdVsOrderItemListMap.get(orderIdItr)) {
    
                    if(null == orderItemItr.Received__c || !orderItemItr.Received__c) {
    
                        orderItemReceived = false;
                        break;
                    }
                }
                if(orderItemReceived) {
                    orderUpdateList.add(new Order__c(Id = orderIdItr, Status__c = 'Closed'));
                }
            }
    
            if(!orderUpdateList.isEmpty()) {
                update orderUpdateList;
            }
        }
    }
        
}