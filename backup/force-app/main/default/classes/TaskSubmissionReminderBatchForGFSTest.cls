@isTest
private class TaskSubmissionReminderBatchForGFSTest {

	private static testMethod void testBatch() {
        User testUser = DIL_TestDataFactory.createBulkUsers(1,true)[0];
        List<Month__c> lstMonths = DIL_TestDataFactory.getMonth(1,true);
        
        DateTime currentTime = datetime.now().addMonths(1);
        String nextMonth = currentTime.format('MMMMM');
        String year = currentTime.format('YYYY');
        
        for(Month__c objMonth : lstMonths){
            objMonth.OwnerId = testUser.Id;
            objMonth.Submitted_for_Approval__c = True;
            objMonth.Month__c = nextMonth;
            objMonth.Year__c = year;
        }
        update lstMonths;
        
        Database.executeBatch(new TaskSubmissionReminderBatchForGFS());
	}

}