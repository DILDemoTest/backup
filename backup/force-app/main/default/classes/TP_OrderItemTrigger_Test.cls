/*-------------------------------------------------------------------------------------------
Author       :   Adjell Pabayos
Created Date :   09.27.2017
Definition   :   Order Item Trigger Test Class
History      :   09.27.2017 -- Adjell Pabayos: Updated
-------------------------------------------------------------------------------------------*/
@isTest
private class TP_OrderItemTrigger_Test{

    /*-------------------------------------------------------------------------------------------
    Author       :   Adjell Pabayos
    Created Date :   09.27.2017
    Definition   :   Method for creating test data (BU, Account, Contacts, Products, UOMs, Conversions, Orders, Order Items)
    History      :   09.27.2017 - Adjell Pabayos: Updated
    -------------------------------------------------------------------------------------------*/
    @testSetup static void initializeTestData() {
        Map<Id, Market__c> businessUnits;
        Map<Id, Account> parentAccounts;
        Map<String, Warehouse__c> warehouseMap = new Map<String, Warehouse__c>(); //GAAC: 6/10/2018
        Map<String, Conversion__c> conversionMap = new Map<String, Conversion__c>(); //GAAC: 6/10/2018
        Map<Id, List<Account>> childrenAccounts;
        Map<Id, Custom_Product__c> products;
        Map<Id, UOM__c> uoms;
        //call your test data here using a test data factory
        User testUser1 = TP_TestDataFactory.createUserAdmin();
        User testDistributorUser1 = TP_TestDataFactory.createUser('Distributor User', 'Test Distributor User', 'tDist', 'test.distributor@distributor.com', 'test.distributor@distributor.com');
        testDistributorUser1.isActive = true;

        insert testDistributorUser1;
        insert testUser1;

        //init Business Units
        List<Market__c> tempBusinessUnits = TP_TestDataFactory.createMultipleBusinessUnits('testBU', 2);

        insert tempBusinessUnits;

        businessUnits = new Map<Id, Market__c>([select Id, Name from Market__c]);

        //init accounts
        List<Account> tempParentAccounts = new List<Account>();

        for(Integer x = 0; x < 2; x++){
            for(Id buIds : businessUnits.keySet()){
                Account account = TP_TestDataFactory.createSingleAccount('Test Name', 'Distributor_Direct', buIds);
                tempParentAccounts.add(account);
            }
        }

        insert tempParentAccounts;

        parentAccounts = new Map<Id, Account>([select Id, AccountNumber, Name from Account]);
        
        //GAAC: 6/10/2018 - START
        List<Warehouse__c> tempWarehouse = new List<Warehouse__c>();
        
        for(Account parentAccount : parentAccounts.values()) {
            
            Warehouse__c warehouse = new Warehouse__c(
            Account__c = parentAccount.Id,
            Address__c = String.valueOf(parentAccount.Id) + String.valueOf(parentAccount.AccountNumber),
            Warehouse_No__c = String.valueOf(parentAccount.AccountNumber)
            );
            
            tempWarehouse.add(warehouse);
        }
        
        insert tempWarehouse;
        //GAAC: 6/10/2018 - END
        
        for(Warehouse__c wh : tempWarehouse) {
            warehouseMap.put(wh.Account__c, wh);
        }
        
        //init customer accounts
        List<Account> tempChildrenAccounts = new List<Account>();

        for(Account parentAccount : parentAccounts.values()){
            for(Integer x = 0; x < 2; x++){
                for(Id buId : businessUnits.keySet()) {
                    Account account = TP_TestDataFactory.createSingleAccount('Test Customer Account', 'Distributor_Customer', buId);
                    account.Distributor__c = parentAccount.Id;
                    account.Warehouse__c = warehouseMap.get(parentAccount.Id).Id;
                    tempChildrenAccounts.add(account);
                }
            }
        }

        insert tempChildrenAccounts;

        tempChildrenAccounts = [select Id, Name, Distributor__c from Account where Distributor__c != null];

        childrenAccounts = new Map<Id, List<Account>>();

        for(Account childAccount : tempChildrenAccounts){
            List<Account> tempChildren = (childrenAccounts.get(childAccount.Distributor__c) != null) ? childrenAccounts.get(childAccount.Distributor__c) : new List<Account>();

            tempChildren.add(childAccount);
            childrenAccounts.put(childAccount.Distributor__c, tempChildren);
        }

        //init contacts
        Map<String, Contact> childrenContacts = new Map<String, Contact>();

        for(Account parent : parentAccounts.values()){
            Contact tempSASContact = TP_TestDataFactory.createSingleContact('Test Name_SAS_'+parent.Name, true, 'SAS', parent.Id);
            Contact tempDSPContact = TP_TestDataFactory.createSingleContact('Test Name_DSP_'+parent.Name, true, 'Distributor_Personnel', parent.Id);

            childrenContacts.put(parent.Id+'_SAS', tempSASContact);
            childrenContacts.put(parent.Id+'_DSP', tempDSPContact);
        }

        insert childrenContacts.values();

        //init uoms
        List<UOM__c> tempUOM = TP_TestDataFactory.createMultipleUOMs('Test UOM', 2);
        insert tempUOM;

        uoms = new Map<Id, UOM__c>([select Id, Name from UOM__c]);

        //init products
        List<Custom_Product__c> tempProducts = new List<Custom_Product__c>();

        String tempSKU = '8732634524';
        List<Id> tempUOMIds = new List<Id>(uoms.keySet());
        for(Integer x = 0; x < tempUOMIds.size(); x++){
            tempProducts.addAll(TP_TestDataFactory.createMultipleCustomProducts('Test Product_' + x + '_', tempSKU + String.valueOf(x),tempUOMIds.get(x), 2));
        }

        insert tempProducts;

        //init SIFs
        List<Order__c> tempOrders = new List<Order__c>();
        List<Schema.PicklistEntry> paymentTerms = Order__c.Payment_Term1__c.getDescribe().getPicklistValues();
        List<Schema.PicklistEntry> salesTypes = Order__c.Sales_Type__c.getDescribe().getPicklistValues();

        Integer counter = 0;
        for(Account account : parentAccounts.values()){
            for(Integer x = 0; x < 2; x++){
                for(Schema.PicklistEntry salesType : salesTypes){
                    Order__c tempOrder = new Order__c();
                    tempOrder.Account__c = childrenAccounts.get(account.Id)[0].Id;
                    tempOrder.SAS__c = childrenContacts.get(account.Id + '_SAS').Id;
                    tempOrder.DSP__c = childrenContacts.get(account.Id + '_DSP').Id;
                    tempOrder.Payment_Term1__c = paymentTerms[x].getValue();
                    tempOrder.Sales_Type__c = salesType.getValue();

                    if(salesType.getValue() == 'Pre-booked'){
                        tempOrder.Sales_Order_Number__c = '4262316432'+counter;
                        tempOrder.Order_Booking_Date__c = Date.today();
                        tempOrder.Requested_Delivery_Date__c = Date.today();
                    }else{
                        tempOrder.Invoice_No__c = '3256324745'+counter;
                        tempOrder.Invoice_Date__c = System.today();
                    }

                    counter++;
                    tempOrders.add(tempOrder);
                }
            }
        }

        insert tempOrders;

        tempOrders = [select Id, Sales_Type__c from Order__c];
        tempProducts = [select Id from Custom_Product__c];
        
        UOM__c uomVal = TP_TestDataFactory.createSingleUOM('KG');
        
        insert uomVal;
        
        List<Conversion__c> convVal = TP_TestDataFactory.createMultipleConversions(tempProducts, uomVal.Id); //GAAC: 6/10/2018
        
        insert convVal;
        
        //GAAC: 6/10/2018
        for(Conversion__c c : convVal) {
            conversionMap.put(c.Product__c, c);
        }
        //GAAC: 6/10/2018

        List<Order_Item__c> orderItems = new List<Order_Item__c>();

        for(Order__c order : tempOrders){
            for(Custom_Product__c product : tempProducts){
                Order_Item__c tempOrderItem = new Order_Item__c();
                tempOrderItem.Order_Form__c = order.Id;
                tempOrderItem.Product__c = product.Id;

                if(order.Sales_Type__c == 'Pre-booked'){
                    tempOrderItem.Invoice_Quantity__c = 0; //GAAC: 6/18/2018
                    tempOrderItem.Order_Quantity__c = 5;
                    tempOrderItem.Order_Price__c = 40.00;
                    tempOrderItem.Order_Discount_1_N__c = 5.00;
                    tempOrderItem.Order_Discount_1_P__c = true;
                    tempOrderItem.Order_Discount_2_N__c = 5.00;
                    tempOrderItem.Order_Discount_2_P__c = true;
                    tempOrderItem.Order_Discount_3_N__c = 5.00;
                    tempOrderItem.Order_Discount_3_P__c = true;
                    tempOrderItem.Order_Discount_4_N__c = 5.00;
                    tempOrderItem.Order_Discount_4_P__c = true;
                    tempOrderItem.Conversion__c = conversionMap.get(product.Id).Id;
                    tempOrderItem.Order_Total_Product_Discount__c = 0.00;
                }else{
                    tempOrderItem.Invoice_Quantity__c = 5;
                    tempOrderItem.Invoice_Price__c = 40.00;
                    tempOrderItem.Invoice_Discount_1__c = 5.00;
                    tempOrderItem.Invoice_Discount_1_P__c = true;
                    tempOrderItem.Invoice_Discount_2__c = 5.00;
                    tempOrderItem.Invoice_Discount_2_P__c = true;
                    tempOrderItem.Invoice_Discount_3__c = 5.00;
                    tempOrderItem.Invoice_Discount_3_P__c = true;
                    tempOrderItem.Invoice_Discount_4__c = 5.00;
                    tempOrderItem.Invoice_Discount_4_P__c = true;
                    tempOrderItem.Conversion__c = conversionMap.get(product.Id).Id;
                    tempOrderItem.Invoice_Total_Product_Discount__c = 0.00;
                }

                orderItems.add(tempOrderItem);
            }
        }

        insert orderItems;
        
        //init custom setting
        TriggerFlagControl__c triggerFlagControl = new TriggerFlagControl__c();
    }

    /*-------------------------------------------------------------------------------------------
    Author       :   Adjell Pabayos
    Created Date :   09.27.2017
    Definition   :   Tests the update on Order's File Counter field when an Order Item is updated
    History      :   09.27.2017 - Adjell Pabayos: Updated
    -------------------------------------------------------------------------------------------*/
    @isTest static void testBeforeInsert(){
        //simulate a upload csv scenario
        Test.startTest();

        Order__c order = [select Id, Name, Sales_Type__c, Invoice_Date__c, Invoice_No__c from Order__c where Sales_Type__c = 'Pre-booked' limit 1];
        Order_Item__c orderItem = [select Id, Name, Invoice_Date__c, Invoice_Quantity__c from Order_Item__c where Order_Form__c = :order.Id limit 1];

        order.Invoice_Date__c = Date.today();
        order.Invoice_No__c = '83212'+System.now().format('MMyyyydd');
        orderItem.Invoice_Quantity__c = 5;

        update order;

        //disable skipping file counter update
        TriggerUtility.BY_PASS_ORDER_FILECOUNTER_UPDATE = false;
        update orderItem;

        Order_Item__c orderItem2 = [select Id, Name, Invoice_Date__c, Invoice_Quantity__c from Order_Item__c where Order_Form__c = :order.Id limit 1 offset 2];

        orderItem2.Invoice_Quantity__c = 5;

        //disable skipping file counter update again
        TriggerUtility.BY_PASS_ORDER_FILECOUNTER_UPDATE = false;
        update orderItem2;

        order = [select File_Counter__c from Order__c where Id = :order.Id];

        Test.stopTest();
    }

    /*-------------------------------------------------------------------------------------------
    Author       :   Adjell Pabayos
    Created Date :   09.27.2017
    Definition   :   Tests the locking validation for the distributor user
    History      :   10.04.2017 - Adjell Pabayos: Updated
    -------------------------------------------------------------------------------------------*/
    @isTest static void testBeforeUpdateValidation(){
        //set an order item record's created date to yesterday
        Account tempAccount = [select Id, OwnerId, Owner.Id from Account where Distributor__c != null limit 1];
        Order__c tempOrder = [select Id from Order__c where Account__c = :tempAccount.Id limit 1];
        Order_Item__c tempOrderItem = [select Order_Price__c from Order_Item__c where Order_Form__c = :tempOrder.Id limit 1];
        User testDistributor = [select Id, ProfileId from User where Profile.Name = 'Distributor User' and isActive = true limit 1];

        Datetime yesterday = Datetime.now().addDays(-2);

        Test.setCreatedDate(tempOrderItem.Id, yesterday);

        tempAccount.OwnerId = testDistributor.Id;
        update tempAccount;

        //update custom settings for distributor
        TriggerFlagControl__c settings = TriggerFlagControl__c.getInstance(testDistributor.ProfileId);
        settings.SIF_AllowEditAndDelete__c = true;
        settings.reflectPicklistValue__c = true;
        settings.OrderItem_AllowEditAndDelete__c = true;

        upsert settings;

        Custom_Product__c tempProduct1  = [select Id, SKU_Code__c from Custom_Product__c limit 1 offset 1];
        Custom_Product__c tempProduct1Clone = tempProduct1.clone(false);
        tempProduct1Clone.SKU_Code__c = '54363346';
        tempProduct1Clone.OwnerId = testDistributor.Id;

        insert tempProduct1Clone;

        Test.startTest();

        System.runAs(testDistributor){
            //attempt to update the order item record
            try{
                tempOrderItem.Order_Price__c = 1000.0;

                update tempOrderItem;
            }catch(Exception e){
            
            }

            //attempt to update the order item's product
            try{
                tempOrderItem.Product__c = tempProduct1Clone.Id;

                update tempOrderItem;
            }catch(Exception e){

            }
        }

        Test.stopTest();
    }

    /*-------------------------------------------------------------------------------------------
    Author       :   Adjell Pabayos
    Created Date :   09.27.2017
    Definition   :   Tests the before insert functionality for distributor users
    History      :   10.04.2017 - Adjell Pabayos: Created
    -------------------------------------------------------------------------------------------*/
    @isTest static void testBeforeInsertValidation(){
        //set an order item record's created date to yesterday
        Account tempAccount = [select Id, OwnerId, Owner.Id from Account where Distributor__c != null limit 1];
        Order__c tempOrder = [select Id from Order__c where Account__c = :tempAccount.Id limit 1];
        Order_Item__c tempOrderItem = [select Order_Price__c from Order_Item__c where Order_Form__c = :tempOrder.Id limit 1];
        User testDistributor = [select Id, ProfileId from User where Profile.Name = 'Distributor User' and isActive = true limit 1];

        Datetime yesterday = Datetime.now().addDays(-2);

        Test.setCreatedDate(tempOrderItem.Id, yesterday);

        tempAccount.OwnerId = testDistributor.Id;
        update tempAccount;

        //update custom settings for distributor
        TriggerFlagControl__c settings = TriggerFlagControl__c.getInstance(testDistributor.ProfileId);
        settings.SIF_AllowEditAndDelete__c = true;
        settings.reflectPicklistValue__c = true;
        settings.OrderItem_AllowEditAndDelete__c = true;

        upsert settings;

        Custom_Product__c tempProduct1  = [select Id, SKU_Code__c from Custom_Product__c limit 1 offset 1];
        Custom_Product__c tempProduct1Clone = tempProduct1.clone(false);
        tempProduct1Clone.SKU_Code__c = '54363346';
        tempProduct1Clone.OwnerId = testDistributor.Id;

        insert tempProduct1Clone;

        Test.startTest();

        System.runAs(testDistributor){
            //attempt to insert a new order item
            try{
                Order_Item__c tempOrderItemClone = tempOrderItem.clone(false);
                tempOrderItemClone.Product__c = tempProduct1Clone.Id;

                insert tempOrderItemClone;
            }catch(Exception e){
            }
        }

        Test.stopTest();
    }

    /*-------------------------------------------------------------------------------------------
    Author       :   Adjell Pabayos
    Created Date :   10.04.2017
    Definition   :   Tests the locking validation for the distributor user
    History      :   10.04.2017 - Adjell Pabayos: Updated
    -------------------------------------------------------------------------------------------*/
    @isTest static void testBeforeDeleteValidation(){
        //set an order item record's created date to yesterday
        Account tempAccount = [select Id, OwnerId, Owner.Id from Account where Distributor__c != null limit 1];
        Order__c tempOrder = [select Id from Order__c where Account__c = :tempAccount.Id limit 1];
        Order_Item__c tempOrderItem = [select Order_Price__c from Order_Item__c where Order_Form__c = :tempOrder.Id limit 1];
        User testDistributor = [select Id, ProfileId from User where Profile.Name = 'Distributor User' and isActive = true limit 1];

        Datetime yesterday = Datetime.now().addDays(-2);

        Test.setCreatedDate(tempOrderItem.Id, yesterday);

        tempAccount.OwnerId = testDistributor.Id;
        update tempAccount;

        //update custom settings for distributor
        TriggerFlagControl__c settings = TriggerFlagControl__c.getInstance(testDistributor.ProfileId);
        settings.SIF_AllowEditAndDelete__c = true;
        settings.reflectPicklistValue__c = true;
        settings.OrderItem_AllowEditAndDelete__c = true;

        upsert settings;

        Custom_Product__c tempProduct1  = [select Id, SKU_Code__c from Custom_Product__c limit 1 offset 1];
        Custom_Product__c tempProduct1Clone = tempProduct1.clone(false);
        tempProduct1Clone.SKU_Code__c = '54363346';
        tempProduct1Clone.OwnerId = testDistributor.Id;

        insert tempProduct1Clone;

        Test.startTest();

        System.runAs(testDistributor){
            //attempt to update the order item's product
            try{
                delete tempOrderItem;
            }catch(Exception e){
                System.assertEquals(true, e.getMessage().contains('You do not have delete permission for old Order Item records. Please contact your system administrator.'));
            }
        }

        Test.stopTest();
    }

    /*-------------------------------------------------------------------------------------------
    Author       :   Adjell Pabayos
    Created Date :   09.27.2017
    Definition   :   Tests the net amount validation for Order Item
    History      :   10.04.2017 - Adjell Pabayos: Updated
    -------------------------------------------------------------------------------------------*/
    @isTest static void testAfterUpdateValidation() {
        Account tempAccount = [select Id, OwnerId, Owner.Id from Account where Distributor__c != null limit 1];
        Order__c tempOrder = [select Id, Discount_From_Total__c, Discount_from_Total_Percentage__c, Total_Order_Net_Amount__c from Order__c where Account__c = :tempAccount.Id and Sales_Type__c = 'Pre-booked' limit 1];
        Order_Item__c tempOrderItem = [select Order_Price__c, Product__c, Order_Discount_1_N__c, Order_Discount_1_P__c from Order_Item__c where Order_Form__c = :tempOrder.Id limit 1];
        User testDistributor = [select Id, ProfileId from User where Profile.Name = 'Distributor User' and isActive = true limit 1];

        tempAccount.OwnerId = testDistributor.Id;
        update tempAccount;

        //update custom settings for distributor
        TriggerFlagControl__c settings = TriggerFlagControl__c.getInstance(testDistributor.ProfileId);
        settings.SIF_AllowEditAndDelete__c = true;
        settings.reflectPicklistValue__c = true;
        settings.OrderItem_AllowEditAndDelete__c = true;

        upsert settings;
 
        Test.startTest();
        
        System.runAs(testDistributor){
            //try to set the Total Discount to be just enough to be valid
            //but also set the order item's discount such that it would exceed with the Total
            //try{
                //set the discount from total to 650
                tempOrder.Discount_From_Total__c = 650;
                
                update tempOrder;

                //set the discount to 110
                tempOrderItem.Order_Discount_1_N__c = 10.0;
                tempOrderItem.Order_Discount_1_P__c = true;

                update tempOrderItem;
            /*}catch(Exception e){
                System.assertEquals(true, e.getMessage().contains('negative'));
            }*/
        }

        Test.stopTest();
    }

    /*-------------------------------------------------------------------------------------------
    Author       :   Adjell Pabayos
    Created Date :   10.05.2017
    Definition   :   Tests the after delete function
    History      :   10.05.2017 - Adjell Pabayos: Created
    -------------------------------------------------------------------------------------------*/
    @isTest static void testAfterDelete(){
        Test.startTest();

        Order__c order = [select Id, Name, Sales_Type__c, Invoice_Date__c, Invoice_No__c from Order__c where Sales_Type__c = 'Pre-booked' limit 1];
        Order_Item__c orderItem = [select Id, Name, Invoice_Date__c, Invoice_Quantity__c from Order_Item__c where Order_Form__c = :order.Id limit 1];

        delete orderItem;
        Test.stopTest();
    }

    /*-------------------------------------------------------------------------------------------
    Author       :   Adjell Pabayos
    Created Date :   11.09.2017
    Definition   :   Tests the Trade Asset Record Type Trigger logic
    History      :   11.09.2017 - Adjell Pabayos: Updated
    -------------------------------------------------------------------------------------------*/
    @isTest static void testTradeAsset(){
        //simulate a upload csv scenario
        Test.startTest();
        Account tempAccount = [select Id, OwnerId, Owner.Id from Account where Distributor__c != null limit 1];
        Order__c tempOrder = [select Id, Discount_From_Total__c from Order__c where Account__c = :tempAccount.Id and Sales_Type__c = 'Pre-booked' limit 1];
        Order_Item__c tempOrderItem = [select Order_Quantity__c,Order_Price__c, Order_Discount_1_N__c, Order_Discount_1_P__c from Order_Item__c where Order_Form__c = :tempOrder.Id limit 1];
        Custom_Product__c tempProduct = [select Id from Custom_Product__c limit 1];
        Order_Item__c cloneOrderItem = tempOrderItem.clone();
        Id actualSales = Schema.sobjectType.Order_Item__c.getRecordTypeInfosByName().get('Actual Sales').getRecordTypeId();
        //Id directInvoice = Schema.sobjectType.Order_Item__c.getRecordTypeInfosByName().get('Direct Invoice Locked').getRecordTypeId(); 

        cloneOrderItem.Order_Form__c = tempOrder.Id;
        cloneOrderItem.Product__c  = tempProduct.Id;
        //cloneOrderItem.RecordTypeId = directInvoice;
        cloneOrderItem.RecordTypeId = actualSales;

        insert cloneOrderItem;

        cloneOrderItem.RecordTypeId = actualSales;
        update cloneOrderItem;

        delete cloneOrderItem;
        Test.stopTest();
    }
}