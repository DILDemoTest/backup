public with sharing class InventoryBalancePageCX{
    
    public List<Inventory_Item__c> inventoryItemList {get;set;}
    //public List<Inventory_Line__c> inventoryLineList {get;set;}
    public List<InventoryWrapper> accountInventories {get;set;}
    public Map<id,InventoryWrapper> inventoryItemLineMap {get;set;}
    public Map<id,InventoryWrapper> inventoryItemLineMapToDisplay {get;set;}
    public Map<id,InventoryWrapper> updatedInventoryItemLineMap {get;set;}
    public String profileName{get;set;}
    //Pagination Variables
    ApexPages.StandardSetController setController;
    public Integer pageSize = 10;
    public Integer pageNumber = 1;
    public Boolean hasNext{get;set;}
    public Boolean hasPrevious{get;set;}

    public Inventory__c currentInventoryRec {get;set;}
    public String saveOnlyTxt {get;set;}
    public String saveAndNewTxt {get;set;}
    public String filterBySKU {get;set;}
    public String filterByName {get;set;}
    public String filterByBU {get;set;}
    public String filterByBrand {get;set;}
    public String filterByCategory {get;set;}
    public String filterBYSONumber{get;set;}

    public List<SelectOption> buOptions {get;set;}
    
    InventoryWrapper tempInventoryWrapper = new InventoryWrapper();

    Set<Id> assignProductIds = new Set<Id>();
    Integer maxItemSize = 20;
    String saveProcess;
    String xProductId;
    Set<id> InventoryItemIdset = new Set<id>();
    List<Inventory_Item__c> newInventoryItems = new List<Inventory_Item__c>();
    Map<String,Inventory_Line__c> linesToSaveMap = new Map<String,Inventory_Line__c>();
    Map<id,Inventory_Item__c> inventoryItemMap  = new Map<id,Inventory_Item__c>();
    Map<id,Inventory_Item__c> inventoryItemMap_iiKey  = new Map<id,Inventory_Item__c>();
    List<Inventory_Line__c> tempInventoryLineList;

    
    public InventoryBalancePageCX(ApexPages.StandardController controller) {
        profileName = [SELECT Id, Profile.Name FROM User WHERE Id = : UserInfo.getUserId()].Profile.Name;
        saveOnlyTxt = 'SaveOnly';
        saveAndNewTxt = 'SaveAndNew';
        currentInventoryRec = new Inventory__c();
        this.currentInventoryRec = (Inventory__c)controller.getRecord();
        if(currentInventoryRec.Account__c!=null){
            initializePage();
        }
        buOptions = new List<SelectOption>();
        buOptions.add(new SelectOption( '', ' '));
        buOptions.add(new SelectOption( 'Branded', 'Branded'));
        buOptions.add(new SelectOption( 'GFS', 'GFS'));
        buOptions.add(new SelectOption( 'Poultry', 'Poultry'));
        buOptions.add(new SelectOption( 'Feeds', 'Feeds'));
    }
    
    public void initializePage(){
        Inventory__c tempInventoryRec = new Inventory__c();
        inventoryItemLineMap  = new Map<id,InventoryWrapper> ();
        inventoryItemLineMapToDisplay = new Map<id,InventoryWrapper> ();
        inventoryItemList = new List<Inventory_Item__c>();
        //accountInventories = new List<InventoryWrapper>();
        Integer wareHouseCount = 0;
        tempInventoryLineList = new List<Inventory_Line__c>();

        
        if(currentInventoryRec.Account__c!=null){
            inventoryItemLineMap = new Map<id,InventoryWrapper> ();
            if(currentInventoryRec.Warehouse1__c ==null){
                wareHouseCount = [SELECT Count() FROM Inventory__c WHERE Account__c = :currentInventoryRec.Account__c];
            }else{
                wareHouseCount = [SELECT Count() FROM Inventory__c 
                                    WHERE Account__c = :currentInventoryRec.Account__c 
                                        AND Warehouse1__c=:currentInventoryRec.Warehouse1__c];
            }

            system.debug('%%% wareHouseCount:'+wareHouseCount);
            if(wareHouseCount > 1 && currentInventoryRec.Warehouse1__c == null ){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'This Account have multiple warehouses. Please specify Warehouse.'));
                return;
            }else if(wareHouseCount > 1 && currentInventoryRec.Warehouse1__c != null ){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'There is a duplicate Inventory for this warehouse. Please contact your system administrator.'));
                return;
            }else if(wareHouseCount == 1 && currentInventoryRec.Account__c != null && currentInventoryRec.Warehouse1__c !=null){
                tempInventoryRec  = [SELECT id, Account__c,Last_Update__c, Warehouse1__c 
                                        FROM Inventory__c 
                                        WHERE Account__c = :currentInventoryRec.Account__c 
                                                AND Warehouse1__c=:currentInventoryRec.Warehouse1__c 
                                        Limit 1];
                
                system.debug('$$ 1 currentInventoryRec:'+currentInventoryRec);
            }else if (wareHouseCount == 1 && currentInventoryRec.Account__c != null){ 
                tempInventoryRec  = [SELECT id, Account__c,Last_Update__c, Warehouse1__c 
                                        FROM Inventory__c 
                                        WHERE Account__c = :currentInventoryRec.Account__c 
                                        Limit 1];
                system.debug('$$ 2 currentInventoryRec:'+currentInventoryRec);
            }else if(currentInventoryRec.Warehouse1__c!=null){
                tempInventoryRec.Warehouse1__c = currentInventoryRec.Warehouse1__c;
            }
            currentInventoryRec.Warehouse1__c = tempInventoryRec.Warehouse1__c;
            currentInventoryRec.Id = tempInventoryRec.id;
            currentInventoryRec.Last_Update__c = date.today();

            if(currentInventoryRec.Warehouse1__c==null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Please select a warehouse.'));
                return;
            }

            inventoryItemList = [SELECT  id, Inventory__c, Brand__c, Category__c, SKU_Code__c, Product__c,
                                     Product__r.name, Product__r.id  
                                FROM Inventory_Item__c 
                                WHERE Inventory__c = :currentInventoryRec.id 
                                ORDER BY Product__r.name ];
            UpdateInventoryItems(inventoryItemList);
            
            // inventoryLineList = [SELECT id, UOM__c, UOM__r.UOM__r.name, Weight_UOM__r.UOM__r.name ,Ending_Balance__c, Batch_Code__c,        
            //                         SIF_Quantity__c, Validated_Quantity__c, Age1__c , Inventory_Item__c, So_No__c, SI_No__c,
            //                         Inventory_Item__r.Product__c, Inventory_Item__r.Product__r.name, Incoming_Quantity__c, Validate_Quantity__c, Validate_Incoming_Quantity__c,
            //                     Validated_Incoming_Quantity__c, Updated__c , Depleted__c, Date_Manufactured__c, Weight_Quantity__c, Weight_UOM__c 
            //             FROM Inventory_Line__c
            //             WHERE Inventory_Item__c IN :InventoryItemIdset AND Depleted__c = false];
            for(Inventory_Line__c il: [SELECT id, UOM__c, UOM__r.UOM__r.name, Weight_UOM__r.UOM__r.name ,Ending_Balance__c, Batch_Code__c,        
                                    SIF_Quantity__c, Validated_Quantity__c, Age1__c , Inventory_Item__c, So_No__c, SI_No__c,
                                    Inventory_Item__r.Product__c, Inventory_Item__r.Product__r.name, Incoming_Quantity__c, Validate_Quantity__c, Validate_Incoming_Quantity__c,
                                Validated_Incoming_Quantity__c, Updated__c , Depleted__c, Date_Manufactured__c, Weight_Quantity__c, Weight_UOM__c 
                        FROM Inventory_Line__c
                        WHERE Inventory_Item__c IN :InventoryItemIdset AND Depleted__c = false]){
                if(inventoryItemLineMap.get(il.Inventory_Item__r.Product__c) == null){
                    tempInventoryWrapper = new InventoryWrapper();
                    tempInventoryLineList  = new List<Inventory_Line__c>();
                    tempInventoryLineList.add(il);
                    tempInventoryWrapper.inventoryItem = inventoryItemMap.get(il.Inventory_Item__r.Product__c);
                    tempInventoryWrapper.productName = inventoryItemMap.get(il.Inventory_Item__r.Product__c).Product__r.name;
                    tempInventoryWrapper.produtSKU = inventoryItemMap.get(il.Inventory_Item__r.Product__c).SKU_Code__c;
                    tempInventoryWrapper.productBrand = inventoryItemMap.get(il.Inventory_Item__r.Product__c).Category__c;
                    tempInventoryWrapper.productCategory = inventoryItemMap.get(il.Inventory_Item__r.Product__c).Category__c;
                    tempInventoryWrapper.productId = inventoryItemMap.get(il.Inventory_Item__r.Product__c).Product__r.id;
                    tempInventoryWrapper.inventoryLines = tempInventoryLineList;
                    inventoryItemLineMap.put(il.Inventory_Item__r.Product__c, tempInventoryWrapper);
                }else{
                    tempInventoryLineList  = new List<Inventory_Line__c>();
                    tempInventoryLineList.add(il);
                    if(inventoryItemLineMap.get(il.Inventory_Item__r.Product__c)!=null){
                        inventoryItemLineMap.get(il.Inventory_Item__r.Product__c).inventoryLines.addAll(tempInventoryLineList);
                    }
                } 
            }

            //get Assigned Products 
            for(Product_Assignment__c pa: [SELECT id, Custom_Product__c 
                                FROM Product_Assignment__c 
                                WHERE Account__r.id = :currentInventoryRec.Account__c]){
                assignProductIds.add(pa.Custom_Product__c);
            } 
            searchProduct();
        }
    }

    public void UpdateInventoryItems(List<Inventory_Item__c> newInventoryList ){
        newInventoryItems = new List<Inventory_Item__c>();
        for(Inventory_Item__c ii: inventoryItemList){
            if(ii.id!=null){
                InventoryItemIdset.add(ii.id);
            }else{
                ii.Inventory__c = currentInventoryRec.id;
                newInventoryItems.add(ii);
            }
            inventoryItemMap.put(ii.Product__c,ii);
            inventoryItemMap_iiKey.put(ii.id,ii);
            tempInventoryWrapper = new InventoryWrapper();
            tempInventoryWrapper.inventoryItem = inventoryItemMap.get(ii.Product__c);
            tempInventoryWrapper.productName = inventoryItemMap.get(ii.Product__c).Product__r.name;
            tempInventoryWrapper.produtSKU = inventoryItemMap.get(ii.Product__c).SKU_Code__c;
            tempInventoryWrapper.productBrand = inventoryItemMap.get(ii.Product__c).Category__c;
            tempInventoryWrapper.productCategory = inventoryItemMap.get(ii.Product__c).Category__c;
            tempInventoryWrapper.productId = inventoryItemMap.get(ii.Product__c).Product__r.id;
            inventoryItemLineMap.put(ii.Product__c, tempInventoryWrapper);
        }
    }
    
    public void AddLineRows(){
        Inventory_Line__c tempInventoryLine = new Inventory_Line__c();
        List<Inventory_Line__c> tempNewInventoryLineList  = new List<Inventory_Line__c>();
        integer numRowToAdd = 0;
        xProductId = System.currentPageReference().getParameters().get('xProductId');
        if(inventoryItemLineMap.get(xProductId) == null && inventoryItemLineMapToDisplay.get(xProductId)!=null){
            inventoryItemLineMap.put(xProductId, inventoryItemLineMapToDisplay.get(xProductId));
        }
        numRowToAdd = inventoryItemLineMap.get(xProductId).rowsToAdd;
        for(integer i = 0; i< numRowToAdd; i++){
            tempInventoryLine = new Inventory_Line__c();
            tempInventoryLine.Inventory_Item__c = inventoryItemLineMap.get(xProductId).inventoryItem.id;
            tempNewInventoryLineList.add(tempInventoryLine);
        }

        inventoryItemLineMap.get(xProductId).inventoryLines.addAll(tempNewInventoryLineList);
        inventoryItemLineMap.get(xProductId).rowsToAdd = 0;
    }

   public  PageReference saveInventories(){
        SaveProgress();
        if(currentInventoryRec.Warehouse1__c==null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid Warehouse.'));
            return null;
        }
        List<Inventory_Line__c> tempLineToInsert = new List<Inventory_Line__c>();
        List<Inventory_Line__c> tempLineToUpdate = new List<Inventory_Line__c>();
        List<Inventory_Item__c> tempInventoryItemsToInsert = new List<Inventory_Item__c>();
        PageReference reDirectPR;
        saveProcess = System.currentPageReference().getParameters().get('saveProcess');
        //check Inventory if it's in the system, else save the inventory record.
        if(currentInventoryRec.id==null){
            insert currentInventoryRec;
            UpdateInventoryItems(inventoryItemList);
        }
        //check for unsaved inventory,Item. Save Inventory Item.
        for(InventoryWrapper iw: inventoryItemLineMap.values()){
            if(iw.inventoryItem.Inventory__c==null){
                iw.inventoryItem.Inventory__c = currentInventoryRec.id;
            }
            if(iw.inventoryItem.id==null && iw.inventoryLines.size()>0){
                tempInventoryItemsToInsert.add(iw.inventoryItem);
            }
        }

        if(tempInventoryItemsToInsert.size()>0){
            insert tempInventoryItemsToInsert;
        }
        
        //update InventoryLine related inventory Item.
        for(Inventory_Item__c iis:tempInventoryItemsToInsert){
            inventoryItemLineMap.get(iis.Product__c).inventoryItem = iis;
            inventoryItemMap.put(iis.Product__c, iis);
            inventoryItemMap_iiKey.put(iis.id,iis);
            for(Inventory_Line__c il: inventoryItemLineMap.get(iis.Product__c).inventoryLines){
                il.Inventory_Item__c = iis.id;
            }
        }


        //get for Inventory Lines to Insert.
        for(InventoryWrapper iw: inventoryItemLineMap.values()){
            tempLineToInsert.addAll(iw.inventoryLines);
        }
        for(Inventory_Line__c lts: tempLineToInsert){
            if(String.isNotEMpty(lts.Batch_Code__c) && (lts.id==null || lts.Updated__c)){
                lts.Updated__c = false;
                linesToSaveMap.put(lts.Batch_Code__c,lts);
            } 
            if (lts.So_No__c == null && lts.id == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'SO No. must have value. Please check inventory lines under '+ inventoryItemLineMap.get(inventoryItemMap_iiKey.get(lts.Inventory_Item__c).Product__c).productName));
            }
            /*
            if (lts.Batch_Code__c == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Batch code must have value. Please check inventory lines under '+ inventoryItemLineMap.get(inventoryItemMap_iiKey.get(lts.Inventory_Item__c).Product__c).productName));
            }*/
            if (lts.Date_Manufactured__c == null && lts.Batch_Code__c != '--RETURNED--' && lts.Batch_Code__c != '--Unknown--' && lts.Batch_Code__c!='--NO STOCK--'){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Date Manufactured/Received field empty for '+ inventoryItemLineMap.get(inventoryItemMap_iiKey.get(lts.Inventory_Item__c).Product__c).productName));
            }
        }

        if(ApexPages.getMessages().size()>0){
            return null;
        }
        try{
            upsert linesToSaveMap.values();
        }catch (DmlException e){
//            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, String.valueOf(e)));
            return null;
        }

        
        if(saveProcess == saveOnlyTxt){
            if(currentInventoryRec.id != null){
                reDirectPR = new PageReference('/'+currentInventoryRec.id);
            }else{
                reDirectPR = new PageReference('/'+currentInventoryRec.Account__r.id);
            }
        }else if(saveProcess == saveAndNewTxt){
            reDirectPR = new PageReference('/apex/InventoryBalancePage');
        }
        reDirectPR.setRedirect(true);
        return reDirectPR;
    }

    public void searchProduct(){
        Set<Id> productIds = new Set<Id>();
        if(String.isNotBlank(filterBYSONumber)){
            for(Inventory_Line__c invLine : [SELECT Id,Inventory_Item__r.Product__c,Inventory_Item__r.Product__r.Name,So_No__c,Inventory_Item__r.Inventory__r.Account__c FROM Inventory_Line__c WHERE So_No__c = : filterBYSONumber AND Inventory_Item__r.Inventory__r.Account__c = : currentInventoryRec.Account__c]){
                productIds.add(invLine.Inventory_Item__r.Product__c);
            }
            assignProductIds.clear();
        }
        String soqlClause = '';
        soqlClause = 'SELECT Id, Name, Brand__r.Name, SKU_Code__c, Category1__c, Category_Name__c, List_Price__c,'+
                    ' Business_Unit__c, List_Price_2__c, Unit_of_Measurement__c, UOM2__c, Price__c,'+
                    ' VAT__c,Alternative_UOM__c, Alternative_UOM__r.name, Catch_Weight__c'+
                ' FROM Custom_Product__c '+
                ' WHERE Active__c = TRUE ';
        if (String.isNotBlank(filterByName))
            soqlClause += ' AND Name LIKE \'%'+filterByName.replace('\'','\\\'')+'%\'';
        if (String.isNotBlank(filterBySKU))
            soqlClause += ' AND  SKU_Code__c LIKE \'%'+filterBySKU.replace('\'','\\\'')+'%\'';  
        if (String.isNotBlank(filterByBrand))
            soqlClause += ' AND Brand__r.Name LIKE \'%'+filterByBrand.replace('\'','\\\'')+'%\'';
        if (String.isNotBlank(filterByCategory))
            soqlClause += ' AND  Category1__r.Name LIKE \'%'+filterByCategory.replace('\'','\\\'')+'%\'';
        if (assignProductIds.size()>0){
            soqlClause += ' AND id IN :assignProductIds';
        }
        if(productIds.size()>0){
            soqlClause += ' AND Id IN:productIds';
        }
        if (String.isNotBlank(filterByBU)){
            String filterString = '';
            if (filterByBU == 'Branded')
                filterString = 'Branded__c';
            if (filterByBU == 'GFS')
                filterString = 'GFS__c';
            if (filterByBU == 'Poultry')
                filterString = 'Poultry__c';
            if (filterByBU == 'Feeds')
                filterString = 'Feeds__c';

            soqlClause += ' AND '+filterString+' = true ';
        }

        soqlClause += ' ORDER BY Name LIMIT 100';
        //List<Custom_Product__c> customProductSearch = (List<Custom_Product__c>)Database.query(soqlClause);
        setController = new ApexPages.StandardSetController(Database.query(soqlClause));
        setController.setPageSize(pageSize);
        setController.setpageNumber(pageNumber);
        if(setController.getHasNext()){
            hasNext = true;
        }else{
            hasNext = false;
        }
        if(setController.getHasPrevious()){
            hasPrevious = true;
        }else{
            hasPrevious = false;
        }
        AddInventoryItemList(setController.getRecords().deepClone(true,true,true));
        
    }
    
    public void nextInventoryList(){
        if(setController.getHasNext()){
            setController.next();
            AddInventoryItemList(setController.getRecords().deepClone(true,true,true));
            if(setController.getHasNext()){
                hasNext = true;
            }else{
                hasNext = false;
            }
            if(setController.getHasPrevious()){
                hasPrevious = true;
            }else{
                hasPrevious = false;
            }
        }
    }
    
    
    public void previousInventoryList(){
        if(setController.getHasPrevious()){
            setController.previous();
            AddInventoryItemList(setController.getRecords().deepClone(true,true,true));
            if(setController.getHasNext()){
                hasNext = true;
            }else{
                hasNext = false;
            }
            if(setController.getHasPrevious()){
                hasPrevious = true;
            }else{
                hasPrevious = false;
            }
        }
    }
    
    public void AddInventoryItemList (List<Custom_Product__c> customProductList){
        SaveProgress();
        inventoryItemLineMapToDisplay = new Map<id,InventoryWrapper> ();
        Inventory_Item__c tempInvetoryItem = new Inventory_Item__c();
        for(Custom_Product__c c :customProductList){
            if(inventoryItemLineMap.get(c.id)==null){
                tempInvetoryItem = new Inventory_Item__c();
                tempInventoryWrapper = new InventoryWrapper();
                tempInvetoryItem.Inventory__c = currentInventoryRec.id;
                tempInvetoryItem.Product__c = c.id;
                tempInventoryWrapper.productName = c.Name;
                tempInventoryWrapper.produtSKU = c.SKU_Code__c;
                tempInventoryWrapper.productBrand = c.Brand__r.Name;
                tempInventoryWrapper.productCategory = c.Category_Name__c;
                tempInventoryWrapper.productId = c.id;
                tempInventoryWrapper.inventoryItem = tempInvetoryItem;
                inventoryItemLineMapToDisplay.put(c.id, tempInventoryWrapper);
            }else{
                inventoryItemLineMapToDisplay.put(c.id,inventoryItemLineMap.get(c.id));
            }
        }
        updateUOMs(inventoryItemLineMapToDisplay.keySet());
    }



    public void updateUOMs(Set<id> productIdSet){
        List<Conversion__c> currentProductConversionsList = new List<Conversion__c>();
        List<SelectOption> tempConversionSelectList = new List<SelectOption>();
        List<SelectOption>  weightTempConversionSelectList = new List<SelectOption>();
        Map<id,List<SelectOption>> productConversionMap = new Map<id,List<SelectOption>>();
        Map<id,List<SelectOption>> productWeightConversionMap = new Map<id,List<SelectOption>>();
        currentProductConversionsList =[SELECT id,Conversion_Rate__c , Denominator__c, Numerator__c, Product__c, 
                            Product__r.name, Product_List_Price__c, UOM__r.name,UOM__c, UOM__r.Catch_Weight_UOM__c,
                            Product__r.Catch_Weight__c
                    FROM Conversion__c 
                    WHERE Product__c IN: productIdSet 
                    ORDER BY Product__r.name];

        //Map conversions for the covertions for the set of ids of products to display. 
        for(Conversion__c c: currentProductConversionsList ){
            tempConversionSelectList = new List<SelectOption>();
            weightTempConversionSelectList = new List<SelectOption>();
            
            
            if(productConversionMap.get(c.product__c)!=null){
                tempConversionSelectList = productConversionMap.get(c.product__c);
            }
            
            if(productWeightConversionMap.get(c.product__c)!=null){
                weightTempConversionSelectList = productWeightConversionMap.get(c.product__c);
            }
            
            if(c.UOM__r.Catch_Weight_UOM__c==true && c.Product__r.Catch_Weight__c == true){
                weightTempConversionSelectList.add(new SelectOption( c.id, c.UOM__r.Name));
            }else{
                tempConversionSelectList.add(new SelectOption( c.id, c.UOM__r.Name));
            }
            
            productWeightConversionMap.put(c.product__c,weightTempConversionSelectList);
            productConversionMap.put(c.product__c,tempConversionSelectList);
            if(inventoryItemLineMapToDisplay.get(c.product__c) != null){
                inventoryItemLineMapToDisplay.get(c.product__c).uomOptions = tempConversionSelectList;
                inventoryItemLineMapToDisplay.get(c.product__c).weightUomOptions = weightTempConversionSelectList;
            }
        }
    }

    public void SaveProgress(){
        for(Id pid : inventoryItemLineMapToDisplay.keySet()){
            inventoryItemLineMap.put(pid,inventoryItemLineMapToDisplay.get(pid));
        }
    }

    public void removeLine(){
        tempInventoryLineList = new List<Inventory_Line__c>();
        xProductId = System.currentPageReference().getParameters().get('xProductId');
        for(Inventory_Line__c il: inventoryItemLineMapToDisplay.get(xProductId).inventoryLines){
            if(!(il.Depleted__c && il.id == null)){
                tempInventoryLineList.add(il);
            }
        }
        inventoryItemLineMapToDisplay.get(xProductId).inventoryLines = tempInventoryLineList;

    }


    public class InventoryWrapper{
        public Inventory_Item__c inventoryItem {get;set;}
        public List<Inventory_Line__c> inventoryLines {get;set;}
        public List<SelectOption> uomOptions {get;set;}
        public List<SelectOption> weightUomOptions {get;set;}
        public Boolean withUpdatedLine {get;set;}
        public Integer rowsToAdd {get;set;}

        //details for new inserted lines
        public String productId {get;set;}
        public String productName {get;set;}
        public String produtSKU {get;set;}
        public String productBrand {get;set;}
        public String productCategory {get;set;}
        
        
        
        public InventoryWrapper(){
            uomOptions = new List<SelectOption> ();
            inventoryItem = new Inventory_Item__c();
            inventoryLines = new List<Inventory_Line__c>();
            weightUomOptions = new List<SelectOption> ();
        }
    }
}