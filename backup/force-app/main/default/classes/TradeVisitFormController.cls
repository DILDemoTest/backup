public with sharing class TradeVisitFormController {

    public TradeVisitFormController(ApexPages.StandardController controller) {

    }

    @RemoteAction
    public static Trade_Visit__c getExistingTrade(Id tradeId) {
        List<Trade_Visit__c> tradeVisitList = [
            SELECT Id
                 , Name
                 , Answer1__c
                 , Answer2__c
                 , Answer3__c
                 , Answer4__c
                 , Answer5__c
                 , Answer6__c
                 , Answer7__c
                 , Answer8__c
                 , Answer9__c
                 , Answer10__c
                 , Answer11__c
                 , Answer12__c
                 , Answer13__c
                 , Answer14__c
                 , Answer15__c
                 , Answer16__c
                 , Business_Address__c
                 , Contact_Number__c
                 , Owner_Name__c
                 , Question1__c
                 , Question2__c
                 , Question3__c
                 , Question4__c
                 , Question5__c
                 , Question6__c
                 , Question7__c
                 , Question8__c
                 , Question9__c
                 , Question10__c
                 , Question11__c
                 , Question12__c
                 , Question13__c
                 , Question14__c
                 , Question15__c
                 , Question16__c
                 , Stack_Properly__c
                 , Account__c
                 , Account__r.Name
             FROM Trade_Visit__c
            WHERE Id = :tradeId
        ];

        if( true == tradeVisitList.isEmpty() ) {
            return new Trade_Visit__c();
        }
        return tradeVisitList[0];
    }
}