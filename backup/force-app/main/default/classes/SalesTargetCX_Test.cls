@isTest
private class SalesTargetCX_Test {
    @testSetup static void setupData(){
        List<Account> accountForInsertList = new List<Account>();
        List<Conversion__c> ConversionforInsertList = new List<Conversion__c>();
        List<Custom_Product__c> customProductForInsert = new List<Custom_Product__c>();
        List<Market__c> marketList = new List<Market__c>();
        Market__c meatMarket = new Market__c(
            Name = 'Meats',
            Active__c = true,
            Market_Code__c = '01'
            );
        marketList.add(meatMarket);
        Market__c gfsMarket = new Market__c(
            Name = 'GFS',
            Active__c = true,
            Market_Code__c = '02'
            );
        marketList.add(gfsMarket);
        Market__c poultryMarket = new Market__c(
            Name = 'Poultry',
            Active__c = true,
            Market_Code__c = '03'
            );
        marketList.add(poultryMarket);
        insert marketList;
        for(integer i = 0; i<=20;i++){
           Account distAccount = new Account(
                Name = 'TestDistributor' + String.valueOf(i),
                RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distributor/ Direct').getRecordTypeId(),
                Branded__c = true,
                GFS__c = true,
                BU_Poultry__c = true,
                BU_Feeds__c = true,
                Market__c = marketList.get(0).Id,
                AccountNumber = 'TestDistributor11'
                );
            accountForInsertList.add(distAccount); 
        }
        
        for(integer i = 0; i<=20;i++){
            Custom_Product__c customProduct = new Custom_Product__c(
                Name = 'Testproduct' + String.valueOf(i),
                Active__c = true,
                Branded__c = true,
                Poultry__c = true,
                GFS__c = true,
                Feeds__c = true,
                Market__c = marketList.get(0).Id,
                SKU_Code__c = '123123' + String.valueOf(i)
                );
            customProductForInsert.add(customProduct);    
        }
        
        if(accountForInsertList.size()>0){
            insert accountForInsertList;
        }
        if(customProductForInsert.size()>0){
            insert customProductForInsert;
        }
        for(Custom_Product__c customProduct : customProductForInsert){
            Conversion__c conversionRecord = new Conversion__c(
                Name = 'KG',
                Product__c = customProduct.Id,
                Numerator__c = 1,
                Denominator__c = 1
                );
            ConversionforInsertList.add(conversionRecord);
            Conversion__c conversionRecordPC = new Conversion__c(
                Name = 'PC',
                Product__c = customProduct.Id,
                Numerator__c = 1,
                Denominator__c = 1
                );
            ConversionforInsertList.add(conversionRecordPC);
        }
        if(ConversionforInsertList.size()>0){
            insert ConversionforInsertList;
        }
        
        
    }
    
    static testMethod void testCreateOrderItem(){
        Account accountRecord = [SELECT Id FROM Account WHERE Name = : 'TestDistributor1' LIMIT 1];
        Test.startTest();
        Test.setCurrentPage(Page.SalesTargetPage);
        ApexPages.currentPage().getParameters().put('accId',accountRecord.Id);
        Order orderRecord = new Order();
        ApexPages.standardController sc = new ApexPages.standardController(orderRecord);
        SalesTargetCX cx = new SalesTargetCX(sc);
        for(SalesTargetCX.AccountSelectionWrapper acc : cx.accountList){
            acc.isSelected = true;            
        }
        cx.searchProducts();
        for(SalesTargetCX.ProductSelectionWrapper p : cx.productList){
            p.isSelected = true;
        }
        cx.nextProductList();
        for(SalesTargetCX.ProductSelectionWrapper p : cx.productList){
            p.isSelected = true;
        }
        cx.previousProductList();
        cx.orderRecord.Invoice_Date__c = Date.today();
        for(SalesTargetCX.ProductSelectionWrapper p : cx.productList){
            p.isSelected = true;
        }
        cx.saveOrder();
        Test.stopTest();
    }
    
    static testMethod void testCreateOrderItem2(){
        Account accountRecord = [SELECT Id FROM Account WHERE Name = : 'TestDistributor1' LIMIT 1];
        Test.startTest();
        Test.setCurrentPage(Page.SalesTargetPage);
        ApexPages.currentPage().getParameters().put('accId',accountRecord.Id);
        Order orderRecord = new Order();
        ApexPages.standardController sc = new ApexPages.standardController(orderRecord);
        SalesTargetCX cx = new SalesTargetCX(sc);
        cx.searchAccounts();
        for(SalesTargetCX.AccountSelectionWrapper acc : cx.accountList){
            acc.isSelected = true;            
        }
        cx.nextAccountList();
        for(SalesTargetCX.AccountSelectionWrapper acc : cx.accountList){
            acc.isSelected = true;            
        }
        cx.previousAccountList();
        cx.searchProducts();
        for(SalesTargetCX.ProductSelectionWrapper p : cx.productList){
            p.isSelected = true;
        }
        cx.nextProductList();
        for(SalesTargetCX.ProductSelectionWrapper p : cx.productList){
            p.isSelected = true;
        }
        cx.previousProductList();
        cx.orderRecord.Invoice_Date__c = Date.today();
        for(SalesTargetCX.ProductSelectionWrapper p : cx.productList){
            p.isSelected = true;
        }
        cx.saveOrder();
        Test.stopTest();
    }
    
    static testMethod void testPageValidation(){
        Account accountRecord = [SELECT Id FROM Account WHERE Name = : 'TestDistributor1' LIMIT 1];
        Test.startTest();
        Test.setCurrentPage(Page.SalesTargetPage);
        ApexPages.currentPage().getParameters().put('accId',accountRecord.Id);
        Order orderRecord = new Order();
        ApexPages.standardController sc = new ApexPages.standardController(orderRecord);
        SalesTargetCX cx = new SalesTargetCX(sc);
        cx.resetAccountSearch();
        cx.resetProductSearch();
        cx.searchProducts();
        cx.nextProductList();
        cx.previousProductList();
        cx.saveOrder();
        Test.stopTest();
    }
}