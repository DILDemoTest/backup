/**
 * AUTHOR       : LENNARD PAUL M SANTOS(DELOITTE)
 * DESCRIPTION  : TEST CLASS FOR CUSTOM PRODUCT TRIGGER
 * HISTORY      : (ANY UPDATES TO THE CODE MUST BE LOGGED HERE)
                : JUN.15.2016 - CREATED.
**/
@isTest
private class CustomProductTrigger_Test {
    @testSetup static void setupData(){
        List<Pricing_Condition__c> pricingConditionList = new List<Pricing_Condition__c>();
        List<Market__c> marketList = new List<Market__c>();
        List<Custom_Product__c> parentProductList = new List<Custom_Product__c>();
        Pricing_Condition__c nationalPricing = new Pricing_Condition__c(
            Name = 'NATIONAL',
            SAP_Code__c = '12345'
            );
        pricingConditionList.add(nationalPricing);
        Pricing_Condition__c regionalPricing = new Pricing_Condition__c(
            Name = 'REGIONAL',
            SAP_Code__c = '89798'
            );
        pricingConditionList.add(regionalPricing);
        Pricing_Condition__c segmentPricing = new Pricing_Condition__c(
            Name = 'SEGMENT',
            SAP_Code__c = '098098'
            );
        pricingConditionList.add(segmentPricing);
        insert pricingConditionList;
        Market__c meatMarket = new Market__c(
            Name = 'Meats',
            Active__c = true,
            Market_Code__c = '01'
            );
        marketList.add(meatMarket);
        Market__c gfsMarket = new Market__c(
            Name = 'GFS',
            Active__c = true,
            Market_Code__c = '02'
            );
        marketList.add(gfsMarket);
        Market__c poultryMarket = new Market__c(
            Name = 'Poultry',
            Active__c = true,
            Market_Code__c = '03'
            );
        marketList.add(poultryMarket);
        insert marketList;
        TriggerFlagControl__c settings = new TriggerFlagControl__c(
            SetupOwnerId = UserInfo.getOrganizationId(),
            reflectPicklistValue__c = true
            );
        insert settings;
        Custom_Product__c prodParent1 = new Custom_Product__c();
        prodParent1.Name = 'ParentProductSMIS';
        prodParent1.Business_Unit__c = 'SMIS';
        prodParent1.Market__c = marketList.get(0).Id;
        prodParent1.SKU_Code__c = '1111';
        parentProductList.add(prodParent1);
        
        Custom_Product__c prodParent2 = new Custom_Product__c();
        prodParent2.Name = 'ParentProductFEEDS';
        prodParent2.Business_Unit__c = 'Feeds';
        prodParent2.Market__c = marketList.get(0).Id;
        prodParent2.SKU_Code__c = '2222';
        parentProductList.add(prodParent2);
        
        Custom_Product__c prodParent3 = new Custom_Product__c();
        prodParent3.Name = 'ParentProductPoultry';
        prodParent3.Business_Unit__c = 'Poultry';
        prodParent3.Market__c = marketList.get(0).Id;
        prodParent3.SKU_Code__c = '4444';
        parentProductList.add(prodParent3);
        
        Custom_Product__c prodParent4 = new Custom_Product__c();
        prodParent4.Name = 'ParentProductGFS';
        prodParent4.Business_Unit__c = 'GFS';
        prodParent4.Market__c = marketList.get(0).Id;
        prodParent4.SKU_Code__c = '5555';
        parentProductList.add(prodParent4);
        
        
        if(parentProductList.size()>0){
            insert parentProductList;
        }
    }

	private static testMethod void testInsertProduct(){
	    List<Market__c> marketList = [SELECT Id,Name FROM Market__c LIMIT 20];
	    Custom_Product__c parentProd = [SELECT Id FROM Custom_Product__c WHERE Name = : 'ParentProductSMIS'];
        Test.startTest();
        Custom_Product__c prod = new Custom_Product__c();
        prod.Name = 'Sample Product1';
        prod.Parent_Product__c = parentProd.Id;
        prod.Business_Unit__c = 'SMIS';
        prod.SKU_Code__c = '6666';
        prod.Market__c = marketList.get(0).Id;
        insert prod;
        Test.stopTest();
	}
	
	private static testMethod void testUpdateProduct(){
	    List<Market__c> marketList = [SELECT Id,Name FROM Market__c LIMIT 20];
	    Custom_Product__c parentProd = [SELECT Id FROM Custom_Product__c WHERE Name = : 'ParentProductFEEDS'];
        Test.startTest();
        Custom_Product__c prod = new Custom_Product__c();
        prod.Name = 'Sample Product1';
        prod.Business_Unit__c = 'Feeds';
        prod.Parent_Product__c = parentProd.Id;
        prod.Market__c = marketList.get(0).Id;
        prod.SKU_Code__c = '7777';
        insert prod;
        Custom_Product__c prodForUpdate = [SELECT Id, Market__c FROM Custom_Product__c WHERE Id=:prod.Id ];
        prodForUpdate.Market__c = marketList.get(1).Id;
        update prodForUpdate;
        
        Test.stopTest();
	}
	
	private static testMethod void testInsertProductGFS(){
	    List<Market__c> marketList = [SELECT Id,Name FROM Market__c LIMIT 20];
	    Custom_Product__c parentProd = [SELECT Id FROM Custom_Product__c WHERE Name = : 'ParentProductGFS'];
        Test.startTest();
        Custom_Product__c prod = new Custom_Product__c();
        prod.Name = 'Sample Product1';
        prod.Parent_Product__c = parentProd.Id;
        prod.Business_Unit__c = 'GFS';
        prod.Market__c = marketList.get(0).Id;
        prod.SKU_Code__c = '8888';
        insert prod;
        Test.stopTest();
	}
	
	private static testMethod void testInsertProductPoultry(){
	    List<Market__c> marketList = [SELECT Id,Name FROM Market__c LIMIT 20];
	    Custom_Product__c parentProd = [SELECT Id FROM Custom_Product__c WHERE Name = : 'ParentProductPoultry'];
	    List<Custom_Product__c> prodList = new List<Custom_Product__c>();
        Test.startTest();
        Custom_Product__c prod = new Custom_Product__c();
        prod.Name = 'Sample Product1';
        prod.Parent_Product__c = parentProd.Id;
        prod.Business_Unit__c = 'Poultry';
        prod.Market__c = marketList.get(0).Id;
        prod.SKU_Code__c = '9999';
        prodList.add(prod);
        Custom_Product__c prod1 = new Custom_Product__c();
        prod1.Name = 'Sample Product2';
        prod1.Parent_Product__c = parentProd.Id;
        prod1.Business_Unit__c = 'Poultry';
        prod1.Market__c = marketList.get(0).Id;
        prod1.SKU_Code__c = '1010';
        prodList.add(prod1);
        insert prodList;
        Test.stopTest();
	}
	

}