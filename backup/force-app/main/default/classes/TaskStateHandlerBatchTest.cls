@isTest
private class TaskStateHandlerBatchTest {

	private static testMethod void testBatch() {
        List<Task> lstTasks = DIL_TestDataFactory.getTasks(1,false);
        
        
        for(Task objTask : lstTasks){
            objTask.Status = 'Edited After Approval';
            objTask.Created_After_Approval__c = False;
        }
        insert lstTasks;
        
        Month__c objMonth = [Select Status__c From Month__c][0];
        objMonth.Status__c = 'Edited After Approval';
        update objMonth;
        
        Test.startTest();
        StaticResources.byPassMonthTrigger = true;
        update lstTasks;
        StaticResources.byPassMonthTrigger = false;
        Database.executeBatch(new TaskStateHandlerBatch());
        Test.stopTest();
	}

}