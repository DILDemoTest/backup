@isTest
private class OrderTrigger_Test {
    @testSetup static void setupdata(){
        List<Pricing_Condition__c> pricingConditionList = new List<Pricing_Condition__c>();
        List<Market__c> marketList = new List<Market__c>();
        Pricing_Condition__c nationalPricing = new Pricing_Condition__c(
            Name = 'National List Price',
            SAP_Code__c = '12345'
            );
        pricingConditionList.add(nationalPricing);
        Pricing_Condition__c regionalPricing = new Pricing_Condition__c(
            Name = 'REGIONAL',
            SAP_Code__c = '89798'
            );
        pricingConditionList.add(regionalPricing);
        Pricing_Condition__c segmentPricing = new Pricing_Condition__c(
            Name = 'SEGMENT',
            SAP_Code__c = '098098'
            );
        pricingConditionList.add(segmentPricing);
        insert pricingConditionList;
        Market__c meatMarket = new Market__c(
            Name = 'Meats',
            Active__c = true,
            Market_Code__c = '01'
            );
        marketList.add(meatMarket);
        Market__c gfsMarket = new Market__c(
            Name = 'GFS',
            Active__c = true,
            Market_Code__c = '02'
            );
        marketList.add(gfsMarket);
        Market__c poultryMarket = new Market__c(
            Name = 'Poultry',
            Active__c = true,
            Market_Code__c = '03'
            );
        marketList.add(poultryMarket);
        insert marketList;
        TriggerFlagControl__c settings = new TriggerFlagControl__c(
            SetupOwnerId = UserInfo.getOrganizationId(),
            reflectPicklistValue__c = true,
            Account_CreateWarehouseForDistributor__c = true,
            Account_CreatePricingCondition__c = true,
            Account_PricingConditionName__c = 'National List Price',
            SIF_AllowEditAndDelete__c = true
            );
        insert settings;
        Id distributorAccountId = Schema.sobjectType.Account.getRecordTypeInfosByName().get('Distributor/ Direct').getRecordTypeId();
        Id distributorCustomerId = Schema.sobjectType.Account.getRecordTypeInfosByName().get('Distributor Customer').getRecordTypeId();        
        Account acc = new Account();
        acc.Name = 'Sample Grocery1';
        acc.Sales_Organization__c  = 'Branded';
        acc.General_Trade__c  = true;
        acc.BU_Feeds__c = true;
        acc.Branded__c = true;
        acc.GFS__c = true;
        acc.BU_Poultry__c = true;
        acc.AccountNumber = '123412344';
        acc.Market__c = marketList.get(0).Id;
        acc.RecordTypeId = distributorAccountId;
        insert acc;
        Account acc2 = new Account();
        acc2.Name = 'Sample Grocery2';
        acc2.Sales_Organization__c  = 'Branded';
        acc2.General_Trade__c  = true;
        acc2.BU_Feeds__c = true;
        acc2.Branded__c = true;
        acc2.GFS__c = true;
        acc2.BU_Poultry__c = true;
        acc2.AccountNumber = '1234123';
        acc2.Market__c = marketList.get(0).Id;
        acc2.Distributor__c = acc.Id;
        acc2.RecordTypeId = distributorCustomerId;
        insert acc2;
        Id actualSalesRecordTypeId = Schema.sObjectType.Order__c.getRecordTypeInfosByName().get('Actual Sales').getRecordTypeId();
        
         //CREATE SAS- Contact
        Id SASrecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('SAS').getRecordTypeId();
        Contact SASvar = new Contact(RecordTypeId=SASrecordTypeId, FirstName = 'xyzFirst', LastName = 'XyZLast', AccountId = acc.Id);
        insert SASvar;
        
        //AARP: Create New DSP
        Id dspRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Distributor Personnel').getRecordTypeId();
        Contact dspVar = new Contact (RecordTypeId=dspRecordTypeId, FirstName = 'xyzSecond1', LastName = 'XyZSecond1', AccountId = acc.Id);
        insert dspVar;        
        
        Order__c orderRecord = new Order__c(
            Account__c = acc2.Id,
            RecordTypeId = actualSalesRecordTypeId,
            Invoice_Date__c = System.today().addDays(-3),
            Invoice_No__c = '1111111',
            SAS__c = SASvar.Id,
            DSP__c = DSPvar.Id
            );
        insert orderRecord;
        Test.setCreatedDate(orderRecord.Id,System.today().addDays(-3));
        
    }
    private static testMethod void test() {
        Account acc2 = [SELECT Id FROM Account WHERE Name = 'Sample Grocery2'];
        Order__c orderRecord = [SELECT Id FROM Order__c LIMIT 1];
        Test.startTest();
        orderRecord.Invoice_Date__c = System.today();
        orderRecord.Invoice_No__c = '1111112';
        orderRecord.Payment_Term1__c = '07D';
        orderRecord.Account__c = acc2.Id;
        try{
            update orderRecord;
        }catch(Exception e){
            //EMPTY CATCH
        }
        Test.stopTest();
    }
    
    private static testMethod void testDelete() {
        Order__c orderRecord = [SELECT Id FROM Order__c LIMIT 1];
        Test.startTest();
        try{
            delete orderRecord;
        }catch(Exception e){
            //EMPTY CATCH
        }
        Test.stopTest();
    }
    
    private static testMethod void test2() {
        Account acc2 = [SELECT Id FROM Account WHERE Name = 'Sample Grocery2'];
        Order__c orderRecord = [SELECT Id FROM Order__c LIMIT 1];
        Test.startTest();
        orderRecord.Invoice_Date__c = System.today();
        orderRecord.Invoice_No__c = '1111112';
        orderRecord.Payment_Term1__c = '07D';
        orderRecord.Account__c = acc2.Id;
        //Create New DSP
        Id dspRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Distributor Personnel').getRecordTypeId();
        Contact dspVar = new Contact (RecordTypeId=dspRecordTypeId, FirstName = 'xyzFirst1', LastName = 'XyZLast1');
        insert dspVar;
        
        //Create New SAS
        Id SASrecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('SAS').getRecordTypeId();
        Contact sasVar = new Contact(RecordTypeId=SASrecordTypeId, FirstName = 'xyzFirst', LastName = 'XyZLast');
        insert sasVar;
        
        orderRecord.DSP__c = dspVar.Id;
        orderRecord.SAS__c = sasVar.Id;
        try{
            update orderRecord;
        }catch(Exception e){
            //EMPTY CATCH
        }
        Test.stopTest();
    }

}