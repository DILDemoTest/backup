@isTest
private class AccountConfigurationTriggerHandlerTest {
	public static List<Account_Configuration__c> createTestConfigurations(Integer num, Datetime startDate) {
        List<Account_Configuration__c> accConfigList = new List<Account_Configuration__c>();
        for(Integer i=0; i< num; i++) {
            accConfigList.add(new Account_Configuration__c( Number_of_Accounts__c = i+1
                                                          , StartDateTime__c = startDate.addDays(i)
                                                          , EndDateTime__c = startDate.addDays(i).addHours(8)
                                                          , User__c = UserInfo.getUserId()
                                                          , Title__c = 'Test Title'
                                                          , Subject__c = 'Test Subject'
                                                          , Status__c = 'New'
                                                          ));
        }
        return accConfigList;
    }


    // Method to create Test Accounts
    public static List<Account> createTestAccount(Integer num) {
        List<Account> accountList = new List<Account>();
        for(Integer i=1; i<=num; i++) {
            accountList.add(new Account(Name='Test Account '+i, AccountNumber='20'));
        }
        return accountList;
    }


    // Method to create test Task Records
    public Static List<Task> createTasks( Integer num
                                        , String accId
                                        , DateTime startDate
                                        , Boolean checkin
                                        , Boolean checkout
                                        , String workType
                                        ) {
        List<Task> taskList = new List<Task>();
        for(Integer i=1; i<=num; i++) {
            Task taskObj = new Task();
            taskObj.Related_Account__c = accId;
            taskObj.StartDateTime__c = startDate.addDays(i);
            taskObj.EndDateTime__c = startDate.addDays(i).addHours(8);
            //taskObj.All_Day__c = true;
            taskObj.Title__c = 'Task '+i;
            taskObj.Status = 'In Progress';
            taskObj.User__c = UserInfo.getUserId();
            taskObj.Work_Type__C = workType;
            if(checkin)
                taskObj.Check_in__c = startDate.addDays(i).addMinutes(10);
            if(checkout)
                taskObj.Check_out__c = startDate.addDays(i).addMinutes(40);
            taskList.add(taskObj);
        }
        return taskList;
    }

	@isTest static void insertConfigRecordsTest() {

        List<Account> accountList = createTestAccount(5);
        insert accountList;

        List<Task> taskList = createTasks(5, accountList[0].Id, System.now(), true, true, 'Field work');
        insert taskList;

		List<Account_Configuration__c> accConfigList = createTestConfigurations(10, System.now());
        Test.startTest();
        insert accConfigList;
        Test.stopTest();
	}

    @isTest static void updateConfigRecordsTest() {
        List<Account> accountList = createTestAccount(5);
        insert accountList;

        List<Task> taskList = createTasks(5, accountList[0].Id, System.now(), true, true, 'Field work');
        insert taskList;

        List<Account_Configuration__c> accConfigList = createTestConfigurations(10, System.now());
        for(Account_Configuration__c accConfigObj : accConfigList) {
            accConfigObj.Accounts_Created__c = 1;
        }
        insert accConfigList;
        Test.startTest();
        update accConfigList[0];
        Test.stopTest();
    }

    @isTest static void updateConfigRecordsTest2() {
        List<Account> accountList = createTestAccount(5);
        insert accountList;

        List<Task> taskList = createTasks(5, accountList[0].Id, System.now(), true, true, 'Field work');
        insert taskList;

        List<Account_Configuration__c> accConfigList = createTestConfigurations(10, System.now());
        for(Account_Configuration__c accConfigObj : accConfigList) {
            accConfigObj.User__c = null;
        }
        insert accConfigList;
        Test.startTest();
        update accConfigList[0];
        Test.stopTest();
    }

    @isTest static void deleteConfigRecordsTest() {
        List<Account_Configuration__c> accConfigList = createTestConfigurations(10, System.now());
        insert accConfigList;

        Test.startTest();
        delete accConfigList[0];
        Test.stopTest();
    }

}