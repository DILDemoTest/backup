public class SMC_TriggerLineItemHandler 
{
    // Uploading Child Line Items only is the concern.
    public static void ChildRequisitionLineItemsOnly(List<ePAW_Line_Item__c> LineItemDetails)
    {
        Set<Id> ePAWId = new Set<Id>(); // Get Unique Child ePAW Id.
        Map<Id, Decimal> MotherEpawAllocatedRequition = new Map<Id,Decimal>();
        Map<Id, Boolean> MotherEpawReferenceChild = new Map<Id, Boolean>();
        Map<Id, Decimal> ComputedLineItemsUpload = new Map<Id, Decimal>();
        Map<Id, ePAW_Line_Item__c> LineItemUpdate = new Map<Id, ePAW_Line_Item__c>();
        
        for(ePAW_Line_Item__c LineItemRecords : LineItemDetails)
        {
            ePAWId.add(LineItemRecords.ePAW_Form__c); // Store Unique ID of Mother Requisition.
        }
        
        for(ePAW_Form__c MotherEpaw : [SELECT Reporting_Total_Amount_Child__c, Child_Requisition__c, Parent_Requisition__c FROM ePAW_Form__c WHERE Id = :ePAWId])
        {
            MotherEpawAllocatedRequition.put(MotherEpaw.Id, MotherEpaw.Reporting_Total_Amount_Child__c); // Getting the Maximum amount needed in order to create line items.
            MotherEpawReferenceChild.put(MotherEpaw.Id, MotherEpaw.Child_Requisition__c); // Use this for Child Requisition Checker.
        }
        
        System.debug('Is Child Requisition ' + ' ? ' + MotherEpawReferenceChild);
        
        for(String EpawReference : MotherEpawAllocatedRequition.keySet())
        {
            Decimal ComputeIndividualLineItems = 0;
            for(ePAW_Line_Item__c ComputeLineItems : LineItemDetails)
            {
                if(ComputeLineItems.ePAW_Form__c == EpawReference)
                {
                    ComputeIndividualLineItems += ComputeLineItems.Sub_Total__c; // Compute individual the line items and get its sum.
                }
            }
            ComputedLineItemsUpload.put(EpawReference, ComputeIndividualLineItems);
        }
        
        System.debug('Maximum Value Expected: ' + MotherEpawAllocatedRequition);
        System.debug('Computed Line Items: ' + ComputedLineItemsUpload);
        
        for(ePAW_Line_Item__c LineItemForUpdate : LineItemDetails)
        {
            if(LineItemForUpdate.Current_User_Profile__c.equals('System Administrator'))
            {
                // Check if the intended line item for update is a child requisition.
                // Check if the intended line item is equal to the RTA Allocated to the child requisition.
                if(MotherEpawReferenceChild.get(LineItemForUpdate.ePAW_Form__c) && 
                  (MotherEpawAllocatedRequition.get(LineItemForUpdate.ePAW_Form__c) == ComputedLineItemsUpload.get(LineItemForUpdate.ePAW_Form__c)))
                {
                    System.debug('Child Requisition: ' + LineItemForUpdate.ePAW_Form__c);
                    LineItemUpdate.put(LineItemForUpdate.Id, LineItemForUpdate);
                }
                else 
                {
                    // Error Checker so that Stand Alone Requisitions/Mother Requisitions can be created.
                    if(MotherEpawReferenceChild.get(LineItemForUpdate.ePAW_Form__c))
                    {
                        LineItemForUpdate.addError('Line Items did not meet the allocated Reporting Total Amount(Child).');
                    }
                }
            }
            else 
            {
                // Check if the intended line item for update is a child requisition.
                // Check if the intended line item is equal to the RTA Allocated to the child requisition.
                if(MotherEpawReferenceChild.get(LineItemForUpdate.ePAW_Form__c) && 
                  (MotherEpawAllocatedRequition.get(LineItemForUpdate.ePAW_Form__c) > ComputedLineItemsUpload.get(LineItemForUpdate.ePAW_Form__c)))
                {
                    System.debug('Child Requisition: ' + LineItemForUpdate.ePAW_Form__c);
                    LineItemUpdate.put(LineItemForUpdate.Id, LineItemForUpdate);
                }
                else 
                {
                    // Error Checker so that Stand Alone Requisitions/Mother Requisitions can be created.
                    if(MotherEpawReferenceChild.get(LineItemForUpdate.ePAW_Form__c))
                    {
                        LineItemForUpdate.addError('Line Items did not meet the allocated Reporting Total Amount(Child).');
                    }
                }
            }
        }
    }
}