/*********************************************************************
Author          Deloitte (RVillanueva)
Description     Controller class for SIFOrderItemResnaphotCC
Date Created:   10/19/2016
*********************************************************************/
public class SIFOrderItemResnaphotCC {
    public Integer snapshotMonth {get;set;}
    public String snapshotYear {get;set;}
    
    public SIFOrderItemResnaphotCC(ApexPages.StandardController controller) {
        snapshotMonth = null;
        snapshotYear = '';
    }
    
    public void runSnapshot() {
        
        //Matcher m = p.matcher(snapshotYear);
        if (snapshotMonth==null || (snapshotYear==null||snapshotYear=='')) {
            ApexPages.Message err = new ApexPages.Message(ApexPages.SEVERITY.ERROR,'Please enter snapshot month and year.');
            ApexPages.addMessage(err); 
        } else {    
            Pattern pYear = Pattern.compile('\\d\\d\\d\\d');
            Matcher mYear = pYear.matcher(snapshotYear);
            if (mYear.matches() == true) {
                Integer sYear = Integer.valueOf(snapshotYear);
                batchSIFOrderItemSnapshot sif = new batchSIFOrderItemSnapshot(snapshotMonth, sYear);
                Database.executeBatch(sif,350);
                ApexPages.Message error = new ApexPages.Message(ApexPages.SEVERITY.INFO,'You have successfully run the snapshot.');
                ApexPages.addMessage(error);
            } else {
                ApexPages.Message error = new ApexPages.Message(ApexPages.SEVERITY.ERROR,'Please Enter a valid year format (YYYY).');
                ApexPages.addMessage(error);
            }
        }    
    }
    
    public List<SelectOption> getmonthsList() {
     List<SelectOption> options = new List<SelectOption>();
     options.add(new SelectOption('','--Month--'));
         for(integer i=1;i<=12;i++) {
             options.add(new SelectOption(String.valueOf(i), String.valueOf(i)));
         }
       return options;
    }
}