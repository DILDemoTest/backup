@isTest
public class EndOfDayTriggerTest {
	@isTest
    public static void StartOfDayTriggerTest_1() {
        StaticResources.byPassStartOrEndOfDayTrigger = true;
        
        End_of_Day__c eodObj = new End_of_Day__c();
        eodObj.Field_Value_Mapping__c = '';
        Test.startTest();
        insert eodObj;
        Test.stopTest();
    }
    @isTest
    public static void StartOfDayTriggerTest_2() {
        Start_of_Day__c sodObj = new Start_of_Day__c();
        sodObj.Field_Value_Mapping__c = '';
        insert sodObj;
        
        End_of_Day__c eodObj = new End_of_Day__c();
        eodObj.Field_Value_Mapping__c = '';
        Test.startTest();
        insert eodObj;
        Test.stopTest();
    }
}