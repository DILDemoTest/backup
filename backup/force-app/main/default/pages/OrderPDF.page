<!-- Author: Hasi S. Mejia -->
<!-- Created     : 06.29.2017 -->
<!-- Description : Order Booking PDF print -->

<apex:page standardController="Order__c" showHeader="false" renderAs="pdf" applyHtmlTag="false" readOnly="True" contentType="application/vnd.pdf#{!Order__c.Name}.pdf">
<html>     
<head>
    <style type="text/css">
    
        @page {
            size:landscape;
            @bottom-center {
                font-family:Arial Unicode MS;
                font-size:80%;
                color: #3f5259;
                content: "Page " counter(page)  " of " counter(pages);
              }
            }
        
        body{
        } 
        
        span { 
            color: #015591;
            font-family: Arial Unicode MS;
            font-size: 20px;
        } 
        
        td, tr{
            border-bottom:  1px solid #e6f0f7; 
            font-family: Arial Unicode MS;
            color: #015591;
        }
        
        .tableHeader {
        background: #e6f0f7;
        font-size: 11px;
        font-weight:bold;
        width:15%;
        padding-left: 5px;
        }
        
        .tableValues {
        font-size: 12px;
        color: #3f5259;
        font-family: Arial Unicode MS;
        width:35%;
        padding-left: 10px;
        }
        
        .title{
        font-weight:bold;
        }
        
        .subtitle{
        font-size: 14px;
        color: #3f5259;
        }

        .itemTitle{
        font-family: Arial Unicode MS;
        color: #015591;
        font-weight:bold;
        }

        .itemHeader {
        background: #e6f0f7;
        font-size: 11px;
        font-weight:bold;
        padding-left: 1px;
        }

        .itemValues {
        color: #3f5259;
        font-family: Arial Unicode MS;
        padding-left: 1px;
        font-size: 12px;
        }

        #test span{
          font-size: 14px !important;
        color: #3f5259 !important;
        }
        
    </style>
</head>


<body>
    <div>  
      <center>
          <apex:image url="https://c.ap7.content.force.com/servlet/servlet.ImageServer?id=0150I000007g8l3&oid=00D280000015qSq&lastMod=1534144318000" width="130" height="55"/>
            <br/>
              <span class="title"><b>ORDER SUMMARY</b></span><br/>
              <span class="subtitle">Created By: &nbsp;<apex:outputText value="{!Order__c.Createdby.Name}"></apex:outputText></span><br/>
              <span id="test" class="subtitle">Date Today: &nbsp;<apex:outputField value="{!Order__c.Date_Today__c}"></apex:outputField></span>
      </center><br/><br/>

      <table style="width:100%;">
        <tr>
          <td class="tableHeader">Order Form Reference No.</td>
          <td class="tableValues">{!Order__c.Name}</td>  
          <td style="border-bottom:  none; width:1%;"> </td>
          <td class="tableHeader">Order Item Total</td>
          <td class="tableValues"><apex:outputText value="{0, number, ###,###,##0}">
              <apex:param value="{!Order__c.Order_Item_Total_N__c}"/></apex:outputText></td>
        </tr>
  
        <tr>
          <td class="tableHeader">PO No.</td>
          <td class="tableValues">{!Order__c.PO_No__c}</td>
          <td style="border-bottom:  none; width:1%;"> </td>
          <td class="tableHeader">Total Estimated Amount</td>
          <td class="tableValues"><apex:outputText value="{0, number, ###,###,##0.00}">
              <apex:param value="{!Order__c.Total_Estimated_Amount_N__c}"/></apex:outputText></td>
        </tr>
      
      <tr>
          <td class="tableHeader">PO Date</td>
          <td class="tableValues"><apex:outputText value="{0,date,MM'/'dd'/'yyyy}">
              <apex:param value="{!Order__c.PO_Date__c}"/></apex:outputText></td>
          <td style="border-bottom:  none; width:1%;"> </td>
          <td class="tableHeader">Account</td>
          <td class="tableValues">{!Order__c.Account__r.Name}</td>
        </tr>

        <tr>
          <td class="tableHeader">Sales Org</td>
          <td class="tableValues">{!Order__c.Sales_Org__c}</td>
          <td style="border-bottom:  none; width:1%;"> </td>
          <td class="tableHeader">SAS</td>
          <td class="tableValues">{!Order__c.SAS__r.Name}</td>
        </tr>

        <tr>
          <td class="tableHeader">Distribution Channel</td>
          <td class="tableValues">{!Order__c.Distribtution_Channel__c}</td>
          <td style="border-bottom:  none; width:1%;"> </td>
          <td class="tableHeader">Division</td>
          <td class="tableValues">{!Order__c.Division__c}</td>
        </tr>
        
        <tr>
          <td class="tableHeader">Ship To</td>
          <td class="tableValues">{!Order__c.Ship_Sold_To__r.Name}</td>
          <td style="border-bottom:  none; width:1%;"> </td>
          <td class="tableHeader">Remarks</td>
          <td class="tableValues">{!Order__c.Remarks__c}</td>
        </tr>
        
        <tr>
          <td class="tableHeader">Delivery Time</td>
          <td class="tableValues">{!Order__c.Delivery_Time__c}</td>
          <td style="border-bottom:  none; width:1%;"> </td>
          <td class="tableHeader">Shipping Instructions</td>
          <td class="tableValues">{!Order__c.Shipping_Instructions2__c}</td>
        </tr>

        <tr>
          <td class="tableHeader">Requested Delivery Date</td>
          <td class="tableValues"><apex:outputText value="{0,date,MM'/'dd'/'yyyy}">
              <apex:param value="{!Order__c.Requested_Delivery_Date__c}"/></apex:outputText></td>
          <td style="border-bottom:  none; width:1%;"> </td>
          <td class="tableHeader">Product Category</td>
          <td class="tableValues">{!Order__c.Product_Category__r.Name}</td>
        </tr>
        
        <tr>
          <td class="tableHeader">Order Submitted Date</td>
          <td class="tableValues"><apex:outputText value="{0,date,MM'/'dd'/'yyyy}">
              <apex:param value="{!Order__c.Requested_Delivery_Date__c}"/></apex:outputText></td>
          <td style="border-bottom:  none; width:1%;"> </td>
          <td class="tableHeader">Status </td>
          <td class="tableValues">{!Order__c.Status__c}</td>
        </tr>
        
        <tr>
          <td class="tableHeader">Order For Receiving Date</td>
          <td class="tableValues"><apex:outputText value="{0,date,MM'/'dd'/'yyyy}">
              <apex:param value="{!Order__c.PO_For_Receiving_Date__c}"/></apex:outputText></td>
          <td style="border-bottom:  none; width:1%;"> </td>
          <td class="tableHeader">Sub Status </td>
          <td class="tableValues">{!Order__c.Sub_Status__c}</td>
        </tr>
        
        <tr>
          <td class="tableHeader">Order Cancelled Date</td>
          <td class="tableValues"><apex:outputText value="{0,date,MM'/'dd'/'yyyy}">
              <apex:param value="{!Order__c.PO_Cancelled_Date__c}"/></apex:outputText></td>
          <td style="border-bottom:  none; width:1%;"> </td>
          <td class="tableHeader">Order Closed Date</td>
          <td class="tableValues"><apex:outputText value="{0,date,MM'/'dd'/'yyyy}">
              <apex:param value="{!Order__c.PO_Closed_Date__c}"/></apex:outputText></td>
        </tr>
        
        <tr>
          <td class="tableHeader">SAP DR No.</td>
          <td class="tableValues">{!Order__c.SAP_DR_No__c   }</td>
          <td style="border-bottom:  none; width:1%;"> </td>
          <td class="tableHeader">SAP Transaction Type</td>
          <td class="tableValues">{!Order__c.SAP_Transaction_Type__c}</td>
        </tr>
        
        <tr>
          <td class="tableHeader">SAP DD</td>
          <td class="tableValues"><apex:outputText value="{0,date,MM'/'dd'/'yyyy}">
              <apex:param value="{!Order__c.SAP_DD__c}"/></apex:outputText></td>
          <td style="border-bottom:  none; width:1%;"> </td>
          <td class="tableHeader">SAP Sales Order</td>
          <td class="tableValues">{!Order__c.SAP_Sales_Order__c}</td>
        </tr>
        
        <tr>
          <td class="tableHeader">SAP Billing Doc</td>
          <td class="tableValues">{!Order__c.SAP_Billing_Doc__c}</td>
          <td style="border-bottom:  none; width:1%;"> </td>
          <td class="tableHeader">SAP Message Log</td>
          <td class="tableValues">{!Order__c.SAP_Error_Log__c}</td>          
        </tr>
      </table>

       <table style="width:100%;">
        <thead tyle="display: {!IF (Order__c.Status__c != 'Submitted', 'block', 'NONE')}">
          <br><div class="itemTitle">ORDER ITEMS</div></br>  
            <apex:variable value="{!1}" var="count"/>
              <tr style="display: {!IF (Order__c.Status__c == 'Submitted', 'flex', 'NONE')}">
                <td class="itemHeader" style="width:1%;">#</td>
                <td class="itemHeader" style="width:13%;">Brand</td>
                <td class="itemHeader" style="width:5%;">Scale</td>
                <td class="itemHeader" style="width:17%;">Product Group</td>
                <td class="itemHeader" style="width:14%;">Material Code</td>
                <td class="itemHeader" style="width:24%;">Product Name</td>
                <td class="itemHeader" style="width:6%;">Order Quantity</td>
                <td class="itemHeader" style="width:7%;">Selling UOM</td>
                <td class="itemHeader" style="width:8%;">Estimated Amount</td>
              </tr>

              <tr style="display: {!IF (Order__c.Status__c != 'Submitted', 'block', 'NONE')}">
                <td class="itemHeader" style="width:5em;">#</td>
                <td class="itemHeader" style="width:15em;">Product Code</td>
                <td class="itemHeader" style="width:45em;">Product Name</td>
                <td class="itemHeader" style="width:7em;">Order Quantity</td>
                <td class="itemHeader" style="width:7em;">UOM</td>
                <td class="itemHeader" style="width:7em;">Quantity Remaining</td>
                <td class="itemHeader" style="width:7em;">Quantity Received</td>
                <td class="itemHeader" style="width:7em;">100% Received</td>
              </tr>
          </thead>
  
        <tbody  >
          <apex:repeat value="{!Order__c.Order_Items__r}" var="orderItem">
            <tr style="display: {!IF (Order__c.Status__c == 'Submitted', 'flex', 'NONE')}">
              <td class="itemValues" style="width:1%;">{!FLOOR(count)}</td>
              <td class="itemValues" style="width:14%;">{!orderItem.Brand__c}</td>
              <td class="itemValues" style="width:5%;">{!orderItem.Product__r.Scale__r.Name}</td>
              <td class="itemValues" style="width:18%;">{!orderItem.Product__r.Product_Group__r.Name}</td>
              <td class="itemValues" style="width:14.5%;">{!orderItem.Product__r.SKU_Code__c}</td>
              <td class="itemValues" style="width:25%;">{!orderItem.Product__r.Name}</td>
              <td class="itemValues" style="width:6%;">{!FLOOR(orderItem.Order_Quantity__c)}</td>
              <td class="itemValues" style="width:7.5%;">{!orderItem.SellingUOM__c}</td>
              <td class="itemValues" style="width:8.5%;"><apex:outputText value="{0, number, ###,###,##0.00}">
              <apex:param value="{!orderItem.Estimated_Amount__c}"/></apex:outputText></td>
            </tr>

            <tr style="display: {!IF (Order__c.Status__c != 'Submitted', 'block', 'NONE')}">
              <td class="itemValues" style="width:2%;">{!FLOOR(count)}</td>
              <td class="itemValues" style="width:9%;">{!orderItem.Product__r.SKU_Code__c}</td>
              <td class="itemValues" style="width:30%;">{!orderItem.Product__r.Name}</td>
              <td class="itemValues" style="width:5%;">{!FLOOR(orderItem.Order_Quantity__c)}</td>
              <td class="itemValues" style="width:5%;">{!orderItem.SellingUOM__c}</td>
              <td class="itemValues" style="width:5%;">{!FLOOR(orderItem.Test__c)}</td>
              <td class="itemValues" style="width:5%;">{!FLOOR(orderItem.Quantity_Received__c)}</td>    
              <td class="itemValues" style="width:5%;"><apex:outputField value="{!orderItem.Received__c}"/></td>   
            </tr>
              
              <apex:variable var="count" value="{!count+ 1}"/> 
          </apex:repeat>
        </tbody>
        </table>
      </div>
      </body>
    
</html>
</apex:page>